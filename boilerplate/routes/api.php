<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/


Route::group(['prefix' => 'user'], function() {

    Route::group(['middleware' => 'auth:api'], function(){
      
        //Users
        Route::apiResource('/', 'UserController');
        Route::get('users/{id}', 'UserController@userDetail');
        Route::get('search', 'UserController@search');
        Route::get('search-all', 'UserController@searchAll');
        Route::get('search-groups', 'UserController@searchGroup');
        Route::get('followings', 'UserController@getFollowings');
        Route::post('update', 'UserController@updateProfile');
        Route::post('update-password', 'UserController@changePassword');

    });

    // Contact US
    Route::post('contact-us/store', 'UserController@contactUs');
    Route::post('subscribe/store', 'UserController@subscribe');
});

