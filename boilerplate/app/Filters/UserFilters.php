<?php

namespace App\Filters;

class UserFilters extends Filters {

    protected $filters = ['name', 'status', 'notIn'];

    public function name($name)
    {   
        if($name !== ""){
            $this->builder->where('id', '!=', auth()->id());
            $this->builder->where(function($query) use ($name) {
                $query->where("first_name", 'LIKE', "%{$name}%");
                $query->orWhere("last_name", 'LIKE', "%{$name}%");
            });    
        }
    }

    public function notIn($notIn){
        if($notIn)
            $this->builder->whereNotIn('id', explode(',',$notIn));

    }

    public function status($status)
    {
        $this->builder->where("status", $status);
    }

}
