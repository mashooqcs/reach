<?php

namespace App\Models;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Laravel\Passport\HasApiTokens;
use Illuminate\Support\Facades\Event;
use Spatie\Searchable\Searchable;
use Spatie\Searchable\SearchResult;
use App\Notifications\ResetPasswordNotification;
use Str;

class User extends Authenticatable implements Searchable {

    use HasApiTokens, Notifiable;

    
    protected $fillable = [
        "first_name",
        "last_name",
        "email",
        "password",
        "profile_image",
        "phone",
        "dob",
        "address",
        "country",
        "state",
        "city",
        "zip",
        "device_id",
        "device_type",
        "status",
        "phone",
        "uuid",
        "gender"
    ];

    protected $hidden = [
        'password', 'remember_token',
    ];
   
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    public static function boot()
    {
        parent::boot();
        self::creating(function ($model) {
            $model->uuid = Str::uuid();
        });
    }

    public function getNameAttribute(){
        return "{$this->first_name} {$this->last_name}";
    } 

    public function scopeFilter($query, $filters)
    {
        return $filters->apply($query);
    }    

    public function scopeActive($query)
    {
        return $query->where('status', '1');
    }    

    public function sendPasswordResetNotification($token)
    {
        $url =$token;
        //$url =$token."?type=".request('type').'&';
        $this->notify(new ResetPasswordNotification($url));
    }

}
