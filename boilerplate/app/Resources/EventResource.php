<?php

namespace App\Core\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class EventResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            "data" => $this->payload['data'],
            "schedule" => $this->payload['schedule'],
            "users" => ParticipantResource::collection($this->participants),
            'created_date' => $this->created_at->format(config('app.date_format')),
        ];
    }
}

class ParticipantResource extends JsonResource {
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            "type" => $this->participantable_type,
            "id" => $this->participantable_id,
            "name" => $this->participantable->name,
            "email" => $this->participantable->email,
            "avatar" => $this->participantable->image,            
        ];
    }
}
