<?php

namespace App\Core\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class LiveStreamResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        $data = [
            'id' => $this->id,
            'title' => $this->title,
            'date' => $this->date,
            'time' => $this->time,
            'description' => $this->description,
            'files' => $this->files,
            'status' => $this->status,
            'live_stream_test' => $this->test,
            'created_date' => $this->created_at->format(config('app.date_format')),
            'trainer' => $this->trainer,
            'isSubscribed' => $this->isSubscribed,
            'isBooked' => $this->isBooked,
            'category' => $this->category,
            'UTCdate' => $this->UTCdate,
            'unformatted_date' => $this->unformatted_date,
            'subs' => (auth()->user()) ? null : $this->subs,
            'avg_rating' => ($this->avg_rating) ?? null,
            'reviews' => $this->reviews,
            'streamvideo' => ($this->video_url) ? asset("storage/{$this->video_url}") : '',
        ];

        return $data;
    }
}
