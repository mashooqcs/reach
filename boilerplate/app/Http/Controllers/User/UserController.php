<?php

namespace App\Http\Controllers;

use App\Core\Filters\UserFilters;
use App\Models\Contact;
use App\Models\Group;
use App\Organization;
use App\Employee;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Lang;
use Multicaret\Acquaintances\Interaction;
use Multicaret\Acquaintances\Models\InteractionRelation;
use Illuminate\Support\Facades\Validator;
use LVR\CreditCard\CardCvc;
use LVR\CreditCard\CardNumber;
use LVR\CreditCard\CardExpirationYear;
use LVR\CreditCard\CardExpirationMonth;
use Multicaret\Acquaintances\Status;
use Spatie\Searchable\ModelSearchAspect;
use Spatie\Searchable\Search;
use App\SecurityQuestion;
use Newsletter;
use Mail;
use Illuminate\Support\Facades\Auth;
use App\Core\Chat\Soachat;

class UserController extends Controller
{

    /**
     * follow
     *
     * @var array
     */
    protected $follow = [
        'pending' => 0,
        'approved' => 1,
    ];
    
    protected function validator(array $data)
    {
        return Validator::make($data, [
            "first_name" => ['required'],
            "last_name" => ['required'],
            "address" => ['required'],
            "phone" => ['required', 'numeric', 'min:9'],
            "city" => ['required'],
            "state" => ['required'],
            "zip" => ['required'],
            "country" => ['required'],
            'recovery_email' => ['required', 'string', 'email', 'max:255'],
            "gender" => ['required'],
        ]);
    }
    public function show(Request $request, User $user){

        $user->isFriendWith = $request->user()->isFriendWith($user);
        $user->hasSentFriendRequestTo = $request->user()->hasSentFriendRequestTo($user);

        return $user;

    }

    public function userDetail(Request $request, $id){
       $user = User::find($id);
    //    dd($user);
        $user->isFriendWith = $request->user()->isFriendWith($user);
        $user->hasSentFriendRequestTo = $request->user()->hasSentFriendRequestTo($user);
        return $user;

    }
    public function changePassword(Request $request){
        $request->validate([
            'password' => ['required', 'string', 'min:8', 'confirmed'],
            'old_password' => ['required'],
        ]);

        extract($request->all());

        $user = $request->user();

        if(Hash::check($old_password, $user->password)){
            $user->password = Hash::make($password);
            $user->save();

            return $this->responseSuccess(Lang::get("passwords.reset"));
        }

        return $this->responseWithError(['password' => Lang::get("validation.password_match")]);
    }



    public function searchGroup(Request $request){
        $q = $request->query('q');
        return Group::where('name', 'LIKE', "%{$q}%")->get();
    }

    public function updateProfile(Request $request){

        $this->validator($request->all())->validate();
        
        $user = $request->user();        
        
        $user->fill(collect($request->all())->toArray());
        
        $user->save();
        $soachat = new Soachat();        
        $soachat->updateUser($user->uuid, $user->name, $user->image, 0);

        return $this->responseSuccess(Lang::get('user.update'));

    }
    

    public function updateSecurityQuestions(Request $request){
    
        $request->validate([
            '*.question_id' => ['required', 'string'],
            '*.answer' => ['required', 'string'],
            
                        
        ]);
        $data = $request;
        $user = User::find($data[0]['model_id']);

        $ifAnswersAlreadyExists = SecurityQuestion::where(['model_id' => $data[0]['model_id'], 'model_type' => 'App\User'])->first();
        if($ifAnswersAlreadyExists){
            $deleletOld = SecurityQuestion::where(['model_id' => $data[0]['model_id'], 'model_type' => 'App\User' ])->delete();
        }
       
            $user->securityQuestion()->saveMany(
                [
                    new SecurityQuestion(['question_id' => $data[0]['question_id'], 'answer' => $data[0]['answer']]),
                    new SecurityQuestion(['question_id' => $data[1]['question_id'], 'answer' => $data[1]['answer']]),
                    new SecurityQuestion(['question_id' => $data[2]['question_id'], 'answer' => $data[2]['answer']])
                ] 
           );
       
    }

    public function getUserSecurityQuestions($id){
        return SecurityQuestion::where(['model_id' => $id, 'model_type' => 'App\User'])->get();
    }

    public function addSecurityQuestions(Request $data){
    
        $data->validate([
            'security_question_1' => ['required', 'string'],
            'security_question_2' => ['required', 'string'],
            'security_question_3' => ['required', 'string'],  
            'security_question_1_answer' => ['required', 'string'],
            'security_question_2_answer' => ['required', 'string'],
            'security_question_3_answer' => ['required', 'string'],
                        
        ]);
        $user = User::find($data['id']);

        $ifAnswersAlreadyExists = SecurityQuestion::where(['model_id' => $data['id'], 'model_type' => 'App\User'])->first();
        if($ifAnswersAlreadyExists){
            $deleletOld = SecurityQuestion::where(['model_id' => $data['id'], 'model_type' => 'App\User'])->delete();
        }
        $user->securityQuestion()->saveMany(
            [
                new SecurityQuestion(['question_id' => $data['security_question_1'], 'answer' => $data['security_question_1_answer']]),
                new SecurityQuestion(['question_id' => $data['security_question_2'], 'answer' => $data['security_question_2_answer']]),
                new SecurityQuestion(['question_id' => $data['security_question_3'], 'answer' => $data['security_question_3_answer']]),
            ] 
       );
    }

    

   

    public function searchAll(Request $request, UserFilters $filters){
        $q = $request->query('q');

        if(!$q) return [];

        return (new Search())
                ->registerModel(Organization::class, 'first_name', 'last_name', 'company_name')
                ->registerModel(User::class, function(ModelSearchAspect $modelSearchAspect) use ($request) {
                    $modelSearchAspect
                       ->addSearchableAttribute('first_name', 'last_name', 'company_name')
                       ->where('id', '!=', $request->user()->id); 
                })
                ->search($q);
                // ->registerModel(Organization::class, 'first_name', 'last_name', 'company_name')

    }

        
    public function search(Request $request, UserFilters $filters){

        if($request->query('resource') && $request->query('resource')  == "bo")
           return Organization::select('id', 'image', 'first_name', 'last_name')->filter($filters)->get();
    
        return User::select('id', 'image', 'first_name', 'last_name')->filter($filters)->get();
    }

    public function getFollowings(Request $request){
        $user = $request->user();

        return $user->getAllFriendships()->where('status', Status::ACCEPTED);
    }

    public function contactUs(Request $request){
        $request->validate([
            'first_name' => 'required',
            'last_name' => 'required',
            'email' => 'required',
            'subject' => 'required',
            'message' => 'required',
        ]);

        $contact = new Contact();
        $data = $request->only($contact->getFillable());        
        $contact->fill($data);
        $contact->save();

        return $this->responseSuccess(Lang::get('user.contact'));
    }

    public function subscribe(Request $request){
        if ( ! Newsletter::isSubscribed($request->email) ) {
            $check = Newsletter::subscribe($request->email);
            
            if($check){
                $userEmail = $request->email;
                    Mail::raw("Thank you for subscribing ConekPro newsletters ", function ($message) use ($userEmail) {
                $message->to($userEmail)
                    ->subject('ConekPro Subscription - ConekPro')->from('salcogen2021@gmail.com');
            });
            }

        }
        return true;
    }
    public function customLogin(Request $request){
        $validator = $request->validate([
            'email'     => 'required',
            'password'  => 'required|min:6'
        ]);
        $email = $request->input('email');
        $password = $request->input('password');


        if (Auth::guard('business')->attempt(['email' => $email, 'password' => $password])) {
            $user = Organization::where('email', $email)->first();
            $token = $user->createToken(config('app.name'))->accessToken;
            setcookie('p_token', $token, time() + (86400 * 30), "/"); // 86400 = 1 day  
            
            return redirect(url('/business/home'));
        }
        if (Auth::guard('employee')->attempt(['email' => $email, 'password' => $password])) {
            $user = Employee::where('email', $email)->first();
            $token = $user->createToken(config('app.name'))->accessToken;
            setcookie('p_token', $token, time() + (86400 * 30), "/"); // 86400 = 1 day  
            
            return redirect(url('/employee/dashboard'));
        }

        if (Auth::attempt($validator)) {
            $user = Auth::user();
            $token = $user->createToken(config('app.name'))->accessToken;

        setcookie('p_token', $token, time() + (86400 * 30), "/"); // 86400 = 1 day  
            return redirect(url('/dashboard'));
        }
        return redirect()->back()->with('error', 'These credentials do not match our records.');
    }

}
