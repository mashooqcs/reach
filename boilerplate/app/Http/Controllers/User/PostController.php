<?php

namespace App\Http\Controllers;

use App\Core\Http\Resources\PostResource;
use App\Media;
use App\CustomNotification;
use App\Models\Comment;
use App\Models\Post;
use App\Models\GroupUser;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Lang;
use Illuminate\Support\Facades\Log;
use phpseclib\Crypt\RC2;
use App\Models\Interaction;
use App\Models\Group;
use Twilio\Rest\Client;
use Twilio\Exceptions\RestException;
use App\User;
use App\Organization;
use Str;



class PostController extends Controller
{
    protected $per_page = 30;
    // public function checkUserExistsInGroup($userId, $groupId){
    //     return GroupUser::where(['group_id' => $groupId, 'entity_id' => $userId, 'entity_type' => 'App\User'])->first();
    // }

    public function index(Request $request){

        // \DB::listen(function($sql){
        //     Log::info($sql->sql);
        //     Log::info($sql->bindings);
        // });

        $posts = Post::query()->where(function($query) use($request){
            //$query->where('privacy', 0)
            $query->where(function($q){
                $q->whereIn('privacy', [0, 3]);
            })            
                ->orWhere(function($q){
                    $q->whereIn('privacy', [0,1,2])
                    ->where('postable_type', 'App\\User')
                    ->where('postable_id', request()->user()->id);
                })
               ->orWhere(function($q){
                    $q->where('privacy', 2)
                    ->where('postable_type', 'App\\User')
                    ->where('postable_id', request()->user()->id);
                })
                ->orWhere(function($q) {
                    
                    $friends = request()->user()->getAcceptedFriendships();
                    
                    foreach($friends as $friend){
                        $q->orWhere(function($q) use($friend) {

                            if($friend->sender_type == 'App\\User' && $friend->sender_id == request()->user()->id)
                            {
                                //look for recipient
                                $postable_id = $friend->recipient_id;
                                $postable_type = $friend->recipient_type;
                            }
                            else{
                                //look for sender
                                $postable_id = $friend->sender_id;
                                $postable_type = $friend->sender_type;
                            }

                            $q->where('postable_id', $postable_id)
                              ->where('postable_type', $postable_type)
                              ->where('privacy', 1);
                        });
                    }

                });

        });

        $posts->withCount('comments', 'group')->latest();
        
        if($request->group_id) $posts = $posts->whereGroupId($request->group_id);

        $posts = $posts->paginate($this->per_page);
        if(!count($posts)){
            return PostResource::collection($posts);
        }
        $res = array();
        $i=0;
        $authUser = Auth()->user();
        $authUserId = $authUser->id;
        foreach($posts as $post){
            if($post->privacy== 3){
                if($post->group){
                   $check =  $this->checkUserExistsInGroup($authUserId, $post->group->id);
                   if(!$check){
                       continue;
                   }
                   $post->disable_group_link = false;
                }    
            }
            elseif($post->privacy== 0 || $post->privacy== 1 || $post->privacy== 2 ){
                if($post->group){
                    $check =  $this->checkUserExistsInGroup($authUserId, $post->group->id);
                    if(!$check){
                       $post->disable_group_link = true; 
                    }
                    else{
                        $post->disable_group_link = false;
                    }
                 }  
            }

            $likeCount = Interaction::where(['subject_type' => 'App\Models\Post', 'subject_id' => $post->id, 'relation' => 'like'])->count();
            $post->likesCount = $likeCount;
            $res[$i] = $post;
            $i++;
        }

         return PostResource::collection($res);
    }

    public function checkUserExistsInGroup($userId, $groupId){
        return GroupUser::where(['group_id' => $groupId, 'entity_id' => $userId, 'entity_type'=>'App\User', 'status'=> 1])->first();
    }

    public function store(Request $request){

        $request->validate([
            'description' => 'required', 
            'file' => 'max:100480',       
        ],[
            'description.required_without' => "Either Media or description is required"
        ]);

        $reg_pattern = "/(((http|https|ftp|ftps)\:\/\/)|(www\.))[a-zA-Z0-9\-\.]+\.[a-zA-Z]{2,3}(\:[0-9]+)?(\/\S*)?/";            
            // make the urls to hyperlinks
            $des =  preg_replace($reg_pattern, '<a href="$0" target="_blank" rel="noopener noreferrer">$0</a>', $request->description);
            $check1 = Str::contains($des, 'http://');
            $check2 = Str::contains($des, 'https://');
            $check3 = Str::contains($des, 'www.');
            if(!$check1 && !$check2 && $check3 ){
               $des =  str_replace('www.','http://www.',$des);
            }


        $user = $request->user();   
        $post = $user->postable()->save(new Post([
            'description' => $des,
            'group_id' => $request->group_id ?? null,
            'privacy' => 3
        ]));

        if($request->has('file') && $request->file('file') !== null){
            $post->addMedia($request->file)->toMediaCollection("post-{$post->id}-media");
        }
        $post->load('media');
        return $this->responseSuccess(Lang::get('user.post.store'), $post);    
    }


    public function update(Request $request, Post $post){
        $post->privacy = $request->privacy;
        $post->save();
        return $this->responseSuccess(Lang::get('user.post.privacy'), $post);    
    }

    public function LikePost(Request $request, Post $post){
        $result = $request->user()->like($post);
        $authUser  = Auth()->user();
        $authUserId = $authUser->id;
        $postAuther = $post->postable_id;

        if($post->postable_type=='App\Organization' || $post->postable_type == 'App\\Organization'){
            if($post->group_id){
                $group = Group::find($post->group_id);
                $url = "/business/networks/group/$group->slug/$group->id";
            }
            else{
                $url = "/business/home";
            }    
        }else{
            if($post->group_id){
                $group = Group::find($post->group_id);
                $url = "/networks/group/$group->slug/$group->id";
            }
            else{
                $url = "/home";
            } 
        }

        if($authUserId != $postAuther){
            $currentDateAndTime = date('Y-m-d H:i:s');
            $dataToAdd['sender_id'] = $authUserId;
            $dataToAdd['recipient_id'] = $postAuther;
            $dataToAdd['sender_type'] = 'App\User';
            $dataToAdd['recipient_type'] = $post->postable_type;
            $dataToAdd['title'] = $authUser->first_name. " ". $authUser->last_name;
            $dataToAdd['body'] = 'has liked your post';
            $dataToAdd['url'] = $url;
            $dataToAdd['status'] = 1;
            $dataToAdd['created_at'] = $currentDateAndTime;
            $dataToAdd['updated_at'] = $currentDateAndTime;
            CustomNotification::create($dataToAdd);

            //send twilio sms
            if($post->postable_type == 'App\User' || $post->postable_type == 'App\\User'){
                $user = User::find($postAuther);
                
            }else{
                $user = Organization::find($postAuther);
            }
            $this->sendMessage('ConekPro - You have a new notification, please login to your account to check.', '+'.$user->phone??'');
            
        }
        return $result;
    }
    public function unLikePost(Request $request, Post $post){
        $result = $request->user()->unlike($post);
        $authUser  = Auth()->user();
        $authUserId = $authUser->id;
        $postAuther = $post->postable_id;
        if($post->postable_type=='App\Organization' || $post->postable_type == 'App\\Organization'){
            if($post->group_id){
                $group = Group::find($post->group_id);
                $url = "/business/networks/group/$group->slug/$group->id";
            }
            else{
                $url = "/business/home";
            }    
        }else{
            if($post->group_id){
                $group = Group::find($post->group_id);
                $url = "/networks/group/$group->slug/$group->id";
            }
            else{
                $url = "/home";
            } 
        }

        if($authUserId != $postAuther){
            $currentDateAndTime = date('Y-m-d H:i:s');
            $dataToAdd['sender_id'] = $authUserId;
            $dataToAdd['recipient_id'] = $postAuther;
            $dataToAdd['sender_type'] = 'App\User';
            $dataToAdd['recipient_type'] = $post->postable_type;
            $dataToAdd['title'] = $authUser->first_name. " ". $authUser->last_name;
            $dataToAdd['body'] = 'has unliked your post';
            $dataToAdd['url'] = $url;
            $dataToAdd['status'] = 1;
            $dataToAdd['created_at'] = $currentDateAndTime;
            $dataToAdd['updated_at'] = $currentDateAndTime;
            CustomNotification::create($dataToAdd);

            //send twilio sms
            if($post->postable_type == 'App\User' || $post->postable_type == 'App\\User'){
                $user = User::find($postAuther);
                
            }else{
                $user = Organization::find($postAuther);
            }
            $this->sendMessage('ConekPro - You have a new notification, please login to your account to check.', '+'.$user->phone??'');
        
        }
        return $result;
    }

    public function getComment(Request $request,  Post $post){
        $comments = $post->comments()->oldest()->where('parent_id', 0)->get();
        $res = array();
        $i=0;
        if(count($comments)){
            foreach($comments as $comment){
                $replies = $post->comments()->oldest()->where('parent_id', $comment->id)->get();
                $comment->replies = $replies;
                $res[$i] = $comment;
                $i++;
            }
        }
        return $res;
    }

    public function saveComment(Request $request, Post $post){

        $comment = $post->comments()->save(new Comment([
                "comment" => $request->comment,
                "commentaddable_type" => get_class($request->user()),
                "commentaddable_id" => $request->user()->id,
            ]));
        $comment->load('commentaddable');
        $authUser  = Auth()->user();
        $authUserId = $authUser->id;
        $postAuther = $post->postable_id;

        if($post->postable_type=='App\Organization' || $post->postable_type == 'App\\Organization'){
            if($post->group_id){
                $group = Group::find($post->group_id);
                $url = "/business/networks/group/$group->slug/$group->id";
            }
            else{
                $url = "/business/home";
            }    
        }else{
            if($post->group_id){
                $group = Group::find($post->group_id);
                $url = "/networks/group/$group->slug/$group->id";
            }
            else{
                $url = "/home";
            } 
        }    

        if($authUserId != $postAuther){
            $currentDateAndTime = date('Y-m-d H:i:s');
            $dataToAdd['sender_id'] = $authUserId;
            $dataToAdd['recipient_id'] = $postAuther;
            $dataToAdd['sender_type'] = 'App\User';
            $dataToAdd['recipient_type'] = $post->postable_type;
            $dataToAdd['title'] = $authUser->first_name. " ". $authUser->last_name;
            $dataToAdd['body'] = 'has commented on your post';
            $dataToAdd['url'] = $url;
            $dataToAdd['status'] = 1;
            $dataToAdd['created_at'] = $currentDateAndTime;
            $dataToAdd['updated_at'] = $currentDateAndTime;
            CustomNotification::create($dataToAdd);
            //send twilio sms
            if($post->postable_type == 'App\User' || $post->postable_type == 'App\\User'){
                $user = User::find($postAuther);
                
            }else{
                $user = Organization::find($postAuther);
            }
            $this->sendMessage('ConekPro - You have a new notification, please login to your account to check.', '+'.$user->phone??'');
        }
        return $comment;
    }
    public function saveReply(Request $request, Post $post, $commentId){
        $orgComment  = Comment::where('id', $commentId)->first();

        $comment = $post->comments()->save(new Comment([
                "comment" => $request->reply,
                "commentaddable_type" => get_class($request->user()),
                "commentaddable_id" => $request->user()->id,
                "parent_id" => $commentId
            ]));
        $comment->load('commentaddable');
        $authUser  = Auth()->user();
        $authUserId = $authUser->id;
        $postAuther = $post->postable_id;

        if($orgComment->commentaddable_type=='App\Organization' || $orgComment->commentaddable_type == 'App\\Organization'){
            if($post->group_id){
                $group = Group::find($post->group_id);
                $url = "/business/networks/group/$group->slug/$group->id";
            }
            else{
                $url = "/business/home";
            }    
        }else{
            if($post->group_id){
                $group = Group::find($post->group_id);
                $url = "/networks/group/$group->slug/$group->id";
            }
            else{
                $url = "/home";
            } 
        }    

        if($authUserId != $postAuther){
            $currentDateAndTime = date('Y-m-d H:i:s');
            $dataToAdd['sender_id'] = $authUserId;
            $dataToAdd['recipient_id'] = $orgComment->commentaddable_id;
            $dataToAdd['sender_type'] = 'App\User';
            $dataToAdd['recipient_type'] =$orgComment->commentaddable_type;   
            $dataToAdd['title'] = $authUser->first_name. " ". $authUser->last_name;
            $dataToAdd['body'] = 'has replied on your comment';
            $dataToAdd['url'] = $url;
            $dataToAdd['status'] = 1;
            $dataToAdd['created_at'] = $currentDateAndTime;
            $dataToAdd['updated_at'] = $currentDateAndTime;
            CustomNotification::create($dataToAdd);
            //send twilio sms
            if($post->postable_type == 'App\User' || $post->postable_type == 'App\\User'){
                $user = User::find($postAuther);
                
            }else{
                $user = Organization::find($postAuther);
            }
            $this->sendMessage('ConekPro - You have a new notification, please login to your account to check.', '+'.$user->phone??'');
        }
        return $comment;
    }

    public function deleteComment(Request $request, $post, $comment){
        (Comment::find($comment))->delete();
        return $this->responseSuccess("Comment is deleted successfully");    
    }    

    public function rePost(Request $request, Post $post){

        $clone = $post->replicate(['media']);

        $clone->postable()->associate($request->user());

        $clone->push();
        
        $post->media()->get()->each(function (Media $media) use ($clone) {
            $media->setAppends([]);
            $props = $media->toArray();
            unset($props['id']);
            $clone->addMedia($media->getPath())
                ->preservingOriginal()
                ->withProperties($props)
                ->toMediaCollection($media->collection_name);
        });        

        return $this->responseSuccess(Lang::get('user.post.store'));    

    }


    public function destroy(Request $request, Post $post){
        $post->delete();
        return $this->responseSuccess(Lang::get('user.post.deleted'));    
    }
    private function sendMessage($message, $recipients)
    {

        try {
            $account_sid = getenv("TWILIO_SID");
            $auth_token = getenv("TWILIO_AUTH_TOKEN");
            $twilio_number = getenv("TWILIO_NUMBER");
            $client = new Client($account_sid, $auth_token);
            $client->messages->create($recipients, ['from' => $twilio_number, 'body' => $message]);
        } catch (RestException $e) {
        }

    }

}
