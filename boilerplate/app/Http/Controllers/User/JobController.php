<?php

namespace App\Http\Controllers;

use App\Filters\JobFilters;
use App\Http\Resources\JobResource;
use App\Http\Controllers\Controller;
use App\Http\Requests\JobRequest;
use App\JobCandidate;
use App\Models\Job;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Lang;
use phpseclib\Crypt\RC2;
use Twilio\Rest\Client;
use App\CustomNotification;
use Twilio\Exceptions\RestException;
use App\User;
use App\Organization;


class JobController extends Controller
{    

    /**
     * index
     *
     * @param  mixed $request
     * @param  mixed $filters
     * @return void
     */
    public function index(Request $request, JobFilters $filters){
        return JobResource::collection(
            Job::query()->filter($filters)->latest()->get()
        );
    }
    
    /**
     * show
     *
     * @param  mixed $request
     * @param  mixed $job
     * @return void
     */
    public function show(Request $request, $job){
        return new JobResource(
            Job::find($job)
        );
    }

    
    public function apply(Request $request, Job $job){

        $request->validate([
            "file" => "required|mimes:pdf|max:10000"
        ], [
            'file.required' => "Resume/CV is required",
            'file.mimes' => "Resume/CV should be in pdf format",
        ]);

        $chk = $job->users()->withPivot('id')->wherePivot('user_id', request()->user()->id)->first();

        if($chk) return $this->responseWithError(['error' => "you have already applied for this job position"]);

        $job->users()->attach($request->user()->id, [
            'status' => 0,
            'created_at' => Carbon::now(),
        ]);        

        $chk = $job->users()->withPivot('id')->wherePivot('user_id', request()->user()->id)->first();

        $jobcandidateid = $chk->pivot->id;
        
        $resource = JobCandidate::find($jobcandidateid);

        $resource->addMedia($request->file)->toMediaCollection("job-{$job->id}-documents");
        
        // $jobable = $job->jobable;

        // sendNotification([
        //     "subject" => "Job Applied Notification",
        //     "name" => $jobable->name,
        //     "type" => "job_applied_notification",
        //     "text" => "{$request->user()->name} has applied for a job that you posted",
        //     "user" => $jobable
        // ]);
        $jobableid      = $job->jobable_id;
        $jobabletype    = $job->jobable_type;
        $authUser       = Auth()->user();

        $currentDateAndTime = date('Y-m-d H:i:s');
        $dataToAdd['sender_id'] = $authUser->id;
        $dataToAdd['recipient_id'] = $jobableid;
        $dataToAdd['sender_type'] = 'App\User';
        $dataToAdd['recipient_type'] = $jobabletype;
        $dataToAdd['title'] = $authUser->first_name. " ". $authUser->last_name;
        $dataToAdd['body'] = 'has applied to your job';
        $dataToAdd['url'] =  "/business/jobs";
      
        $dataToAdd['status'] = 1;
        $dataToAdd['created_at'] = $currentDateAndTime;
        $dataToAdd['updated_at'] = $currentDateAndTime;
        CustomNotification::create($dataToAdd);
        if($jobabletype== 'App\User' || $jobabletype== 'App\\User'){
            $user = User::find($jobableid);
            
        }else{
            $user = Organization::find($jobableid);
        }
        //send twilio sms
        
        $this->sendMessage('ConekPro - You have a new notification, please login to your account to check.', '+'.$user->phone??'');


        return $this->responseSuccess("You have successfully applied for this job position");
    }

    public function myJobs(Request $request, JobFilters $filters){
        $user =  $request->user();
        return $user->jobs()->filter($filters)->withPivot('id','status')->latest()->get();
    }
    private function sendMessage($message, $recipients)
    {

        try {
            $account_sid = getenv("TWILIO_SID");
            $auth_token = getenv("TWILIO_AUTH_TOKEN");
            $twilio_number = getenv("TWILIO_NUMBER");
            $client = new Client($account_sid, $auth_token);
            $client->messages->create($recipients, ['from' => $twilio_number, 'body' => $message]);
        } catch (RestException $e) {
        }

    }
}
