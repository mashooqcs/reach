<?php

namespace App\Http\Controllers;

use App\Core\Filters\ProjectFilters;
use App\Http\Controllers\Controller;
use App\Models\Keyword;
use App\Models\Project;
use App\Models\ProjectBids;
use App\Models\Comment;
use App\Models\ProjectJob;
use App\Models\ProjectRequest;
use App\Models\Task;
use App\Organization;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Lang;
use Twilio\Rest\Client;
use App\CustomNotification;
use Twilio\Exceptions\RestException;


class ProjectController extends Controller
{

    protected $incomming = 1;
    
    protected $outgoing = 0;

    /**
     * index
     *
     * @param  mixed $request
     * @param  mixed $filters
     * @return void
     */
    public function index(Request $request, ProjectFilters $filters){
        $user = $request->user();

        return $user->creatable()->withCount('bids')->filter($filters)->with('task')->latest()->paginate(20);
    }
    
    /**
     * update
     *
     * @param  mixed $project
     * @return void
     */
    public function update(Project $project){
        $project->add_board = 1;
        $project->save();
        return $this->responseSuccess(Lang::get('user.project.board'));
    }
    
    /**
     * show
     *
     * @param  mixed $request
     * @param  mixed $filters
     * @param  mixed $project
     * @return void
     */
    public function show(Request $request, ProjectFilters $filters, $project){
        $user = $request->user();
        $project = $user->creatable()->filter($filters)->with('media', 'bids', 'jobs.bids')->whereId($project)->first();
        $project->media->each(function(&$record) {
            $record->getFullUrl = $record->getFullUrl();
        });
        
        return [
            'project' => $project,
            'my_bids' => [],
        ];
    }


    /**
     * getKeyword
     *
     * @param  mixed $request
     * @return void
     */
    public function getKeyword(Request $request){
        $query = $request->query('query');
        $tags = Keyword::select('id', 'name')->where("name", "LIKE", "%{$query}%")->get();
        return $tags;

    }
    
    /**
     * addKeyword
     *
     * @param  mixed $tag
     * @return void
     */
    public function addKeyword($tag){
        $chk = Keyword::where('name',$tag)->first();

        if($chk) return;

        Keyword::create([
            'name' => $tag
        ]);
    }    
    
    /**
     * validationRules
     *
     * @param  mixed $request
     * @return void
     */
    public function validationRules(Request $request){
        $rules = [
            'project.name' => 'required',
            'project.type' => 'required',
            'project.description' => 'required',
        ];
        if($request->project['type'] == 0){
            $rules['project.duration'] = 'required|numeric';
            $rules['project.budget'] = 'required|numeric';
            $rules['project.tags'] = 'required';
            $rules['user'] = 'required';

        }
        else{
            $rules['jobs.*.*'] = 'required';
        }

        return $rules;

    }
    
    
    /**
     * store
     *
     * @param  mixed $request
     * @return void
     */
    public function store(Request $request){

        $rules = $this->validationRules($request);

        $request->validate($rules,[
            'name.required' => Lang::get('user.project.title.required')
        ]);


        $user = $request->user();
        $authUser  = Auth()->user();
        
        $project = $user->creatable()->save(new Project([
            'name' => $request->project['name'],
            'description' => $request->project['description'],
            'type' => $request->project['type'],
            'duration' => $request->project['duration'] ?? 0,
            'budget' => $request->project['budget'] ?? 0,
            'skills' => $request->project['tags'] ? implode(',',$request->project['tags']) : null,
            'jobs' => $request->jobs,        
            'status' => 1,
        ]));

        if($request->jobs && (int) $request->project['type'] !== 0){
            foreach($request->jobs as $job):
                $project->jobs()->save(new ProjectJob([
                    "title" => $job['title'],
                    "category" => $job['category'],
                    "duration" => $job['duration'],
                    "budget" => $job['budget'],
                    'user_id' => auth()->user()->id
                ]));
            endforeach;
        }
        
        if(isset($request->project['tags'])){
            foreach($request->project['tags'] as $tag ){
                $this->addKeyword($tag);
            }    
        }

        $u = collect($request->user);

        if(count($u)){
            $users = Organization::whereIn('id', collect($request->user)->pluck('id'))->get();

            $users->each(function($user) use ($project, $authUser) {
                $user->requestable()->save(new ProjectRequest([
                    'project_id' => $project->id,
                    'status' => 0,
                    'created_at' => Carbon::now(),
                    'user_id' => auth()->user()->id
                ]));

                $currentDateAndTime = date('Y-m-d H:i:s');
                $dataToAdd['sender_id'] = $authUser->id;
                $dataToAdd['recipient_id'] = $user->id;
                $dataToAdd['sender_type'] = 'App\User';
                $dataToAdd['recipient_type'] = 'App\Organization';;
                $dataToAdd['title'] = $authUser->first_name. " ". $authUser->last_name;
                $dataToAdd['body'] = 'has sent you a project request.';
                $dataToAdd['url'] =  "/business/projects/bid-request/$project->id?type=incomming";
                $dataToAdd['status'] = 1;
                $dataToAdd['created_at'] = $currentDateAndTime;
                $dataToAdd['updated_at'] = $currentDateAndTime;
                CustomNotification::create($dataToAdd);
                //send twilio sms
                
                $this->sendMessage('ConekPro - You have a new notification, please login to your account to check.', '+'.$user->phone??'');
            });
        }

        return $this->responseSuccess(Lang::get('user.project.store'), $project);
    }   
    
    /**
     * upload
     *
     * @param  mixed $request
     * @param  mixed $project
     * @return void
     */
    public function upload(Request $request, Project $project){
        $project->addMedia($request->file)->toMediaCollection("project-{$project->id}-documents");
        return $this->responseSuccess(Lang::get('user.project.doc_upload'), $project);

    }
    
    /**
     * storeTasks
     *
     * @param  mixed $request
     * @return void
     */
    public function storeTasks(Request $request){

        $request->validate([
            'name' => 'required',
            'users.*' => 'required',
            'due' => 'required',
            'description' => 'required',
            'project' => 'required',
        ]);

        $project = Project::find($request->project['id']);

        $task = $project->task()->save(new Task([
            'title' => $request->name,
            'description' => $request->description,
            'due' => $request->due,
        ]));

        $users = collect($request->users);

        $userIds = $users->pluck('id');

        $task->users()->attach($userIds, [
            'status' => 1,
            'created_at' => Carbon::now(),
        ]);

        return $this->responseSuccess(Lang::get('user.project.task.store'), $task);
    }


    public function bidRequest(Request $request, ProjectFilters $filters, $project = null){
        $user = $request->user();

        $project_requests = $user->creatable()->filter($filters)->whereHas('request', function($query) use ($request) {
            if($request->type == "outgoing")
                $query->outgoing();
            else
                $query->incomming();

        })->with('request.requestable');
        
        if($project) return $project_requests = $project_requests->whereId($project)->first();

        return $project_requests->get();

    }

    public function getProjectLogs(Request $request){

        $user = $request->user();
        $pids = $user->creatable()->pluck('id');
        $k = request('keyword');

        return ProjectBids::with([ 'project' => function($q) use($k){
            $q->where('name', 'like', '%'.$k.'%')->with('creatable', 'task')->withCount('bids');
        }, 'job', 'bidable'])
        ->whereIn('project_id', $pids)
        ->whereNotIn('status', [0, 2])
        ->where('only_board', request('only_board',0))
        ->get();               
    }

    public function addToBoard(Request $request, $id){
        $project = ProjectBids::find($id);
        $project->only_board = 1;
        $project->save();
    }
    public function addComment(Request $request, Task $task){
        $request->validate([
            'comment' => 'required',
        ]);

        $comment = $task->comments()->save(new Comment([
            "comment" => $request->comment,
            "commentaddable_type" => get_class($request->user()),
            "commentaddable_id" => $request->user()->id,
        ]));
        $comment->load('commentaddable');

        $task->refresh();
        //for now the in vue component refetch function is called instead
        //use the argument reciveed in aprams to modify the resource
        return $this->responseSuccess("The comment is saved successfully", $task);
    }
    private function sendMessage($message, $recipients)
    {

        try {
            $account_sid = getenv("TWILIO_SID");
            $auth_token = getenv("TWILIO_AUTH_TOKEN");
            $twilio_number = getenv("TWILIO_NUMBER");
            $client = new Client($account_sid, $auth_token);
            $client->messages->create($recipients, ['from' => $twilio_number, 'body' => $message]);
        } catch (RestException $e) {
        }

    }


}
