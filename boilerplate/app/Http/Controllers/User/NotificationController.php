<?php

namespace App\Http\Controllers;

use App\Core\Filters\UserFilters;
use App\Models\Contact;
use App\Models\Group;
use App\Organization;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Lang;
use Multicaret\Acquaintances\Interaction;
use Multicaret\Acquaintances\Models\InteractionRelation;
use Illuminate\Support\Facades\Validator;
use LVR\CreditCard\CardCvc;
use LVR\CreditCard\CardNumber;
use LVR\CreditCard\CardExpirationYear;
use LVR\CreditCard\CardExpirationMonth;
use Multicaret\Acquaintances\Status;
use Spatie\Searchable\ModelSearchAspect;
use Spatie\Searchable\Search;
use App\CustomNotification;

class NotificationController extends Controller
{
    public function markReaBellCoutdNotification(){
        $authUser   = Auth()->user();
        $authUserId = $authUser->id;
        return $update = CustomNotification::where(['recipient_id' => $authUserId, 'recipient_type'=> 'App\User'])->update(['bell_count'=> "0"]);
    }

    public function markReeadNotification($id){
        return $update = CustomNotification::where('id', $id)->update(['status'=> "0"]);
     }
    public function getNotifications(){
        $authUser   = Auth()->user();
        $authUserId = $authUser->id;
        $temp = array();
        $temp['notifications'] = CustomNotification::limit(5)->orderBy('id', 'DESC')->where(['recipient_id' => $authUserId, 'recipient_type' => 'App\User'])->get();
        $temp['unreadNotifications'] = CustomNotification::orderBy('id', 'DESC')->where(['recipient_id' => $authUserId, 'recipient_type' => 'App\User', 'bell_count' => "1"])->count();
        $temp['message_counter'] = $authUser->notification_count;
        return $temp;
        
    }
    public function getAllNotifications(){

    $authUser   = Auth()->user();
        $authUserId = $authUser->id;
        $get = CustomNotification::orderBy('id', 'DESC')->where(['recipient_id' => $authUserId, 'recipient_type' => 'App\User'])->get();
        if(!count($get)){
            return array();
        }
        $i=0;
        $res=array();
        $userModel = new User;
        $orgModel  = new Organization; 
        foreach($get as $data){
            if($data->sender_type =='App\\User'){
                $userData = $userModel->where('id', $data->sender_id)->first();
                // if($userData->image){
                //     $data->sender_image = $userData->image;
                // }else{
                    $data->sender_image = 'default.png';
                // }
                
            }
            if($data->sender_type =='App\\Organization'){
                $userData = $orgModel->where('id', $data->sender_id)->first();
                // if($userData->image){
                //     $data->sender_image = $userData->image;
                // }else{
                    $data->sender_image = 'default.png';
                // }
            }
            $data->date = substr($data->created_at, 0, 10);

            $res[$i]=$data;
            $i++;
        }

        // $updateToSeen = CustomNotification::where(['recipient_id' => $authUserId, 'recipient_type' => 'App\User'])->update(['status' => 0]);
        return $res;
    }
    public function getUserImage($userId, $userType){
        //dd($userType);
        if($userType =='App'){
            return $a =  User::where('id', $userId)->first();
        }
        if($userType =='Organization'){
            return $a =  Organization::where('id', $userId)->first();
        }

    }

    public function update_notification_count(Request $request){
        
        $user = User::where('uuid',$request->receipant_id)->first();
        if($user){
            $user->notification_count += 1;
            $user->save();
            return response()->json(['status' => true,'message' => 'notification counter updated successfully']);
        }
        $user = Organization::where('uuid',$request->receipant_id)->first();
        if($user){
            $user->notification_count += 1;
            $user->save();
            return response()->json(['status' => true,'message' => 'notification counter updated successfully']);

        }
    }

    public function reset_notification_count(Request $request){
        $user = $request->user();
        $user->notification_count = 0;
        $user->save();
    }

}
