<?php

use App\Employee;
use App\Organization;
use App\User;
use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        // factory(User::class, 20)->create();
        // factory(Employee::class, 50)->create();
        // factory(Organization::class, 20)->create();
        // $this->call(AdminSeeder::class);
        // $this->call(JobSeeder::class);
        // $this->call(JobCandidateSeede::class);
        // $this->call(CommisionSeeder::class);
        // $this->call(ReportSeeder::class);
        // $this->call(ReviewSeeder::class);
        // $this->call(GroupSeeder::class);
        $this->call([
            FriendshipSeeder::class
        ]);
        
    }
}
