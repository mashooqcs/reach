<div id="step2">

<div class="signup-text-main">
    <h3>Set your Own Privacy...
        <span>Security Questions:</span>
    </h3>
    <p>We may use security questions to verify you identity when you sign in to your account or if you need to reset
        your password.
        <br>Please select security questions from the dropdown menus.
    </p>
</div>

<div class="sign-up-line">

    <div class="form-group">
        <label>Select Question 1 of 3 <span>*</span></label>
        <select required name="security_question_1"  class="new-input form-control">
            <option>Select</option>
            <option  value="1">What is the first and last name of your first boyfriend or girlfriend?</option>
            <option  value="2">Which phone number do you remember most from your childhood?</option>
            <option  value="3">What was your favorite place to visit as a child?</option>
            <option  value="4">Who is your favorite actor, musician, or artist??</option>
            <option  value="5">What is the name of your favorite pet??</option>
            <option  value="6">In what city were you born?</option>
        </select>

        <textarea required name="security_question_1_answer" class="txtara" placeholder="Your answer"></textarea>
    </div>

    <div class="form-group">
        <label>Select Question 2 of 3 <span>*</span></label>
        <select required name="security_question_2"  class="new-input form-control">
            <option>Select</option>
            <option  value="1">What is the first and last name of your first boyfriend or girlfriend?</option>
            <option  value="2">Which phone number do you remember most from your childhood?</option>
            <option  value="3">What was your favorite place to visit as a child?</option>
            <option  value="4">Who is your favorite actor, musician, or artist??</option>
            <option  value="5">What is the name of your favorite pet??</option>
            <option  value="6">In what city were you born?</option>
        </select>

        <textarea required name="security_question_2_answer" class="txtara" placeholder="Your answer"></textarea>
    </div>


    <div class="form-group">
        <label>Select Question 3 of 3 <span>*</span></label>
        <select required name="security_question_3"  class="new-input form-control">
            <option>Select</option>
            <option  value="1">What is the first and last name of your first boyfriend or girlfriend?</option>
            <option  value="2">Which phone number do you remember most from your childhood?</option>
            <option  value="3">What was your favorite place to visit as a child?</option>
            <option  value="4">Who is your favorite actor, musician, or artist??</option>
            <option  value="5">What is the name of your favorite pet??</option>
            <option  value="6">In what city were you born?</option>
        </select>

        <textarea required name="security_question_3_answer" class="txtara" placeholder="Your answer"></textarea>

    </div>

</div>
</div>