<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <base href="{{url('/')}}/">
    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <title>{{ config('app.name') }}</title>
    <link rel="shortcut icon" href="{{ asset('images/favicon.png') }}"/>
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.11.2/css/all.min.css">
	<link href="https://fonts.googleapis.com/css?family=Poppins:300,400,500,600,700,800,900&display=swap" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="{{ asset('users/css/app.css') }}">
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimal-ui">

    <link href="https://fonts.googleapis.com/css?family=Poppins:300,400,500,600,700&display=swap" rel="stylesheet">
    <!-- BEGIN VENDOR CSS-->
    <style>
        /* .loading-indicator:before{content:'';background:#000000cc;position:fixed;width:100%;height:100%;top:0;left:0;z-index:10000000000}.loading-indicator:after{content:'Loading ...';position:fixed;width:100%;top:50%;left:0;z-index:10001;color:#fff;text-align:center;font-weight:700;font-size:1.5rem}.approve .dg-content:before{background:url("{{asset('/images/add-cat-popup.png')}}") no-repeat center !important}.reject .dg-content:before{background:url("{{asset('/images/block.png')}}") no-repeat center !important}.dg-content:before{background:url("{{asset('/images/block.png')}}") no-repeat center !important;} */
        .owl-nav{
                display: flex;
                flex-direction: row-reverse;
                justify-content: center;
        }
        .v-list.v-list--dense.theme--light > div:last-child{ 
            display:none;
       }

      
       .v-window .v-window__container .v-card__text > div:nth-child(6n){
           display: none ;
       }
       .v-window .v-window__container .v-card__text > div:nth-child(5n){
           display: none ;
       }

       .v-window .v-window__container .v-card__text > div:nth-child(4n){
           display: none;
       }
       .v-menu.v-menu--inline .v-menu__activator button{
           display:none;
       }
       .ds-schedule-type .v-input__control .v-input__slot{
           display: none;
       }
       /* .layout .ds-schedule-times{
           display: none;
       } */
       .ds-app-calendar-toolbar .v-btn--icon{
           display:none;
       }
       .v-input--checkbox .v-input__slot .v-input--selection-controls__input{
           display:none;
       }


    </style>
    <script type="text/javascript">
        window.base_url = "{{ url('/') }}";
        window.user = @json(auth()->user());
        window.appname = "{{config('app.name')}}";
        window.job_category = @json(config('app.job'));
        window.industires = @json(config('app.industries'));
        window.soachat = {
            appid: "{{config('services.soachat.appid')}}",
            appkey: "{{config('services.soachat.appkey')}}",
            domain: "{{config('services.soachat.domain')}}",
        }        
    </script>
        <style>
            .magnify-modal{
                z-index: 1090; width: 100%; height: 100%; left: 0px; top: 0px ;
            }
            
        </style>

</head>

<body class="vertical-layout vertical-menu 2-columns menu-expanded fixed-navbar @yield('body-class')">

<div id="app">

    <app/>

</div>
<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyAHPUufTlBkF5NfBT3uhS9K4BbW2N-mkb4&libraries=places"></script>

<script src="{{ asset('users/js/app.js') }}"></script>
@if(auth()->check())
<script uid="{{auth()->user()->uuid}}" src="https://dev28.onlinetestingserver.com/soachatcentralizedWeb/js/ocs.js"></script>
@endif
{{-- <script src="http://dev71.onlinetestingserver.com/chat_janus_v1/js/ocs.js"></script> --}}
<script src="https://cdn.socket.io/socket.io-1.0.0.js"></script>

<script src="https://code.jquery.com/jquery-3.6.0.js" integrity="sha256-H+K7U5CnXl1h5ywQfKtSj8PCmoN9aaq30gDh27Xc0jk=" crossorigin="anonymous"></script>

<script>
    // $(".v-input__control .v-input__slot .v-text-field__slot input").attr("type","time");
</script>
</body>
</html>
