<!DOCTYPE html>
<html class="loading" lang="en" data-textdirection="ltr">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimal-ui">
    <title>{{ config('app.name') }} - Login</title>
    <link rel="shortcut icon" href="{{ asset('images/favicon.png') }}" />
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.11.2/css/all.min.css">
	<link href="https://fonts.googleapis.com/css?family=Poppins:300,400,500,600,700,800,900&display=swap" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="{{ asset('app-assets/css/vendors.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('app-assets/css/app.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('app-assets/css/core/menu/menu-types/vertical-menu.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('admins/assets/css/style.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('admins/assets/css/responsive.css') }}">
</head>

<body class="vertical-layout vertical-menu 2-columns menu-expanded" data-open="click" data-menu="vertical-menu" data-col="2-columns">

<!--register page start here-->


<section class="register loginn">
    <div class="container">
      <div class="login-inner">
        <div class="row d-flex">
          <div class="col-lg-6 d-lg-block d-none col-12"><img src="{{ asset('admins/images/login-left.png') }}" class="login-image"></div>
          <div class="col-lg-6 col-12">
            <div class="right"> <img src="{{ asset('admins/images/login-logo.png') }}" class="img-fluid" alt="" />
              <div class="row ">
                <div class="col-12">
                  <h1>Admin Login</h1>
                </div>
              </div>
              <form action="{{ route('admins.login') }}" method="post">
                @csrf
                @if (session('status'))
                <div class="row">
                    <div class="col">
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    </div>
                </div>
                @endif

                @if($errors->any())
                    @foreach($errors->getMessages() as $this_error)
                        <div class="alert alert-danger">{{$this_error[0]}}</div>
                    @endforeach
                @endif      
              <div class="row">
                  <div class="col-12 form-group">
                    <input type="email" contenteditable="true" name="email" value="{{old('email')}}" spellcheck="true" class="form-control" placeholder="Enter Email Address" />
                    <i class="fa fa-envelope"></i> </div>
                  <div class="col-12 form-group position-relative">
                    <input type="password" id="password" name="password" class="form-control" placeholder="Password" />
                    <i class="fa fa-lock"></i>
                    <button type="button" class="view-btn position-absolute" onclick="togglePassword()">
                        <i class="fa fa-eye-slash"></i>
                    </button>
                  </div>
                </div>
                <div class="d-flex justify-content-between">
                  <div class="">
                    <label class="login-check">Remember Me
                      <input name="remember" type="checkbox"/>
                      <span class="checkmark"></span></label>
                  </div>
                  <div class=""> <a href="{{ route('admins.password.request') }}" class="forgot"> Forgot Password?</a> </div>
                </div>
                <button type="submit" class="blue-btn">login</button>
                <div class="new-user bottomlink">
                    <a href="{{ url('/') }}" class="login form-control"><i class="fa fa-arrow-circle-left"></i>Back To Website</a> </div>
              </form>
            </div>
          </div>
        </div>
      </div>
    </div>
  </section>




<script src="{{ asset('app-assets/vendors/js/vendors.min.js') }}" type="text/javascript"></script>
<script>
    function togglePassword(handler){
        let elem = $('#password');
        if('password' == $(elem).attr('type')){
            $(elem).prop('type', 'text');
            handler.innerHTML = '<i class="fa fa-eye-slash"></i>';
        }else{
            $(elem).prop('type', 'password');
            handler.innerHTML = '<i class="fa fa-lock"></i>';
        }        
    }
</script>
</body>
</html>
