const J_IndexComponent = () => import(/* webpackChunkName: "job" */ './IndexComponent.vue');
const J_ShowComponent = () => import(/* webpackChunkName: "job-view" */ './ShowComponent.vue');
const J_RequestedComponent = () => import(/* webpackChunkName: "job-requested" */ './RequestedComponent.vue');


export default [
    {
        path: '/jobs',
        name: 'jobs',
        component: J_IndexComponent,
        meta: {
            title: "Job",
            description: ""
        }
    },       
    {
        path: '/jobs/:slug/:id',
        name: 'jobs.show',
        component: J_ShowComponent,
        meta: {
            title: "Job Detail",
            description: ""
        }
    },           
    {
        path: '/jobs/requested',
        name: 'jobs.requested',
        component: J_RequestedComponent,
        meta: {
            title: "Job Requested",
            description: ""
        }
    },           
]