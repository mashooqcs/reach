const N_IndexComponent = () => import(/* webpackChunkName: "network" */ './IndexComponent.vue');
const N_CreateGroup = () => import(/* webpackChunkName: "network-create-group" */ './CreateGroup.vue');
const N_ShowGroup = () => import(/* webpackChunkName: "network-view-group" */ './ShowGroup.vue');
const N_EditGroup =  () => import(/* webpackChunkName: "network-edit-group" */ './EditGroup.vue');


export default [
    {
        path: '/networks/tab/:id?',
        name: 'network',
        component: N_IndexComponent,
        meta: {
            title: "Network",
            description: ""
        }
    },
    {
        path: '/group',
        name: 'group',
        component: N_IndexComponent,
        meta: {
            title: "Network",
            description: ""
        }
    },         
    {
        path: '/networks/create-group',
        name: 'network.create-group',
        component: N_CreateGroup,
        meta: {
            title: "Network - Create Group",
            description: ""
        }
    },     
    {
        path: '/networks/group/:slug/:id',
        name: 'group.show',
        component: N_ShowGroup,
        meta: {
            title: "Network - View Group",
            description: ""
        }
    },
    {
        path: '/networks/group/edit/:slug/:id',
        name: 'group.edit',
        component: N_EditGroup,
        meta: {
            title: "Network -  Edit Group",
            description: ""
        }
    },          
]