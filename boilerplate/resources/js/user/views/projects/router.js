const P_IndexComponent = () => import(/* webpackChunkName: "project" */ './IndexComponent.vue');
const P_CreateComponent = () => import(/* webpackChunkName: "project-create" */ './CreateComponent.vue');
const P_BidDetailComponent = () => import(/* webpackChunkName: "project-detail" */ './BidDetailComponent.vue');
const P_ShowComponent = () => import(/* webpackChunkName: "project-show" */ './ShowComponent.vue');
const P_BidComponent = () => import(/* webpackChunkName: "project-bid" */ './BidComponent.vue');
const P_ProjectLogComponent = () => import(/* webpackChunkName: "project-bid" */ './ProjectLogComponent.vue');
const P_TaskManagmentComponent = () => import(/* webpackChunkName: "project-tm" */ './TaskManagmentComponent.vue');
const CalenderComponent = () => import(/* webpackChunkName: "project-cal" */ './CalenderComponent.vue');

export default [
    {
        path: '/projects',
        name: 'projects',
        component: P_IndexComponent,
        meta: {
            title: "Projects",
            description: ""
        }
    },
    {
        path: '/calendar',
        name: 'projects.calender',
        component: CalenderComponent,
        meta: {
            title: "calendar",
            description: ""
        }
    },        
    {
        path: '/projects/create',
        name: 'projects.create',
        component: P_CreateComponent,
        meta: {
            title: "Project Initiation",
            description: ""
        }
    },       
    {
        path: '/projects/bid-request',
        name: 'projects.bid',
        component: P_BidComponent,
        meta: {
            title: "Project Bid Requests",
            description: ""
        }
    },    
    {
        path: '/projects/bid-request/:id',
        name: 'projects.bid.internal',
        component: P_BidComponent,
        meta: {
            title: "Project Bid Requests - Details",
            description: ""
        }
    },    
    {
        path: '/bids/detail/:id',
        name: 'projects.bid.detail',
        component: P_BidDetailComponent,
        meta: {
            title: "Project - Bid Details",
            description: ""
        }
    },    
    {
        path: '/projects/logs',
        name: 'projects.logs',
        component: P_ProjectLogComponent,
        meta: {
            title: "Project Logs",
            description: ""
        }
    },    
    {
        path: '/projects/task-managment',
        name: 'projects.tm',
        component: P_TaskManagmentComponent,
        meta: {
            title: "Projects",
            description: ""
        }
    },
    {
        path: '/projects/:id',
        name: 'projects.show',
        component: P_ShowComponent,
        meta: {
            title: "Project Initiation",
            description: ""
        }
    },     
]