<?php 
  $title = "User Profile";
  include('header.php');
  include('sidebar.php');
?>

<div class="app-content content dashboard">
    <div class="content-wrapper">
        <div class="content-body">
            <!-- Basic form layout section start -->
            <section id="configuration">
                <div class="row">
                    <div class="col-12">
                        <div class="card ">
                            <div class="card-content collapse show">
                                <div class="card-dashboard">
                                    <div class="row">
                                        <div class="col-12">
                                            <h1 class="mb-2"><a href="user-profile.php"><i class="fas fa-chevron-left mr-1"></i> PROFILE</a></h1>
                                        </div>
                                    </div>
                                    <div class="custom-card">
                                        <div class="card-heading">
                                            <p>view profile</p>
                                        </div>
                                        <div class="card-area">
                                            <div class="text-center">
                                                <a class="site-btn sm-btn blue mb-2" data-toggle="modal" data-target=".changePwd">CHANGE PASSWORD</a>
                                            </div>
                                            <div class="profile-picture-div">
                                                    <img src="./images/img-placeholder.png" class="profile-pic img-fluid" alt="">
                                                    <label for="picture">
                                                        <i class="fas fa-camera profile-pic-icon"></i>
                                                    </label>
                                                        <form style="display: none;">
                                                        <input type="file" name="pic" accept=".gif,.jpg,.png,.tif|image/*" id="picture">
                                                        <input type="submit">
                                                    </form>
                                            </div>
                                            <p class="form-heading pt-1 mb-3">Personal Information</p>
                                            <div class="row">
                                                <div class="col-md-6 col-12">
                                                    <div class="form-field">
                                                        <label for="" class="site-label">First Name*</label>
                                                        <input type="text" class="site-input" placeholder="E.g James" value="Lorem" >
                                                    </div>
                                                </div>
                                                <div class="col-md-6 col-12">
                                                    <div class="form-field">
                                                        <label for="" class="site-label">Last Name*</label>
                                                        <input type="text" class="site-input" placeholder="Eg James" value="Ipsum" >
                                                    </div>
                                                </div>
                                                <div class="col-md-6 col-12">
                                                    <div class="form-field">
                                                        <label for="" class="site-label">Phone Number*</label>
                                                        <input type="number" id="phone" class="site-input" placeholder="Enter Phone Number" value="232148" >
                                                    </div>
                                                </div>
                                                <div class="col-md-6 col-12">
                                                    <div class="form-field">
                                                        <label for="" class="site-label">Email*</label>
                                                        <input type="email" class="site-input" placeholder="Eg James" value="hamza@gmail" readonly>
                                                    </div>
                                                </div>
                                            </div>
                                            <p class="form-heading pt-1 mb-3">Address Details</p>
                                            <div class="row">
                                                <div class="col-md-6 col-12">
                                                    <div class="form-field">
                                                        <label for="" class="site-label">Address*</label>
                                                        <input type="text" class="site-input" placeholder="E.g James" value="12sad23" >
                                                    </div>
                                                </div>
                                                <div class="col-md-6 col-12">
                                                    <div class="form-field">
                                                        <label for="" class="site-label">Country*</label>
                                                        <input type="text" class="site-input" placeholder="Eg James" value="USA" >
                                                    </div>
                                                </div>
                                                <div class="col-md-6 col-12">
                                                    <div class="form-field">
                                                        <label for="" class="site-label">State*</label>
                                                        <input type="text" class="site-input" placeholder="Abc" value="Texas" >
                                                    </div>
                                                </div>
                                                <div class="col-md-6 col-12">
                                                    <div class="form-field">
                                                        <label for="" class="site-label">City*</label>
                                                        <input type="text" class="site-input" placeholder="Abc" value="abc" >
                                                    </div>
                                                </div>
                                                <div class="col-md-6 col-12">
                                                    <label for="" class="site-label">Zip Code*</label>
                                                    <div class="form-field">
                                                        <input type="number" class="site-input enter-input" placeholder="1223" value="123" >
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="text-center">
                                                <a href="user-profile.php" class="site-btn blue">Update</a>
                                            </div>
                                        </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
        </div>
    </div>
</div>

<!-- Inactive Agent Modal -->
<div class="modal fade changePwd" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content site-modal">
            <i class="fas fa-times close modal-close" data-dismiss="modal" aria-label="Close"></i>
          
                <p class="modal-heading text-center">Change Password</p>
                <label for="" class="site-label">Current Password*</label>
                <div class="form-field">
                    <input type="password" class="site-input right-icon enter-input" placeholder="Enter Password" name="" id="">
                    <i class="fa fa-eye-slash enter-icon right-icon" aria-hidden="true"></i>
                </div>
                <label for="" class="site-label">New Password*</label>
                <div class="form-field">
                    <input type="password" class="site-input right-icon current-input" placeholder="Enter New Password" name="" id="">
                    <i class="fas fa-eye-slash current-icon right-icon" aria-hidden="true"></i>
                </div>
                <label for="" class="site-label">Confirm Password*</label>
                <div class="form-field">
                    <input type="password" class="site-input right-icon confirm-input" placeholder="Confirm Password" name="" id="">
                    <i class="fa fa-eye-slash confirm-icon right-icon" aria-hidden="true"></i>
                </div>
                <div class="modal-btn-div">
                    <a class="site-btn blue" data-dismiss="modal" aria-label="Close">UPDATE</a>
                </div>
       
        </div>
    </div>
</div>

<?php include('footer.php'); ?>
