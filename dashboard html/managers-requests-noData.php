<?php 
  $pg='managers';
  $title = "Manager's Requests";
  include('header.php');
  include('sidebar.php');
?>

<div class="app-content content dashboard">
    <div class="content-wrapper">
        <div class="content-body">
            <!-- Basic form layout section start -->
            <section id="configuration">
                <div class="row">
                    <div class="col-12">
                        <div class="card ">
                            <div class="card-content collapse show">
                                <div class="card-dashboard">
                                    <div class="row">
                                        <div class="col-12">
                                            <h1 class="mb-2"><a href="managers.php"><i class="fas fa-chevron-left mr-1"></i> MANAGERS</a></h1>
                                        </div>
                                    </div>
                                    <ul class="nav nav-pills agent-pills" id="pills-tab" role="tablist">
                                        <li class="nav-item">
                                            <a class="nav-link active" id="recieved-pills-tab" data-toggle="pill" href="#recieved-tab" role="tab" aria-controls="recieved-tab" aria-selected="true">Recieved</a>
                                        </li>
                                        <li class="nav-item">
                                            <a class="nav-link" id="rejected-pills-tab" data-toggle="pill" href="#rejected-tab" role="tab" aria-controls="rejected-tab" aria-selected="false">Rejected</a>
                                        </li>
                                    </ul>
                                    <p class="form-heading">Requests</p>
                                    <div class="tab-content" id="pills-tabContent">
                                        <div class="tab-pane fade show active" id="recieved-tab" role="tabpanel" aria-labelledby="recieved-pills-tab">
                                            <div class="empty-div">
                                                <h1>No Data Found</h1>
                                            </div>
                                        </div>
                                        <div class="tab-pane fade" id="rejected-tab" role="tabpanel" aria-labelledby="rejected-pills-tab">
                                            <div class="empty-div">
                                                <h1>No Data Found</h1>
                                            </div>
                                        </div>
                                    </div>

                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
        </div>
    </div>
</div>

<?php include('footer.php') ?>

