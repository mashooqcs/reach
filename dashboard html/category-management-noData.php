<?php 
  $pg='category';
  $title = "Category Management";
  include('header.php');
  include('sidebar.php');
?>

<div class="app-content content dashboard">
    <div class="content-wrapper">
        <div class="content-body">
            <!-- Basic form layout section start -->
            <section id="configuration">
                <div class="row">
                    <div class="col-12">
                        <div class="card ">
                            <div class="card-content collapse show">
                                <div class="card-dashboard">
                                    <div class="row">
                                        <div class="col-12">
                                            <h1 class="mb-2">CATEGORY MANAGEMENT</h1>
                                        </div>
                                    </div>
                                    <div class="text-md-right text-center">
                                        <a data-toggle="modal" data-target=".addCategoryModal" class="site-btn blue mb-1">ADD NEW</a>
                                    </div>
                                    <div class="empty-div">
                                        <h1>No Data Found</h1>
                                    </div>

                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
        </div>
    </div>
</div>

<?php include('footer.php') ?>


<!-- Add Category Modal -->
<div class="modal fade addCategoryModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content site-modal">
            <i class="fas fa-times close modal-close" data-dismiss="modal" aria-label="Close"></i>
            <p class="modal-heading text-center">Add Category</p>
            <div class="row">
                <div class="col-12">
                    <div class="form-field">
                        <label for="" class="site-label">Category Name*</label>
                        <input type="text" class="site-input" placeholder="Enter Category Name">
                    </div>
                </div>
            </div>
            <div class="text-center mt-2">
                <a href="#" class="site-btn blue">Add</a>
            </div>
        </div>
    </div>
</div>