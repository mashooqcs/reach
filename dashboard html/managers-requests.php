<?php 
  $pg='managers';
  $title = "Manager's Requests";
  include('header.php');
  include('sidebar.php');
?>

<div class="app-content content dashboard">
    <div class="content-wrapper">
        <div class="content-body">
            <!-- Basic form layout section start -->
            <section id="configuration">
                <div class="row">
                    <div class="col-12">
                        <div class="card ">
                            <div class="card-content collapse show">
                                <div class="card-dashboard">
                                    <div class="row">
                                        <div class="col-12">
                                            <h1 class="mb-2"><a href="managers.php"><i class="fas fa-chevron-left mr-1"></i> MANAGERS</a></h1>
                                        </div>
                                    </div>
                                    <ul class="nav nav-pills agent-pills" id="pills-tab" role="tablist">
                                        <li class="nav-item">
                                            <a class="nav-link active" id="recieved-pills-tab" data-toggle="pill" href="#recieved-tab" role="tab" aria-controls="recieved-tab" aria-selected="true">Recieved</a>
                                        </li>
                                        <li class="nav-item">
                                            <a class="nav-link" id="rejected-pills-tab" data-toggle="pill" href="#rejected-tab" role="tab" aria-controls="rejected-tab" aria-selected="false">Rejected</a>
                                        </li>
                                    </ul>
                                    <p class="form-heading">Requests</p>
                                    <div class="tab-content" id="pills-tabContent">
                                        <div class="tab-pane fade show active" id="recieved-tab" role="tabpanel" aria-labelledby="recieved-pills-tab">
                                            <div class="clearfix"></div>
                                            <div class="maain-tabble table-responsive">
                                                <table class="table table-striped table-bordered zero-configuration">
                                                    <thead>
                                                        <tr>
                                                            <th>REQUEST ID</th>
                                                            <th>MANAGER ID</th>
                                                            <th>FULL NAME</th>
                                                            <th>EMAIL ADDRESS</th>
                                                            <th>ACTIONS</th>
                                                        </tr>
                                                    </thead>
                                                    <tbody>
                                                        <tr>
                                                            <td>002</td>
                                                            <td>001</td>
                                                            <td>Abce</td>
                                                            <td>abc@gmail.com</td>
                                                            <td>
                                                                <div class="btn-group custom-dropdown ml-2 mb-1">
                                                                    <button type="button" class="btn btn-drop-table btn-sm" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"> <i class="fa fa-ellipsis-v"></i></button>
                                                                    <div class="dropdown-menu custom-dropdown"> 
                                                                        <a class="dropdown-item" href="recieved-request-details.php"><i class="fa fa-eye"></i>VIEW </a> 
                                                                    </div>
                                                                </div>
                                                            </td>
                                                        </tr>
                                                    </tbody>
                                                </table>
                                            </div> 
                                        </div>
                                        <div class="tab-pane fade" id="rejected-tab" role="tabpanel" aria-labelledby="rejected-pills-tab">
                                            <div class="clearfix"></div>
                                            <div class="maain-tabble table-responsive">
                                                <table class="table table-striped table-bordered zero-configuration">
                                                    <thead>
                                                        <tr>
                                                            <th>REQUEST ID</th>
                                                            <th>MANAGER ID</th>
                                                            <th>FULL NAME</th>
                                                            <th>EMAIL ADDRESS</th>
                                                            <th>ACTIONS</th>
                                                        </tr>
                                                    </thead>
                                                    <tbody>
                                                        <tr>
                                                            <td>002</td>
                                                            <td>001</td>
                                                            <td>Abce</td>
                                                            <td>abc@gmail.com</td>
                                                            <td>
                                                                <div class="btn-group custom-dropdown ml-2 mb-1">
                                                                    <button type="button" class="btn btn-drop-table btn-sm" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"> <i class="fa fa-ellipsis-v"></i></button>
                                                                    <div class="dropdown-menu custom-dropdown"> 
                                                                        <a class="dropdown-item" href="rejected-request-details.php"><i class="fa fa-eye"></i>VIEW </a> 
                                                                    </div>
                                                                </div>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td>002</td>
                                                            <td>001</td>
                                                            <td>Abce</td>
                                                            <td>abc@gmail.com</td>
                                                            <td>
                                                                <div class="btn-group custom-dropdown ml-2 mb-1">
                                                                    <button type="button" class="btn btn-drop-table btn-sm" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"> <i class="fa fa-ellipsis-v"></i></button>
                                                                    <div class="dropdown-menu custom-dropdown"> 
                                                                        <a class="dropdown-item" href="rejected-request-details.php" data-toggle="modal" data-target=".inactiveAgent"><i class="fa fa-eye"></i>VIEW </a> 
                                                                    </div>
                                                                </div>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td>002</td>
                                                            <td>001</td>
                                                            <td>Abce</td>
                                                            <td>abc@gmail.com</td>
                                                            <td>
                                                                <div class="btn-group custom-dropdown ml-2 mb-1">
                                                                    <button type="button" class="btn btn-drop-table btn-sm" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"> <i class="fa fa-ellipsis-v"></i></button>
                                                                    <div class="dropdown-menu custom-dropdown"> 
                                                                        <a class="dropdown-item" href="rejected-request-details.php" data-toggle="modal" data-target=".inactiveAgent"><i class="fa fa-eye"></i>VIEW </a> 
                                                                    </div>
                                                                </div>
                                                            </td>
                                                        </tr>

                                                    </tbody>
                                                </table>
                                            </div> 
                                        </div>
                                    </div>

                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
        </div>
    </div>
</div>

<!-- Inactive Agent Modal -->
<div class="modal fade inactiveAgent" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content site-modal">
            <i class="fas fa-times close modal-close" data-dismiss="modal" aria-label="Close"></i>
            <div class="text-center">
                <div class="modal-icon-div">
                    <i class="fas fa-ban modal-icon"></i>
                </div>
                <p class="modal-heading">System Message</p>
                <p class="modal-text">Are you sure you want to inactive this agent's profile ?</p>
                <div class="modal-btn-div">
                    <a class="site-btn orange" id="yesinactive">Yes</a>
                    <a class="site-btn blue" data-dismiss="modal" aria-label="Close">No</a>
                </div>
            </div>
        </div>
    </div>
</div>

<!-- Register Agent Confirmation -->
<div class="modal fade confirmInactive" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content site-modal">
            <i class="fas fa-times close modal-close" data-dismiss="modal" aria-label="Close"></i>
            <div class="text-center">
                <div class="modal-icon-div">
                    <i class="fas fa-ban modal-icon"></i>
                </div>
                <p class="modal-heading">System Message</p>
                <p class="modal-text">Agent xyz Profile has been inactivated</p>
                <div class="modal-btn-div">
                    <a class="site-btn blue" data-dismiss="modal" aria-label="Close">GOT IT</a>
                </div>
            </div>
        </div>
    </div>
</div>


<!-- Agent New Activity -->
<div class="modal fade assignTask" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content site-modal">
            <i class="fas fa-times close modal-close" data-dismiss="modal" aria-label="Close"></i>
                <p class="modal-heading text-center mb-2">Assign New Activity</p>
                <label for="" class="site-label">Select Category*</label>
                <div class="form-field">
                    <select class="site-input border" name="" id="">
                        <option value="">Numeric</option>
                        <option value="">Alphabetic</option>
                        <option value="">Numeric</option>
                    </select>
                    <i class="fas fa-caret-down right-icon"></i>
                </div>
                <label for="" class="site-label">Select Activity*</label>
                <div class="form-field">
                    <select class="site-input border" name="" id="">
                        <option value="">Numeric</option>
                        <option value="">Alphabetic</option>
                        <option value="">Numeric</option>
                    </select>
                    <i class="fas fa-caret-down right-icon"></i>
                </div>
                <div class="form-field">
                    <label for="" class="site-label">Goal*</label>
                    <input type="text" class="site-input border" name="" placeholder="Enter Goal">
                </div>
                <div class="modal-btn-div">
                    <a class="site-btn blue" data-dismiss="modal" aria-label="Close">ASSIGN</a>
                </div>
        </div>
    </div>
</div>

<?php include('footer.php') ?>

