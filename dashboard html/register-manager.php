<?php 
  $pg='managers';
  $title = "Register Manager";
  include('header.php');
  include('sidebar.php');
?>

<div class="app-content content dashboard">
    <div class="content-wrapper">
        <div class="content-body">
            <!-- Basic form layout section start -->
            <section id="configuration">
                <div class="row">
                    <div class="col-12">
                        <div class="card ">
                            <div class="card-content collapse show">
                                <div class="card-dashboard">
                                    <div class="row">
                                        <div class="col-12">
                                            <h1 class="mb-2"><a href="managers.php"><i class="fas fa-chevron-left mr-1"></i> MANAGERS</a></h1>
                                        </div>
                                    </div>

                                    <div class="custom-card">
                                        <div class="card-heading">
                                            <p>Register Agent</p> 
                                        </div>
                                        <div class="card-area">
                                            <div class="profile-picture-div">
                                                <img src="./images/img-placeholder.png" class="profile-pic img-fluid" alt="">
                                                    <label for="picture">
                                                        <i class="fas fa-camera profile-pic-icon"></i>
                                                    </label>
                                                        <form style="display: none;">
                                                        <input type="file" name="pic" accept=".gif,.jpg,.png,.tif|image/*" id="picture">
                                                        <input type="submit">
                                                    </form>
                                            </div>
                                            <p class="form-heading orange-text pt-1 mb-3">Personal Information</p>
                                            <div class="row">
                                                <div class="col-md-6 col-12">
                                                    <div class="form-field">
                                                        <label for="" class="site-label">First Name*</label>
                                                        <input type="text" class="site-input" placeholder="Enter First Name">
                                                    </div>
                                                </div>
                                                <div class="col-md-6 col-12">
                                                    <div class="form-field">
                                                        <label for="" class="site-label">Last Name*</label>
                                                        <input type="text" class="site-input" placeholder="Enter Last Name">
                                                    </div>
                                                </div>
                                                <div class="col-md-6 col-12">
                                                    <div class="form-field">
                                                        <label for="" class="site-label">Phone Number*</label>
                                                        <input type="number" id="phone" class="site-input" placeholder="Enter Phone Number">
                                                    </div>
                                                </div>
                                                <div class="col-md-6 col-12">
                                                    <div class="form-field">
                                                        <label for="" class="site-label">Email*</label>
                                                        <input type="email" class="site-input" placeholder="Enter Email">
                                                    </div>
                                                </div>
                                                <div class="col-md-6 col-12">
                                                    <label for="" class="site-label">Password*</label>
                                                    <div class="form-field">
                                                        <input type="password" class="site-input confirm-input" placeholder="enter password">
                                                        <i class="fas fa-eye-slash right-icon confirm-icon" aria-hidden="true"></i>
                                                    </div>
                                                </div>
                                                <div class="col-md-6 col-12">
                                                    <label for="" class="site-label">Confirm Password*</label>
                                                    <div class="form-field">
                                                        <input type="password" class="site-input current-input" placeholder="enter password">
                                                        <i class="fas fa-eye-slash current-icon right-icon" aria-hidden="true"></i>
                                                    </div>
                                                </div>
                                            </div>
                                            <p class="form-heading orange-text pt-3 mb-3">Address Details</p>
                                            <div class="row">
                                                <div class="col-md-6 col-12">
                                                    <div class="form-field">
                                                        <label for="" class="site-label">Address*</label>
                                                        <input type="text" class="site-input" placeholder="Enter address">
                                                    </div>
                                                </div>
                                                <div class="col-md-6 col-12">
                                                    <div class="form-field">
                                                        <label for="" class="site-label">Country*</label>
                                                        <input type="text" class="site-input" placeholder="enter country">
                                                    </div>
                                                </div>
                                                <div class="col-md-6 col-12">
                                                    <div class="form-field">
                                                        <label for="" class="site-label">State*</label>
                                                        <input type="text" class="site-input" placeholder="enter state">
                                                    </div>
                                                </div>
                                                <div class="col-md-6 col-12">
                                                    <div class="form-field">
                                                        <label for="" class="site-label">City*</label>
                                                        <input type="email" class="site-input" placeholder="enter city">
                                                    </div>
                                                </div>
                                                <div class="col-12">
                                                    <label for="" class="site-label">Zip Code*</label>
                                                    <div class="form-field">
                                                        <input type="number" class="site-input enter-input" placeholder="enter zip code">
                                                    </div>
                                                </div>
                                                <div class="col-12">
                                                    <p class="black-text mb-0">
                                                        <input type="checkbox" id="stopover" name="radio-group">
                                                        <label for="stopover" class="bordered mb-0">Enable Registration Fee</label>
                                                    </p>
                                                </div>
                                            </div>
                                            <div class="text-center mt-3">
                                                <a class="site-btn blue" data-toggle="modal" data-target=".registerAgent">REGISTER</a>
                                            </div>
                                        </div>
                                    </div>

                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
        </div>
    </div>
</div>


<!-- Register Agent Modal -->
<div class="modal fade registerAgent" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content site-modal">
            <i class="fas fa-times close modal-close" data-dismiss="modal" aria-label="Close"></i>
            <div class="text-center">
                <div class="modal-icon-div">
                    <i class="fas fa-ban modal-icon"></i>
                </div>
                <p class="modal-heading">System Message</p>
                <p class="modal-text">Are you sure you want to register this agent's?</p>
                <div class="modal-btn-div">
                    <a class="site-btn orange" id="yesregister">Yes</a>
                    <a class="site-btn blue" data-dismiss="modal" aria-label="Close">No</a>
                </div>
            </div>
        </div>
    </div>
</div>

<!-- Register Agent Confirmation -->
<div class="modal fade yesregisterModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content site-modal">
            <i class="fas fa-times close modal-close" data-dismiss="modal" aria-label="Close"></i>
            <div class="text-center">
                <div class="modal-icon-div">
                    <i class="fas fa-ban modal-icon"></i>
                </div>
                <p class="modal-heading">System Message</p>
                <p class="modal-text">Agent has been registered successfully</p>
                <div class="modal-btn-div">
                    <a class="site-btn blue" data-dismiss="modal" aria-label="Close">GOT IT</a>
                </div>
            </div>
        </div>
    </div>
</div>

<?php include('footer.php') ?>
