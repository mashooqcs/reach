<?php 
  $title = "Notifications";
  include('header.php');
  include('sidebar.php');
?>

<div class="app-content content dashboard">
  <div class="content-wrapper">
    <div class="content-body"> 
      <!-- Basic form layout section start -->
      <section id="configuration">
        <div class="row">
          <div class="col-12">
            <div class="card ">
              <div class="card-content collapse show">
                <div class="card-dashboard">
                        <div class="row">
                            <div class="col-12">
                            <h1 class="yellow-text">NOTIFICATIONS</h1>
                            </div>
                      </div>
                    <div class="col-12">
                      <div class="row">
                        <div class="col-md-6 col-12">
                          <label>Sort By:</label>
                        </div>
                        <!-- <div class="col-md-6 col-12">
                          <div class="add-block-div">
                            <a href="add-user.html" class="site-btn dark-pink">ADD USERS</a>
                            <a href="blocked-users.html" class="site-btn pink">BLOCKED USERS</a>
                          </div>
                        </div> -->

                      </div>
                    </div>


												<div class="row align-items-center">
													<div class="col-lg-3 col-md-6 col-sm-6">
														<label  for="">From:</label>
														<input id="datepicker-1" class="site-input border" type="text" readonly="">
													</div>
													<div class="col-lg-3 col-md-6 col-sm-6">
														<label  for="">To:</label>
														<input id="datepicker-2" class="site-input border" type="text" readonly="">
													</div>

												</div>
				

                <div class="clearfix"></div>
                <div class="maain-tabble table-responsive">
                  <table class="table table-striped table-bordered zero-configuration">
                    <thead>
                      <tr>
                        <th>S.NO</th>
                        <th>DESCRIPTION</th>
                        <th>DATE</th>
                        <th>TIME</th>
                        <th>ACTION</th>
                      </tr>
                    </thead>
                    <tbody>
                      <tr>
                        <td>01</td>
                        <td>Lorem Ipsum is simply dummy text of the printing</td>
                        <td>May 2, 2020</td>
                        <td>8:30 PM</td>
                        <td>
                            <div class="btn-group custom-dropdown ml-2 mb-1">
                                <button type="button" class="btn btn-drop-table btn-sm" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"> <i class="fa fa-ellipsis-v"></i></button>
                                <div class="dropdown-menu custom-dropdown"> 
                                  <a class="dropdown-item" href="view-notification.html"> <i class="fa fa-eye"></i>VIEW </a> 
                                </div>
                              </div>
                        </td>
                      </tr>
                      <tr>
                        <td>02</td>
                        <td>Lorem Ipsum is simply dummy text of the printing</td>
                        <td>May 2, 2020</td>
                        <td>8:30 PM</td>
                        <td>
                            <div class="btn-group custom-dropdown ml-2 mb-1">
                                <button type="button" class="btn btn-drop-table btn-sm" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"> <i class="fa fa-ellipsis-v"></i></button>
                                <div class="dropdown-menu custom-dropdown"> 
                                  <a class="dropdown-item" href="view-notification.html"> <i class="fa fa-eye"></i>VIEW </a> 
                                </div>
                              </div>
                        </td>
                      </tr>
                      <tr>
                        <td>03</td>
                        <td>Lorem Ipsum is simply dummy text of the printing</td>
                        <td>May 2, 2020</td>
                        <td>8:30 PM</td>
                        <td>
                            <div class="btn-group custom-dropdown ml-2 mb-1">
                                <button type="button" class="btn btn-drop-table btn-sm" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"> <i class="fa fa-ellipsis-v"></i></button>
                                <div class="dropdown-menu custom-dropdown"> 
                                  <a class="dropdown-item" href="view-notification.html"> <i class="fa fa-eye"></i>VIEW </a> 
                                </div>
                              </div>
                        </td>
                      </tr>
                    </tbody>
                  </table>
                </div>   

			
				
                </div>
              </div>
            </div>
          </div>
        </div>
      </section>
    </div>
  </div>
</div>

<?php include('footer.php') ?>