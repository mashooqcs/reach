<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('role_id');
            $table->unsignedBigInteger('manager_id')->default(0);
            $table->string('first_name');
            $table->string('last_name');
            $table->string('email')->unique();
            $table->string('password')->nullable();
            $table->string('profile_image')->nullable();
            $table->string('phone_number')->nullable();
            $table->enum('type', [
                'manager',
                'agent',
            ]);
            $table->string('address')->nullable();
            $table->string('country')->nullable();
            $table->string('state')->nullable();
            $table->string('city')->nullable();
            $table->string('zip_code')->nullable();
            $table->string('fee_enabled')->default(0);

            $table->enum('status', [
                0,
                1,
                2,
                3,
            ])->default(0)->comment = '0=inactive, 1=active, 2=deleted, 3=rejected';
            $table->string('rejection_reason')->nullable();
            $table->rememberToken();
            $table->timestamps();
             
            // Foreign keys
             $table->foreign('role_id')
             ->references('id')
             ->on('roles')
             ->onDelete('restrict');


        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
