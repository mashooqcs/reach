<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePaymentLogsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('payment_logs', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('user_id');
            $table->unsignedBigInteger('payment_id');
            $table->enum('status', [
                0,
                1,
                2,
            ])->default(1)->comment = '0=inactive, 1=active, 2=deleted';;
            $table->timestamps();

            // Foreign keys
            $table->foreign('user_id')
            ->references('id')
            ->on('users')
            ->onDelete('restrict');
             // Foreign keys
             $table->foreign('payment_id')
             ->references('id')
             ->on('actual_payments')
             ->onDelete('restrict');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('payment_logs');
    }
}
