<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use DB;
class Role extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        $data = [
            ['name'=>'admin', 'status'=> '1'],
            ['name'=>'manager', 'status'=> '1'],
            ['name'=>'agent', 'status'=> '1'],
            
        ];
        $add = DB::table('roles')->insert($data);
    }
}
