<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Str;

class Admin extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
      //find role id
     $roles =  DB::table('roles')->where('name', 'admin')->first();
     if(!$roles){
        //create admin role
        $adminDataToAdd['name']     = 'admin';
        $adminDataToAdd['status']   =  '1';

        $roles =DB::table('roles')->create($adminDataToAdd);
     }  
     $adminRoleId = $roles->id;
      DB::table('users')->insert([
            'role_id' => $adminRoleId,
            'first_name' => 'Admin',
            'last_name' => 'Reach',
            'status' => '1',
            'email' => 'adminreach@yopmail.com',
            'password' => Hash::make('admin123'),
        ]);
    }
}
