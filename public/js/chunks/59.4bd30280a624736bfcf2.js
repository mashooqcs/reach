(window["webpackJsonp"] = window["webpackJsonp"] || []).push([[59],{

/***/ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/src/views/Pages/admin/User/Logs.vue?vue&type=script&lang=js&":
/*!*******************************************************************************************************************************************************************************!*\
  !*** ./node_modules/babel-loader/lib??ref--4-0!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/src/views/Pages/admin/User/Logs.vue?vue&type=script&lang=js& ***!
  \*******************************************************************************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @babel/runtime/regenerator */ "./node_modules/@babel/runtime/regenerator/index.js");
/* harmony import */ var _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var vuex__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! vuex */ "./node_modules/vuex/dist/vuex.esm.js");


function ownKeys(object, enumerableOnly) { var keys = Object.keys(object); if (Object.getOwnPropertySymbols) { var symbols = Object.getOwnPropertySymbols(object); if (enumerableOnly) symbols = symbols.filter(function (sym) { return Object.getOwnPropertyDescriptor(object, sym).enumerable; }); keys.push.apply(keys, symbols); } return keys; }

function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; if (i % 2) { ownKeys(Object(source), true).forEach(function (key) { _defineProperty(target, key, source[key]); }); } else if (Object.getOwnPropertyDescriptors) { Object.defineProperties(target, Object.getOwnPropertyDescriptors(source)); } else { ownKeys(Object(source)).forEach(function (key) { Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key)); }); } } return target; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

function asyncGeneratorStep(gen, resolve, reject, _next, _throw, key, arg) { try { var info = gen[key](arg); var value = info.value; } catch (error) { reject(error); return; } if (info.done) { resolve(value); } else { Promise.resolve(value).then(_next, _throw); } }

function _asyncToGenerator(fn) { return function () { var self = this, args = arguments; return new Promise(function (resolve, reject) { var gen = fn.apply(self, args); function _next(value) { asyncGeneratorStep(gen, resolve, reject, _next, _throw, "next", value); } function _throw(err) { asyncGeneratorStep(gen, resolve, reject, _next, _throw, "throw", err); } _next(undefined); }); }; }

//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//

/* harmony default export */ __webpack_exports__["default"] = ({
  created: function created() {
    var _this = this;

    return _asyncToGenerator( /*#__PURE__*/_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.mark(function _callee() {
      return _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.wrap(function _callee$(_context) {
        while (1) {
          switch (_context.prev = _context.next) {
            case 0:
              _context.next = 2;
              return _this.fetch();

            case 2:
            case "end":
              return _context.stop();
          }
        }
      }, _callee);
    }))();
  },
  data: function data() {
    return {
      assignPoperty: 0,
      task: {}
    };
  },
  computed: {// ...mapState('employee', ['task']),
  },
  methods: _objectSpread(_objectSpread({}, Object(vuex__WEBPACK_IMPORTED_MODULE_1__["mapActions"])('employee', ['get', 'store'])), {}, {
    fetch: function fetch() {
      var _this2 = this;

      return _asyncToGenerator( /*#__PURE__*/_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.mark(function _callee2() {
        var user, params, _yield$_this2$get, data;

        return _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.wrap(function _callee2$(_context2) {
          while (1) {
            switch (_context2.prev = _context2.next) {
              case 0:
                user = _this2.$route.params.user;
                params = {
                  route: route('admin.employee.tasks', {
                    employee: user
                  }),
                  data: {}
                };
                _context2.next = 4;
                return _this2.get(params);

              case 4:
                _yield$_this2$get = _context2.sent;
                data = _yield$_this2$get.data;
                _this2.task = data.task;

              case 7:
              case "end":
                return _context2.stop();
            }
          }
        }, _callee2);
      }))();
    }
  })
});

/***/ }),

/***/ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/src/views/Pages/admin/User/Logs.vue?vue&type=template&id=1bc845ae&":
/*!***********************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/src/views/Pages/admin/User/Logs.vue?vue&type=template&id=1bc845ae& ***!
  \***********************************************************************************************************************************************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "render", function() { return render; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return staticRenderFns; });
var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c("div", { staticClass: "content-wrapper" }, [
    _c("div", { staticClass: "content-body" }, [
      _c(
        "section",
        { staticClass: "search view-cause", attrs: { id: "configuration" } },
        [
          _c("div", { staticClass: "row" }, [
            _c("div", { staticClass: "col-12" }, [
              _c("div", { staticClass: "card pad-20" }, [
                _c("div", { staticClass: "card-content collapse show" }, [
                  _c(
                    "div",
                    {
                      staticClass: "card-body table-responsive card-dashboard"
                    },
                    [
                      _c(
                        "router-link",
                        {
                          attrs: {
                            to: {
                              name: "admin.users.index",
                              params: { type: "employee" }
                            }
                          }
                        },
                        [_c("h2", [_vm._v("< USER MANAGEMENT")])]
                      ),
                      _vm._v(" "),
                      _c("section", { staticClass: "assign-task-box" }, [
                        _c("div", {}, [
                          _c("h2", [_vm._v("Employee Assigned Tasks")]),
                          _vm._v(" "),
                          _c("h6", [
                            _vm._v("Emp_Id: E-" + _vm._s(_vm.routeParams.user))
                          ]),
                          _vm._v(" "),
                          _c(
                            "div",
                            {
                              staticClass: "accordion md-accordion",
                              attrs: {
                                id: "accordionEx",
                                role: "tablist",
                                "aria-multiselectable": "true"
                              }
                            },
                            [
                              _c("div", { staticClass: "card" }, [
                                _vm._m(0),
                                _vm._v(" "),
                                _c(
                                  "div",
                                  {
                                    staticClass: "collapse show",
                                    attrs: {
                                      id: "collapseOne1",
                                      role: "tabpanel",
                                      "aria-labelledby": "headingOne1",
                                      "data-parent": "#accordionEx"
                                    }
                                  },
                                  [
                                    _c(
                                      "div",
                                      { staticClass: "card-body" },
                                      [
                                        _vm._l(_vm.task.recentTasks, function(
                                          tasks
                                        ) {
                                          return _c(
                                            "div",
                                            { staticClass: "row" },
                                            [
                                              _c(
                                                "div",
                                                { staticClass: "col-md-6" },
                                                [
                                                  _c("i", {
                                                    staticClass: "fas",
                                                    class: {
                                                      "fa-check-circle":
                                                        tasks.status == 1,
                                                      "fa-times-circle":
                                                        tasks.status == 0
                                                    }
                                                  }),
                                                  _vm._v(" "),
                                                  _c(
                                                    "span",
                                                    {
                                                      staticClass: "gray-text"
                                                    },
                                                    [_vm._v(_vm._s(tasks.name))]
                                                  )
                                                ]
                                              ),
                                              _vm._v(" "),
                                              _c(
                                                "div",
                                                { staticClass: "col-md-4" },
                                                [
                                                  _c("form", [
                                                    _c(
                                                      "div",
                                                      {
                                                        staticClass:
                                                          "form-group"
                                                      },
                                                      [
                                                        _c("p", [
                                                          _vm._v(
                                                            _vm._s(
                                                              tasks.property
                                                                .name
                                                            )
                                                          )
                                                        ])
                                                      ]
                                                    )
                                                  ])
                                                ]
                                              ),
                                              _vm._v(" "),
                                              _c(
                                                "div",
                                                { staticClass: "col-md-2" },
                                                [
                                                  _c(
                                                    "span",
                                                    {
                                                      staticClass: "days-green"
                                                    },
                                                    [
                                                      _vm._v(
                                                        _vm._s(tasks.task_date)
                                                      )
                                                    ]
                                                  )
                                                ]
                                              )
                                            ]
                                          )
                                        }),
                                        _vm._v(" "),
                                        _c(
                                          "no-record",
                                          {
                                            attrs: {
                                              tag: "div",
                                              data: _vm.task.recentTasks
                                            }
                                          },
                                          [
                                            _c(
                                              "div",
                                              { staticClass: "col-m-12" },
                                              [_vm._v("No Data Available")]
                                            )
                                          ]
                                        )
                                      ],
                                      2
                                    )
                                  ]
                                )
                              ]),
                              _vm._v(" "),
                              _c("div", { staticClass: "card" }, [
                                _vm._m(1),
                                _vm._v(" "),
                                _c(
                                  "div",
                                  {
                                    staticClass: "collapse",
                                    attrs: {
                                      id: "collapseTwo2",
                                      role: "tabpanel",
                                      "aria-labelledby": "headingTwo2",
                                      "data-parent": "#accordionEx"
                                    }
                                  },
                                  [
                                    _c(
                                      "div",
                                      { staticClass: "card-body" },
                                      [
                                        _vm._l(_vm.task.todaystasks, function(
                                          tasks
                                        ) {
                                          return _c(
                                            "div",
                                            { staticClass: "row" },
                                            [
                                              _c(
                                                "div",
                                                { staticClass: "col-md-6" },
                                                [
                                                  _c("i", {
                                                    staticClass: "fas",
                                                    class: {
                                                      "fa-check-circle":
                                                        tasks.status == 1,
                                                      "fa-times-circle":
                                                        tasks.status == 0
                                                    }
                                                  }),
                                                  _vm._v(" "),
                                                  _c(
                                                    "span",
                                                    {
                                                      staticClass: "gray-text"
                                                    },
                                                    [_vm._v(_vm._s(tasks.name))]
                                                  )
                                                ]
                                              ),
                                              _vm._v(" "),
                                              _c(
                                                "div",
                                                { staticClass: "col-md-4" },
                                                [
                                                  _c("form", [
                                                    _c(
                                                      "div",
                                                      {
                                                        staticClass:
                                                          "form-group"
                                                      },
                                                      [
                                                        _c("p", [
                                                          _vm._v(
                                                            _vm._s(
                                                              tasks.property
                                                                .name
                                                            )
                                                          )
                                                        ])
                                                      ]
                                                    )
                                                  ])
                                                ]
                                              ),
                                              _vm._v(" "),
                                              _c(
                                                "div",
                                                { staticClass: "col-md-2" },
                                                [
                                                  _c(
                                                    "span",
                                                    {
                                                      staticClass: "days-green"
                                                    },
                                                    [
                                                      _vm._v(
                                                        _vm._s(tasks.task_date)
                                                      )
                                                    ]
                                                  )
                                                ]
                                              )
                                            ]
                                          )
                                        }),
                                        _vm._v(" "),
                                        _c(
                                          "no-record",
                                          {
                                            attrs: {
                                              tag: "div",
                                              data: _vm.task.todaystasks
                                            }
                                          },
                                          [
                                            _c(
                                              "div",
                                              { staticClass: "col-m-12" },
                                              [_vm._v("No Data Available")]
                                            )
                                          ]
                                        )
                                      ],
                                      2
                                    )
                                  ]
                                )
                              ]),
                              _vm._v(" "),
                              _c("div", { staticClass: "card" }, [
                                _vm._m(2),
                                _vm._v(" "),
                                _c(
                                  "div",
                                  {
                                    staticClass: "collapse",
                                    attrs: {
                                      id: "collapseThree3",
                                      role: "tabpanel",
                                      "aria-labelledby": "headingThree3",
                                      "data-parent": "#accordionEx"
                                    }
                                  },
                                  [
                                    _c(
                                      "div",
                                      { staticClass: "card-body" },
                                      [
                                        _vm._l(_vm.task.upcomingTasks, function(
                                          tasks
                                        ) {
                                          return _c(
                                            "div",
                                            { staticClass: "row" },
                                            [
                                              _c(
                                                "div",
                                                { staticClass: "col-md-6" },
                                                [
                                                  _c("i", {
                                                    staticClass: "fas",
                                                    class: {
                                                      "fa-check-circle":
                                                        tasks.status == 1,
                                                      "fa-times-circle":
                                                        tasks.status == 0
                                                    }
                                                  }),
                                                  _vm._v(" "),
                                                  _c(
                                                    "span",
                                                    {
                                                      staticClass: "gray-text"
                                                    },
                                                    [_vm._v(_vm._s(tasks.name))]
                                                  )
                                                ]
                                              ),
                                              _vm._v(" "),
                                              _c(
                                                "div",
                                                { staticClass: "col-md-4" },
                                                [
                                                  _c("form", [
                                                    _c(
                                                      "div",
                                                      {
                                                        staticClass:
                                                          "form-group"
                                                      },
                                                      [
                                                        _c("p", [
                                                          _vm._v(
                                                            _vm._s(
                                                              tasks.property
                                                                .name
                                                            )
                                                          )
                                                        ])
                                                      ]
                                                    )
                                                  ])
                                                ]
                                              ),
                                              _vm._v(" "),
                                              _c(
                                                "div",
                                                { staticClass: "col-md-2" },
                                                [
                                                  _c(
                                                    "span",
                                                    {
                                                      staticClass: "days-green"
                                                    },
                                                    [
                                                      _vm._v(
                                                        _vm._s(tasks.task_date)
                                                      )
                                                    ]
                                                  )
                                                ]
                                              )
                                            ]
                                          )
                                        }),
                                        _vm._v(" "),
                                        _c(
                                          "no-record",
                                          {
                                            attrs: {
                                              tag: "div",
                                              data: _vm.task.upcomingTasks
                                            }
                                          },
                                          [
                                            _c(
                                              "div",
                                              { staticClass: "col-m-12" },
                                              [_vm._v("No Data Available")]
                                            )
                                          ]
                                        )
                                      ],
                                      2
                                    )
                                  ]
                                )
                              ])
                            ]
                          )
                        ])
                      ])
                    ],
                    1
                  )
                ])
              ])
            ])
          ])
        ]
      )
    ])
  ])
}
var staticRenderFns = [
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c(
      "div",
      { staticClass: "card-header", attrs: { role: "tab", id: "headingOne1" } },
      [
        _c(
          "a",
          {
            attrs: {
              "data-toggle": "collapse",
              "data-parent": "#accordionEx",
              href: "#collapseOne1",
              "aria-expanded": "true",
              "aria-controls": "collapseOne1"
            }
          },
          [
            _c("h5", { staticClass: "mb-0" }, [
              _c("i", { staticClass: "fas fa-chevron-circle-down" }),
              _vm._v(
                " Recently\n                                                                    Assigned\n                                                                "
              )
            ])
          ]
        )
      ]
    )
  },
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c(
      "div",
      { staticClass: "card-header", attrs: { role: "tab", id: "headingTwo2" } },
      [
        _c(
          "a",
          {
            staticClass: "collapsed",
            attrs: {
              "data-toggle": "collapse",
              "data-parent": "#accordionEx",
              href: "#collapseTwo2",
              "aria-expanded": "false",
              "aria-controls": "collapseTwo2"
            }
          },
          [
            _c("h5", { staticClass: "mb-0" }, [
              _c("i", { staticClass: "fas fa-chevron-circle-down" }),
              _vm._v(
                " Today\n                                                                "
              )
            ])
          ]
        )
      ]
    )
  },
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c(
      "div",
      {
        staticClass: "card-header",
        attrs: { role: "tab", id: "headingThree3" }
      },
      [
        _c(
          "a",
          {
            staticClass: "collapsed",
            attrs: {
              "data-toggle": "collapse",
              "data-parent": "#accordionEx",
              href: "#collapseThree3",
              "aria-expanded": "false",
              "aria-controls": "collapseThree3"
            }
          },
          [
            _c("h5", { staticClass: "mb-0" }, [
              _c("i", { staticClass: "fas fa-chevron-circle-down" }),
              _vm._v(
                " Upcoming\n                                                                "
              )
            ])
          ]
        )
      ]
    )
  }
]
render._withStripped = true



/***/ }),

/***/ "./resources/js/src/views/Pages/admin/User/Logs.vue":
/*!**********************************************************!*\
  !*** ./resources/js/src/views/Pages/admin/User/Logs.vue ***!
  \**********************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _Logs_vue_vue_type_template_id_1bc845ae___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./Logs.vue?vue&type=template&id=1bc845ae& */ "./resources/js/src/views/Pages/admin/User/Logs.vue?vue&type=template&id=1bc845ae&");
/* harmony import */ var _Logs_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./Logs.vue?vue&type=script&lang=js& */ "./resources/js/src/views/Pages/admin/User/Logs.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport *//* harmony import */ var _node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../../../../../../node_modules/vue-loader/lib/runtime/componentNormalizer.js */ "./node_modules/vue-loader/lib/runtime/componentNormalizer.js");





/* normalize component */

var component = Object(_node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__["default"])(
  _Logs_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__["default"],
  _Logs_vue_vue_type_template_id_1bc845ae___WEBPACK_IMPORTED_MODULE_0__["render"],
  _Logs_vue_vue_type_template_id_1bc845ae___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"],
  false,
  null,
  null,
  null
  
)

/* hot reload */
if (false) { var api; }
component.options.__file = "resources/js/src/views/Pages/admin/User/Logs.vue"
/* harmony default export */ __webpack_exports__["default"] = (component.exports);

/***/ }),

/***/ "./resources/js/src/views/Pages/admin/User/Logs.vue?vue&type=script&lang=js&":
/*!***********************************************************************************!*\
  !*** ./resources/js/src/views/Pages/admin/User/Logs.vue?vue&type=script&lang=js& ***!
  \***********************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_Logs_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../../../../node_modules/babel-loader/lib??ref--4-0!../../../../../../../node_modules/vue-loader/lib??vue-loader-options!./Logs.vue?vue&type=script&lang=js& */ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/src/views/Pages/admin/User/Logs.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport */ /* harmony default export */ __webpack_exports__["default"] = (_node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_Logs_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__["default"]); 

/***/ }),

/***/ "./resources/js/src/views/Pages/admin/User/Logs.vue?vue&type=template&id=1bc845ae&":
/*!*****************************************************************************************!*\
  !*** ./resources/js/src/views/Pages/admin/User/Logs.vue?vue&type=template&id=1bc845ae& ***!
  \*****************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_Logs_vue_vue_type_template_id_1bc845ae___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../../../../node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!../../../../../../../node_modules/vue-loader/lib??vue-loader-options!./Logs.vue?vue&type=template&id=1bc845ae& */ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/src/views/Pages/admin/User/Logs.vue?vue&type=template&id=1bc845ae&");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "render", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_Logs_vue_vue_type_template_id_1bc845ae___WEBPACK_IMPORTED_MODULE_0__["render"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_Logs_vue_vue_type_template_id_1bc845ae___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"]; });



/***/ })

}]);