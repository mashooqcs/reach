(window["webpackJsonp"] = window["webpackJsonp"] || []).push([[10],{

/***/ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/src/views/Pages/web/Task/components/View.vue?vue&type=script&lang=js&":
/*!****************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/babel-loader/lib??ref--4-0!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/src/views/Pages/web/Task/components/View.vue?vue&type=script&lang=js& ***!
  \****************************************************************************************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @babel/runtime/regenerator */ "./node_modules/@babel/runtime/regenerator/index.js");
/* harmony import */ var _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var vuex__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! vuex */ "./node_modules/vuex/dist/vuex.esm.js");


function asyncGeneratorStep(gen, resolve, reject, _next, _throw, key, arg) { try { var info = gen[key](arg); var value = info.value; } catch (error) { reject(error); return; } if (info.done) { resolve(value); } else { Promise.resolve(value).then(_next, _throw); } }

function _asyncToGenerator(fn) { return function () { var self = this, args = arguments; return new Promise(function (resolve, reject) { var gen = fn.apply(self, args); function _next(value) { asyncGeneratorStep(gen, resolve, reject, _next, _throw, "next", value); } function _throw(err) { asyncGeneratorStep(gen, resolve, reject, _next, _throw, "throw", err); } _next(undefined); }); }; }

function ownKeys(object, enumerableOnly) { var keys = Object.keys(object); if (Object.getOwnPropertySymbols) { var symbols = Object.getOwnPropertySymbols(object); if (enumerableOnly) symbols = symbols.filter(function (sym) { return Object.getOwnPropertyDescriptor(object, sym).enumerable; }); keys.push.apply(keys, symbols); } return keys; }

function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; if (i % 2) { ownKeys(Object(source), true).forEach(function (key) { _defineProperty(target, key, source[key]); }); } else if (Object.getOwnPropertyDescriptors) { Object.defineProperties(target, Object.getOwnPropertyDescriptors(source)); } else { ownKeys(Object(source)).forEach(function (key) { Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key)); }); } } return target; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//


var Discrepancy = function Discrepancy() {
  return __webpack_require__.e(/*! import() */ 99).then(__webpack_require__.bind(null, /*! ./Discrepancy.vue */ "./resources/js/src/views/Pages/web/Task/components/Discrepancy.vue"));
};

/* harmony default export */ __webpack_exports__["default"] = ({
  props: {
    task: {
      type: Object,
      // String, Number, Boolean, Function, Object, Array
      required: true,
      "default": null
    }
  },
  data: function data() {
    return {
      discrepancyPreview: 0,
      comment: ''
    };
  },
  components: {
    Discrepancy: Discrepancy
  },
  methods: _objectSpread(_objectSpread({}, Object(vuex__WEBPACK_IMPORTED_MODULE_1__["mapMutations"])('task', ['show_discrepancy_popup'])), {}, {
    updateStatus: function updateStatus() {
      var _this = this;

      return _asyncToGenerator( /*#__PURE__*/_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.mark(function _callee() {
        var status, _yield$axios$post, data;

        return _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.wrap(function _callee$(_context) {
          while (1) {
            switch (_context.prev = _context.next) {
              case 0:
                if (_this.task.status == 0 || _this.task.status == -1) {
                  status = 1;
                } else {
                  status = 0;
                }

                _context.prev = 1;
                _context.next = 4;
                return axios.post(route('task.status', {
                  task: _this.task.id
                }), {
                  status: status
                });

              case 4:
                _yield$axios$post = _context.sent;
                data = _yield$axios$post.data;

                if (data.status) {
                  _this.$snotify.success(data.msg);

                  _this.$emit('status-change', {
                    status: status
                  });
                } // this.log(params);
                // drag = false
                // statements


                _context.next = 12;
                break;

              case 9:
                _context.prev = 9;
                _context.t0 = _context["catch"](1);
                // statements
                console.log(_context.t0);

              case 12:
              case "end":
                return _context.stop();
            }
          }
        }, _callee, null, [[1, 9]]);
      }))();
    },
    discrepancy_reported: function discrepancy_reported() {
      $('#discrepancy-modal').modal('hide');
      this.discrepancyPreview++;
    },
    show_discrepancy_form: function show_discrepancy_form() {
      $('#discrepancy-modal').modal('show');
    },
    sendComment: function sendComment() {
      var _this2 = this;

      return _asyncToGenerator( /*#__PURE__*/_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.mark(function _callee2() {
        var fd, _yield$axios$post2, data;

        return _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.wrap(function _callee2$(_context2) {
          while (1) {
            switch (_context2.prev = _context2.next) {
              case 0:
                fd = new FormData();
                fd.append('comment', _this2.comment);
                fd.append('task', _this2.task.id);
                _context2.next = 5;
                return axios.post(route('task.add-comment', {
                  task: _this2.task.id
                }));

              case 5:
                _yield$axios$post2 = _context2.sent;
                data = _yield$axios$post2.data;

                if (data.status) {
                  _this2.comment = '';

                  _this2.$refs.commentObserver.reset();
                }

              case 8:
              case "end":
                return _context2.stop();
            }
          }
        }, _callee2);
      }))();
    }
  })
});

/***/ }),

/***/ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/src/views/Pages/web/Task/components/View.vue?vue&type=template&id=5286e3b8&":
/*!********************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/src/views/Pages/web/Task/components/View.vue?vue&type=template&id=5286e3b8& ***!
  \********************************************************************************************************************************************************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "render", function() { return render; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return staticRenderFns; });
var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c(
    "div",
    { staticClass: "task-view-modal" },
    [
      _c(
        "div",
        {
          staticClass: "modal fade",
          attrs: {
            id: "view-task",
            tabindex: "-1",
            "aria-labelledby": "exampleModalLabel",
            "aria-hidden": "true"
          }
        },
        [
          _c("div", { staticClass: "modal-dialog modal-dialog-centered" }, [
            _c(
              "div",
              { staticClass: "modal-content" },
              [
                _vm._m(0),
                _vm._v(" "),
                _c("ValidationObserver", {
                  ref: "commentObserver",
                  scopedSlots: _vm._u([
                    {
                      key: "default",
                      fn: function(ref) {
                        var handleSubmit = ref.handleSubmit
                        return [
                          _c(
                            "form",
                            {
                              on: {
                                submit: function($event) {
                                  $event.preventDefault()
                                  return handleSubmit(_vm.sendComment)
                                }
                              }
                            },
                            [
                              _c(
                                "div",
                                { staticClass: "modal-body" },
                                [
                                  _c(
                                    "a",
                                    {
                                      class:
                                        _vm.task.status == 0 ||
                                        _vm.task.status == -1
                                          ? "m-complete"
                                          : "ml-0 board-btn",
                                      attrs: { href: "javascript:void(0)" },
                                      on: { click: _vm.updateStatus }
                                    },
                                    [
                                      _c("i", {
                                        staticClass: "fa ",
                                        class:
                                          _vm.task.status == 0 ||
                                          _vm.task.status == -1
                                            ? "fa-times"
                                            : "fa-check"
                                      }),
                                      _vm._v(
                                        "\n                                    " +
                                          _vm._s(
                                            _vm.task.status == 0 ||
                                              _vm.task.status == -1
                                              ? "Mark Complete"
                                              : "Mark uncomplete"
                                          ) +
                                          "\n                                "
                                      )
                                    ]
                                  ),
                                  _vm._v(" "),
                                  _vm.isEmployee
                                    ? _c(
                                        "a",
                                        {
                                          staticClass: "ml-1 discrepancy-btn",
                                          attrs: {
                                            href: "javascript:void(0)",
                                            "data-toggle": "modal",
                                            "data-target": "#discrepancy-modal"
                                          },
                                          on: {
                                            click: _vm.show_discrepancy_form
                                          }
                                        },
                                        [_vm._v("Fill Discrepancy")]
                                      )
                                    : _vm._e(),
                                  _vm._v(" "),
                                  _c("h6", [_vm._v(_vm._s(_vm.task.name))]),
                                  _vm._v(" "),
                                  _c("div", { staticClass: "row" }, [
                                    _c("div", { staticClass: "col-md-3" }, [
                                      _vm._v("Assignee")
                                    ]),
                                    _vm._v(" "),
                                    _c("div", { staticClass: "col-md-9" }, [
                                      _vm._v(_vm._s(_vm.$user.name))
                                    ])
                                  ]),
                                  _vm._v(" "),
                                  _c("div", { staticClass: "row" }, [
                                    _c("div", { staticClass: "col-md-3" }, [
                                      _vm._v("Due Date")
                                    ]),
                                    _vm._v(" "),
                                    _c("div", { staticClass: "col-md-9" }, [
                                      _vm._v(
                                        _vm._s(
                                          _vm.formatDate(_vm.task.due_date)
                                        )
                                      )
                                    ])
                                  ]),
                                  _vm._v(" "),
                                  _c("div", { staticClass: "row" }, [
                                    _c("div", { staticClass: "col-md-3" }, [
                                      _vm._v("Time:")
                                    ]),
                                    _vm._v(" "),
                                    _c("div", { staticClass: "col-md-9" }, [
                                      _vm._v(
                                        _vm._s(_vm.task.start) +
                                          " To " +
                                          _vm._s(_vm.task.to)
                                      )
                                    ])
                                  ]),
                                  _vm._v(" "),
                                  _vm.task.property
                                    ? _c("div", { staticClass: "row" }, [
                                        _c("div", { staticClass: "col-md-3" }, [
                                          _vm._v("Property Name")
                                        ]),
                                        _vm._v(" "),
                                        _c("div", { staticClass: "col-md-9" }, [
                                          _vm._v(_vm._s(_vm.task.property.name))
                                        ])
                                      ])
                                    : _vm._e(),
                                  _vm._v(" "),
                                  _vm.task.images && _vm.task.images.length > 0
                                    ? _c("div", { staticClass: "row" }, [
                                        _c("div", { staticClass: "col-md-3" }, [
                                          _vm._v("Image:")
                                        ]),
                                        _vm._v(" "),
                                        _c("div", { staticClass: "col-md-9" }, [
                                          _c(
                                            "div",
                                            { staticClass: "d-flex mt-3 mb-3" },
                                            _vm._l(_vm.task.images, function(
                                              image,
                                              index
                                            ) {
                                              return _c(
                                                "div",
                                                {
                                                  staticClass:
                                                    "w-75 p-2 m-2 border border-dark"
                                                },
                                                [
                                                  _c("img", {
                                                    key: index,
                                                    staticClass: "w-100 h-100",
                                                    staticStyle: {
                                                      "object-fit": "cover"
                                                    },
                                                    attrs: { src: image.url }
                                                  })
                                                ]
                                              )
                                            }),
                                            0
                                          )
                                        ])
                                      ])
                                    : _vm._e(),
                                  _vm._v(" "),
                                  _c("h6", [_vm._v("Task Description:")]),
                                  _vm._v(" "),
                                  _c("span", [
                                    _vm._v(_vm._s(_vm.task.description || "NA"))
                                  ]),
                                  _c("br"),
                                  _vm._v(" "),
                                  _c("div", { staticClass: "april-text" }, [
                                    _c("i", {
                                      staticClass: "fas fa-user-circle"
                                    }),
                                    _vm._v(" "),
                                    _c("strong", [
                                      _vm._v("You Created this Task")
                                    ]),
                                    _vm._v(
                                      "  \n                                    "
                                    ),
                                    _c("span", [
                                      _vm._v(
                                        _vm._s(
                                          _vm.formatDate(_vm.task.created_at)
                                        )
                                      )
                                    ])
                                  ]),
                                  _vm._v(" "),
                                  _c("ValidationProvider", {
                                    staticClass: "form-group",
                                    attrs: {
                                      tag: "div",
                                      rules: "required",
                                      name: "Comment"
                                    },
                                    scopedSlots: _vm._u(
                                      [
                                        {
                                          key: "default",
                                          fn: function(ref) {
                                            var errors = ref.errors
                                            return [
                                              _c("textarea", {
                                                directives: [
                                                  {
                                                    name: "model",
                                                    rawName: "v-model",
                                                    value: _vm.comment,
                                                    expression: "comment"
                                                  }
                                                ],
                                                staticClass: "form-control",
                                                attrs: {
                                                  id:
                                                    "exampleFormControlTextarea1",
                                                  rows: "3",
                                                  placeholder:
                                                    "Ask a question or post an update..."
                                                },
                                                domProps: {
                                                  value: _vm.comment
                                                },
                                                on: {
                                                  input: function($event) {
                                                    if (
                                                      $event.target.composing
                                                    ) {
                                                      return
                                                    }
                                                    _vm.comment =
                                                      $event.target.value
                                                  }
                                                }
                                              }),
                                              _vm._v(" "),
                                              _c(
                                                "span",
                                                { staticClass: "text-danger" },
                                                [_vm._v(_vm._s(errors[0]))]
                                              )
                                            ]
                                          }
                                        }
                                      ],
                                      null,
                                      true
                                    )
                                  })
                                ],
                                1
                              ),
                              _vm._v(" "),
                              _c("div", { staticClass: "modal-footer" }, [
                                _c(
                                  "button",
                                  {
                                    staticClass: "view-task-footer-btn",
                                    attrs: { type: "submit" }
                                  },
                                  [_vm._v("Comment")]
                                )
                              ])
                            ]
                          )
                        ]
                      }
                    }
                  ])
                })
              ],
              1
            )
          ])
        ]
      ),
      _vm._v(" "),
      _c("discrepancy", {
        key: _vm.discrepancyPreview,
        attrs: { task: _vm.task },
        on: { reported: _vm.discrepancy_reported }
      })
    ],
    1
  )
}
var staticRenderFns = [
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("div", { staticClass: "modal-header" }, [
      _c(
        "button",
        {
          staticClass: "close",
          attrs: {
            type: "button",
            "data-dismiss": "modal",
            "aria-label": "Close"
          }
        },
        [_c("span", { attrs: { "aria-hidden": "true" } }, [_vm._v("×")])]
      )
    ])
  }
]
render._withStripped = true



/***/ }),

/***/ "./resources/js/src/views/Pages/web/Task/components/View.vue":
/*!*******************************************************************!*\
  !*** ./resources/js/src/views/Pages/web/Task/components/View.vue ***!
  \*******************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _View_vue_vue_type_template_id_5286e3b8___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./View.vue?vue&type=template&id=5286e3b8& */ "./resources/js/src/views/Pages/web/Task/components/View.vue?vue&type=template&id=5286e3b8&");
/* harmony import */ var _View_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./View.vue?vue&type=script&lang=js& */ "./resources/js/src/views/Pages/web/Task/components/View.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport *//* harmony import */ var _node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../../../../../../../node_modules/vue-loader/lib/runtime/componentNormalizer.js */ "./node_modules/vue-loader/lib/runtime/componentNormalizer.js");





/* normalize component */

var component = Object(_node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__["default"])(
  _View_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__["default"],
  _View_vue_vue_type_template_id_5286e3b8___WEBPACK_IMPORTED_MODULE_0__["render"],
  _View_vue_vue_type_template_id_5286e3b8___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"],
  false,
  null,
  null,
  null
  
)

/* hot reload */
if (false) { var api; }
component.options.__file = "resources/js/src/views/Pages/web/Task/components/View.vue"
/* harmony default export */ __webpack_exports__["default"] = (component.exports);

/***/ }),

/***/ "./resources/js/src/views/Pages/web/Task/components/View.vue?vue&type=script&lang=js&":
/*!********************************************************************************************!*\
  !*** ./resources/js/src/views/Pages/web/Task/components/View.vue?vue&type=script&lang=js& ***!
  \********************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_View_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../../../../../node_modules/babel-loader/lib??ref--4-0!../../../../../../../../node_modules/vue-loader/lib??vue-loader-options!./View.vue?vue&type=script&lang=js& */ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/src/views/Pages/web/Task/components/View.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport */ /* harmony default export */ __webpack_exports__["default"] = (_node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_View_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__["default"]); 

/***/ }),

/***/ "./resources/js/src/views/Pages/web/Task/components/View.vue?vue&type=template&id=5286e3b8&":
/*!**************************************************************************************************!*\
  !*** ./resources/js/src/views/Pages/web/Task/components/View.vue?vue&type=template&id=5286e3b8& ***!
  \**************************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_View_vue_vue_type_template_id_5286e3b8___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../../../../../node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!../../../../../../../../node_modules/vue-loader/lib??vue-loader-options!./View.vue?vue&type=template&id=5286e3b8& */ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/src/views/Pages/web/Task/components/View.vue?vue&type=template&id=5286e3b8&");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "render", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_View_vue_vue_type_template_id_5286e3b8___WEBPACK_IMPORTED_MODULE_0__["render"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_View_vue_vue_type_template_id_5286e3b8___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"]; });



/***/ })

}]);