(window["webpackJsonp"] = window["webpackJsonp"] || []).push([[110],{

/***/ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/src/views/Pages/web/Account/components/ApplicationStatus.vue?vue&type=template&id=fe8bff8a&":
/*!************************************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/src/views/Pages/web/Account/components/ApplicationStatus.vue?vue&type=template&id=fe8bff8a& ***!
  \************************************************************************************************************************************************************************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "render", function() { return render; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return staticRenderFns; });
var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c("section", { staticClass: "employee-rejected-box" }, [
    _c("div", { staticClass: "container" }, [
      _c(
        "div",
        { staticClass: "bg" },
        [
          _c("div", { staticClass: "employee-rejected" }, [
            _c("h1", [_vm._v("Application Status")]),
            _vm._v(" "),
            _c("hr"),
            _vm._v(" "),
            _vm.$user.application.status == "applied" ||
            _vm.$user.application.status == "pending"
              ? _c("span", [
                  _vm._v(
                    "Your application is still in pending. You'll soon be contacted!"
                  )
                ])
              : _vm.$user.application.status == "rejected"
              ? _c("span", [
                  _vm._v("We regret you inform you that your application "),
                  _c("br"),
                  _vm._v(" has been Reject")
                ])
              : _vm._e()
          ]),
          _vm._v(" "),
          _vm.$user.application.status == "rejected"
            ? [
                _c("p", [_vm._v("Rejection Reason:")]),
                _vm._v(" "),
                _c("div", { staticClass: "reason-text" }, [
                  _vm._v(_vm._s(_vm.$user.application.reason || "N/A"))
                ]),
                _vm._v(" "),
                _vm._m(0)
              ]
            : _vm._e()
        ],
        2
      )
    ])
  ])
}
var staticRenderFns = [
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("div", { staticClass: "resubmit-box" }, [
      _c("a", { staticClass: "resubmit-btn", attrs: { href: "#" } }, [
        _vm._v("RESUBMIT")
      ])
    ])
  }
]
render._withStripped = true



/***/ }),

/***/ "./resources/js/src/views/Pages/web/Account/components/ApplicationStatus.vue":
/*!***********************************************************************************!*\
  !*** ./resources/js/src/views/Pages/web/Account/components/ApplicationStatus.vue ***!
  \***********************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _ApplicationStatus_vue_vue_type_template_id_fe8bff8a___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./ApplicationStatus.vue?vue&type=template&id=fe8bff8a& */ "./resources/js/src/views/Pages/web/Account/components/ApplicationStatus.vue?vue&type=template&id=fe8bff8a&");
/* harmony import */ var _node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../../../../../../../../node_modules/vue-loader/lib/runtime/componentNormalizer.js */ "./node_modules/vue-loader/lib/runtime/componentNormalizer.js");

var script = {}


/* normalize component */

var component = Object(_node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_1__["default"])(
  script,
  _ApplicationStatus_vue_vue_type_template_id_fe8bff8a___WEBPACK_IMPORTED_MODULE_0__["render"],
  _ApplicationStatus_vue_vue_type_template_id_fe8bff8a___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"],
  false,
  null,
  null,
  null
  
)

/* hot reload */
if (false) { var api; }
component.options.__file = "resources/js/src/views/Pages/web/Account/components/ApplicationStatus.vue"
/* harmony default export */ __webpack_exports__["default"] = (component.exports);

/***/ }),

/***/ "./resources/js/src/views/Pages/web/Account/components/ApplicationStatus.vue?vue&type=template&id=fe8bff8a&":
/*!******************************************************************************************************************!*\
  !*** ./resources/js/src/views/Pages/web/Account/components/ApplicationStatus.vue?vue&type=template&id=fe8bff8a& ***!
  \******************************************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_ApplicationStatus_vue_vue_type_template_id_fe8bff8a___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../../../../../node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!../../../../../../../../node_modules/vue-loader/lib??vue-loader-options!./ApplicationStatus.vue?vue&type=template&id=fe8bff8a& */ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/src/views/Pages/web/Account/components/ApplicationStatus.vue?vue&type=template&id=fe8bff8a&");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "render", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_ApplicationStatus_vue_vue_type_template_id_fe8bff8a___WEBPACK_IMPORTED_MODULE_0__["render"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_ApplicationStatus_vue_vue_type_template_id_fe8bff8a___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"]; });



/***/ })

}]);