(window["webpackJsonp"] = window["webpackJsonp"] || []).push([[76],{

/***/ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/src/views/Pages/web/Employee/components/AssignProperty.vue?vue&type=script&lang=js&":
/*!******************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/babel-loader/lib??ref--4-0!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/src/views/Pages/web/Employee/components/AssignProperty.vue?vue&type=script&lang=js& ***!
  \******************************************************************************************************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @babel/runtime/regenerator */ "./node_modules/@babel/runtime/regenerator/index.js");
/* harmony import */ var _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var vuex__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! vuex */ "./node_modules/vuex/dist/vuex.esm.js");


function asyncGeneratorStep(gen, resolve, reject, _next, _throw, key, arg) { try { var info = gen[key](arg); var value = info.value; } catch (error) { reject(error); return; } if (info.done) { resolve(value); } else { Promise.resolve(value).then(_next, _throw); } }

function _asyncToGenerator(fn) { return function () { var self = this, args = arguments; return new Promise(function (resolve, reject) { var gen = fn.apply(self, args); function _next(value) { asyncGeneratorStep(gen, resolve, reject, _next, _throw, "next", value); } function _throw(err) { asyncGeneratorStep(gen, resolve, reject, _next, _throw, "throw", err); } _next(undefined); }); }; }

function ownKeys(object, enumerableOnly) { var keys = Object.keys(object); if (Object.getOwnPropertySymbols) { var symbols = Object.getOwnPropertySymbols(object); if (enumerableOnly) symbols = symbols.filter(function (sym) { return Object.getOwnPropertyDescriptor(object, sym).enumerable; }); keys.push.apply(keys, symbols); } return keys; }

function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; if (i % 2) { ownKeys(Object(source), true).forEach(function (key) { _defineProperty(target, key, source[key]); }); } else if (Object.getOwnPropertyDescriptors) { Object.defineProperties(target, Object.getOwnPropertyDescriptors(source)); } else { ownKeys(Object(source)).forEach(function (key) { Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key)); }); } } return target; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//


var success = function success() {
  return __webpack_require__.e(/*! import() */ 0).then(__webpack_require__.bind(null, /*! @views/Partials/web/popups/Success.vue */ "./resources/js/src/views/Partials/web/popups/Success.vue"));
};

/* harmony default export */ __webpack_exports__["default"] = ({
  data: function data() {
    return {
      properties: {},
      selectedProperty: 0
    };
  },
  components: {
    success: success
  },
  computed: {// ...mapState('property', ['properties']),
  },
  created: function created() {
    this.fetch();
  },
  methods: _objectSpread(_objectSpread({}, Object(vuex__WEBPACK_IMPORTED_MODULE_1__["mapActions"])('property', ['getAll', 'store'])), {}, {
    fetch: function fetch() {
      var _arguments = arguments,
          _this = this;

      return _asyncToGenerator( /*#__PURE__*/_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.mark(function _callee() {
        var page, employee, params, _yield$_this$getAll, data;

        return _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.wrap(function _callee$(_context) {
          while (1) {
            switch (_context.prev = _context.next) {
              case 0:
                page = _arguments.length > 0 && _arguments[0] !== undefined ? _arguments[0] : 1;
                employee = _this.$route.params.employee;
                params = {
                  route: route('property.get-all-properties', {
                    employee: employee
                  }),
                  data: {
                    page: page
                  }
                };
                _context.next = 5;
                return _this.getAll(params);

              case 5:
                _yield$_this$getAll = _context.sent;
                data = _yield$_this$getAll.data;
                _this.properties = data.properties;

              case 8:
              case "end":
                return _context.stop();
            }
          }
        }, _callee);
      }))();
    },
    assignProperty: function assignProperty() {
      var _this2 = this;

      return _asyncToGenerator( /*#__PURE__*/_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.mark(function _callee2() {
        var employee, fd, params, _yield$_this2$store, data, propertyId, index, property;

        return _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.wrap(function _callee2$(_context2) {
          while (1) {
            switch (_context2.prev = _context2.next) {
              case 0:
                employee = _this2.$route.params.employee;

                if (!(_this2.selectedProperty == 0)) {
                  _context2.next = 4;
                  break;
                }

                _this2.$snotify.error('please select a property first');

                return _context2.abrupt("return");

              case 4:
                fd = new FormData();
                fd.append('property', _this2.selectedProperty);
                params = {
                  route: route('property.assign-property', {
                    employee: employee
                  }),
                  data: fd,
                  method: 'POST'
                };
                _context2.next = 9;
                return _this2.store(params);

              case 9:
                _yield$_this2$store = _context2.sent;
                data = _yield$_this2$store.data;

                if (data.status) {
                  window.$('#successPopup').modal('show');
                  propertyId = parseInt(_this2.selectedProperty);
                  index = _.findIndex(_this2.properties.data, function (property) {
                    return property.id == propertyId;
                  });
                  property = _this2.properties.data[index];

                  _this2.$emit('assigned-property', property);

                  _this2.properties.data.splice(index, 1);
                }

              case 12:
              case "end":
                return _context2.stop();
            }
          }
        }, _callee2);
      }))();
    }
  })
});

/***/ }),

/***/ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/src/views/Pages/web/Employee/components/AssignProperty.vue?vue&type=template&id=55d4728c&":
/*!**********************************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/src/views/Pages/web/Employee/components/AssignProperty.vue?vue&type=template&id=55d4728c& ***!
  \**********************************************************************************************************************************************************************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "render", function() { return render; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return staticRenderFns; });
var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c(
    "div",
    [
      _c(
        "div",
        {
          staticClass: "modal fade",
          attrs: {
            "data-backdrop": "static",
            "data-keyboard": "false",
            id: "assign-property",
            tabindex: "-1",
            role: "dialog",
            "aria-labelledby": "exampleModalCenterTitle",
            "aria-hidden": "true"
          }
        },
        [
          _c(
            "div",
            {
              staticClass: "modal-dialog modal-dialog-centered",
              attrs: { role: "document" }
            },
            [
              _c("div", { staticClass: "modal-content" }, [
                _c("div", { staticClass: "my-modal-header" }, [
                  _c(
                    "button",
                    {
                      staticClass: "close",
                      attrs: {
                        type: "button",
                        "data-dismiss": "modal",
                        "aria-label": "Close"
                      },
                      on: {
                        click: function($event) {
                          return _vm.$emit("closed")
                        }
                      }
                    },
                    [
                      _c("span", { attrs: { "aria-hidden": "true" } }, [
                        _vm._v("×")
                      ])
                    ]
                  ),
                  _vm._v(" "),
                  _c(
                    "h4",
                    {
                      staticClass: "modal-title",
                      attrs: { id: "exampleModalLongTitle" }
                    },
                    [_vm._v("Assign Employee")]
                  ),
                  _vm._v(" "),
                  _vm._m(0)
                ]),
                _vm._v(" "),
                _c("div", { staticClass: "modal-body" }, [
                  _c(
                    "ul",
                    { staticClass: "list-unstyled" },
                    [
                      _vm._l(_vm.properties.data, function(property) {
                        return _c(
                          "li",
                          {
                            staticClass: "media",
                            attrs: { for: "customRadio" + property.id }
                          },
                          [
                            _vm._m(1, true),
                            _vm._v(" "),
                            _c("img", {
                              staticClass: "mr-3",
                              attrs: {
                                src: "assets/images/blank-img.png",
                                alt: "...",
                                width: "14%"
                              }
                            }),
                            _vm._v(" "),
                            _c("div", { staticClass: "media-body" }, [
                              _c("h6", { staticClass: "mt-0 mb-1" }, [
                                _vm._v(_vm._s(property.name))
                              ]),
                              _vm._v(" "),
                              _c("h6", { staticClass: "mt-0 mb-1" }, [
                                _vm._v("Property_ID: P-" + _vm._s(property.id))
                              ]),
                              _vm._v(" "),
                              _c(
                                "div",
                                { staticClass: "custom-control custom-radio" },
                                [
                                  _c("input", {
                                    staticClass: "custom-control-input",
                                    attrs: {
                                      type: "radio",
                                      id: "customRadio" + property.id,
                                      name: "customRadio"
                                    },
                                    domProps: { value: property.id },
                                    on: {
                                      input: function($event) {
                                        _vm.selectedProperty =
                                          $event.target.value
                                      }
                                    }
                                  }),
                                  _vm._v(" "),
                                  _c("label", {
                                    staticClass: "custom-control-label",
                                    attrs: { for: "customRadio" + property.id }
                                  })
                                ]
                              )
                            ])
                          ]
                        )
                      }),
                      _vm._v(" "),
                      typeof _vm.properties.data != "undefined" &&
                      _vm.properties.data.length == 0
                        ? _c("li", { staticClass: "media" }, [
                            _c("h3", [_vm._v("You don't have more Properties")])
                          ])
                        : _vm._e()
                    ],
                    2
                  )
                ]),
                _vm._v(" "),
                _c("div", { staticClass: "modal-footer" }, [
                  _c(
                    "a",
                    {
                      staticClass: "modal-add-btn",
                      attrs: { href: "javascript:void(0)" },
                      on: { click: _vm.assignProperty }
                    },
                    [_vm._v("ASSIGN")]
                  )
                ])
              ])
            ]
          )
        ]
      ),
      _vm._v(" "),
      _c("success", {
        attrs: {
          title: "System Message",
          subtitle: "Property has been Successfully assign to the employee"
        }
      })
    ],
    1
  )
}
var staticRenderFns = [
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c(
      "div",
      { staticClass: "input-group md-form form-sm form-2 pl-0 my-search" },
      [
        _c("input", {
          staticClass: "form-control my-0 py-1 lime-border",
          attrs: { type: "text", placeholder: "Search", "aria-label": "Search" }
        }),
        _vm._v(" "),
        _c("div", { staticClass: "input-group-append" }, [
          _c(
            "span",
            {
              staticClass: "input-group-text lime lighten-2",
              attrs: { id: "basic-text1" }
            },
            [
              _c("i", {
                staticClass: "fas fa-search text-grey",
                attrs: { "aria-hidden": "true" }
              })
            ]
          )
        ])
      ]
    )
  },
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c(
      "a",
      {
        attrs: {
          href: "#",
          "data-toggle": "modal",
          "data-target": "#delete-employee"
        }
      },
      [_c("i", { staticClass: "fas fa-times-circle" })]
    )
  }
]
render._withStripped = true



/***/ }),

/***/ "./resources/js/src/views/Pages/web/Employee/components/AssignProperty.vue":
/*!*********************************************************************************!*\
  !*** ./resources/js/src/views/Pages/web/Employee/components/AssignProperty.vue ***!
  \*********************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _AssignProperty_vue_vue_type_template_id_55d4728c___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./AssignProperty.vue?vue&type=template&id=55d4728c& */ "./resources/js/src/views/Pages/web/Employee/components/AssignProperty.vue?vue&type=template&id=55d4728c&");
/* harmony import */ var _AssignProperty_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./AssignProperty.vue?vue&type=script&lang=js& */ "./resources/js/src/views/Pages/web/Employee/components/AssignProperty.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport *//* harmony import */ var _node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../../../../../../../node_modules/vue-loader/lib/runtime/componentNormalizer.js */ "./node_modules/vue-loader/lib/runtime/componentNormalizer.js");





/* normalize component */

var component = Object(_node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__["default"])(
  _AssignProperty_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__["default"],
  _AssignProperty_vue_vue_type_template_id_55d4728c___WEBPACK_IMPORTED_MODULE_0__["render"],
  _AssignProperty_vue_vue_type_template_id_55d4728c___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"],
  false,
  null,
  null,
  null
  
)

/* hot reload */
if (false) { var api; }
component.options.__file = "resources/js/src/views/Pages/web/Employee/components/AssignProperty.vue"
/* harmony default export */ __webpack_exports__["default"] = (component.exports);

/***/ }),

/***/ "./resources/js/src/views/Pages/web/Employee/components/AssignProperty.vue?vue&type=script&lang=js&":
/*!**********************************************************************************************************!*\
  !*** ./resources/js/src/views/Pages/web/Employee/components/AssignProperty.vue?vue&type=script&lang=js& ***!
  \**********************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_AssignProperty_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../../../../../node_modules/babel-loader/lib??ref--4-0!../../../../../../../../node_modules/vue-loader/lib??vue-loader-options!./AssignProperty.vue?vue&type=script&lang=js& */ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/src/views/Pages/web/Employee/components/AssignProperty.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport */ /* harmony default export */ __webpack_exports__["default"] = (_node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_AssignProperty_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__["default"]); 

/***/ }),

/***/ "./resources/js/src/views/Pages/web/Employee/components/AssignProperty.vue?vue&type=template&id=55d4728c&":
/*!****************************************************************************************************************!*\
  !*** ./resources/js/src/views/Pages/web/Employee/components/AssignProperty.vue?vue&type=template&id=55d4728c& ***!
  \****************************************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_AssignProperty_vue_vue_type_template_id_55d4728c___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../../../../../node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!../../../../../../../../node_modules/vue-loader/lib??vue-loader-options!./AssignProperty.vue?vue&type=template&id=55d4728c& */ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/src/views/Pages/web/Employee/components/AssignProperty.vue?vue&type=template&id=55d4728c&");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "render", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_AssignProperty_vue_vue_type_template_id_55d4728c___WEBPACK_IMPORTED_MODULE_0__["render"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_AssignProperty_vue_vue_type_template_id_55d4728c___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"]; });



/***/ })

}]);