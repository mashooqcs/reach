(window["webpackJsonp"] = window["webpackJsonp"] || []).push([[103],{

/***/ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/src/views/Partials/web/Navbar.vue?vue&type=script&lang=js&":
/*!*****************************************************************************************************************************************************************************!*\
  !*** ./node_modules/babel-loader/lib??ref--4-0!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/src/views/Partials/web/Navbar.vue?vue&type=script&lang=js& ***!
  \*****************************************************************************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
var Links = function Links() {
  return __webpack_require__.e(/*! import() */ 114).then(__webpack_require__.bind(null, /*! @views/Partials/web/navigation/Links.vue */ "./resources/js/src/views/Partials/web/navigation/Links.vue"));
};

var BellNotification = function BellNotification() {
  return __webpack_require__.e(/*! import() */ 104).then(__webpack_require__.bind(null, /*! @views/Partials/web/navigation/BellNotification.vue */ "./resources/js/src/views/Partials/web/navigation/BellNotification.vue"));
};

var LoginLinks = function LoginLinks() {
  return __webpack_require__.e(/*! import() */ 115).then(__webpack_require__.bind(null, /*! @views/Partials/web/navigation/LoginLinks.vue */ "./resources/js/src/views/Partials/web/navigation/LoginLinks.vue"));
};

var ProfileLinks = function ProfileLinks() {
  return __webpack_require__.e(/*! import() */ 105).then(__webpack_require__.bind(null, /*! @views/Partials/web/navigation/ProfileLinks.vue */ "./resources/js/src/views/Partials/web/navigation/ProfileLinks.vue"));
};

/* harmony default export */ __webpack_exports__["default"] = ({
  components: {
    Links: Links,
    BellNotification: BellNotification,
    ProfileLinks: ProfileLinks,
    LoginLinks: LoginLinks
  },
  computed: {
    shouldShown: function shouldShown() {
      if (!this.$user) return false;

      if (this.isEmployee && !this.$user.owner_id) {
        return false;
      } else {
        return true;
      }
    }
  }
});

/***/ }),

/***/ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/src/views/Partials/web/Navbar.vue?vue&type=template&id=7831812e&":
/*!*********************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/src/views/Partials/web/Navbar.vue?vue&type=template&id=7831812e& ***!
  \*********************************************************************************************************************************************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "render", function() { return render; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return staticRenderFns; });
var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c(
    "section",
    {
      class: [{ "main-page-slider-about-us": _vm.$route.name !== "web.home" }]
    },
    [
      _c("div", { staticClass: "for-nav" }, [
        _c("div", { staticClass: "container" }, [
          _c(
            "nav",
            { staticClass: "navbar navbar-expand-lg navbar-light" },
            [
              _c(
                "router-link",
                {
                  staticClass: "navbar-brand",
                  attrs: { to: { name: "web.home" } }
                },
                [
                  _c("img", {
                    attrs: { src: "assets/images/logo.png", alt: "" }
                  })
                ]
              ),
              _vm._v(" "),
              _vm._m(0),
              _vm._v(" "),
              _c(
                "div",
                {
                  staticClass: "collapse navbar-collapse",
                  attrs: { id: "navbarSupportedContent" }
                },
                [
                  _c(
                    "links",
                    [
                      _vm.$user !== null && !_vm.isEmployee
                        ? _c("template", { slot: "owner-links" }, [
                            _c(
                              "li",
                              { staticClass: "nav-item text-center" },
                              [
                                _c(
                                  "router-link",
                                  {
                                    staticClass: "nav-link",
                                    attrs: {
                                      to: { name: "web.properties.index" }
                                    }
                                  },
                                  [
                                    _vm.$route.name != "web.home" && _vm.$user
                                      ? [
                                          _c("i", {
                                            staticClass: "fas fa-building"
                                          }),
                                          _vm._v(" "),
                                          _c("br")
                                        ]
                                      : _vm._e(),
                                    _vm._v(
                                      "\n                                    My Properties"
                                    )
                                  ],
                                  2
                                )
                              ],
                              1
                            ),
                            _vm._v(" "),
                            _c(
                              "li",
                              { staticClass: "nav-item text-center" },
                              [
                                _c(
                                  "router-link",
                                  {
                                    staticClass: "nav-link",
                                    attrs: {
                                      to: { name: "web.employees.index" }
                                    }
                                  },
                                  [
                                    _vm.$route.name != "web.home" && _vm.$user
                                      ? [
                                          _c("i", {
                                            staticClass: "fas fa-users"
                                          }),
                                          _vm._v(" "),
                                          _c("br")
                                        ]
                                      : _vm._e(),
                                    _vm._v(
                                      "\n                                    My Employees"
                                    )
                                  ],
                                  2
                                )
                              ],
                              1
                            ),
                            _vm._v(" "),
                            _c(
                              "li",
                              { staticClass: "nav-item text-center" },
                              [
                                _c(
                                  "router-link",
                                  {
                                    staticClass: "nav-link",
                                    attrs: {
                                      to: {
                                        name: "web.tasks.board",
                                        params: {
                                          type: "calendar"
                                        }
                                      }
                                    }
                                  },
                                  [
                                    _vm.$route.name != "web.home" && _vm.$user
                                      ? [
                                          _c("i", {
                                            staticClass: "fas fa-calendar-alt"
                                          }),
                                          _vm._v(" "),
                                          _c("br")
                                        ]
                                      : _vm._e(),
                                    _vm._v(
                                      "\n                                    Calendar\n                                "
                                    )
                                  ],
                                  2
                                )
                              ],
                              1
                            )
                          ])
                        : _vm._e(),
                      _vm._v(" "),
                      _vm.isEmployee && _vm.$user.owner_id
                        ? [
                            _c(
                              "li",
                              { staticClass: "nav-item" },
                              [
                                _c(
                                  "router-link",
                                  {
                                    staticClass: "nav-link",
                                    attrs: {
                                      to: {
                                        name: "web.tasks.board",
                                        params: {
                                          type: "calendar"
                                        }
                                      }
                                    }
                                  },
                                  [
                                    _c("i", {
                                      staticClass: "fas fa-calendar-alt"
                                    }),
                                    _vm._v(" "),
                                    _vm.$route.name != "web.home" && _vm.$user
                                      ? _c("br")
                                      : _vm._e(),
                                    _vm._v(
                                      "\n                                    Assigned Tasks\n                                "
                                    )
                                  ]
                                )
                              ],
                              1
                            ),
                            _vm._v(" "),
                            _c(
                              "li",
                              { staticClass: "nav-item" },
                              [
                                _c(
                                  "router-link",
                                  {
                                    staticClass: "nav-link",
                                    attrs: { to: { name: "web.tasks.index" } }
                                  },
                                  [
                                    _c("i", {
                                      staticClass: "fas fa-briefcase"
                                    }),
                                    _vm._v(" "),
                                    _vm.$route.name != "web.home" && _vm.$user
                                      ? _c("br")
                                      : _vm._e(),
                                    _vm._v(
                                      "Task Log\n                                "
                                    )
                                  ]
                                )
                              ],
                              1
                            )
                          ]
                        : _vm._e(),
                      _vm._v(" "),
                      _c(
                        "li",
                        { staticClass: "nav-item text-center" },
                        [
                          _c(
                            "router-link",
                            {
                              staticClass: "nav-link text-white",
                              attrs: { to: { name: "web.contact" } }
                            },
                            [
                              _vm.$route.name != "web.home" && _vm.$user
                                ? [
                                    _c("i", {
                                      staticClass: "fas fa-phone-alt"
                                    }),
                                    _vm._v(" "),
                                    _c("br")
                                  ]
                                : _vm._e(),
                              _vm._v(
                                "\n                                Contact us"
                              )
                            ],
                            2
                          )
                        ],
                        1
                      ),
                      _vm._v(" "),
                      _vm.shouldShown ? _c("bell-notification") : _vm._e()
                    ],
                    2
                  ),
                  _vm._v(" "),
                  _c(
                    "div",
                    { staticClass: "left-nav" },
                    [!_vm.$user ? _c("login-links") : _c("profile-links")],
                    1
                  )
                ],
                1
              )
            ],
            1
          )
        ])
      ]),
      _vm._v(" "),
      _vm._t("slider")
    ],
    2
  )
}
var staticRenderFns = [
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c(
      "button",
      {
        staticClass: "navbar-toggler",
        attrs: {
          type: "button",
          "data-toggle": "collapse",
          "data-target": "#navbarSupportedContent",
          "aria-controls": "navbarSupportedContent",
          "aria-expanded": "false",
          "aria-label": "Toggle navigation"
        }
      },
      [_c("span", { staticClass: "navbar-toggler-icon" })]
    )
  }
]
render._withStripped = true



/***/ }),

/***/ "./resources/js/src/views/Partials/web/Navbar.vue":
/*!********************************************************!*\
  !*** ./resources/js/src/views/Partials/web/Navbar.vue ***!
  \********************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _Navbar_vue_vue_type_template_id_7831812e___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./Navbar.vue?vue&type=template&id=7831812e& */ "./resources/js/src/views/Partials/web/Navbar.vue?vue&type=template&id=7831812e&");
/* harmony import */ var _Navbar_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./Navbar.vue?vue&type=script&lang=js& */ "./resources/js/src/views/Partials/web/Navbar.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport *//* harmony import */ var _node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../../../../../node_modules/vue-loader/lib/runtime/componentNormalizer.js */ "./node_modules/vue-loader/lib/runtime/componentNormalizer.js");





/* normalize component */

var component = Object(_node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__["default"])(
  _Navbar_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__["default"],
  _Navbar_vue_vue_type_template_id_7831812e___WEBPACK_IMPORTED_MODULE_0__["render"],
  _Navbar_vue_vue_type_template_id_7831812e___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"],
  false,
  null,
  null,
  null
  
)

/* hot reload */
if (false) { var api; }
component.options.__file = "resources/js/src/views/Partials/web/Navbar.vue"
/* harmony default export */ __webpack_exports__["default"] = (component.exports);

/***/ }),

/***/ "./resources/js/src/views/Partials/web/Navbar.vue?vue&type=script&lang=js&":
/*!*********************************************************************************!*\
  !*** ./resources/js/src/views/Partials/web/Navbar.vue?vue&type=script&lang=js& ***!
  \*********************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_Navbar_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../../../node_modules/babel-loader/lib??ref--4-0!../../../../../../node_modules/vue-loader/lib??vue-loader-options!./Navbar.vue?vue&type=script&lang=js& */ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/src/views/Partials/web/Navbar.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport */ /* harmony default export */ __webpack_exports__["default"] = (_node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_Navbar_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__["default"]); 

/***/ }),

/***/ "./resources/js/src/views/Partials/web/Navbar.vue?vue&type=template&id=7831812e&":
/*!***************************************************************************************!*\
  !*** ./resources/js/src/views/Partials/web/Navbar.vue?vue&type=template&id=7831812e& ***!
  \***************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_Navbar_vue_vue_type_template_id_7831812e___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../../../node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!../../../../../../node_modules/vue-loader/lib??vue-loader-options!./Navbar.vue?vue&type=template&id=7831812e& */ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/src/views/Partials/web/Navbar.vue?vue&type=template&id=7831812e&");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "render", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_Navbar_vue_vue_type_template_id_7831812e___WEBPACK_IMPORTED_MODULE_0__["render"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_Navbar_vue_vue_type_template_id_7831812e___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"]; });



/***/ })

}]);