(window["webpackJsonp"] = window["webpackJsonp"] || []).push([[114],{

/***/ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/src/views/Pages/admin/Agent/components/EmployeeTable.vue?vue&type=script&lang=js&":
/*!****************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/babel-loader/lib??ref--4-0!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/src/views/Pages/admin/Agent/components/EmployeeTable.vue?vue&type=script&lang=js& ***!
  \****************************************************************************************************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @babel/runtime/regenerator */ "./node_modules/@babel/runtime/regenerator/index.js");
/* harmony import */ var _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var vuex__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! vuex */ "./node_modules/vuex/dist/vuex.esm.js");


function asyncGeneratorStep(gen, resolve, reject, _next, _throw, key, arg) { try { var info = gen[key](arg); var value = info.value; } catch (error) { reject(error); return; } if (info.done) { resolve(value); } else { Promise.resolve(value).then(_next, _throw); } }

function _asyncToGenerator(fn) { return function () { var self = this, args = arguments; return new Promise(function (resolve, reject) { var gen = fn.apply(self, args); function _next(value) { asyncGeneratorStep(gen, resolve, reject, _next, _throw, "next", value); } function _throw(err) { asyncGeneratorStep(gen, resolve, reject, _next, _throw, "throw", err); } _next(undefined); }); }; }

function ownKeys(object, enumerableOnly) { var keys = Object.keys(object); if (Object.getOwnPropertySymbols) { var symbols = Object.getOwnPropertySymbols(object); if (enumerableOnly) symbols = symbols.filter(function (sym) { return Object.getOwnPropertyDescriptor(object, sym).enumerable; }); keys.push.apply(keys, symbols); } return keys; }

function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; if (i % 2) { ownKeys(Object(source), true).forEach(function (key) { _defineProperty(target, key, source[key]); }); } else if (Object.getOwnPropertyDescriptors) { Object.defineProperties(target, Object.getOwnPropertyDescriptors(source)); } else { ownKeys(Object(source)).forEach(function (key) { Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key)); }); } } return target; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
var Reason = function Reason() {
  return __webpack_require__.e(/*! import() */ 117).then(__webpack_require__.bind(null, /*! ./Reason.vue */ "./resources/js/src/views/Pages/admin/Agent/components/Reason.vue"));
};

var Confirm = function Confirm() {
  return __webpack_require__.e(/*! import() */ 1).then(__webpack_require__.bind(null, /*! @core/components/Popups/Confirm.vue */ "./resources/js/src/Core/components/Popups/Confirm.vue"));
};

var Associate = function Associate() {
  return __webpack_require__.e(/*! import() */ 113).then(__webpack_require__.bind(null, /*! ./Associate.vue */ "./resources/js/src/views/Pages/admin/Agent/components/Associate.vue"));
};


/* harmony default export */ __webpack_exports__["default"] = ({
  components: {
    Reason: Reason,
    Confirm: Confirm,
    Associate: Associate
  },
  computed: _objectSpread(_objectSpread({}, Object(vuex__WEBPACK_IMPORTED_MODULE_1__["mapState"])('admin', ['users'])), {}, {
    status: function status() {
      return this.$route.params.status;
    }
  }),
  data: function data() {
    return {
      application_id: 0,
      excludeOwner: null,
      rerenderAccociate: 0
    };
  },
  methods: {
    accociate: function accociate() {},
    showSuccess: function showSuccess() {
      $('#successPopup').modal('show');
      this.$emit('update');
    },
    showAssociateSuccess: function showAssociateSuccess() {
      $('#associate-success').modal('show');
      this.$emit('update');
    },
    showOwners: function showOwners(employee, application) {
      var _this = this;

      return _asyncToGenerator( /*#__PURE__*/_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.mark(function _callee() {
        return _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.wrap(function _callee$(_context) {
          while (1) {
            switch (_context.prev = _context.next) {
              case 0:
                _context.next = 2;
                return _this.rerenderAccociate++;

              case 2:
                _this.excludeOwner = employee.owner_id;
                _this.application_id = application;
                setTimeout(function () {
                  $('#associate-owner').modal('show');
                }, 1000);

              case 5:
              case "end":
                return _context.stop();
            }
          }
        }, _callee);
      }))();
    },
    reject: function reject(application) {
      var _this2 = this;

      return _asyncToGenerator( /*#__PURE__*/_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.mark(function _callee2() {
        return _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.wrap(function _callee2$(_context2) {
          while (1) {
            switch (_context2.prev = _context2.next) {
              case 0:
                _this2.application_id = application;
                $('#rejected-modal').modal('show');
                /*let { data } = await axios.post(route('admin.user.application_status', { application, status: 'rejected' }));
                if (data) {
                    this.$snotify.success(data.msg);
                    this.$emit('update');
                }*/

              case 2:
              case "end":
                return _context2.stop();
            }
          }
        }, _callee2);
      }))();
    }
  }
});

/***/ }),

/***/ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/src/views/Pages/admin/Agent/components/EmployeeTable.vue?vue&type=template&id=d7ef65b4&":
/*!********************************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/src/views/Pages/admin/Agent/components/EmployeeTable.vue?vue&type=template&id=d7ef65b4& ***!
  \********************************************************************************************************************************************************************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "render", function() { return render; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return staticRenderFns; });
var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c(
    "div",
    { staticClass: "w-100" },
    [
      _c(
        "table",
        {
          staticClass: "table table-striped table-bordered zero-configuration"
        },
        [
          _c("thead", [
            _c(
              "tr",
              [
                _c("th", [_vm._v("S.No")]),
                _vm._v(" "),
                _c("th", [_vm._v("EMPLOYEE_ID")]),
                _vm._v(" "),
                _c("th", [_vm._v("Full Name")]),
                _vm._v(" "),
                _c("th", [_vm._v("DATE")]),
                _vm._v(" "),
                [
                  _vm.routeParams.status == "application"
                    ? _c("th", [_vm._v("STATUS")])
                    : _c("th", [_vm._v("ADDED BY")])
                ],
                _vm._v(" "),
                _c("th", [_vm._v("ACTIONS")])
              ],
              2
            )
          ]),
          _vm._v(" "),
          _c(
            "tbody",
            [
              _vm._l(_vm.users.data, function(user, index) {
                return _c(
                  "tr",
                  { key: index },
                  [
                    !_vm.status
                      ? [
                          _c("td", [
                            _vm._v(_vm._s(_vm.serialNumber("users", index)))
                          ]),
                          _vm._v(" "),
                          _c("td", [_vm._v(_vm._s(user.id))]),
                          _vm._v(" "),
                          _c("td", [_vm._v(_vm._s(user.name))]),
                          _vm._v(" "),
                          _c("td", [
                            _vm._v(_vm._s(_vm.formatDate(user.created_at)))
                          ]),
                          _vm._v(" "),
                          _c("td", [
                            _vm._v(
                              _vm._s(
                                user.added_by == "proprietor"
                                  ? "Property Owner"
                                  : "Admin"
                              )
                            )
                          ]),
                          _vm._v(" "),
                          _c("td", [
                            _c("div", { staticClass: "btn-group mr-1 mb-1" }, [
                              _vm._m(0, true),
                              _vm._v(" "),
                              _c(
                                "div",
                                {
                                  staticClass:
                                    "dropdown-menu dropdown-menu-right",
                                  staticStyle: {
                                    position: "absolute",
                                    transform: "translate3d(4px, 23px, 0px)",
                                    top: "0px",
                                    left: "0px",
                                    "will-change": "transform"
                                  },
                                  attrs: { "x-placement": "bottom-start" }
                                },
                                [
                                  _c(
                                    "router-link",
                                    {
                                      staticClass: "dropdown-item",
                                      attrs: {
                                        to: {
                                          name: "admin.users.show",
                                          params: {
                                            type: "employee",
                                            user: user.id
                                          }
                                        }
                                      }
                                    },
                                    [
                                      _c("i", { staticClass: "fa fa-eye" }),
                                      _vm._v("VIEW PROFILE")
                                    ]
                                  ),
                                  _vm._v(" "),
                                  _c(
                                    "router-link",
                                    {
                                      staticClass: "dropdown-item",
                                      attrs: {
                                        to: {
                                          name: "admin.users.logs",
                                          params: { user: user.id }
                                        }
                                      }
                                    },
                                    [
                                      _c("i", {
                                        staticClass: "fas fa-bars",
                                        attrs: { "aria-hidden": "true" }
                                      }),
                                      _vm._v("ASSIGNED TASKS")
                                    ]
                                  )
                                ],
                                1
                              )
                            ])
                          ])
                        ]
                      : [
                          _c("td", [
                            _vm._v(_vm._s(_vm.serialNumber("users", index)))
                          ]),
                          _vm._v(" "),
                          user.employee
                            ? _c("td", [_vm._v(_vm._s(user.employee.id))])
                            : _vm._e(),
                          _vm._v(" "),
                          _c("td", [_vm._v(_vm._s(user.employee.name))]),
                          _vm._v(" "),
                          _c("td", [
                            _vm._v(_vm._s(_vm.formatDate(user.created_at)))
                          ]),
                          _vm._v(" "),
                          _c("td", [_vm._v(_vm._s(user.status))]),
                          _vm._v(" "),
                          _c("td", [
                            _c("div", { staticClass: "btn-group mr-1 mb-1" }, [
                              _vm._m(1, true),
                              _vm._v(" "),
                              _c(
                                "div",
                                {
                                  staticClass:
                                    "dropdown-menu dropdown-menu-right",
                                  staticStyle: {
                                    position: "absolute",
                                    transform: "translate3d(4px, 23px, 0px)",
                                    top: "0px",
                                    left: "0px",
                                    "will-change": "transform"
                                  },
                                  attrs: { "x-placement": "bottom-start" }
                                },
                                [
                                  user.status == "applied"
                                    ? _c(
                                        "a",
                                        {
                                          staticClass: "dropdown-item",
                                          attrs: { href: "javascript:void(0)" },
                                          on: {
                                            click: function($event) {
                                              return _vm.showOwners(
                                                user.employee,
                                                user.id
                                              )
                                            }
                                          }
                                        },
                                        [
                                          _c("i", { staticClass: "fa fa-eye" }),
                                          _vm._v("ASSOCIATE")
                                        ]
                                      )
                                    : _vm._e(),
                                  _vm._v(" "),
                                  user.status == "applied"
                                    ? _c(
                                        "a",
                                        {
                                          staticClass: "dropdown-item",
                                          attrs: { href: "javascript:void(0)" },
                                          on: {
                                            click: function($event) {
                                              return _vm.reject(user.id)
                                            }
                                          }
                                        },
                                        [
                                          _c("i", {
                                            staticClass: "fa fa-times"
                                          }),
                                          _vm._v("Reject")
                                        ]
                                      )
                                    : _vm._e(),
                                  _vm._v(" "),
                                  _c(
                                    "router-link",
                                    {
                                      staticClass: "dropdown-item",
                                      attrs: {
                                        to: {
                                          name: "admin.users.show",
                                          params: {
                                            type: "employee",
                                            user: user.id,
                                            status: "application"
                                          }
                                        }
                                      }
                                    },
                                    [
                                      _c("i", { staticClass: "fa fa-eye" }),
                                      _vm._v("VIEW Detail")
                                    ]
                                  )
                                ],
                                1
                              )
                            ])
                          ])
                        ]
                  ],
                  2
                )
              }),
              _vm._v(" "),
              _c("no-record", {
                staticClass: "col-md-3",
                attrs: { tag: "tr", colspan: 6, data: _vm.users.data }
              })
            ],
            2
          )
        ]
      ),
      _vm._v(" "),
      _c("reason", {
        attrs: { application: _vm.application_id },
        on: { update: _vm.showSuccess }
      }),
      _vm._v(" "),
      _vm.rerenderAccociate > 0
        ? _c("associate", {
            key: _vm.rerenderAccociate,
            attrs: {
              "exclude-owner": _vm.excludeOwner,
              application: _vm.application_id
            },
            on: { update: _vm.showAssociateSuccess }
          })
        : _vm._e(),
      _vm._v(" "),
      _c(
        "confirm",
        {
          attrs: {
            title: "System Message",
            subtitle: "Employee application has been Rejected!"
          }
        },
        [
          _c("div", { attrs: { slot: "footer" }, slot: "footer" }, [
            _c(
              "button",
              {
                staticClass: "con",
                attrs: { type: "submit", "data-dismiss": "modal" }
              },
              [_vm._v("CLOSE")]
            )
          ])
        ]
      ),
      _vm._v(" "),
      _c(
        "confirm",
        {
          attrs: {
            title: "System Message",
            el: "associate-success",
            subtitle:
              "Employee has been successfully associated with  Property owner!"
          }
        },
        [
          _c("div", { attrs: { slot: "footer" }, slot: "footer" }, [
            _c(
              "button",
              {
                staticClass: "con",
                attrs: { type: "submit", "data-dismiss": "modal" }
              },
              [_vm._v("CLOSE")]
            )
          ])
        ]
      )
    ],
    1
  )
}
var staticRenderFns = [
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c(
      "button",
      {
        staticClass: "btn  btn-drop-table btn-sm",
        attrs: {
          type: "button",
          "data-toggle": "dropdown",
          "aria-haspopup": "true",
          "aria-expanded": "false"
        }
      },
      [_c("i", { staticClass: "fa fa-ellipsis-v" })]
    )
  },
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c(
      "button",
      {
        staticClass: "btn  btn-drop-table btn-sm",
        attrs: {
          type: "button",
          "data-toggle": "dropdown",
          "aria-haspopup": "true",
          "aria-expanded": "false"
        }
      },
      [_c("i", { staticClass: "fa fa-ellipsis-v" })]
    )
  }
]
render._withStripped = true



/***/ }),

/***/ "./resources/js/src/views/Pages/admin/Agent/components/EmployeeTable.vue":
/*!*******************************************************************************!*\
  !*** ./resources/js/src/views/Pages/admin/Agent/components/EmployeeTable.vue ***!
  \*******************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _EmployeeTable_vue_vue_type_template_id_d7ef65b4___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./EmployeeTable.vue?vue&type=template&id=d7ef65b4& */ "./resources/js/src/views/Pages/admin/Agent/components/EmployeeTable.vue?vue&type=template&id=d7ef65b4&");
/* harmony import */ var _EmployeeTable_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./EmployeeTable.vue?vue&type=script&lang=js& */ "./resources/js/src/views/Pages/admin/Agent/components/EmployeeTable.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport *//* harmony import */ var _node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../../../../../../../node_modules/vue-loader/lib/runtime/componentNormalizer.js */ "./node_modules/vue-loader/lib/runtime/componentNormalizer.js");





/* normalize component */

var component = Object(_node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__["default"])(
  _EmployeeTable_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__["default"],
  _EmployeeTable_vue_vue_type_template_id_d7ef65b4___WEBPACK_IMPORTED_MODULE_0__["render"],
  _EmployeeTable_vue_vue_type_template_id_d7ef65b4___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"],
  false,
  null,
  null,
  null
  
)

/* hot reload */
if (false) { var api; }
component.options.__file = "resources/js/src/views/Pages/admin/Agent/components/EmployeeTable.vue"
/* harmony default export */ __webpack_exports__["default"] = (component.exports);

/***/ }),

/***/ "./resources/js/src/views/Pages/admin/Agent/components/EmployeeTable.vue?vue&type=script&lang=js&":
/*!********************************************************************************************************!*\
  !*** ./resources/js/src/views/Pages/admin/Agent/components/EmployeeTable.vue?vue&type=script&lang=js& ***!
  \********************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_EmployeeTable_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../../../../../node_modules/babel-loader/lib??ref--4-0!../../../../../../../../node_modules/vue-loader/lib??vue-loader-options!./EmployeeTable.vue?vue&type=script&lang=js& */ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/src/views/Pages/admin/Agent/components/EmployeeTable.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport */ /* harmony default export */ __webpack_exports__["default"] = (_node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_EmployeeTable_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__["default"]); 

/***/ }),

/***/ "./resources/js/src/views/Pages/admin/Agent/components/EmployeeTable.vue?vue&type=template&id=d7ef65b4&":
/*!**************************************************************************************************************!*\
  !*** ./resources/js/src/views/Pages/admin/Agent/components/EmployeeTable.vue?vue&type=template&id=d7ef65b4& ***!
  \**************************************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_EmployeeTable_vue_vue_type_template_id_d7ef65b4___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../../../../../node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!../../../../../../../../node_modules/vue-loader/lib??vue-loader-options!./EmployeeTable.vue?vue&type=template&id=d7ef65b4& */ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/src/views/Pages/admin/Agent/components/EmployeeTable.vue?vue&type=template&id=d7ef65b4&");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "render", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_EmployeeTable_vue_vue_type_template_id_d7ef65b4___WEBPACK_IMPORTED_MODULE_0__["render"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_EmployeeTable_vue_vue_type_template_id_d7ef65b4___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"]; });



/***/ })

}]);