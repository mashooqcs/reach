(window["webpackJsonp"] = window["webpackJsonp"] || []).push([[112],{

/***/ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/src/views/Pages/admin/Agent/Index.vue?vue&type=script&lang=js&":
/*!*********************************************************************************************************************************************************************************!*\
  !*** ./node_modules/babel-loader/lib??ref--4-0!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/src/views/Pages/admin/Agent/Index.vue?vue&type=script&lang=js& ***!
  \*********************************************************************************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @babel/runtime/regenerator */ "./node_modules/@babel/runtime/regenerator/index.js");
/* harmony import */ var _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var vuex__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! vuex */ "./node_modules/vuex/dist/vuex.esm.js");


function asyncGeneratorStep(gen, resolve, reject, _next, _throw, key, arg) { try { var info = gen[key](arg); var value = info.value; } catch (error) { reject(error); return; } if (info.done) { resolve(value); } else { Promise.resolve(value).then(_next, _throw); } }

function _asyncToGenerator(fn) { return function () { var self = this, args = arguments; return new Promise(function (resolve, reject) { var gen = fn.apply(self, args); function _next(value) { asyncGeneratorStep(gen, resolve, reject, _next, _throw, "next", value); } function _throw(err) { asyncGeneratorStep(gen, resolve, reject, _next, _throw, "throw", err); } _next(undefined); }); }; }

function ownKeys(object, enumerableOnly) { var keys = Object.keys(object); if (Object.getOwnPropertySymbols) { var symbols = Object.getOwnPropertySymbols(object); if (enumerableOnly) symbols = symbols.filter(function (sym) { return Object.getOwnPropertyDescriptor(object, sym).enumerable; }); keys.push.apply(keys, symbols); } return keys; }

function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; if (i % 2) { ownKeys(Object(source), true).forEach(function (key) { _defineProperty(target, key, source[key]); }); } else if (Object.getOwnPropertyDescriptors) { Object.defineProperties(target, Object.getOwnPropertyDescriptors(source)); } else { ownKeys(Object(source)).forEach(function (key) { Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key)); }); } } return target; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//


var EmployeeTable = function EmployeeTable() {
  return __webpack_require__.e(/*! import() */ 114).then(__webpack_require__.bind(null, /*! ./components/EmployeeTable.vue */ "./resources/js/src/views/Pages/admin/Agent/components/EmployeeTable.vue"));
};

var OwnerTable = function OwnerTable() {
  return __webpack_require__.e(/*! import() */ 115).then(__webpack_require__.bind(null, /*! ./components/OwnerTable.vue */ "./resources/js/src/views/Pages/admin/Agent/components/OwnerTable.vue"));
};

/* harmony default export */ __webpack_exports__["default"] = ({
  computed: _objectSpread({}, Object(vuex__WEBPACK_IMPORTED_MODULE_1__["mapState"])('admin', ['users', 'search', 'entries'])),
  components: {
    EmployeeTable: EmployeeTable,
    OwnerTable: OwnerTable
  },
  created: function created() {
    var page = this.$route.query.page;
    this.fetch(page);
  },
  watch: {
    search: function search(val, oldVal) {
      this.fetch();
    },
    entries: function entries(val, oldVal) {
      this.fetch();
    }
  },
  methods: _objectSpread(_objectSpread({}, Object(vuex__WEBPACK_IMPORTED_MODULE_1__["mapActions"])('admin', ['getAll'])), {}, {
    fetch: function fetch() {
      var _arguments = arguments,
          _this = this;

      return _asyncToGenerator( /*#__PURE__*/_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.mark(function _callee() {
        var page, _this$$route$params, type, status, params, _yield$_this$getAll, data;

        return _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.wrap(function _callee$(_context) {
          while (1) {
            switch (_context.prev = _context.next) {
              case 0:
                page = _arguments.length > 0 && _arguments[0] !== undefined ? _arguments[0] : 1;
                _this$$route$params = _this.$route.params, type = _this$$route$params.type, status = _this$$route$params.status;
                params = {
                  route: route('admin.user.index'),
                  mutation: 'SET_USERS',
                  variable: 'users',
                  data: {
                    page: page,
                    type: type,
                    status: status,
                    search: _this.search,
                    entries: _this.entries
                  }
                };
                _context.next = 5;
                return _this.getAll(params);

              case 5:
                _yield$_this$getAll = _context.sent;
                data = _yield$_this$getAll.data;

                if (page != 1) {
                  _this.addRouteQuery({
                    page: page
                  });
                } else {
                  _this.addRouteQuery({});
                }

              case 8:
              case "end":
                return _context.stop();
            }
          }
        }, _callee);
      }))();
    }
  })
});

/***/ }),

/***/ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/src/views/Pages/admin/Agent/Index.vue?vue&type=template&id=1f28a203&":
/*!*************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/src/views/Pages/admin/Agent/Index.vue?vue&type=template&id=1f28a203& ***!
  \*************************************************************************************************************************************************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "render", function() { return render; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return staticRenderFns; });
var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c("div", { staticClass: "app-content content dashboard" }, [
    _c("div", { staticClass: "content-wrapper" }, [
      _c("div", { staticClass: "content-body" }, [
        _c("section", { attrs: { id: "configuration" } }, [
          _c("div", { staticClass: "row" }, [
            _c("div", { staticClass: "col-12" }, [
              _c("div", { staticClass: "card" }, [
                _c("div", { staticClass: "card-content collapse show" }, [
                  _c("div", { staticClass: "card-dashboard" }, [
                    _vm._m(0),
                    _vm._v(" "),
                    _vm._m(1),
                    _vm._v(" "),
                    _c(
                      "div",
                      {
                        staticClass: "tab-content",
                        attrs: { id: "pills-tabContent" }
                      },
                      [
                        _c(
                          "div",
                          {
                            staticClass: "tab-pane fade show active",
                            attrs: {
                              id: "pills-home",
                              role: "tabpanel",
                              "aria-labelledby": "pills-home-tab"
                            }
                          },
                          [
                            _vm._m(2),
                            _vm._v(" "),
                            _c("div", { staticClass: "clearfix" }),
                            _vm._v(" "),
                            _c(
                              "div",
                              { staticClass: "maain-tabble table-responsive" },
                              [
                                _c(
                                  "table",
                                  {
                                    staticClass:
                                      "table table-striped table-bordered zero-configuration"
                                  },
                                  [
                                    _vm._m(3),
                                    _vm._v(" "),
                                    _c(
                                      "tbody",
                                      [
                                        _vm._l(_vm.users.data, function(
                                          user,
                                          index
                                        ) {
                                          return _c("tr", [
                                            _c("td", [
                                              _vm._v(
                                                _vm._s(
                                                  _vm.serialNumber(
                                                    "feedbacks",
                                                    index
                                                  )
                                                )
                                              )
                                            ]),
                                            _vm._v(" "),
                                            _c("td", [_vm._v("001")]),
                                            _vm._v(" "),
                                            _c("td", [_vm._v("Abc")]),
                                            _vm._v(" "),
                                            _c("td", [_vm._v("May 2 ,2020")]),
                                            _vm._v(" "),
                                            _c("td", [_vm._v("$123")]),
                                            _vm._v(" "),
                                            _c("td", [_vm._v("002")]),
                                            _vm._v(" "),
                                            _c("td", [_vm._v("Abc")]),
                                            _vm._v(" "),
                                            _vm._m(4, true)
                                          ])
                                        }),
                                        _vm._v(" "),
                                        _vm._m(5),
                                        _vm._v(" "),
                                        _vm._m(6)
                                      ],
                                      2
                                    )
                                  ]
                                )
                              ]
                            )
                          ]
                        ),
                        _vm._v(" "),
                        _vm._m(7)
                      ]
                    )
                  ])
                ])
              ])
            ])
          ])
        ])
      ])
    ])
  ])
}
var staticRenderFns = [
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("div", { staticClass: "row" }, [
      _c("div", { staticClass: "col-12" }, [
        _c("h1", { staticClass: "mb-2" }, [_vm._v("agents")])
      ])
    ])
  },
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c(
      "ul",
      {
        staticClass: "nav nav-pills agent-pills",
        attrs: { id: "pills-tab", role: "tablist" }
      },
      [
        _c("li", { staticClass: "nav-item" }, [
          _c(
            "a",
            {
              staticClass: "nav-link active",
              attrs: {
                id: "pills-home-tab",
                "data-toggle": "pill",
                href: "#pills-home",
                role: "tab",
                "aria-controls": "pills-home",
                "aria-selected": "true"
              }
            },
            [_vm._v("Manager's Agents")]
          )
        ]),
        _vm._v(" "),
        _c("li", { staticClass: "nav-item" }, [
          _c(
            "a",
            {
              staticClass: "nav-link",
              attrs: {
                id: "pills-profile-tab",
                "data-toggle": "pill",
                href: "#pills-profile",
                role: "tab",
                "aria-controls": "pills-profile",
                "aria-selected": "false"
              }
            },
            [_vm._v("My Agents")]
          )
        ])
      ]
    )
  },
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("div", { staticClass: "text-md-right text-left mb-2" }, [
      _c(
        "a",
        {
          staticClass: "site-btn lg-btn blue",
          attrs: { href: "inactive-agents.php" }
        },
        [_vm._v("INACTIVE AGENTS")]
      ),
      _vm._v(" "),
      _c(
        "a",
        {
          staticClass: "site-btn lg-btn orange text-uppercase ml-2",
          attrs: { href: "agents-register.php" }
        },
        [_vm._v("register AGENTS")]
      )
    ])
  },
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("thead", [
      _c("tr", [
        _c("th", [_vm._v("S.NO")]),
        _vm._v(" "),
        _c("th", [_vm._v("AGENT ID")]),
        _vm._v(" "),
        _c("th", [_vm._v("FULL NAME")]),
        _vm._v(" "),
        _c("th", [_vm._v("REGISTRATION DATE")]),
        _vm._v(" "),
        _c("th", [_vm._v("FEE")]),
        _vm._v(" "),
        _c("th", [_vm._v("MANAGER ID")]),
        _vm._v(" "),
        _c("th", [_vm._v("FULL NAME")]),
        _vm._v(" "),
        _c("th", [_vm._v("ACTIONS")])
      ])
    ])
  },
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("td", [
      _c("div", { staticClass: "btn-group custom-dropdown ml-2 mb-1" }, [
        _c(
          "button",
          {
            staticClass: "btn btn-drop-table btn-sm",
            attrs: {
              type: "button",
              "data-toggle": "dropdown",
              "aria-haspopup": "true",
              "aria-expanded": "false"
            }
          },
          [_c("i", { staticClass: "fa fa-ellipsis-v" })]
        ),
        _vm._v(" "),
        _c("div", { staticClass: "dropdown-menu custom-dropdown" }, [
          _c(
            "a",
            {
              staticClass: "dropdown-item",
              attrs: {
                href: "payment-detail.php",
                "data-toggle": "modal",
                "data-target": ".inactiveAgent"
              }
            },
            [_c("i", { staticClass: "fa fa-eye-slash" }), _vm._v("INACTIVE ")]
          ),
          _vm._v(" "),
          _c(
            "a",
            {
              staticClass: "dropdown-item",
              attrs: { "data-toggle": "modal", "data-target": ".assignTask" }
            },
            [_c("i", { staticClass: "fas fa-edit" }), _vm._v("ASSIGN NEW TASK")]
          ),
          _vm._v(" "),
          _c(
            "a",
            {
              staticClass: "dropdown-item",
              attrs: { href: "agent-profile.php" }
            },
            [
              _c("i", {
                staticClass: "fa fa-user-circle",
                attrs: { "aria-hidden": "true" }
              }),
              _vm._v("View Profile")
            ]
          ),
          _vm._v(" "),
          _c(
            "a",
            {
              staticClass: "dropdown-item",
              attrs: { href: "view-assigned-activities.php" }
            },
            [
              _c("i", { staticClass: "fas fa-clipboard-list" }),
              _vm._v("View Assigned Activities")
            ]
          ),
          _vm._v(" "),
          _c(
            "a",
            {
              staticClass: "dropdown-item",
              attrs: { href: "monthly-progress.php" }
            },
            [
              _c("i", { staticClass: "fas fa-chart-line" }),
              _vm._v("View Monthly Progress")
            ]
          ),
          _vm._v(" "),
          _c(
            "a",
            {
              staticClass: "dropdown-item",
              attrs: { href: "discussion-board.php" }
            },
            [
              _c("i", { staticClass: "fas fa-comment" }),
              _vm._v("View Discussion Board")
            ]
          )
        ])
      ])
    ])
  },
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("tr", [
      _c("td", [_vm._v("02")]),
      _vm._v(" "),
      _c("td", [_vm._v("001")]),
      _vm._v(" "),
      _c("td", [_vm._v("Abc")]),
      _vm._v(" "),
      _c("td", [_vm._v("May 2 ,2020")]),
      _vm._v(" "),
      _c("td", [_vm._v("$123")]),
      _vm._v(" "),
      _c("td", [_vm._v("002")]),
      _vm._v(" "),
      _c("td", [_vm._v("Abc")]),
      _vm._v(" "),
      _c("td", [
        _c("div", { staticClass: "btn-group custom-dropdown ml-2 mb-1" }, [
          _c(
            "button",
            {
              staticClass: "btn btn-drop-table btn-sm",
              attrs: {
                type: "button",
                "data-toggle": "dropdown",
                "aria-haspopup": "true",
                "aria-expanded": "false"
              }
            },
            [_c("i", { staticClass: "fa fa-ellipsis-v" })]
          ),
          _vm._v(" "),
          _c("div", { staticClass: "dropdown-menu custom-dropdown" }, [
            _c(
              "a",
              {
                staticClass: "dropdown-item",
                attrs: {
                  href: "payment-detail.php",
                  "data-toggle": "modal",
                  "data-target": ".inactiveAgent"
                }
              },
              [_c("i", { staticClass: "fa fa-eye-slash" }), _vm._v("INACTIVE ")]
            ),
            _vm._v(" "),
            _c(
              "a",
              {
                staticClass: "dropdown-item",
                attrs: { "data-toggle": "modal", "data-target": ".assignTask" }
              },
              [
                _c("i", { staticClass: "fas fa-edit" }),
                _vm._v("ASSIGN NEW TASK")
              ]
            ),
            _vm._v(" "),
            _c(
              "a",
              {
                staticClass: "dropdown-item",
                attrs: { href: "agent-profile.php" }
              },
              [
                _c("i", {
                  staticClass: "fa fa-user-circle",
                  attrs: { "aria-hidden": "true" }
                }),
                _vm._v("View Profile")
              ]
            ),
            _vm._v(" "),
            _c(
              "a",
              {
                staticClass: "dropdown-item",
                attrs: { href: "view-assigned-activities.php" }
              },
              [
                _c("i", { staticClass: "fas fa-clipboard-list" }),
                _vm._v("View Assigned Activities")
              ]
            ),
            _vm._v(" "),
            _c(
              "a",
              {
                staticClass: "dropdown-item",
                attrs: { href: "monthly-progress.php" }
              },
              [
                _c("i", { staticClass: "fas fa-chart-line" }),
                _vm._v("View Monthly Progress")
              ]
            ),
            _vm._v(" "),
            _c(
              "a",
              {
                staticClass: "dropdown-item",
                attrs: { href: "discussion-board.php" }
              },
              [
                _c("i", { staticClass: "fas fa-comment" }),
                _vm._v("View Discussion Board")
              ]
            )
          ])
        ])
      ])
    ])
  },
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("tr", [
      _c("td", [_vm._v("03")]),
      _vm._v(" "),
      _c("td", [_vm._v("001")]),
      _vm._v(" "),
      _c("td", [_vm._v("Abc")]),
      _vm._v(" "),
      _c("td", [_vm._v("May 2 ,2020")]),
      _vm._v(" "),
      _c("td", [_vm._v("$123")]),
      _vm._v(" "),
      _c("td", [_vm._v("002")]),
      _vm._v(" "),
      _c("td", [_vm._v("Abc")]),
      _vm._v(" "),
      _c("td", [
        _c("div", { staticClass: "btn-group custom-dropdown ml-2 mb-1" }, [
          _c(
            "button",
            {
              staticClass: "btn btn-drop-table btn-sm",
              attrs: {
                type: "button",
                "data-toggle": "dropdown",
                "aria-haspopup": "true",
                "aria-expanded": "false"
              }
            },
            [_c("i", { staticClass: "fa fa-ellipsis-v" })]
          ),
          _vm._v(" "),
          _c("div", { staticClass: "dropdown-menu custom-dropdown" }, [
            _c(
              "a",
              {
                staticClass: "dropdown-item",
                attrs: {
                  href: "payment-detail.php",
                  "data-toggle": "modal",
                  "data-target": ".inactiveAgent"
                }
              },
              [_c("i", { staticClass: "fa fa-eye-slash" }), _vm._v("INACTIVE ")]
            ),
            _vm._v(" "),
            _c(
              "a",
              {
                staticClass: "dropdown-item",
                attrs: { "data-toggle": "modal", "data-target": ".assignTask" }
              },
              [
                _c("i", { staticClass: "fas fa-edit" }),
                _vm._v("ASSIGN NEW TASK")
              ]
            ),
            _vm._v(" "),
            _c(
              "a",
              {
                staticClass: "dropdown-item",
                attrs: { href: "agent-profile.php" }
              },
              [
                _c("i", {
                  staticClass: "fa fa-user-circle",
                  attrs: { "aria-hidden": "true" }
                }),
                _vm._v("View Profile")
              ]
            ),
            _vm._v(" "),
            _c(
              "a",
              {
                staticClass: "dropdown-item",
                attrs: { href: "view-assigned-activities.php" }
              },
              [
                _c("i", { staticClass: "fas fa-clipboard-list" }),
                _vm._v("View Assigned Activities")
              ]
            ),
            _vm._v(" "),
            _c(
              "a",
              {
                staticClass: "dropdown-item",
                attrs: { href: "monthly-progress.php" }
              },
              [
                _c("i", { staticClass: "fas fa-chart-line" }),
                _vm._v("View Monthly Progress")
              ]
            ),
            _vm._v(" "),
            _c(
              "a",
              {
                staticClass: "dropdown-item",
                attrs: { href: "discussion-board.php" }
              },
              [
                _c("i", { staticClass: "fas fa-comment" }),
                _vm._v("View Discussion Board")
              ]
            )
          ])
        ])
      ])
    ])
  },
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c(
      "div",
      {
        staticClass: "tab-pane fade",
        attrs: {
          id: "pills-profile",
          role: "tabpanel",
          "aria-labelledby": "pills-profile-tab"
        }
      },
      [
        _c("div", { staticClass: "text-md-right text-left mb-2" }, [
          _c(
            "a",
            {
              staticClass: "site-btn lg-btn blue",
              attrs: { href: "inactive-agents.php" }
            },
            [_vm._v("INACTIVE AGENTS")]
          ),
          _vm._v(" "),
          _c(
            "a",
            {
              staticClass: "site-btn lg-btn orange text-uppercase ml-2",
              attrs: { href: "agents-register.php" }
            },
            [_vm._v("register AGENTS")]
          )
        ]),
        _vm._v(" "),
        _c("div", { staticClass: "clearfix" }),
        _vm._v(" "),
        _c("div", { staticClass: "maain-tabble table-responsive" }, [
          _c(
            "table",
            {
              staticClass:
                "table table-striped table-bordered zero-configuration"
            },
            [
              _c("thead", [
                _c("tr", [
                  _c("th", [_vm._v("S.NO")]),
                  _vm._v(" "),
                  _c("th", [_vm._v("AGENT ID")]),
                  _vm._v(" "),
                  _c("th", [_vm._v("FULL NAME")]),
                  _vm._v(" "),
                  _c("th", [_vm._v("REGISTRATION DATE")]),
                  _vm._v(" "),
                  _c("th", [_vm._v("FULL NAME")]),
                  _vm._v(" "),
                  _c("th", [_vm._v("ACTIONS")])
                ])
              ]),
              _vm._v(" "),
              _c("tbody", [
                _c("tr", [
                  _c("td", [_vm._v("01")]),
                  _vm._v(" "),
                  _c("td", [_vm._v("001")]),
                  _vm._v(" "),
                  _c("td", [_vm._v("Abc")]),
                  _vm._v(" "),
                  _c("td", [_vm._v("May 2 ,2020")]),
                  _vm._v(" "),
                  _c("td", [_vm._v("Abce")]),
                  _vm._v(" "),
                  _c("td", [
                    _c(
                      "div",
                      { staticClass: "btn-group custom-dropdown ml-2 mb-1" },
                      [
                        _c(
                          "button",
                          {
                            staticClass: "btn btn-drop-table btn-sm",
                            attrs: {
                              type: "button",
                              "data-toggle": "dropdown",
                              "aria-haspopup": "true",
                              "aria-expanded": "false"
                            }
                          },
                          [_c("i", { staticClass: "fa fa-ellipsis-v" })]
                        ),
                        _vm._v(" "),
                        _c(
                          "div",
                          { staticClass: "dropdown-menu custom-dropdown" },
                          [
                            _c(
                              "a",
                              {
                                staticClass: "dropdown-item",
                                attrs: {
                                  href: "payment-detail.php",
                                  "data-toggle": "modal",
                                  "data-target": ".inactiveAgent"
                                }
                              },
                              [
                                _c("i", { staticClass: "fa fa-eye-slash" }),
                                _vm._v("ACTIVE / INACTIVE ")
                              ]
                            ),
                            _vm._v(" "),
                            _c(
                              "a",
                              {
                                staticClass: "dropdown-item",
                                attrs: {
                                  "data-toggle": "modal",
                                  "data-target": ".assignTask"
                                }
                              },
                              [
                                _c("i", { staticClass: "fas fa-edit" }),
                                _vm._v("ASSIGN NEW TASK")
                              ]
                            ),
                            _vm._v(" "),
                            _c(
                              "a",
                              {
                                staticClass: "dropdown-item",
                                attrs: { href: "agent-profile2.php" }
                              },
                              [
                                _c("i", {
                                  staticClass: "fa fa-user-circle",
                                  attrs: { "aria-hidden": "true" }
                                }),
                                _vm._v("View Profile")
                              ]
                            ),
                            _vm._v(" "),
                            _c(
                              "a",
                              {
                                staticClass: "dropdown-item",
                                attrs: { href: "view-assigned-activities.php" }
                              },
                              [
                                _c("i", {
                                  staticClass: "fas fa-clipboard-list"
                                }),
                                _vm._v("View Assigned tasks")
                              ]
                            ),
                            _vm._v(" "),
                            _c(
                              "a",
                              {
                                staticClass: "dropdown-item",
                                attrs: { href: "payment-detail.php" }
                              },
                              [
                                _c("i", { staticClass: "fas fa-chart-line" }),
                                _vm._v("View Monthly Progress")
                              ]
                            ),
                            _vm._v(" "),
                            _c(
                              "a",
                              {
                                staticClass: "dropdown-item",
                                attrs: { href: "discussion-board.php" }
                              },
                              [
                                _c("i", { staticClass: "fas fa-comment" }),
                                _vm._v("View Discussion Board")
                              ]
                            )
                          ]
                        )
                      ]
                    )
                  ])
                ]),
                _vm._v(" "),
                _c("tr", [
                  _c("td", [_vm._v("02")]),
                  _vm._v(" "),
                  _c("td", [_vm._v("001")]),
                  _vm._v(" "),
                  _c("td", [_vm._v("Abc")]),
                  _vm._v(" "),
                  _c("td", [_vm._v("May 2 ,2020")]),
                  _vm._v(" "),
                  _c("td", [_vm._v("Abce")]),
                  _vm._v(" "),
                  _c("td", [
                    _c(
                      "div",
                      { staticClass: "btn-group custom-dropdown ml-2 mb-1" },
                      [
                        _c(
                          "button",
                          {
                            staticClass: "btn btn-drop-table btn-sm",
                            attrs: {
                              type: "button",
                              "data-toggle": "dropdown",
                              "aria-haspopup": "true",
                              "aria-expanded": "false"
                            }
                          },
                          [_c("i", { staticClass: "fa fa-ellipsis-v" })]
                        ),
                        _vm._v(" "),
                        _c(
                          "div",
                          { staticClass: "dropdown-menu custom-dropdown" },
                          [
                            _c(
                              "a",
                              {
                                staticClass: "dropdown-item",
                                attrs: {
                                  href: "payment-detail.php",
                                  "data-toggle": "modal",
                                  "data-target": ".inactiveAgent"
                                }
                              },
                              [
                                _c("i", { staticClass: "fa fa-eye-slash" }),
                                _vm._v("ACTIVE / INACTIVE ")
                              ]
                            ),
                            _vm._v(" "),
                            _c(
                              "a",
                              {
                                staticClass: "dropdown-item",
                                attrs: {
                                  "data-toggle": "modal",
                                  "data-target": ".assignTask"
                                }
                              },
                              [
                                _c("i", { staticClass: "fas fa-edit" }),
                                _vm._v("ASSIGN NEW TASK")
                              ]
                            ),
                            _vm._v(" "),
                            _c(
                              "a",
                              {
                                staticClass: "dropdown-item",
                                attrs: { href: "agent-profile2.php" }
                              },
                              [
                                _c("i", {
                                  staticClass: "fa fa-user-circle",
                                  attrs: { "aria-hidden": "true" }
                                }),
                                _vm._v("View Profile")
                              ]
                            ),
                            _vm._v(" "),
                            _c(
                              "a",
                              {
                                staticClass: "dropdown-item",
                                attrs: { href: "payment-detail.php" }
                              },
                              [
                                _c("i", {
                                  staticClass: "fas fa-clipboard-list"
                                }),
                                _vm._v("View Assigned tasks")
                              ]
                            ),
                            _vm._v(" "),
                            _c(
                              "a",
                              {
                                staticClass: "dropdown-item",
                                attrs: { href: "payment-detail.php" }
                              },
                              [
                                _c("i", { staticClass: "fas fa-chart-line" }),
                                _vm._v("View Monthly Progress")
                              ]
                            ),
                            _vm._v(" "),
                            _c(
                              "a",
                              {
                                staticClass: "dropdown-item",
                                attrs: { href: "discussion-board.php" }
                              },
                              [
                                _c("i", { staticClass: "fas fa-comment" }),
                                _vm._v("View Discussion Board")
                              ]
                            )
                          ]
                        )
                      ]
                    )
                  ])
                ])
              ])
            ]
          )
        ])
      ]
    )
  }
]
render._withStripped = true



/***/ }),

/***/ "./resources/js/src/views/Pages/admin/Agent/Index.vue":
/*!************************************************************!*\
  !*** ./resources/js/src/views/Pages/admin/Agent/Index.vue ***!
  \************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _Index_vue_vue_type_template_id_1f28a203___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./Index.vue?vue&type=template&id=1f28a203& */ "./resources/js/src/views/Pages/admin/Agent/Index.vue?vue&type=template&id=1f28a203&");
/* harmony import */ var _Index_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./Index.vue?vue&type=script&lang=js& */ "./resources/js/src/views/Pages/admin/Agent/Index.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport *//* harmony import */ var _node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../../../../../../node_modules/vue-loader/lib/runtime/componentNormalizer.js */ "./node_modules/vue-loader/lib/runtime/componentNormalizer.js");





/* normalize component */

var component = Object(_node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__["default"])(
  _Index_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__["default"],
  _Index_vue_vue_type_template_id_1f28a203___WEBPACK_IMPORTED_MODULE_0__["render"],
  _Index_vue_vue_type_template_id_1f28a203___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"],
  false,
  null,
  null,
  null
  
)

/* hot reload */
if (false) { var api; }
component.options.__file = "resources/js/src/views/Pages/admin/Agent/Index.vue"
/* harmony default export */ __webpack_exports__["default"] = (component.exports);

/***/ }),

/***/ "./resources/js/src/views/Pages/admin/Agent/Index.vue?vue&type=script&lang=js&":
/*!*************************************************************************************!*\
  !*** ./resources/js/src/views/Pages/admin/Agent/Index.vue?vue&type=script&lang=js& ***!
  \*************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_Index_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../../../../node_modules/babel-loader/lib??ref--4-0!../../../../../../../node_modules/vue-loader/lib??vue-loader-options!./Index.vue?vue&type=script&lang=js& */ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/src/views/Pages/admin/Agent/Index.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport */ /* harmony default export */ __webpack_exports__["default"] = (_node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_Index_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__["default"]); 

/***/ }),

/***/ "./resources/js/src/views/Pages/admin/Agent/Index.vue?vue&type=template&id=1f28a203&":
/*!*******************************************************************************************!*\
  !*** ./resources/js/src/views/Pages/admin/Agent/Index.vue?vue&type=template&id=1f28a203& ***!
  \*******************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_Index_vue_vue_type_template_id_1f28a203___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../../../../node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!../../../../../../../node_modules/vue-loader/lib??vue-loader-options!./Index.vue?vue&type=template&id=1f28a203& */ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/src/views/Pages/admin/Agent/Index.vue?vue&type=template&id=1f28a203&");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "render", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_Index_vue_vue_type_template_id_1f28a203___WEBPACK_IMPORTED_MODULE_0__["render"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_Index_vue_vue_type_template_id_1f28a203___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"]; });



/***/ })

}]);