(window["webpackJsonp"] = window["webpackJsonp"] || []).push([[73],{

/***/ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/src/views/Pages/web/Auth/Register/Employee/EmployeeSteps.vue?vue&type=script&lang=js&":
/*!********************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/babel-loader/lib??ref--4-0!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/src/views/Pages/web/Auth/Register/Employee/EmployeeSteps.vue?vue&type=script&lang=js& ***!
  \********************************************************************************************************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @babel/runtime/regenerator */ "./node_modules/@babel/runtime/regenerator/index.js");
/* harmony import */ var _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var vue_form_wizard__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! vue-form-wizard */ "./node_modules/vue-form-wizard/dist/vue-form-wizard.js");
/* harmony import */ var vue_form_wizard__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(vue_form_wizard__WEBPACK_IMPORTED_MODULE_1__);


function asyncGeneratorStep(gen, resolve, reject, _next, _throw, key, arg) { try { var info = gen[key](arg); var value = info.value; } catch (error) { reject(error); return; } if (info.done) { resolve(value); } else { Promise.resolve(value).then(_next, _throw); } }

function _asyncToGenerator(fn) { return function () { var self = this, args = arguments; return new Promise(function (resolve, reject) { var gen = fn.apply(self, args); function _next(value) { asyncGeneratorStep(gen, resolve, reject, _next, _throw, "next", value); } function _throw(err) { asyncGeneratorStep(gen, resolve, reject, _next, _throw, "throw", err); } _next(undefined); }); }; }

//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//


var Success = function Success() {
  return __webpack_require__.e(/*! import() */ 0).then(__webpack_require__.bind(null, /*! @views/Partials/web/popups/Success.vue */ "./resources/js/src/views/Partials/web/popups/Success.vue"));
};

/* harmony default export */ __webpack_exports__["default"] = ({
  props: {
    application: {
      type: Object,
      // String, Number, Boolean, Function, Object, Array
      required: true,
      "default": null
    }
  },
  components: {
    FormWizard: vue_form_wizard__WEBPACK_IMPORTED_MODULE_1__["FormWizard"],
    TabContent: vue_form_wizard__WEBPACK_IMPORTED_MODULE_1__["TabContent"],
    WizardStep: vue_form_wizard__WEBPACK_IMPORTED_MODULE_1__["WizardStep"],
    Success: Success
  },
  created: function created() {
    this.userId = localStorage.getItem('step-id');

    if (Object.keys(this.application).length > 0) {
      var self = this;

      _.each(this.application, function (item, key) {
        if (self.steps.includes(key)) {
          if (item != null) {
            self.form[key] = item;
          }
        }
      });
    }
  },
  mounted: function mounted() {
    var self = this;
    $(document).on('hide.bs.modal', '#successPopup', function (e) {
      self.$router.push({
        name: 'web.home'
      });
    });
  },
  data: function data() {
    return {
      steps: ['education', 'institutions', 'experiences', 'certifications', 'cv', 'skills'],
      userId: 0,
      form: {
        education: {
          bio: ''
        },
        institutions: [{
          name: '',
          discipline: '',
          start_date: '',
          end_date: '',
          currently_here: false
        }],
        experiences: [{
          company_name: '',
          designation: '',
          start_date: '',
          end_date: '',
          is_working_currently: false
        }],
        certifications: [{
          title: '',
          start_date: '',
          end_date: '',
          file: {}
        }],
        cv: {
          file: {},
          cover_letter: ''
        },
        skills: []
      }
    };
  },
  methods: {
    setCertificationFile: function setCertificationFile(e, index) {
      this.form.certifications[index].file = e.target.files[0];
    },
    setCvFile: function setCvFile(e) {
      this.form.cv.file = e.target.files[0];
    },
    addMore: function addMore(variable) {
      var params;

      if (variable == 'institutions') {
        params = {
          name: '',
          discipline: '',
          start_date: '',
          end_date: '',
          is_working_currently: ''
        };
      } else if (variable == 'experiences') {
        params = {
          company_name: '',
          designation: '',
          start_date: '',
          end_date: '',
          is_working_currently: ''
        };
      } else if (variable == 'certifications') {
        params = {
          title: '',
          start_date: '',
          end_date: '',
          file: {}
        };
      }

      this.form[variable].push(params);
    },
    removeMore: function removeMore(variable, index) {
      // alert()
      this.form[variable].splice(index, 1);
    },
    sendInfo: function sendInfo(prevIndex, nextIndex) {
      if (prevIndex > nextIndex) {
        return;
      }

      var fd = new FormData();
      var stepName = this.steps[prevIndex];
      var stepFields = this.form[stepName];
      fd.append('user', this.userId);
      this.buildFormData(fd, stepFields, stepName);
      axios.post(route('employee.store-appliction'), fd).then(function (response) {});
      this.$emit('on-complete');
    },
    applyJob: function applyJob() {
      var _this = this;

      return _asyncToGenerator( /*#__PURE__*/_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.mark(function _callee() {
        var fd, stepFields, _yield$axios$post, data;

        return _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.wrap(function _callee$(_context) {
          while (1) {
            switch (_context.prev = _context.next) {
              case 0:
                fd = new FormData();
                stepFields = _this.form.skills;

                _this.buildFormData(fd, stepFields, 'skills');

                fd.append('user', _this.userId);
                _context.next = 6;
                return axios.post(route('employee.complete-application'), fd);

              case 6:
                _yield$axios$post = _context.sent;
                data = _yield$axios$post.data;

                if (data.status) {
                  localStorage.removeItem('step-id');
                  window.$('#successPopup').modal('show');
                }

              case 9:
              case "end":
                return _context.stop();
            }
          }
        }, _callee);
      }))();
    },
    changingStep: function changingStep(info) {
      return _asyncToGenerator( /*#__PURE__*/_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.mark(function _callee2() {
        return _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.wrap(function _callee2$(_context2) {
          while (1) {
            switch (_context2.prev = _context2.next) {
              case 0:
                return _context2.abrupt("return", true);

              case 1:
              case "end":
                return _context2.stop();
            }
          }
        }, _callee2);
      }))();
    }
  }
});

/***/ }),

/***/ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/src/views/Pages/web/Auth/Register/Employee/EmployeeSteps.vue?vue&type=template&id=111cb826&":
/*!************************************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/src/views/Pages/web/Auth/Register/Employee/EmployeeSteps.vue?vue&type=template&id=111cb826& ***!
  \************************************************************************************************************************************************************************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "render", function() { return render; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return staticRenderFns; });
var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c(
    "div",
    { staticClass: "bg" },
    [
      _c(
        "form-wizard",
        {
          ref: "wizard",
          attrs: { stepsClasses: "d-flex justify-content-around" },
          on: { "on-complete": _vm.applyJob, "on-change": _vm.sendInfo },
          scopedSlots: _vm._u([
            {
              key: "step",
              fn: function(tabs) {
                return _c("div", { staticClass: "rpp-stepper" }, [
                  _c("div", { staticClass: "rpp-stepper-item rpp-active" }, [
                    _c("span", { staticClass: "rpp-stepper-item-content" }, [
                      _vm._v(_vm._s(tabs.index + 1))
                    ]),
                    _vm._v(" "),
                    _c("span", { staticClass: "set-headings" }, [
                      _vm._v(_vm._s(tabs.tab.title))
                    ])
                  ]),
                  _vm._v(" "),
                  _c("div", { staticClass: "rpp-stepper-divider" })
                ])
              }
            },
            {
              key: "footer",
              fn: function(props) {
                return _c("div", { staticClass: "back-next-btn" }, [
                  _c(
                    "button",
                    {
                      staticClass: "for-back",
                      attrs: { type: "button" },
                      on: {
                        click: function($event) {
                          return props.prevTab()
                        }
                      }
                    },
                    [_vm._v("BACK")]
                  ),
                  _vm._v(" "),
                  !props.isLastStep
                    ? _c(
                        "button",
                        {
                          staticClass: "for-next",
                          attrs: { type: "submit" },
                          on: {
                            click: function($event) {
                              return props.nextTab()
                            }
                          }
                        },
                        [_vm._v("NEXT")]
                      )
                    : _vm._e(),
                  _vm._v(" "),
                  props.isLastStep
                    ? _c(
                        "button",
                        {
                          staticClass: "for-next",
                          on: { click: _vm.applyJob }
                        },
                        [_vm._v("Apply for job")]
                      )
                    : _vm._e()
                ])
              }
            }
          ])
        },
        [
          _c("template", { slot: "title" }, [
            _c("h1", [_vm._v("Set Profile")]),
            _vm._v(" "),
            _c("hr")
          ]),
          _vm._v(" "),
          _vm._v(" "),
          _c(
            "tab-content",
            {
              attrs: { title: "Description", "before-change": _vm.changingStep }
            },
            [
              _c("div", { staticClass: "description-box" }, [
                _c("h2", [_vm._v("Description")]),
                _vm._v(" "),
                _c("span", [
                  _vm._v("Add Description which will help the recruiters "),
                  _c("br"),
                  _vm._v(" to know a bit more about you.")
                ])
              ]),
              _vm._v(" "),
              _c("div", { staticClass: "description-deatail" }, [
                _c("textarea", {
                  directives: [
                    {
                      name: "model",
                      rawName: "v-model",
                      value: _vm.form.education.bio,
                      expression: "form.education.bio"
                    }
                  ],
                  staticClass: "w-100",
                  domProps: { value: _vm.form.education.bio },
                  on: {
                    input: function($event) {
                      if ($event.target.composing) {
                        return
                      }
                      _vm.$set(_vm.form.education, "bio", $event.target.value)
                    }
                  }
                })
              ])
            ]
          ),
          _vm._v(" "),
          _c(
            "tab-content",
            {
              attrs: { title: "Education", "before-change": _vm.changingStep }
            },
            [
              _c("div", { staticClass: "description-box" }, [
                _c("h2", [_vm._v("Education")]),
                _vm._v(" "),
                _c("span", [
                  _vm._v("Add your education details. "),
                  _c("br"),
                  _vm._v(" Note: Add the latest first.")
                ])
              ]),
              _vm._v(" "),
              _c("div", { staticClass: "education-form" }, [
                _c(
                  "form",
                  _vm._l(_vm.form.institutions, function(institution, index) {
                    return _c("div", { staticClass: "form-row" }, [
                      index != 0
                        ? _c("div", { staticClass: "col-md-12" }, [
                            _c(
                              "button",
                              {
                                staticClass: "close",
                                attrs: { type: "button" },
                                on: {
                                  click: function($event) {
                                    return _vm.removeMore("institutions", index)
                                  }
                                }
                              },
                              [_c("span", [_vm._v("×")])]
                            )
                          ])
                        : _vm._e(),
                      _vm._v(" "),
                      _c("div", { staticClass: "col-md-6" }, [
                        _c("label", { attrs: { for: "inputEmail4" } }, [
                          _vm._v("Institute Name:")
                        ]),
                        _vm._v(" "),
                        _c("input", {
                          directives: [
                            {
                              name: "model",
                              rawName: "v-model",
                              value: institution.name,
                              expression: "institution.name"
                            }
                          ],
                          staticClass: "form-control",
                          attrs: {
                            type: "text",
                            placeholder: "Enter Institute Name"
                          },
                          domProps: { value: institution.name },
                          on: {
                            input: function($event) {
                              if ($event.target.composing) {
                                return
                              }
                              _vm.$set(institution, "name", $event.target.value)
                            }
                          }
                        })
                      ]),
                      _vm._v(" "),
                      _c("div", { staticClass: "col-md-6" }, [
                        _c("label", { attrs: { for: "inputEmail4" } }, [
                          _vm._v("Discipline:")
                        ]),
                        _vm._v(" "),
                        _c("input", {
                          directives: [
                            {
                              name: "model",
                              rawName: "v-model",
                              value: institution.discipline,
                              expression: "institution.discipline"
                            }
                          ],
                          staticClass: "form-control",
                          attrs: {
                            type: "text",
                            placeholder: "Enter Discipline"
                          },
                          domProps: { value: institution.discipline },
                          on: {
                            input: function($event) {
                              if ($event.target.composing) {
                                return
                              }
                              _vm.$set(
                                institution,
                                "discipline",
                                $event.target.value
                              )
                            }
                          }
                        })
                      ]),
                      _vm._v(" "),
                      _c("div", { staticClass: "form-group col-md-6" }, [
                        _c("label", { attrs: { for: "inputEmail4" } }, [
                          _vm._v("From Date:")
                        ]),
                        _vm._v(" "),
                        _c("input", {
                          directives: [
                            {
                              name: "model",
                              rawName: "v-model",
                              value: institution.start_date,
                              expression: "institution.start_date"
                            }
                          ],
                          staticClass: "form-control",
                          attrs: {
                            type: "date",
                            id: "inputEmail4",
                            placeholder: "May 2, 2020"
                          },
                          domProps: { value: institution.start_date },
                          on: {
                            input: function($event) {
                              if ($event.target.composing) {
                                return
                              }
                              _vm.$set(
                                institution,
                                "start_date",
                                $event.target.value
                              )
                            }
                          }
                        })
                      ]),
                      _vm._v(" "),
                      _c("div", { staticClass: "form-group col-md-6" }, [
                        _c("label", { attrs: { for: "inputEmail4" } }, [
                          _vm._v("To Date:")
                        ]),
                        _vm._v(" "),
                        _c("input", {
                          directives: [
                            {
                              name: "model",
                              rawName: "v-model",
                              value: institution.end_date,
                              expression: "institution.end_date"
                            }
                          ],
                          staticClass: "form-control",
                          attrs: {
                            type: "date",
                            id: "inputEmail4",
                            placeholder: "May 2, 2020"
                          },
                          domProps: { value: institution.end_date },
                          on: {
                            input: function($event) {
                              if ($event.target.composing) {
                                return
                              }
                              _vm.$set(
                                institution,
                                "end_date",
                                $event.target.value
                              )
                            }
                          }
                        }),
                        _vm._v(" "),
                        _c("div", { staticClass: "form-group form-check" }, [
                          _c("input", {
                            directives: [
                              {
                                name: "model",
                                rawName: "v-model",
                                value: institution.currently_here,
                                expression: "institution.currently_here"
                              }
                            ],
                            staticClass: "form-check-input",
                            attrs: { type: "checkbox", id: "exampleCheck1" },
                            domProps: {
                              checked: Array.isArray(institution.currently_here)
                                ? _vm._i(institution.currently_here, null) > -1
                                : institution.currently_here
                            },
                            on: {
                              change: function($event) {
                                var $$a = institution.currently_here,
                                  $$el = $event.target,
                                  $$c = $$el.checked ? true : false
                                if (Array.isArray($$a)) {
                                  var $$v = null,
                                    $$i = _vm._i($$a, $$v)
                                  if ($$el.checked) {
                                    $$i < 0 &&
                                      _vm.$set(
                                        institution,
                                        "currently_here",
                                        $$a.concat([$$v])
                                      )
                                  } else {
                                    $$i > -1 &&
                                      _vm.$set(
                                        institution,
                                        "currently_here",
                                        $$a
                                          .slice(0, $$i)
                                          .concat($$a.slice($$i + 1))
                                      )
                                  }
                                } else {
                                  _vm.$set(institution, "currently_here", $$c)
                                }
                              }
                            }
                          }),
                          _vm._v(" "),
                          _c(
                            "label",
                            {
                              staticClass: "form-check-label",
                              attrs: { for: "exampleCheck1" }
                            },
                            [_vm._v("Currently Here")]
                          ),
                          _vm._v(" "),
                          _c(
                            "a",
                            {
                              staticClass: "add-another",
                              attrs: { href: "javascript:void(0)" },
                              on: {
                                click: function($event) {
                                  return _vm.addMore("institutions")
                                }
                              }
                            },
                            [
                              _c("i", { staticClass: "fas fa-plus-circle" }),
                              _vm._v(" Add Another")
                            ]
                          )
                        ])
                      ])
                    ])
                  }),
                  0
                )
              ])
            ]
          ),
          _vm._v(" "),
          _c(
            "tab-content",
            {
              attrs: { title: "Experience", "before-change": _vm.changingStep }
            },
            [
              _c("div", { staticClass: "description-box" }, [
                _c("h2", [_vm._v("Work Experience")]),
                _vm._v(" "),
                _c("span", [
                  _vm._v("Add your work experience. "),
                  _c("br"),
                  _vm._v(" Note: Add the latest first.")
                ])
              ]),
              _vm._v(" "),
              _c(
                "div",
                { staticClass: "education-form" },
                _vm._l(_vm.form.experiences, function(experience, index) {
                  return _c("div", { staticClass: "form-row" }, [
                    index != 0
                      ? _c("div", { staticClass: "col-md-12" }, [
                          _c(
                            "button",
                            {
                              staticClass: "close",
                              attrs: { type: "button" },
                              on: {
                                click: function($event) {
                                  return _vm.removeMore("experiences", index)
                                }
                              }
                            },
                            [_c("span", [_vm._v("×")])]
                          )
                        ])
                      : _vm._e(),
                    _vm._v(" "),
                    _c("div", { staticClass: "col-md-6" }, [
                      _c("label", { attrs: { for: "inputEmail4" } }, [
                        _vm._v("Company Name:")
                      ]),
                      _vm._v(" "),
                      _c("input", {
                        directives: [
                          {
                            name: "model",
                            rawName: "v-model",
                            value: experience.company_name,
                            expression: "experience.company_name"
                          }
                        ],
                        staticClass: "form-control",
                        attrs: {
                          type: "text",
                          placeholder: "Enter Company Name"
                        },
                        domProps: { value: experience.company_name },
                        on: {
                          input: function($event) {
                            if ($event.target.composing) {
                              return
                            }
                            _vm.$set(
                              experience,
                              "company_name",
                              $event.target.value
                            )
                          }
                        }
                      })
                    ]),
                    _vm._v(" "),
                    _c("div", { staticClass: "col-md-6" }, [
                      _c("label", { attrs: { for: "inputEmail4" } }, [
                        _vm._v("Designation:")
                      ]),
                      _vm._v(" "),
                      _c("input", {
                        directives: [
                          {
                            name: "model",
                            rawName: "v-model",
                            value: experience.designation,
                            expression: "experience.designation"
                          }
                        ],
                        staticClass: "form-control",
                        attrs: {
                          type: "text",
                          placeholder: "Enter Designation"
                        },
                        domProps: { value: experience.designation },
                        on: {
                          input: function($event) {
                            if ($event.target.composing) {
                              return
                            }
                            _vm.$set(
                              experience,
                              "designation",
                              $event.target.value
                            )
                          }
                        }
                      })
                    ]),
                    _vm._v(" "),
                    _c("div", { staticClass: "form-group col-md-6" }, [
                      _c("label", { attrs: { for: "inputEmail4" } }, [
                        _vm._v("From Date:")
                      ]),
                      _vm._v(" "),
                      _c("input", {
                        directives: [
                          {
                            name: "model",
                            rawName: "v-model",
                            value: experience.start_date,
                            expression: "experience.start_date"
                          }
                        ],
                        staticClass: "form-control",
                        attrs: {
                          type: "date",
                          id: "inputEmail4",
                          placeholder: "May 2, 2020"
                        },
                        domProps: { value: experience.start_date },
                        on: {
                          input: function($event) {
                            if ($event.target.composing) {
                              return
                            }
                            _vm.$set(
                              experience,
                              "start_date",
                              $event.target.value
                            )
                          }
                        }
                      })
                    ]),
                    _vm._v(" "),
                    _c("div", { staticClass: "form-group col-md-6" }, [
                      _c("label", { attrs: { for: "inputEmail4" } }, [
                        _vm._v("To Date:")
                      ]),
                      _vm._v(" "),
                      _c("input", {
                        directives: [
                          {
                            name: "model",
                            rawName: "v-model",
                            value: experience.end_date,
                            expression: "experience.end_date"
                          }
                        ],
                        staticClass: "form-control",
                        attrs: {
                          type: "date",
                          id: "inputEmail4",
                          placeholder: "May 2, 2020"
                        },
                        domProps: { value: experience.end_date },
                        on: {
                          input: function($event) {
                            if ($event.target.composing) {
                              return
                            }
                            _vm.$set(
                              experience,
                              "end_date",
                              $event.target.value
                            )
                          }
                        }
                      }),
                      _vm._v(" "),
                      _c("div", { staticClass: "form-group form-check" }, [
                        _c("input", {
                          directives: [
                            {
                              name: "model",
                              rawName: "v-model",
                              value: experience.is_working_currently,
                              expression: "experience.is_working_currently"
                            }
                          ],
                          staticClass: "form-check-input",
                          attrs: { type: "checkbox", id: "exampleCheck1" },
                          domProps: {
                            checked: Array.isArray(
                              experience.is_working_currently
                            )
                              ? _vm._i(experience.is_working_currently, null) >
                                -1
                              : experience.is_working_currently
                          },
                          on: {
                            change: function($event) {
                              var $$a = experience.is_working_currently,
                                $$el = $event.target,
                                $$c = $$el.checked ? true : false
                              if (Array.isArray($$a)) {
                                var $$v = null,
                                  $$i = _vm._i($$a, $$v)
                                if ($$el.checked) {
                                  $$i < 0 &&
                                    _vm.$set(
                                      experience,
                                      "is_working_currently",
                                      $$a.concat([$$v])
                                    )
                                } else {
                                  $$i > -1 &&
                                    _vm.$set(
                                      experience,
                                      "is_working_currently",
                                      $$a
                                        .slice(0, $$i)
                                        .concat($$a.slice($$i + 1))
                                    )
                                }
                              } else {
                                _vm.$set(
                                  experience,
                                  "is_working_currently",
                                  $$c
                                )
                              }
                            }
                          }
                        }),
                        _vm._v(" "),
                        _c(
                          "label",
                          {
                            staticClass: "form-check-label",
                            attrs: { for: "exampleCheck1" }
                          },
                          [_vm._v("Currently Work Here")]
                        ),
                        _vm._v(" "),
                        _c(
                          "a",
                          {
                            staticClass: "add-another",
                            attrs: { href: "javascript:void(0)" },
                            on: {
                              click: function($event) {
                                return _vm.addMore("experiences")
                              }
                            }
                          },
                          [
                            _c("i", { staticClass: "fas fa-plus-circle" }),
                            _vm._v(" Add Another")
                          ]
                        )
                      ])
                    ])
                  ])
                }),
                0
              )
            ]
          ),
          _vm._v(" "),
          _c(
            "tab-content",
            {
              attrs: {
                title: "Certifications",
                "before-change": _vm.changingStep
              }
            },
            [
              _c("div", { staticClass: "description-box" }, [
                _c("h2", [_vm._v("Add Certification")])
              ]),
              _vm._v(" "),
              _c(
                "div",
                { staticClass: "education-form" },
                _vm._l(_vm.form.certifications, function(certification, index) {
                  return _c("div", { staticClass: "form-row" }, [
                    index != 0
                      ? _c("div", { staticClass: "col-md-12" }, [
                          _c(
                            "button",
                            {
                              staticClass: "close",
                              attrs: { type: "button" },
                              on: {
                                click: function($event) {
                                  return _vm.removeMore("certifications", index)
                                }
                              }
                            },
                            [_c("span", [_vm._v("×")])]
                          )
                        ])
                      : _vm._e(),
                    _vm._v(" "),
                    _c("div", { staticClass: "col-md-12" }, [
                      _c("label", { attrs: { for: "inputEmail4" } }, [
                        _vm._v("Certification Name:")
                      ]),
                      _vm._v(" "),
                      _c("input", {
                        directives: [
                          {
                            name: "model",
                            rawName: "v-model",
                            value: certification.title,
                            expression: "certification.title"
                          }
                        ],
                        staticClass: "form-control",
                        attrs: {
                          type: "text",
                          placeholder: "Enter Certification Name"
                        },
                        domProps: { value: certification.title },
                        on: {
                          input: function($event) {
                            if ($event.target.composing) {
                              return
                            }
                            _vm.$set(
                              certification,
                              "title",
                              $event.target.value
                            )
                          }
                        }
                      })
                    ]),
                    _vm._v(" "),
                    _c("div", { staticClass: "form-group col-md-6" }, [
                      _c("label", { attrs: { for: "inputEmail4" } }, [
                        _vm._v("From Date:")
                      ]),
                      _vm._v(" "),
                      _c("input", {
                        directives: [
                          {
                            name: "model",
                            rawName: "v-model",
                            value: certification.start_date,
                            expression: "certification.start_date"
                          }
                        ],
                        staticClass: "form-control",
                        attrs: {
                          type: "date",
                          id: "inputEmail4",
                          placeholder: "May 2, 2020"
                        },
                        domProps: { value: certification.start_date },
                        on: {
                          input: function($event) {
                            if ($event.target.composing) {
                              return
                            }
                            _vm.$set(
                              certification,
                              "start_date",
                              $event.target.value
                            )
                          }
                        }
                      })
                    ]),
                    _vm._v(" "),
                    _c("div", { staticClass: "form-group col-md-6" }, [
                      _c("label", { attrs: { for: "inputEmail4" } }, [
                        _vm._v("From Date:")
                      ]),
                      _vm._v(" "),
                      _c("input", {
                        directives: [
                          {
                            name: "model",
                            rawName: "v-model",
                            value: certification.end_date,
                            expression: "certification.end_date"
                          }
                        ],
                        staticClass: "form-control",
                        attrs: {
                          type: "date",
                          id: "inputEmail4",
                          placeholder: "May 2, 2020"
                        },
                        domProps: { value: certification.end_date },
                        on: {
                          input: function($event) {
                            if ($event.target.composing) {
                              return
                            }
                            _vm.$set(
                              certification,
                              "end_date",
                              $event.target.value
                            )
                          }
                        }
                      }),
                      _vm._v(" "),
                      _c(
                        "a",
                        {
                          staticClass: "add-another",
                          attrs: { href: "javascript:void(0)" },
                          on: {
                            click: function($event) {
                              return _vm.addMore("certifications")
                            }
                          }
                        },
                        [
                          _c("i", { staticClass: "fas fa-plus-circle" }),
                          _vm._v(" Add Another")
                        ]
                      )
                    ]),
                    _vm._v(" "),
                    _c("div", { staticClass: "upload-btn" }, [
                      _vm._v(
                        "\n                        UPLOAD\n                        "
                      ),
                      _c("input", {
                        attrs: { type: "file", name: "file" },
                        on: {
                          change: function($event) {
                            return _vm.setCertificationFile($event, index)
                          }
                        }
                      })
                    ]),
                    _vm._v(" "),
                    _c("span", { staticClass: "red-text-png" }, [
                      _vm._v("Only PDF or PNG")
                    ])
                  ])
                }),
                0
              )
            ]
          ),
          _vm._v(" "),
          _c(
            "tab-content",
            {
              attrs: { title: "Upload CV", "before-change": _vm.changingStep }
            },
            [
              _c("div", { staticClass: "description-box" }, [
                _c("h2", [_vm._v("Upload CV")])
              ]),
              _vm._v(" "),
              _c("div", { staticClass: "upload-box" }, [
                _c("div", { staticClass: "upload-btn" }, [
                  _vm._v(
                    "\n                    Click Here to upload your CV\n                    "
                  ),
                  _c("input", {
                    attrs: { type: "file", name: "file" },
                    on: { change: _vm.setCvFile }
                  })
                ])
              ]),
              _vm._v(" "),
              _c("form", [
                _c("div", { staticClass: "form-group" }, [
                  _c("textarea", {
                    staticClass: "form-control",
                    attrs: {
                      id: "exampleFormControlTextarea1",
                      rows: "5",
                      placeholder: "Cover Letter(Optional):"
                    }
                  })
                ])
              ])
            ]
          ),
          _vm._v(" "),
          _c(
            "tab-content",
            {
              attrs: { title: "Add Skills", "before-change": _vm.changingStep }
            },
            [
              _c(
                "div",
                { staticClass: "description-box" },
                [
                  _c("h2", [_vm._v("Add Skills")]),
                  _vm._v(" "),
                  _c("p", [
                    _vm._v("Add skills to showcase what you are good at: "),
                    _c("span", { staticClass: "red-text" }, [
                      _vm._v(" Add upto 05 Skills")
                    ])
                  ]),
                  _vm._v(" "),
                  _c("v-select", {
                    attrs: {
                      taggable: "",
                      multiple: "",
                      pushTags: true,
                      options: _vm.form.skills
                    }
                  }),
                  _vm._v(" "),
                  _c("p", [
                    _c("span", [_vm._v(" Skill (ex: Window Cleaning)")])
                  ])
                ],
                1
              )
            ]
          )
        ],
        2
      ),
      _vm._v(" "),
      _c(
        "success",
        {
          attrs: {
            title: "System Message",
            subtitle:
              "Your job request has beeen sent successfully! You will soon be contacted by the admin."
          }
        },
        [
          _c(
            "template",
            { slot: "footer" },
            [
              _c(
                "router-link",
                {
                  staticClass: "emplyee-add-btn",
                  attrs: { "data-dismiss": "modal", to: { name: "web.home" } }
                },
                [_vm._v("Close")]
              )
            ],
            1
          )
        ],
        2
      )
    ],
    1
  )
}
var staticRenderFns = []
render._withStripped = true



/***/ }),

/***/ "./resources/js/src/views/Pages/web/Auth/Register/Employee/EmployeeSteps.vue":
/*!***********************************************************************************!*\
  !*** ./resources/js/src/views/Pages/web/Auth/Register/Employee/EmployeeSteps.vue ***!
  \***********************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _EmployeeSteps_vue_vue_type_template_id_111cb826___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./EmployeeSteps.vue?vue&type=template&id=111cb826& */ "./resources/js/src/views/Pages/web/Auth/Register/Employee/EmployeeSteps.vue?vue&type=template&id=111cb826&");
/* harmony import */ var _EmployeeSteps_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./EmployeeSteps.vue?vue&type=script&lang=js& */ "./resources/js/src/views/Pages/web/Auth/Register/Employee/EmployeeSteps.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport *//* harmony import */ var _node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../../../../../../../../node_modules/vue-loader/lib/runtime/componentNormalizer.js */ "./node_modules/vue-loader/lib/runtime/componentNormalizer.js");





/* normalize component */

var component = Object(_node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__["default"])(
  _EmployeeSteps_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__["default"],
  _EmployeeSteps_vue_vue_type_template_id_111cb826___WEBPACK_IMPORTED_MODULE_0__["render"],
  _EmployeeSteps_vue_vue_type_template_id_111cb826___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"],
  false,
  null,
  null,
  null
  
)

/* hot reload */
if (false) { var api; }
component.options.__file = "resources/js/src/views/Pages/web/Auth/Register/Employee/EmployeeSteps.vue"
/* harmony default export */ __webpack_exports__["default"] = (component.exports);

/***/ }),

/***/ "./resources/js/src/views/Pages/web/Auth/Register/Employee/EmployeeSteps.vue?vue&type=script&lang=js&":
/*!************************************************************************************************************!*\
  !*** ./resources/js/src/views/Pages/web/Auth/Register/Employee/EmployeeSteps.vue?vue&type=script&lang=js& ***!
  \************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_EmployeeSteps_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../../../../../../node_modules/babel-loader/lib??ref--4-0!../../../../../../../../../node_modules/vue-loader/lib??vue-loader-options!./EmployeeSteps.vue?vue&type=script&lang=js& */ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/src/views/Pages/web/Auth/Register/Employee/EmployeeSteps.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport */ /* harmony default export */ __webpack_exports__["default"] = (_node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_EmployeeSteps_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__["default"]); 

/***/ }),

/***/ "./resources/js/src/views/Pages/web/Auth/Register/Employee/EmployeeSteps.vue?vue&type=template&id=111cb826&":
/*!******************************************************************************************************************!*\
  !*** ./resources/js/src/views/Pages/web/Auth/Register/Employee/EmployeeSteps.vue?vue&type=template&id=111cb826& ***!
  \******************************************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_EmployeeSteps_vue_vue_type_template_id_111cb826___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../../../../../../node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!../../../../../../../../../node_modules/vue-loader/lib??vue-loader-options!./EmployeeSteps.vue?vue&type=template&id=111cb826& */ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/src/views/Pages/web/Auth/Register/Employee/EmployeeSteps.vue?vue&type=template&id=111cb826&");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "render", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_EmployeeSteps_vue_vue_type_template_id_111cb826___WEBPACK_IMPORTED_MODULE_0__["render"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_EmployeeSteps_vue_vue_type_template_id_111cb826___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"]; });



/***/ })

}]);