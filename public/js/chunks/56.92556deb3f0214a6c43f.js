(window["webpackJsonp"] = window["webpackJsonp"] || []).push([[56],{

/***/ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/src/views/Pages/admin/User/components/EmployeeDetail.vue?vue&type=script&lang=js&":
/*!****************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/babel-loader/lib??ref--4-0!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/src/views/Pages/admin/User/components/EmployeeDetail.vue?vue&type=script&lang=js& ***!
  \****************************************************************************************************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @babel/runtime/regenerator */ "./node_modules/@babel/runtime/regenerator/index.js");
/* harmony import */ var _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var vuex__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! vuex */ "./node_modules/vuex/dist/vuex.esm.js");


function asyncGeneratorStep(gen, resolve, reject, _next, _throw, key, arg) { try { var info = gen[key](arg); var value = info.value; } catch (error) { reject(error); return; } if (info.done) { resolve(value); } else { Promise.resolve(value).then(_next, _throw); } }

function _asyncToGenerator(fn) { return function () { var self = this, args = arguments; return new Promise(function (resolve, reject) { var gen = fn.apply(self, args); function _next(value) { asyncGeneratorStep(gen, resolve, reject, _next, _throw, "next", value); } function _throw(err) { asyncGeneratorStep(gen, resolve, reject, _next, _throw, "throw", err); } _next(undefined); }); }; }

function ownKeys(object, enumerableOnly) { var keys = Object.keys(object); if (Object.getOwnPropertySymbols) { var symbols = Object.getOwnPropertySymbols(object); if (enumerableOnly) symbols = symbols.filter(function (sym) { return Object.getOwnPropertyDescriptor(object, sym).enumerable; }); keys.push.apply(keys, symbols); } return keys; }

function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; if (i % 2) { ownKeys(Object(source), true).forEach(function (key) { _defineProperty(target, key, source[key]); }); } else if (Object.getOwnPropertyDescriptors) { Object.defineProperties(target, Object.getOwnPropertyDescriptors(source)); } else { ownKeys(Object(source)).forEach(function (key) { Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key)); }); } } return target; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//

/* harmony default export */ __webpack_exports__["default"] = ({
  computed: _objectSpread({}, Object(vuex__WEBPACK_IMPORTED_MODULE_1__["mapState"])('admin', ['user', 'search'])),
  methods: _objectSpread(_objectSpread({}, Object(vuex__WEBPACK_IMPORTED_MODULE_1__["mapActions"])('admin', ['store'])), {}, {
    removeProperty: function removeProperty(property) {
      var _this = this;

      return _asyncToGenerator( /*#__PURE__*/_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.mark(function _callee() {
        var _total_tasks_pending, id, params, _yield$_this$store, data;

        return _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.wrap(function _callee$(_context) {
          while (1) {
            switch (_context.prev = _context.next) {
              case 0:
                _total_tasks_pending = property._total_tasks_pending, id = property.id;

                if (!(_total_tasks_pending == 0)) {
                  _context.next = 10;
                  break;
                }

                params = {
                  route: route('admin.employee.remove-property', {
                    employee: _this.user.id,
                    property: id
                  }),
                  method: 'POST',
                  data: {}
                };
                _context.next = 5;
                return _this.store(params);

              case 5:
                _yield$_this$store = _context.sent;
                data = _yield$_this$store.data;

                if (data.status) {
                  _this.$snotify.success(data.msg);

                  _this.$emit('on-update');
                }

                _context.next = 13;
                break;

              case 10:
                _this.exclude = _this.user.id;
                _this.assignTask++;
                $('#confirm-popup').modal('show');

              case 13:
              case "end":
                return _context.stop();
            }
          }
        }, _callee);
      }))();
    }
  })
});

/***/ }),

/***/ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/src/views/Pages/admin/User/components/EmployeeDetail.vue?vue&type=template&id=0f0c25ee&":
/*!********************************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/src/views/Pages/admin/User/components/EmployeeDetail.vue?vue&type=template&id=0f0c25ee& ***!
  \********************************************************************************************************************************************************************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "render", function() { return render; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return staticRenderFns; });
var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c(
    "section",
    { staticClass: "search view-cause", attrs: { id: "configuration" } },
    [
      _c("div", { staticClass: "row" }, [
        _c("div", { staticClass: "col-12" }, [
          _c("div", { staticClass: "card pad-20" }, [
            _c("div", { staticClass: "card-content collapse show" }, [
              _c(
                "div",
                { staticClass: "card-body table-responsive card-dashboard" },
                [
                  _c(
                    "router-link",
                    {
                      attrs: {
                        to: {
                          name: "admin.users.index",
                          params: { type: "employee" }
                        }
                      }
                    },
                    [
                      _c("h2", [
                        _vm._v(
                          "\n                                < USER MANAGEMENT"
                        )
                      ])
                    ]
                  ),
                  _vm._v(" "),
                  _c("section", { staticClass: "new-employee-formbox" }, [
                    _c("div", {}, [
                      _c("span", { staticClass: "emp-green" }, [
                        _vm._v("Emp_Id: E-" + _vm._s(_vm.user.id))
                      ]),
                      _vm._v(" "),
                      _c("div", { staticClass: "row" }, [
                        _vm._m(0),
                        _vm._v(" "),
                        _c("div", { staticClass: "col-md-6" }),
                        _vm._v(" "),
                        _c("div", { staticClass: "col-md-12" }, [
                          _c("p", [_vm._v("Employee Name:")]),
                          _vm._v(" "),
                          _c("p", { staticClass: "text-muted" }, [
                            _vm._v(_vm._s(_vm.user.name))
                          ]),
                          _vm._v(" "),
                          _c("p", [_vm._v("Employee Email Address:")]),
                          _vm._v(" "),
                          _c("p", { staticClass: "text-muted" }, [
                            _vm._v(_vm._s(_vm.user.email))
                          ]),
                          _vm._v(" "),
                          _c("p", [_vm._v("Employee Gender:")]),
                          _vm._v(" "),
                          _c("p", { staticClass: "text-muted" }, [
                            _vm._v(_vm._s(_vm.user.gender))
                          ]),
                          _vm._v(" "),
                          _c("p", [_vm._v("Assigned Property:")]),
                          _vm._v(" "),
                          _c("p", { staticClass: "for-green" }, [
                            _vm._v(
                              "Total Properties: " +
                                _vm._s(_vm.user.properties_count) +
                                ", Total Tasks :" +
                                _vm._s(_vm.user.tasks_count)
                            )
                          ])
                        ])
                      ]),
                      _vm._v(" "),
                      _c(
                        "div",
                        { staticClass: "card" },
                        [
                          _c("table-search", { staticClass: "my-search" }),
                          _vm._v(" "),
                          _c("div", {
                            staticClass:
                              "input-group md-form form-sm form-2 pl-0 my-search"
                          }),
                          _vm._v(" "),
                          _vm._l(_vm.user.properties, function(property) {
                            return _c(
                              "div",
                              { key: property.id, staticClass: "media" },
                              [
                                _c("img", {
                                  staticClass: "mr-3",
                                  attrs: {
                                    src: property.media
                                      ? property.media.url
                                      : "assets/images/placeholder.png",
                                    alt: "..."
                                  }
                                }),
                                _vm._v(" "),
                                _c("div", { staticClass: "media-body" }, [
                                  _c("span", [
                                    _vm._v(_vm._s(property.name) + " "),
                                    _c("br"),
                                    _vm._v(
                                      " Property_ID: P-" + _vm._s(property.id)
                                    )
                                  ]),
                                  _vm._v(" "),
                                  _c(
                                    "a",
                                    {
                                      staticClass: "remove-property-btn",
                                      attrs: { href: "javascript:void(0)" },
                                      on: {
                                        click: function($event) {
                                          return _vm.removeProperty(property)
                                        }
                                      }
                                    },
                                    [_vm._v("Remove Property")]
                                  )
                                ])
                              ]
                            )
                          }),
                          _vm._v(" "),
                          _c(
                            "no-record",
                            {
                              staticClass: "media",
                              attrs: { tag: "div", data: _vm.user.properties }
                            },
                            [
                              _c("h6", { staticClass: "text-center" }, [
                                _vm._v("No Data Available")
                              ])
                            ]
                          )
                        ],
                        2
                      ),
                      _vm._v(" "),
                      _vm._m(1),
                      _vm._v(" "),
                      _c("div", { staticClass: "col-md-12" }, [
                        _c("p", [_vm._v("Email Address:")]),
                        _vm._v(" "),
                        _c("p", { staticClass: "text-muted" }, [
                          _vm._v(_vm._s(_vm.user.email))
                        ]),
                        _vm._v(" "),
                        _c("p", [_vm._v("Auto-generated Password:")]),
                        _vm._v(" "),
                        _c("p", { staticClass: "text-muted" }, [
                          _vm._v("********")
                        ]),
                        _vm._v(" "),
                        _c("p", [_vm._v("Unique Code:")]),
                        _vm._v(" "),
                        _c("p", { staticClass: "text-muted" }, [
                          _vm._v(_vm._s(_vm.user.code || "NA"))
                        ])
                      ])
                    ])
                  ])
                ],
                1
              )
            ])
          ])
        ])
      ])
    ]
  )
}
var staticRenderFns = [
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("div", { staticClass: "col-md-6" }, [
      _c("h2", [_vm._v("Employee Details")]),
      _vm._v(" "),
      _c("h5", [_vm._v(" Employee Personal Details:")])
    ])
  },
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("div", { staticClass: "col-md-12" }, [
      _c("h6", [_vm._v("Login Details:")])
    ])
  }
]
render._withStripped = true



/***/ }),

/***/ "./resources/js/src/views/Pages/admin/User/components/EmployeeDetail.vue":
/*!*******************************************************************************!*\
  !*** ./resources/js/src/views/Pages/admin/User/components/EmployeeDetail.vue ***!
  \*******************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _EmployeeDetail_vue_vue_type_template_id_0f0c25ee___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./EmployeeDetail.vue?vue&type=template&id=0f0c25ee& */ "./resources/js/src/views/Pages/admin/User/components/EmployeeDetail.vue?vue&type=template&id=0f0c25ee&");
/* harmony import */ var _EmployeeDetail_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./EmployeeDetail.vue?vue&type=script&lang=js& */ "./resources/js/src/views/Pages/admin/User/components/EmployeeDetail.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport *//* harmony import */ var _node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../../../../../../../node_modules/vue-loader/lib/runtime/componentNormalizer.js */ "./node_modules/vue-loader/lib/runtime/componentNormalizer.js");





/* normalize component */

var component = Object(_node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__["default"])(
  _EmployeeDetail_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__["default"],
  _EmployeeDetail_vue_vue_type_template_id_0f0c25ee___WEBPACK_IMPORTED_MODULE_0__["render"],
  _EmployeeDetail_vue_vue_type_template_id_0f0c25ee___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"],
  false,
  null,
  null,
  null
  
)

/* hot reload */
if (false) { var api; }
component.options.__file = "resources/js/src/views/Pages/admin/User/components/EmployeeDetail.vue"
/* harmony default export */ __webpack_exports__["default"] = (component.exports);

/***/ }),

/***/ "./resources/js/src/views/Pages/admin/User/components/EmployeeDetail.vue?vue&type=script&lang=js&":
/*!********************************************************************************************************!*\
  !*** ./resources/js/src/views/Pages/admin/User/components/EmployeeDetail.vue?vue&type=script&lang=js& ***!
  \********************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_EmployeeDetail_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../../../../../node_modules/babel-loader/lib??ref--4-0!../../../../../../../../node_modules/vue-loader/lib??vue-loader-options!./EmployeeDetail.vue?vue&type=script&lang=js& */ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/src/views/Pages/admin/User/components/EmployeeDetail.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport */ /* harmony default export */ __webpack_exports__["default"] = (_node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_EmployeeDetail_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__["default"]); 

/***/ }),

/***/ "./resources/js/src/views/Pages/admin/User/components/EmployeeDetail.vue?vue&type=template&id=0f0c25ee&":
/*!**************************************************************************************************************!*\
  !*** ./resources/js/src/views/Pages/admin/User/components/EmployeeDetail.vue?vue&type=template&id=0f0c25ee& ***!
  \**************************************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_EmployeeDetail_vue_vue_type_template_id_0f0c25ee___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../../../../../node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!../../../../../../../../node_modules/vue-loader/lib??vue-loader-options!./EmployeeDetail.vue?vue&type=template&id=0f0c25ee& */ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/src/views/Pages/admin/User/components/EmployeeDetail.vue?vue&type=template&id=0f0c25ee&");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "render", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_EmployeeDetail_vue_vue_type_template_id_0f0c25ee___WEBPACK_IMPORTED_MODULE_0__["render"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_EmployeeDetail_vue_vue_type_template_id_0f0c25ee___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"]; });



/***/ })

}]);