(window["webpackJsonp"] = window["webpackJsonp"] || []).push([[78],{

/***/ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/src/views/Pages/web/Employee/Index.vue?vue&type=script&lang=js&":
/*!**********************************************************************************************************************************************************************************!*\
  !*** ./node_modules/babel-loader/lib??ref--4-0!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/src/views/Pages/web/Employee/Index.vue?vue&type=script&lang=js& ***!
  \**********************************************************************************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @babel/runtime/regenerator */ "./node_modules/@babel/runtime/regenerator/index.js");
/* harmony import */ var _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var vuex__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! vuex */ "./node_modules/vuex/dist/vuex.esm.js");


function ownKeys(object, enumerableOnly) { var keys = Object.keys(object); if (Object.getOwnPropertySymbols) { var symbols = Object.getOwnPropertySymbols(object); if (enumerableOnly) symbols = symbols.filter(function (sym) { return Object.getOwnPropertyDescriptor(object, sym).enumerable; }); keys.push.apply(keys, symbols); } return keys; }

function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; if (i % 2) { ownKeys(Object(source), true).forEach(function (key) { _defineProperty(target, key, source[key]); }); } else if (Object.getOwnPropertyDescriptors) { Object.defineProperties(target, Object.getOwnPropertyDescriptors(source)); } else { ownKeys(Object(source)).forEach(function (key) { Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key)); }); } } return target; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

function asyncGeneratorStep(gen, resolve, reject, _next, _throw, key, arg) { try { var info = gen[key](arg); var value = info.value; } catch (error) { reject(error); return; } if (info.done) { resolve(value); } else { Promise.resolve(value).then(_next, _throw); } }

function _asyncToGenerator(fn) { return function () { var self = this, args = arguments; return new Promise(function (resolve, reject) { var gen = fn.apply(self, args); function _next(value) { asyncGeneratorStep(gen, resolve, reject, _next, _throw, "next", value); } function _throw(err) { asyncGeneratorStep(gen, resolve, reject, _next, _throw, "throw", err); } _next(undefined); }); }; }

//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//


var AssignTask = function AssignTask() {
  return __webpack_require__.e(/*! import() */ 8).then(__webpack_require__.bind(null, /*! ./components/AssignTask.vue */ "./resources/js/src/views/Pages/web/Employee/components/AssignTask.vue"));
};

var Success = function Success() {
  return __webpack_require__.e(/*! import() */ 0).then(__webpack_require__.bind(null, /*! @views/Partials/web/popups/Success.vue */ "./resources/js/src/views/Partials/web/popups/Success.vue"));
};

/* harmony default export */ __webpack_exports__["default"] = ({
  created: function created() {
    var _this = this;

    return _asyncToGenerator( /*#__PURE__*/_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.mark(function _callee() {
      return _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.wrap(function _callee$(_context) {
        while (1) {
          switch (_context.prev = _context.next) {
            case 0:
              _context.next = 2;
              return _this.fetch();

            case 2:
              _this.fetch = _.debounce(_this.fetch, 500);

            case 3:
            case "end":
              return _context.stop();
          }
        }
      }, _callee);
    }))();
  },
  data: function data() {
    return {
      assignTask: 0,
      exclude: '',
      search: ''
    };
  },
  watch: {
    search: function search(val, oldVal) {
      this.fetch();
    }
  },
  components: {
    AssignTask: AssignTask,
    Success: Success
  },
  computed: _objectSpread({}, Object(vuex__WEBPACK_IMPORTED_MODULE_1__["mapState"])('employee', ['employees'])),
  methods: _objectSpread(_objectSpread({}, Object(vuex__WEBPACK_IMPORTED_MODULE_1__["mapActions"])('employee', ['getAll', 'delete'])), {}, {
    deleteEmployee: function deleteEmployee(employee) {
      var _this2 = this;

      return _asyncToGenerator( /*#__PURE__*/_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.mark(function _callee2() {
        var pending_tasks, id, params, _yield$_this2$delete, data;

        return _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.wrap(function _callee2$(_context2) {
          while (1) {
            switch (_context2.prev = _context2.next) {
              case 0:
                pending_tasks = employee.pending_tasks, id = employee.id; // alert(_total_tasks_pending);

                if (!(pending_tasks == 0)) {
                  _context2.next = 10;
                  break;
                }

                params = {
                  route: route('employee.destroy', {
                    employee: employee.id
                  }),
                  method: 'DELETE',
                  mutation: '',
                  data: {}
                };
                _context2.next = 5;
                return _this2["delete"](params);

              case 5:
                _yield$_this2$delete = _context2.sent;
                data = _yield$_this2$delete.data;

                if (data.status) {
                  _this2.$snotify.success(data.msg);
                }

                _context2.next = 13;
                break;

              case 10:
                _this2.exclude = employee.id;
                _this2.assignTask++;
                $('#confirm-popup').modal('show');

              case 13:
              case "end":
                return _context2.stop();
            }
          }
        }, _callee2);
      }))();
    },
    fetch: function fetch() {
      var _arguments = arguments,
          _this3 = this;

      return _asyncToGenerator( /*#__PURE__*/_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.mark(function _callee3() {
        var page, params;
        return _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.wrap(function _callee3$(_context3) {
          while (1) {
            switch (_context3.prev = _context3.next) {
              case 0:
                page = _arguments.length > 0 && _arguments[0] !== undefined ? _arguments[0] : 1;
                params = {
                  route: route('employee.index'),
                  data: {
                    page: page,
                    search: _this3.search
                  }
                };
                _context3.next = 4;
                return _this3.getAll(params);

              case 4:
              case "end":
                return _context3.stop();
            }
          }
        }, _callee3);
      }))();
    },
    popupClosed: function popupClosed() {
      this.assignTask++;
    },
    showEmployeesPopup: function showEmployeesPopup(el) {
      $(el).modal('hide');
      $('#assign-task').modal('show');
    }
  })
});

/***/ }),

/***/ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/src/views/Pages/web/Employee/Index.vue?vue&type=template&id=d5046bbe&":
/*!**************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/src/views/Pages/web/Employee/Index.vue?vue&type=template&id=d5046bbe& ***!
  \**************************************************************************************************************************************************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "render", function() { return render; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return staticRenderFns; });
var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c(
    "section",
    { staticClass: "list-employee" },
    [
      _c("div", { staticClass: "container" }, [
        _c("h1", [_vm._v("Employee List")]),
        _vm._v(" "),
        _c("hr"),
        _vm._v(" "),
        _c("div", { staticClass: "row" }, [
          _vm._m(0),
          _vm._v(" "),
          _c("div", { staticClass: "col-md-6" }, [
            _c(
              "div",
              { staticClass: "input-group md-form form-sm form-2 pl-0" },
              [
                _c("input", {
                  directives: [
                    {
                      name: "model",
                      rawName: "v-model",
                      value: _vm.search,
                      expression: "search"
                    }
                  ],
                  staticClass: "form-control my-0 py-1 lime-border",
                  attrs: {
                    type: "text",
                    placeholder: "Search",
                    "aria-label": "Search"
                  },
                  domProps: { value: _vm.search },
                  on: {
                    input: function($event) {
                      if ($event.target.composing) {
                        return
                      }
                      _vm.search = $event.target.value
                    }
                  }
                }),
                _vm._v(" "),
                _vm._m(1),
                _vm._v(" "),
                _c(
                  "router-link",
                  {
                    staticClass: "add-employees-btn",
                    attrs: { to: { name: "web.employees.create" } }
                  },
                  [
                    _c("i", { staticClass: "fas fa-plus-circle" }),
                    _vm._v(" ADD EMPLOYEE")
                  ]
                )
              ],
              1
            )
          ])
        ]),
        _vm._v(" "),
        _c(
          "div",
          { staticClass: "row" },
          [
            _vm._l(_vm.employees.data, function(employee) {
              return _c("div", { staticClass: "col-md-4" }, [
                _c("div", { staticClass: "card text-center" }, [
                  _c("div", { staticClass: "card-body" }, [
                    _c("div", { staticClass: "dropdown" }, [
                      _vm._m(2, true),
                      _vm._v(" "),
                      _c(
                        "div",
                        {
                          staticClass: "dropdown-menu",
                          attrs: { "aria-labelledby": "dropdownMenuLink" }
                        },
                        [
                          _c(
                            "router-link",
                            {
                              staticClass: "dropdown-item",
                              attrs: {
                                to: {
                                  name: "web.employees.show",
                                  params: { employee: employee.employee_id }
                                }
                              }
                            },
                            [
                              _c("i", { staticClass: "fas fa-eye" }),
                              _vm._v(" VIEW EMPLOYEE PROFILE")
                            ]
                          ),
                          _vm._v(" "),
                          _c(
                            "a",
                            {
                              staticClass: "dropdown-item",
                              attrs: { href: "javascript:void(0)" },
                              on: {
                                click: function($event) {
                                  return _vm.deleteEmployee(employee)
                                }
                              }
                            },
                            [
                              _c("i", { staticClass: "fas fa-trash" }),
                              _vm._v(" DELETE EMPLOYEE")
                            ]
                          ),
                          _vm._v(" "),
                          _c(
                            "router-link",
                            {
                              staticClass: "dropdown-item",
                              attrs: {
                                to: {
                                  name: "web.employees.tasks",
                                  params: { employee: employee.employee_id }
                                }
                              }
                            },
                            [
                              _c("i", {
                                staticClass: "fas fa-clipboard-check"
                              }),
                              _vm._v(" ASSIGNED TASK")
                            ]
                          )
                        ],
                        1
                      )
                    ]),
                    _vm._v(" "),
                    _c("img", {
                      attrs: { src: "assets/images/download.png", alt: "" }
                    }),
                    _vm._v(" "),
                    _c("h5", { staticClass: "card-title" }, [
                      _vm._v(_vm._s(employee.name))
                    ]),
                    _vm._v(" "),
                    _c("small", [_vm._v("Employee_Id " + _vm._s(employee.id))]),
                    _vm._v(" "),
                    _c("p", { staticClass: "card-text" }, [
                      _vm._v(
                        "Assigned Properties: " +
                          _vm._s(employee.properties_count || 0) +
                          " "
                      ),
                      _c("br"),
                      _vm._v(
                        " Assigned Tasks: " + _vm._s(employee.tasks_count || 0)
                      )
                    ])
                  ])
                ])
              ])
            }),
            _vm._v(" "),
            !_vm.$_.isUndefined(_vm.employees.data) &&
            _vm.employees.data.length == 0
              ? _c("div", { staticClass: "col-md-12 " }, [
                  _c("h3", { staticClass: "text-center" }, [
                    _vm._v("No Employee Found")
                  ])
                ])
              : _vm._e()
          ],
          2
        )
      ]),
      _vm._v(" "),
      _c("section", { staticClass: "pagination-box" }, [
        _c("div", { staticClass: "container" }, [
          _c("div", { staticClass: "row" }, [
            _c("div", { staticClass: "col-md-6" }),
            _vm._v(" "),
            _c(
              "div",
              { staticClass: "col-md-6" },
              [
                _c("pagination", {
                  attrs: { data: _vm.employees },
                  on: { "pagination-change-page": _vm.fetch }
                })
              ],
              1
            )
          ])
        ])
      ]),
      _vm._v(" "),
      _vm.exclude
        ? _c("assign-task", {
            key: _vm.assignTask,
            attrs: { exclude: _vm.exclude },
            on: { "task-assigned": _vm.fetch, closed: _vm.popupClosed }
          })
        : _vm._e(),
      _vm._v(" "),
      _c(
        "success",
        {
          attrs: {
            el: "confirm-popup",
            title: "System Message",
            subtitle:
              "Sorry! You won't be able to delete this employee until you assign his/her tasks to any other employee!"
          }
        },
        [
          _c("template", { slot: "footer" }, [
            _c(
              "a",
              {
                staticClass: "emplyee-add-btn",
                attrs: {
                  href: "javascript:void(0)",
                  "data-toggle": "modal",
                  "data-target": "#assign-task"
                },
                on: {
                  click: function($event) {
                    return _vm.showEmployeesPopup("#confirm-popup")
                  }
                }
              },
              [_vm._v("Assign Another employee")]
            ),
            _vm._v(" "),
            _c(
              "a",
              {
                staticClass: "emplyee-add-btn",
                attrs: { href: "javascript:void(0)", "data-dismiss": "modal" }
              },
              [_vm._v("Close")]
            )
          ])
        ],
        2
      )
    ],
    1
  )
}
var staticRenderFns = [
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("div", { staticClass: "col-md-6" }, [
      _c("small", [_vm._v("Total Employees: 123")])
    ])
  },
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("div", { staticClass: "input-group-append" }, [
      _c(
        "span",
        {
          staticClass: "input-group-text lime lighten-2",
          attrs: { id: "basic-text1" }
        },
        [
          _c("i", {
            staticClass: "fas fa-search text-grey",
            attrs: { "aria-hidden": "true" }
          })
        ]
      )
    ])
  },
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c(
      "a",
      {
        attrs: {
          href: "#",
          role: "button",
          id: "dropdownMenuLink",
          "data-toggle": "dropdown",
          "aria-haspopup": "true",
          "aria-expanded": "false"
        }
      },
      [_c("i", { staticClass: "fas fa-ellipsis-v" })]
    )
  }
]
render._withStripped = true



/***/ }),

/***/ "./resources/js/src/views/Pages/web/Employee/Index.vue":
/*!*************************************************************!*\
  !*** ./resources/js/src/views/Pages/web/Employee/Index.vue ***!
  \*************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _Index_vue_vue_type_template_id_d5046bbe___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./Index.vue?vue&type=template&id=d5046bbe& */ "./resources/js/src/views/Pages/web/Employee/Index.vue?vue&type=template&id=d5046bbe&");
/* harmony import */ var _Index_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./Index.vue?vue&type=script&lang=js& */ "./resources/js/src/views/Pages/web/Employee/Index.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport *//* harmony import */ var _node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../../../../../../node_modules/vue-loader/lib/runtime/componentNormalizer.js */ "./node_modules/vue-loader/lib/runtime/componentNormalizer.js");





/* normalize component */

var component = Object(_node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__["default"])(
  _Index_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__["default"],
  _Index_vue_vue_type_template_id_d5046bbe___WEBPACK_IMPORTED_MODULE_0__["render"],
  _Index_vue_vue_type_template_id_d5046bbe___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"],
  false,
  null,
  null,
  null
  
)

/* hot reload */
if (false) { var api; }
component.options.__file = "resources/js/src/views/Pages/web/Employee/Index.vue"
/* harmony default export */ __webpack_exports__["default"] = (component.exports);

/***/ }),

/***/ "./resources/js/src/views/Pages/web/Employee/Index.vue?vue&type=script&lang=js&":
/*!**************************************************************************************!*\
  !*** ./resources/js/src/views/Pages/web/Employee/Index.vue?vue&type=script&lang=js& ***!
  \**************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_Index_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../../../../node_modules/babel-loader/lib??ref--4-0!../../../../../../../node_modules/vue-loader/lib??vue-loader-options!./Index.vue?vue&type=script&lang=js& */ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/src/views/Pages/web/Employee/Index.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport */ /* harmony default export */ __webpack_exports__["default"] = (_node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_Index_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__["default"]); 

/***/ }),

/***/ "./resources/js/src/views/Pages/web/Employee/Index.vue?vue&type=template&id=d5046bbe&":
/*!********************************************************************************************!*\
  !*** ./resources/js/src/views/Pages/web/Employee/Index.vue?vue&type=template&id=d5046bbe& ***!
  \********************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_Index_vue_vue_type_template_id_d5046bbe___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../../../../node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!../../../../../../../node_modules/vue-loader/lib??vue-loader-options!./Index.vue?vue&type=template&id=d5046bbe& */ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/src/views/Pages/web/Employee/Index.vue?vue&type=template&id=d5046bbe&");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "render", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_Index_vue_vue_type_template_id_d5046bbe___WEBPACK_IMPORTED_MODULE_0__["render"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_Index_vue_vue_type_template_id_d5046bbe___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"]; });



/***/ })

}]);