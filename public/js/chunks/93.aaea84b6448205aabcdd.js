(window["webpackJsonp"] = window["webpackJsonp"] || []).push([[93],{

/***/ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/src/views/Pages/web/Task/Board.vue?vue&type=script&lang=js&":
/*!******************************************************************************************************************************************************************************!*\
  !*** ./node_modules/babel-loader/lib??ref--4-0!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/src/views/Pages/web/Task/Board.vue?vue&type=script&lang=js& ***!
  \******************************************************************************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @babel/runtime/regenerator */ "./node_modules/@babel/runtime/regenerator/index.js");
/* harmony import */ var _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var vuex__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! vuex */ "./node_modules/vuex/dist/vuex.esm.js");


function asyncGeneratorStep(gen, resolve, reject, _next, _throw, key, arg) { try { var info = gen[key](arg); var value = info.value; } catch (error) { reject(error); return; } if (info.done) { resolve(value); } else { Promise.resolve(value).then(_next, _throw); } }

function _asyncToGenerator(fn) { return function () { var self = this, args = arguments; return new Promise(function (resolve, reject) { var gen = fn.apply(self, args); function _next(value) { asyncGeneratorStep(gen, resolve, reject, _next, _throw, "next", value); } function _throw(err) { asyncGeneratorStep(gen, resolve, reject, _next, _throw, "throw", err); } _next(undefined); }); }; }

function ownKeys(object, enumerableOnly) { var keys = Object.keys(object); if (Object.getOwnPropertySymbols) { var symbols = Object.getOwnPropertySymbols(object); if (enumerableOnly) symbols = symbols.filter(function (sym) { return Object.getOwnPropertyDescriptor(object, sym).enumerable; }); keys.push.apply(keys, symbols); } return keys; }

function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; if (i % 2) { ownKeys(Object(source), true).forEach(function (key) { _defineProperty(target, key, source[key]); }); } else if (Object.getOwnPropertyDescriptors) { Object.defineProperties(target, Object.getOwnPropertyDescriptors(source)); } else { ownKeys(Object(source)).forEach(function (key) { Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key)); }); } } return target; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
var CreateTask = function CreateTask() {
  return Promise.all(/*! import() */[__webpack_require__.e(15), __webpack_require__.e(98)]).then(__webpack_require__.bind(null, /*! ./components/Create.vue */ "./resources/js/src/views/Pages/web/Task/components/Create.vue"));
};



var BoardView = function BoardView() {
  return __webpack_require__.e(/*! import() */ 96).then(__webpack_require__.bind(null, /*! ./components/BoardView.vue */ "./resources/js/src/views/Pages/web/Task/components/BoardView.vue"));
};

var CalendarView = function CalendarView() {
  return Promise.all(/*! import() */[__webpack_require__.e(14), __webpack_require__.e(97)]).then(__webpack_require__.bind(null, /*! ./components/CalendarView.vue */ "./resources/js/src/views/Pages/web/Task/components/CalendarView.vue"));
};

/* harmony default export */ __webpack_exports__["default"] = ({
  data: function data() {
    return {
      createPopup: 0,
      in_progress: 0,
      all_logs: 0,
      completed: 0,
      reRenderCalendar: 0,
      properties: [],
      selectedProperty: null,
      search: '',
      taskDate: ''
    };
  },
  components: {
    CreateTask: CreateTask,
    BoardView: BoardView,
    CalendarView: CalendarView
  },
  created: function created() {
    this.taskCreated = _.debounce(this.taskCreated, 500);
    this.fetchProperties();
  },
  computed: _objectSpread({}, Object(vuex__WEBPACK_IMPORTED_MODULE_1__["mapState"])('task', ['task_status', ''])),
  watch: {
    selectedProperty: function selectedProperty(val, oldVal) {
      this.taskCreated();
    },
    search: function search(val, oldVal) {
      this.updateAll();
    }
  },
  methods: _objectSpread(_objectSpread({}, Object(vuex__WEBPACK_IMPORTED_MODULE_1__["mapActions"])('property', ['getAll'])), {}, {
    fetchProperties: function fetchProperties() {
      var _this = this;

      return _asyncToGenerator( /*#__PURE__*/_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.mark(function _callee() {
        var params, _yield$_this$getAll, data;

        return _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.wrap(function _callee$(_context) {
          while (1) {
            switch (_context.prev = _context.next) {
              case 0:
                params = {
                  route: route('property.index'),
                  data: {}
                };
                _context.next = 3;
                return _this.getAll(params);

              case 3:
                _yield$_this$getAll = _context.sent;
                data = _yield$_this$getAll.data;
                _this.properties = data.properties;

              case 6:
              case "end":
                return _context.stop();
            }
          }
        }, _callee);
      }))();
    },
    setTaskDate: function setTaskDate(obj) {
      var _this2 = this;

      return _asyncToGenerator( /*#__PURE__*/_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.mark(function _callee2() {
        return _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.wrap(function _callee2$(_context2) {
          while (1) {
            switch (_context2.prev = _context2.next) {
              case 0:
                _context2.next = 2;
                return obj.date;

              case 2:
                _this2.taskDate = _context2.sent;
                // await this.createPopup++;
                $('#create-task').modal('show');
                setTimeout(function () {}, 1000); // this.showCreateTask();

              case 5:
              case "end":
                return _context2.stop();
            }
          }
        }, _callee2);
      }))();
    },
    showCreateTask: function showCreateTask() {
      $('#create-task').modal('show');
    },
    updateMe: function updateMe() {
      this.all_logs++;
      this.in_progress++;
      this.completed++;
    },
    updateAll: function updateAll() {
      if (this.routeParams.type == 'calendar') {
        this.reRenderCalendar++;
      } else {
        this.all_logs++;
        this.in_progress++;
        this.completed++;
      }
    },
    taskCreated: function taskCreated() {
      this.createPopup++;

      if (this.routeParams.type == 'calendar') {
        this.reRenderCalendar++;
      }

      if (this.task_status == '-1') {
        this.all_logs++;
      } else if (this.task_status == '0') {
        this.in_progress++;
      } else {
        this.completed++;
      }
    }
  })
});

/***/ }),

/***/ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/src/views/Pages/web/Task/Board.vue?vue&type=template&id=0d8c104c&":
/*!**********************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/src/views/Pages/web/Task/Board.vue?vue&type=template&id=0d8c104c& ***!
  \**********************************************************************************************************************************************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "render", function() { return render; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return staticRenderFns; });
var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c(
    "section",
    { staticClass: "board-box" },
    [
      _c(
        "div",
        { staticClass: "container" },
        [
          _c("h1", [_vm._v("Calendar")]),
          _vm._v(" "),
          _c("hr"),
          _vm._v(" "),
          _c("div", { staticClass: "row" }, [
            _c(
              "div",
              { staticClass: "col-md-6 mb-4" },
              [
                _c("div", { staticClass: "for-sidebar" }, [
                  _c("div", { staticClass: "dropdown" }, [
                    _vm._m(0),
                    _vm._v(" "),
                    _c(
                      "div",
                      {
                        staticClass: "dropdown-menu",
                        attrs: { "aria-labelledby": "dropdownMenuLink" }
                      },
                      [
                        _c("h4", [_vm._v("Assigned Properties")]),
                        _vm._v(" "),
                        _vm._l(_vm.properties, function(property) {
                          return _c(
                            "a",
                            {
                              staticClass: "dropdown-item",
                              attrs: { href: "javascript:void(0)" },
                              on: {
                                click: function($event) {
                                  _vm.selectedProperty = property.id
                                }
                              }
                            },
                            [_vm._v(_vm._s(property.name))]
                          )
                        })
                      ],
                      2
                    )
                  ])
                ]),
                _vm._v(" "),
                _c(
                  "router-link",
                  {
                    staticClass: "board-btn",
                    attrs: {
                      to: {
                        name: "web.tasks.board",
                        params: {
                          type: "board"
                        }
                      }
                    }
                  },
                  [_vm._v("BOARD")]
                ),
                _vm._v(" "),
                _c(
                  "router-link",
                  {
                    staticClass: "calendar-btn",
                    attrs: {
                      to: {
                        name: "web.tasks.board",
                        params: {
                          type: "calendar"
                        }
                      }
                    }
                  },
                  [_vm._v("CALENDAR")]
                )
              ],
              1
            ),
            _vm._v(" "),
            _c("div", { staticClass: "col-md-6" }, [
              _c(
                "div",
                { staticClass: "input-group md-form form-sm form-2 pl-0" },
                [
                  !_vm.isEmployee
                    ? _c(
                        "a",
                        {
                          staticClass: "add-task-btn",
                          attrs: { href: "javascript:void(0)" },
                          on: {
                            click: function($event) {
                              return _vm.showCreateTask()
                            }
                          }
                        },
                        [
                          _c("i", { staticClass: "fas fa-plus-circle" }),
                          _vm._v(" Add Task")
                        ]
                      )
                    : _vm._e(),
                  _vm._v(" "),
                  _c("input", {
                    directives: [
                      {
                        name: "model",
                        rawName: "v-model",
                        value: _vm.search,
                        expression: "search"
                      }
                    ],
                    staticClass: "form-control my-0 py-1 lime-border",
                    attrs: {
                      type: "text",
                      placeholder: "Search",
                      "aria-label": "Search"
                    },
                    domProps: { value: _vm.search },
                    on: {
                      input: function($event) {
                        if ($event.target.composing) {
                          return
                        }
                        _vm.search = $event.target.value
                      }
                    }
                  }),
                  _vm._v(" "),
                  _vm._m(1)
                ]
              )
            ])
          ]),
          _vm._v(" "),
          _c("span", [_vm._v("Last task completed on Friday")]),
          _vm._v(" "),
          _vm.routeParams.type == "board"
            ? _c("board-view", {
                attrs: {
                  search: _vm.search,
                  "in-progress-key": _vm.in_progress,
                  "all-logs-key": _vm.all_logs,
                  "completed-key": _vm.completed,
                  "selected-property": _vm.selectedProperty
                },
                on: { update: _vm.updateMe }
              })
            : _c("calendar-view", {
                key: _vm.reRenderCalendar,
                attrs: { "selected-property": _vm.selectedProperty },
                on: { "selected-date": _vm.setTaskDate }
              })
        ],
        1
      ),
      _vm._v(" "),
      !_vm.isEmployee
        ? _c("create-task", {
            key: _vm.createPopup,
            attrs: { taskDate: _vm.taskDate },
            on: { created: _vm.taskCreated }
          })
        : _vm._e()
    ],
    1
  )
}
var staticRenderFns = [
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c(
      "a",
      {
        attrs: {
          href: "#",
          role: "button",
          id: "dropdownMenuLink",
          "data-toggle": "dropdown",
          "aria-haspopup": "true",
          "aria-expanded": "false"
        }
      },
      [_c("i", { staticClass: "fas fa-sliders-h" })]
    )
  },
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("div", { staticClass: "input-group-append" }, [
      _c(
        "span",
        {
          staticClass: "input-group-text lime lighten-2",
          attrs: { id: "basic-text1" }
        },
        [
          _c("i", {
            staticClass: "fas fa-search text-grey",
            attrs: { "aria-hidden": "true" }
          })
        ]
      )
    ])
  }
]
render._withStripped = true



/***/ }),

/***/ "./resources/js/src/views/Pages/web/Task/Board.vue":
/*!*********************************************************!*\
  !*** ./resources/js/src/views/Pages/web/Task/Board.vue ***!
  \*********************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _Board_vue_vue_type_template_id_0d8c104c___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./Board.vue?vue&type=template&id=0d8c104c& */ "./resources/js/src/views/Pages/web/Task/Board.vue?vue&type=template&id=0d8c104c&");
/* harmony import */ var _Board_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./Board.vue?vue&type=script&lang=js& */ "./resources/js/src/views/Pages/web/Task/Board.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport *//* harmony import */ var _node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../../../../../../node_modules/vue-loader/lib/runtime/componentNormalizer.js */ "./node_modules/vue-loader/lib/runtime/componentNormalizer.js");





/* normalize component */

var component = Object(_node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__["default"])(
  _Board_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__["default"],
  _Board_vue_vue_type_template_id_0d8c104c___WEBPACK_IMPORTED_MODULE_0__["render"],
  _Board_vue_vue_type_template_id_0d8c104c___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"],
  false,
  null,
  null,
  null
  
)

/* hot reload */
if (false) { var api; }
component.options.__file = "resources/js/src/views/Pages/web/Task/Board.vue"
/* harmony default export */ __webpack_exports__["default"] = (component.exports);

/***/ }),

/***/ "./resources/js/src/views/Pages/web/Task/Board.vue?vue&type=script&lang=js&":
/*!**********************************************************************************!*\
  !*** ./resources/js/src/views/Pages/web/Task/Board.vue?vue&type=script&lang=js& ***!
  \**********************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_Board_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../../../../node_modules/babel-loader/lib??ref--4-0!../../../../../../../node_modules/vue-loader/lib??vue-loader-options!./Board.vue?vue&type=script&lang=js& */ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/src/views/Pages/web/Task/Board.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport */ /* harmony default export */ __webpack_exports__["default"] = (_node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_Board_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__["default"]); 

/***/ }),

/***/ "./resources/js/src/views/Pages/web/Task/Board.vue?vue&type=template&id=0d8c104c&":
/*!****************************************************************************************!*\
  !*** ./resources/js/src/views/Pages/web/Task/Board.vue?vue&type=template&id=0d8c104c& ***!
  \****************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_Board_vue_vue_type_template_id_0d8c104c___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../../../../node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!../../../../../../../node_modules/vue-loader/lib??vue-loader-options!./Board.vue?vue&type=template&id=0d8c104c& */ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/src/views/Pages/web/Task/Board.vue?vue&type=template&id=0d8c104c&");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "render", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_Board_vue_vue_type_template_id_0d8c104c___WEBPACK_IMPORTED_MODULE_0__["render"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_Board_vue_vue_type_template_id_0d8c104c___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"]; });



/***/ })

}]);