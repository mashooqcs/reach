(window["webpackJsonp"] = window["webpackJsonp"] || []).push([[57],{

/***/ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/src/views/Pages/admin/User/components/OwnerDetail.vue?vue&type=script&lang=js&":
/*!*************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/babel-loader/lib??ref--4-0!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/src/views/Pages/admin/User/components/OwnerDetail.vue?vue&type=script&lang=js& ***!
  \*************************************************************************************************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var vuex__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! vuex */ "./node_modules/vuex/dist/vuex.esm.js");
function ownKeys(object, enumerableOnly) { var keys = Object.keys(object); if (Object.getOwnPropertySymbols) { var symbols = Object.getOwnPropertySymbols(object); if (enumerableOnly) symbols = symbols.filter(function (sym) { return Object.getOwnPropertyDescriptor(object, sym).enumerable; }); keys.push.apply(keys, symbols); } return keys; }

function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; if (i % 2) { ownKeys(Object(source), true).forEach(function (key) { _defineProperty(target, key, source[key]); }); } else if (Object.getOwnPropertyDescriptors) { Object.defineProperties(target, Object.getOwnPropertyDescriptors(source)); } else { ownKeys(Object(source)).forEach(function (key) { Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key)); }); } } return target; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//

/* harmony default export */ __webpack_exports__["default"] = ({
  computed: _objectSpread({}, Object(vuex__WEBPACK_IMPORTED_MODULE_0__["mapState"])('admin', ['user']))
});

/***/ }),

/***/ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/src/views/Pages/admin/User/components/OwnerDetail.vue?vue&type=template&id=b5a5352c&":
/*!*****************************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/src/views/Pages/admin/User/components/OwnerDetail.vue?vue&type=template&id=b5a5352c& ***!
  \*****************************************************************************************************************************************************************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "render", function() { return render; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return staticRenderFns; });
var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c(
    "section",
    { staticClass: "search view-cause", attrs: { id: "configuration" } },
    [
      _c("div", { staticClass: "row" }, [
        _c("div", { staticClass: "col-12" }, [
          _c("div", { staticClass: "card pad-20" }, [
            _c("div", { staticClass: "card-content collapse show" }, [
              _c(
                "div",
                { staticClass: "card-body table-responsive card-dashboard" },
                [
                  _c(
                    "router-link",
                    {
                      attrs: {
                        to: {
                          name: "admin.users.index",
                          params: { type: "property-owner" }
                        }
                      }
                    },
                    [
                      _c("h2", [
                        _vm._v(
                          "\n                                < USER MANAGEMENT"
                        )
                      ])
                    ]
                  ),
                  _vm._v(" "),
                  _c("h3", { staticClass: "profile-heading" }, [
                    _vm._v("Property Owner Profile")
                  ]),
                  _vm._v(" "),
                  _c("img", {
                    attrs: {
                      src: _vm.user.media
                        ? _vm.user.media.url
                        : "assets/images/download.png",
                      alt: ""
                    }
                  }),
                  _vm._v(" "),
                  _c("div", { staticClass: "row" }, [
                    _c("div", { staticClass: "col-md-4" }, [
                      _c("p", [_vm._v("Full Name")]),
                      _vm._v(" "),
                      _c("p", [_vm._v(_vm._s(_vm.user.name))])
                    ]),
                    _vm._v(" "),
                    _c("div", { staticClass: "col-md-4" }, [
                      _c("p", [_vm._v("Email")]),
                      _vm._v(" "),
                      _c("p", [_vm._v(_vm._s(_vm.user.email))])
                    ]),
                    _vm._v(" "),
                    _c("div", { staticClass: "col-md-4" }, [
                      _c("p", [_vm._v("Phone")]),
                      _vm._v(" "),
                      _c("p", [_vm._v(_vm._s(_vm.user.phone || "NA"))])
                    ])
                  ]),
                  _vm._v(" "),
                  _c("div", { staticClass: "row" }, [
                    _c(
                      "div",
                      { staticClass: "col-xl-3 col-md-6 col-12 d-flex" },
                      [
                        _c("div", { staticClass: "card rounded w-50" }, [
                          _c("div", { staticClass: "card-content" }, [
                            _c("div", { staticClass: "card-body cleartfix" }, [
                              _c("div", { staticClass: "media" }, [
                                _vm._m(0),
                                _vm._v(" "),
                                _c(
                                  "div",
                                  { staticClass: "align-self-center" },
                                  [
                                    _c("h2", [
                                      _vm._v(
                                        _vm._s(_vm.user.employees_count) + " "
                                      ),
                                      _c("i", { staticClass: "fas fa-users" })
                                    ])
                                  ]
                                )
                              ])
                            ])
                          ])
                        ])
                      ]
                    ),
                    _vm._v(" "),
                    _c(
                      "div",
                      { staticClass: "col-xl-3 col-md-6 col-12 d-flex" },
                      [
                        _c("div", { staticClass: "card rounded w-50" }, [
                          _c("div", { staticClass: "card-content" }, [
                            _c("div", { staticClass: "card-body cleartfix" }, [
                              _c("div", { staticClass: "media" }, [
                                _vm._m(1),
                                _vm._v(" "),
                                _c(
                                  "div",
                                  { staticClass: "align-self-center" },
                                  [
                                    _c("h2", [
                                      _vm._v(
                                        _vm._s(_vm.user.properties_count) + " "
                                      ),
                                      _c("i", { staticClass: "fas fa-home" })
                                    ])
                                  ]
                                )
                              ])
                            ])
                          ])
                        ])
                      ]
                    ),
                    _vm._v(" "),
                    _c("div", {
                      staticClass: "col-xl-3 col-md-6 col-12 d-flex"
                    }),
                    _vm._v(" "),
                    _c("div", {
                      staticClass: "col-xl-3 col-md-6 col-12 d-flex"
                    })
                  ])
                ],
                1
              )
            ])
          ])
        ])
      ])
    ]
  )
}
var staticRenderFns = [
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("div", { staticClass: "media-body align-self-center" }, [
      _c("p", { staticClass: "m-0" }, [_vm._v("REGISTERED EMPLOYEES")])
    ])
  },
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("div", { staticClass: "media-body align-self-center" }, [
      _c("p", { staticClass: "m-0" }, [_vm._v("REGISTERED PROPERTIES")])
    ])
  }
]
render._withStripped = true



/***/ }),

/***/ "./resources/js/src/views/Pages/admin/User/components/OwnerDetail.vue":
/*!****************************************************************************!*\
  !*** ./resources/js/src/views/Pages/admin/User/components/OwnerDetail.vue ***!
  \****************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _OwnerDetail_vue_vue_type_template_id_b5a5352c___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./OwnerDetail.vue?vue&type=template&id=b5a5352c& */ "./resources/js/src/views/Pages/admin/User/components/OwnerDetail.vue?vue&type=template&id=b5a5352c&");
/* harmony import */ var _OwnerDetail_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./OwnerDetail.vue?vue&type=script&lang=js& */ "./resources/js/src/views/Pages/admin/User/components/OwnerDetail.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport *//* harmony import */ var _node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../../../../../../../node_modules/vue-loader/lib/runtime/componentNormalizer.js */ "./node_modules/vue-loader/lib/runtime/componentNormalizer.js");





/* normalize component */

var component = Object(_node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__["default"])(
  _OwnerDetail_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__["default"],
  _OwnerDetail_vue_vue_type_template_id_b5a5352c___WEBPACK_IMPORTED_MODULE_0__["render"],
  _OwnerDetail_vue_vue_type_template_id_b5a5352c___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"],
  false,
  null,
  null,
  null
  
)

/* hot reload */
if (false) { var api; }
component.options.__file = "resources/js/src/views/Pages/admin/User/components/OwnerDetail.vue"
/* harmony default export */ __webpack_exports__["default"] = (component.exports);

/***/ }),

/***/ "./resources/js/src/views/Pages/admin/User/components/OwnerDetail.vue?vue&type=script&lang=js&":
/*!*****************************************************************************************************!*\
  !*** ./resources/js/src/views/Pages/admin/User/components/OwnerDetail.vue?vue&type=script&lang=js& ***!
  \*****************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_OwnerDetail_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../../../../../node_modules/babel-loader/lib??ref--4-0!../../../../../../../../node_modules/vue-loader/lib??vue-loader-options!./OwnerDetail.vue?vue&type=script&lang=js& */ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/src/views/Pages/admin/User/components/OwnerDetail.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport */ /* harmony default export */ __webpack_exports__["default"] = (_node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_OwnerDetail_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__["default"]); 

/***/ }),

/***/ "./resources/js/src/views/Pages/admin/User/components/OwnerDetail.vue?vue&type=template&id=b5a5352c&":
/*!***********************************************************************************************************!*\
  !*** ./resources/js/src/views/Pages/admin/User/components/OwnerDetail.vue?vue&type=template&id=b5a5352c& ***!
  \***********************************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_OwnerDetail_vue_vue_type_template_id_b5a5352c___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../../../../../node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!../../../../../../../../node_modules/vue-loader/lib??vue-loader-options!./OwnerDetail.vue?vue&type=template&id=b5a5352c& */ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/src/views/Pages/admin/User/components/OwnerDetail.vue?vue&type=template&id=b5a5352c&");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "render", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_OwnerDetail_vue_vue_type_template_id_b5a5352c___WEBPACK_IMPORTED_MODULE_0__["render"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_OwnerDetail_vue_vue_type_template_id_b5a5352c___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"]; });



/***/ })

}]);