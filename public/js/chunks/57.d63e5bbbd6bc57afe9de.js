(window["webpackJsonp"] = window["webpackJsonp"] || []).push([[57],{

/***/ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/src/views/Pages/admin/Task/Show.vue?vue&type=script&lang=js&":
/*!*******************************************************************************************************************************************************************************!*\
  !*** ./node_modules/babel-loader/lib??ref--4-0!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/src/views/Pages/admin/Task/Show.vue?vue&type=script&lang=js& ***!
  \*******************************************************************************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @babel/runtime/regenerator */ "./node_modules/@babel/runtime/regenerator/index.js");
/* harmony import */ var _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var vuex__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! vuex */ "./node_modules/vuex/dist/vuex.esm.js");


function ownKeys(object, enumerableOnly) { var keys = Object.keys(object); if (Object.getOwnPropertySymbols) { var symbols = Object.getOwnPropertySymbols(object); if (enumerableOnly) symbols = symbols.filter(function (sym) { return Object.getOwnPropertyDescriptor(object, sym).enumerable; }); keys.push.apply(keys, symbols); } return keys; }

function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; if (i % 2) { ownKeys(Object(source), true).forEach(function (key) { _defineProperty(target, key, source[key]); }); } else if (Object.getOwnPropertyDescriptors) { Object.defineProperties(target, Object.getOwnPropertyDescriptors(source)); } else { ownKeys(Object(source)).forEach(function (key) { Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key)); }); } } return target; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

function asyncGeneratorStep(gen, resolve, reject, _next, _throw, key, arg) { try { var info = gen[key](arg); var value = info.value; } catch (error) { reject(error); return; } if (info.done) { resolve(value); } else { Promise.resolve(value).then(_next, _throw); } }

function _asyncToGenerator(fn) { return function () { var self = this, args = arguments; return new Promise(function (resolve, reject) { var gen = fn.apply(self, args); function _next(value) { asyncGeneratorStep(gen, resolve, reject, _next, _throw, "next", value); } function _throw(err) { asyncGeneratorStep(gen, resolve, reject, _next, _throw, "throw", err); } _next(undefined); }); }; }

//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//

/* harmony default export */ __webpack_exports__["default"] = ({
  created: function created() {
    var _this = this;

    return _asyncToGenerator( /*#__PURE__*/_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.mark(function _callee() {
      return _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.wrap(function _callee$(_context) {
        while (1) {
          switch (_context.prev = _context.next) {
            case 0:
              _context.next = 2;
              return _this.fetch();

            case 2:
            case "end":
              return _context.stop();
          }
        }
      }, _callee);
    }))();
  },
  computed: _objectSpread({}, Object(vuex__WEBPACK_IMPORTED_MODULE_1__["mapState"])('admin', ['task'])),
  methods: _objectSpread(_objectSpread({}, Object(vuex__WEBPACK_IMPORTED_MODULE_1__["mapActions"])('admin', ['get'])), {}, {
    fetch: function fetch() {
      var _this2 = this;

      return _asyncToGenerator( /*#__PURE__*/_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.mark(function _callee2() {
        var task, params;
        return _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.wrap(function _callee2$(_context2) {
          while (1) {
            switch (_context2.prev = _context2.next) {
              case 0:
                task = _this2.$route.params.task;
                params = {
                  route: route('admin.task.show', {
                    task: task
                  }),
                  mutation: 'SET_TASK',
                  variable: 'task',
                  data: {}
                };
                _context2.prev = 2;
                _context2.next = 5;
                return _this2.get(params);

              case 5:
                _context2.next = 11;
                break;

              case 7:
                _context2.prev = 7;
                _context2.t0 = _context2["catch"](2);

                // statements
                _this2.$router.push({
                  name: 'admin.404'
                });

                console.log(_context2.t0);

              case 11:
              case "end":
                return _context2.stop();
            }
          }
        }, _callee2, null, [[2, 7]]);
      }))();
    }
  })
});

/***/ }),

/***/ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/src/views/Pages/admin/Task/Show.vue?vue&type=template&id=e7f9e8fc&":
/*!***********************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/src/views/Pages/admin/Task/Show.vue?vue&type=template&id=e7f9e8fc& ***!
  \***********************************************************************************************************************************************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "render", function() { return render; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return staticRenderFns; });
var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c("div", { staticClass: "content-wrapper" }, [
    _c("div", { staticClass: "content-body" }, [
      _c(
        "section",
        { staticClass: "search view-cause", attrs: { id: "configuration" } },
        [
          _c("div", { staticClass: "row" }, [
            _c("div", { staticClass: "col-12" }, [
              _c("div", { staticClass: "card pad-20" }, [
                _c("div", { staticClass: "card-content collapse show" }, [
                  _c(
                    "div",
                    {
                      staticClass: "card-body table-responsive card-dashboard"
                    },
                    [
                      _c(
                        "router-link",
                        { attrs: { to: { name: "admin.logs.index" } } },
                        [
                          _c("h2", [
                            _vm._v(
                              "\n                                        < TASK DETAILS"
                            )
                          ])
                        ]
                      ),
                      _vm._v(" "),
                      _c("section", { staticClass: "task-log-completed-box" }, [
                        _c("div", {}, [
                          _vm._v(
                            "\n                                                <"
                          ),
                          _c("div", { staticClass: "row" }, [
                            _c("div", { staticClass: "col-md-2" }, [
                              _c("h6", [
                                _vm._v(
                                  "Emp Id " +
                                    _vm._s(
                                      _vm.task.task
                                        ? _vm.task.task.employee_id
                                        : "NA"
                                    )
                                )
                              ])
                            ]),
                            _vm._v(" "),
                            _c("div", { staticClass: "col-md-10" }, [
                              _c("h6", { staticClass: "for-floating" }, [
                                _vm._v("Task Id " + _vm._s(_vm.task.id))
                              ])
                            ]),
                            _vm._v(" "),
                            _vm._m(0),
                            _vm._v(" "),
                            _c("div", { staticClass: "col-md-10 mt-2" }, [
                              _c("span", { staticClass: "gray-color" }, [
                                _vm._v(
                                  _vm._s(
                                    _vm.task.task
                                      ? _vm.task.task.employee.name
                                      : "NA"
                                  )
                                )
                              ])
                            ]),
                            _vm._v(" "),
                            _vm._m(1),
                            _vm._v(" "),
                            _c("div", { staticClass: "col-md-10 mt-2" }, [
                              _c("span", { staticClass: "gray-color" }, [
                                _vm._v(_vm._s(_vm.task.name || "NA"))
                              ])
                            ]),
                            _vm._v(" "),
                            _vm._m(2),
                            _vm._v(" "),
                            _c("div", { staticClass: "col-md-10 mt-2" }, [
                              _vm.task.property
                                ? _c("span", { staticClass: "gray-color" }, [
                                    _c("i", {
                                      staticClass: "fas fa-user-circle"
                                    }),
                                    _vm._v(
                                      " " + _vm._s(_vm.task.property.owner.name)
                                    )
                                  ])
                                : _vm._e()
                            ]),
                            _vm._v(" "),
                            _vm._m(3),
                            _vm._v(" "),
                            _c("div", { staticClass: "col-md-10 mt-2" }, [
                              _c("span", { staticClass: "gray-color" }, [
                                _vm._v(
                                  _vm._s(_vm.formatDate(_vm.task.due_date))
                                )
                              ])
                            ]),
                            _vm._v(" "),
                            _vm._m(4),
                            _vm._v(" "),
                            _c("div", { staticClass: "col-md-10 mt-2" }, [
                              _c("span", { staticClass: "gray-color" }, [
                                _vm._v(
                                  _vm._s(_vm.task.start) +
                                    " To " +
                                    _vm._s(_vm.task.to)
                                )
                              ])
                            ]),
                            _vm._v(" "),
                            _vm._m(5),
                            _vm._v(" "),
                            _c("div", { staticClass: "col-md-10 mt-2" }, [
                              _vm.task.property
                                ? _c("span", { staticClass: "gray-color" }, [
                                    _vm._v(_vm._s(_vm.task.property.name))
                                  ])
                                : _vm._e()
                            ]),
                            _vm._v(" "),
                            _vm._m(6),
                            _vm._v(" "),
                            _c("div", { staticClass: "col-md-10 mt-2" }, [
                              _c("span", { staticClass: "gray-color" }, [
                                _vm._v(_vm._s(_vm.task.description || "NA"))
                              ])
                            ]),
                            _vm._v(" "),
                            _vm._m(7),
                            _vm._v(" "),
                            _c("div", { staticClass: "col-md-10 mt-2" }, [
                              _c("span", { staticClass: "gray-color" }, [
                                _vm._v(_vm._s(_vm.task.status_text))
                              ])
                            ])
                          ])
                        ])
                      ])
                    ],
                    1
                  )
                ])
              ])
            ])
          ])
        ]
      )
    ])
  ])
}
var staticRenderFns = [
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("div", { staticClass: "col-md-2 mt-2" }, [
      _c("h6", [_vm._v("Employee:")])
    ])
  },
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("div", { staticClass: "col-md-2 mt-2" }, [
      _c("h6", [_vm._v("Task Title:")])
    ])
  },
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("div", { staticClass: "col-md-2 mt-2" }, [
      _c("h6", [_vm._v("Assignee:")])
    ])
  },
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("div", { staticClass: "col-md-2 mt-2" }, [
      _c("h6", [_vm._v("Due Date:")])
    ])
  },
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("div", { staticClass: "col-md-2 mt-2" }, [
      _c("h6", [_vm._v("Time:")])
    ])
  },
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("div", { staticClass: "col-md-2 mt-2" }, [
      _c("h6", [_vm._v("Property:")])
    ])
  },
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("div", { staticClass: "col-md-2 mt-2" }, [
      _c("h6", [_vm._v("Description:")])
    ])
  },
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("div", { staticClass: "col-md-2 mt-2" }, [
      _c("h6", [_vm._v("Task Status:")])
    ])
  }
]
render._withStripped = true



/***/ }),

/***/ "./resources/js/src/views/Pages/admin/Task/Show.vue":
/*!**********************************************************!*\
  !*** ./resources/js/src/views/Pages/admin/Task/Show.vue ***!
  \**********************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _Show_vue_vue_type_template_id_e7f9e8fc___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./Show.vue?vue&type=template&id=e7f9e8fc& */ "./resources/js/src/views/Pages/admin/Task/Show.vue?vue&type=template&id=e7f9e8fc&");
/* harmony import */ var _Show_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./Show.vue?vue&type=script&lang=js& */ "./resources/js/src/views/Pages/admin/Task/Show.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport *//* harmony import */ var _node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../../../../../../node_modules/vue-loader/lib/runtime/componentNormalizer.js */ "./node_modules/vue-loader/lib/runtime/componentNormalizer.js");





/* normalize component */

var component = Object(_node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__["default"])(
  _Show_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__["default"],
  _Show_vue_vue_type_template_id_e7f9e8fc___WEBPACK_IMPORTED_MODULE_0__["render"],
  _Show_vue_vue_type_template_id_e7f9e8fc___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"],
  false,
  null,
  null,
  null
  
)

/* hot reload */
if (false) { var api; }
component.options.__file = "resources/js/src/views/Pages/admin/Task/Show.vue"
/* harmony default export */ __webpack_exports__["default"] = (component.exports);

/***/ }),

/***/ "./resources/js/src/views/Pages/admin/Task/Show.vue?vue&type=script&lang=js&":
/*!***********************************************************************************!*\
  !*** ./resources/js/src/views/Pages/admin/Task/Show.vue?vue&type=script&lang=js& ***!
  \***********************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_Show_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../../../../node_modules/babel-loader/lib??ref--4-0!../../../../../../../node_modules/vue-loader/lib??vue-loader-options!./Show.vue?vue&type=script&lang=js& */ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/src/views/Pages/admin/Task/Show.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport */ /* harmony default export */ __webpack_exports__["default"] = (_node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_Show_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__["default"]); 

/***/ }),

/***/ "./resources/js/src/views/Pages/admin/Task/Show.vue?vue&type=template&id=e7f9e8fc&":
/*!*****************************************************************************************!*\
  !*** ./resources/js/src/views/Pages/admin/Task/Show.vue?vue&type=template&id=e7f9e8fc& ***!
  \*****************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_Show_vue_vue_type_template_id_e7f9e8fc___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../../../../node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!../../../../../../../node_modules/vue-loader/lib??vue-loader-options!./Show.vue?vue&type=template&id=e7f9e8fc& */ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/src/views/Pages/admin/Task/Show.vue?vue&type=template&id=e7f9e8fc&");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "render", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_Show_vue_vue_type_template_id_e7f9e8fc___WEBPACK_IMPORTED_MODULE_0__["render"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_Show_vue_vue_type_template_id_e7f9e8fc___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"]; });



/***/ })

}]);