(window["webpackJsonp"] = window["webpackJsonp"] || []).push([[97],{

/***/ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/src/views/Pages/web/Task/components/CalendarView.vue?vue&type=script&lang=js&":
/*!************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/babel-loader/lib??ref--4-0!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/src/views/Pages/web/Task/components/CalendarView.vue?vue&type=script&lang=js& ***!
  \************************************************************************************************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @babel/runtime/regenerator */ "./node_modules/@babel/runtime/regenerator/index.js");
/* harmony import */ var _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var _fullcalendar_vue__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @fullcalendar/vue */ "./node_modules/@fullcalendar/vue/dist/main.js");
/* harmony import */ var _fullcalendar_daygrid__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @fullcalendar/daygrid */ "./node_modules/@fullcalendar/daygrid/main.js");
/* harmony import */ var _fullcalendar_interaction__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @fullcalendar/interaction */ "./node_modules/@fullcalendar/interaction/main.js");
/* harmony import */ var vuex__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! vuex */ "./node_modules/vuex/dist/vuex.esm.js");


function ownKeys(object, enumerableOnly) { var keys = Object.keys(object); if (Object.getOwnPropertySymbols) { var symbols = Object.getOwnPropertySymbols(object); if (enumerableOnly) symbols = symbols.filter(function (sym) { return Object.getOwnPropertyDescriptor(object, sym).enumerable; }); keys.push.apply(keys, symbols); } return keys; }

function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; if (i % 2) { ownKeys(Object(source), true).forEach(function (key) { _defineProperty(target, key, source[key]); }); } else if (Object.getOwnPropertyDescriptors) { Object.defineProperties(target, Object.getOwnPropertyDescriptors(source)); } else { ownKeys(Object(source)).forEach(function (key) { Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key)); }); } } return target; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

function asyncGeneratorStep(gen, resolve, reject, _next, _throw, key, arg) { try { var info = gen[key](arg); var value = info.value; } catch (error) { reject(error); return; } if (info.done) { resolve(value); } else { Promise.resolve(value).then(_next, _throw); } }

function _asyncToGenerator(fn) { return function () { var self = this, args = arguments; return new Promise(function (resolve, reject) { var gen = fn.apply(self, args); function _next(value) { asyncGeneratorStep(gen, resolve, reject, _next, _throw, "next", value); } function _throw(err) { asyncGeneratorStep(gen, resolve, reject, _next, _throw, "throw", err); } _next(undefined); }); }; }

//
//
//
//
//
//
//
//
//
//




var ViewTask = function ViewTask() {
  return __webpack_require__.e(/*! import() */ 10).then(__webpack_require__.bind(null, /*! ./View.vue */ "./resources/js/src/views/Pages/web/Task/components/View.vue"));
};


/* harmony default export */ __webpack_exports__["default"] = ({
  created: function created() {
    var _this = this;

    return _asyncToGenerator( /*#__PURE__*/_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.mark(function _callee() {
      return _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.wrap(function _callee$(_context) {
        while (1) {
          switch (_context.prev = _context.next) {
            case 0:
              _context.next = 2;
              return _this.fetch();

            case 2:
            case "end":
              return _context.stop();
          }
        }
      }, _callee);
    }))();
  },
  props: {
    title: {
      type: String,
      // String, Number, Boolean, Function, Object, Array
      required: false,
      "default": 'Task Log'
    },
    status: {
      type: String,
      // String, Number, Boolean, Function, Object, Array
      required: false,
      "default": ''
    },
    route: {
      type: String,
      // String, Number, Boolean, Function, Object, Array
      required: false,
      "default": 'task.calendar'
    },
    variable: {
      type: String,
      // String, Number, Boolean, Function, Object, Array
      required: false,
      "default": 'tasks'
    },
    mutation: {
      type: String,
      // String, Number, Boolean, Function, Object, Array
      required: false,
      "default": 'index'
    },
    property: {
      type: Number,
      // String, Number, Boolean, Function, Object, Array
      required: false,
      "default": null
    },
    search: {
      type: String,
      // String, Number, Boolean, Function, Object, Array
      required: false,
      "default": null
    },
    allLogsKey: {
      type: Number,
      "default": 0
    }
  },
  watch: {
    'renderId': function renderId(val, oldVal) {
      var calenderApi = this.$refs.calendar.getApi();
      console.log(calenderApi);
      this.fetch();
    }
  },
  computed: _objectSpread(_objectSpread({}, Object(vuex__WEBPACK_IMPORTED_MODULE_4__["mapState"])('task', ['task'])), {}, {
    tasks: function tasks() {
      var tasks = this.$store.state.task[this.variable];
      return tasks;
    }
  }),
  data: function data() {
    return {
      calendarOptions: {
        plugins: [_fullcalendar_daygrid__WEBPACK_IMPORTED_MODULE_2__["default"], _fullcalendar_interaction__WEBPACK_IMPORTED_MODULE_3__["default"]],
        initialView: 'dayGridMonth',
        events: [],
        dateClick: this.handleDateClick,
        eventClick: this.handleEventClick
      },
      from: '',
      to: '',
      counter: 0,
      calendarApi: undefined,
      renderId: 0
    };
  },
  methods: _objectSpread(_objectSpread(_objectSpread({}, Object(vuex__WEBPACK_IMPORTED_MODULE_4__["mapActions"])('task', ['getAll', 'get', 'delete'])), Object(vuex__WEBPACK_IMPORTED_MODULE_4__["mapMutations"])('task', ['set_status'])), {}, {
    fetch: function fetch() {
      var _arguments = arguments,
          _this2 = this;

      return _asyncToGenerator( /*#__PURE__*/_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.mark(function _callee2() {
        var page, params, _yield$_this2$getAll, data;

        return _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.wrap(function _callee2$(_context2) {
          while (1) {
            switch (_context2.prev = _context2.next) {
              case 0:
                page = _arguments.length > 0 && _arguments[0] !== undefined ? _arguments[0] : 1;
                params = {
                  route: route(_this2.route),
                  mutation: _this2.mutation,
                  data: {
                    status: _this2.status,
                    property_id: _this2.property,
                    search: _this2.search,
                    from: _this2.from,
                    to: _this2.to
                  }
                };
                _context2.next = 4;
                return _this2.getAll(params);

              case 4:
                _yield$_this2$getAll = _context2.sent;
                data = _yield$_this2$getAll.data;

                _this2.setEvents();

              case 7:
              case "end":
                return _context2.stop();
            }
          }
        }, _callee2);
      }))();
    },
    statusUpdated: function statusUpdated(data) {
      this.set_status(data.status);
      this.fetch();
    },
    monthChanged: function monthChanged(info) {
      var _this3 = this;

      return _asyncToGenerator( /*#__PURE__*/_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.mark(function _callee3() {
        return _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.wrap(function _callee3$(_context3) {
          while (1) {
            switch (_context3.prev = _context3.next) {
              case 0:
                _this3.from = _this3.formatDate(info.start, 'YYYY-MM-DD');
                _this3.to = _this3.formatDate(info.end, 'YYYY-MM-DD'); // await this.fetch();

                _this3.renderId++; // this.counter = 1;

                return _context3.abrupt("return");

              case 4:
              case "end":
                return _context3.stop();
            }
          }
        }, _callee3);
      }))();
    },
    handleDateClick: function handleDateClick(info) {
      console.log(info.dateStr);
      this.$emit('selected-date', {
        date: info.dateStr
      });
      $('#create-task').modal('show');
    },
    handleEventClick: function handleEventClick(info) {
      var id = info.event.id;
      this.previewTask(id);
    },
    previewTask: function previewTask(taskId) {
      var _this4 = this;

      return _asyncToGenerator( /*#__PURE__*/_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.mark(function _callee4() {
        var params;
        return _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.wrap(function _callee4$(_context4) {
          while (1) {
            switch (_context4.prev = _context4.next) {
              case 0:
                params = {
                  route: route('task.board-show', {
                    task: taskId
                  }),
                  data: {}
                };
                _context4.next = 3;
                return _this4.get(params);

              case 3:
                $('#view-task').modal('show');

              case 4:
              case "end":
                return _context4.stop();
            }
          }
        }, _callee4);
      }))();
    },
    setEvents: function setEvents() {
      var events = new Array();

      _.each(this.tasks, function (task, index) {
        events.push({
          id: task.id,
          title: task.name,
          start: task.due_date,
          end: task.due_date,
          backgroundColor: task.status == -1 ? 'transparent' : task.status == 0 ? 'grey' : '#52b68d',
          textColor: task.status == -1 ? 'black' : task.status == 0 ? 'white' : 'white',
          borderColor: task.status == -1 ? 'black' : task.status == 0 ? '#52b68d' : 'grey'
        });
      });

      this.calendarOptions.events = events; // console.log(this.calendarOptions.events);
      // console.log(events);
      // console.log('setEvents');
    }
  }),
  components: {
    FullCalendar: _fullcalendar_vue__WEBPACK_IMPORTED_MODULE_1__["default"],
    ViewTask: ViewTask
  }
});

/***/ }),

/***/ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/src/views/Pages/web/Task/components/CalendarView.vue?vue&type=template&id=363b7882&":
/*!****************************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/src/views/Pages/web/Task/components/CalendarView.vue?vue&type=template&id=363b7882& ***!
  \****************************************************************************************************************************************************************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "render", function() { return render; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return staticRenderFns; });
var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c(
    "div",
    { staticClass: "main-board" },
    [
      _c("div", { staticClass: "row" }, [
        _c(
          "div",
          { staticClass: "col-md-12" },
          [
            _c("FullCalendar", {
              ref: "calendar",
              attrs: { options: _vm.calendarOptions }
            })
          ],
          1
        )
      ]),
      _vm._v(" "),
      _c("view-task", {
        attrs: { task: _vm.task },
        on: { "status-change": _vm.statusUpdated }
      })
    ],
    1
  )
}
var staticRenderFns = []
render._withStripped = true



/***/ }),

/***/ "./resources/js/src/views/Pages/web/Task/components/CalendarView.vue":
/*!***************************************************************************!*\
  !*** ./resources/js/src/views/Pages/web/Task/components/CalendarView.vue ***!
  \***************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _CalendarView_vue_vue_type_template_id_363b7882___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./CalendarView.vue?vue&type=template&id=363b7882& */ "./resources/js/src/views/Pages/web/Task/components/CalendarView.vue?vue&type=template&id=363b7882&");
/* harmony import */ var _CalendarView_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./CalendarView.vue?vue&type=script&lang=js& */ "./resources/js/src/views/Pages/web/Task/components/CalendarView.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport *//* harmony import */ var _node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../../../../../../../node_modules/vue-loader/lib/runtime/componentNormalizer.js */ "./node_modules/vue-loader/lib/runtime/componentNormalizer.js");





/* normalize component */

var component = Object(_node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__["default"])(
  _CalendarView_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__["default"],
  _CalendarView_vue_vue_type_template_id_363b7882___WEBPACK_IMPORTED_MODULE_0__["render"],
  _CalendarView_vue_vue_type_template_id_363b7882___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"],
  false,
  null,
  null,
  null
  
)

/* hot reload */
if (false) { var api; }
component.options.__file = "resources/js/src/views/Pages/web/Task/components/CalendarView.vue"
/* harmony default export */ __webpack_exports__["default"] = (component.exports);

/***/ }),

/***/ "./resources/js/src/views/Pages/web/Task/components/CalendarView.vue?vue&type=script&lang=js&":
/*!****************************************************************************************************!*\
  !*** ./resources/js/src/views/Pages/web/Task/components/CalendarView.vue?vue&type=script&lang=js& ***!
  \****************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_CalendarView_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../../../../../node_modules/babel-loader/lib??ref--4-0!../../../../../../../../node_modules/vue-loader/lib??vue-loader-options!./CalendarView.vue?vue&type=script&lang=js& */ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/src/views/Pages/web/Task/components/CalendarView.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport */ /* harmony default export */ __webpack_exports__["default"] = (_node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_CalendarView_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__["default"]); 

/***/ }),

/***/ "./resources/js/src/views/Pages/web/Task/components/CalendarView.vue?vue&type=template&id=363b7882&":
/*!**********************************************************************************************************!*\
  !*** ./resources/js/src/views/Pages/web/Task/components/CalendarView.vue?vue&type=template&id=363b7882& ***!
  \**********************************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_CalendarView_vue_vue_type_template_id_363b7882___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../../../../../node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!../../../../../../../../node_modules/vue-loader/lib??vue-loader-options!./CalendarView.vue?vue&type=template&id=363b7882& */ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/src/views/Pages/web/Task/components/CalendarView.vue?vue&type=template&id=363b7882&");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "render", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_CalendarView_vue_vue_type_template_id_363b7882___WEBPACK_IMPORTED_MODULE_0__["render"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_CalendarView_vue_vue_type_template_id_363b7882___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"]; });



/***/ })

}]);