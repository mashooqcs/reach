(window["webpackJsonp"] = window["webpackJsonp"] || []).push([[8],{

/***/ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/src/views/Pages/web/Employee/components/AssignTask.vue?vue&type=script&lang=js&":
/*!**************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/babel-loader/lib??ref--4-0!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/src/views/Pages/web/Employee/components/AssignTask.vue?vue&type=script&lang=js& ***!
  \**************************************************************************************************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @babel/runtime/regenerator */ "./node_modules/@babel/runtime/regenerator/index.js");
/* harmony import */ var _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var vuex__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! vuex */ "./node_modules/vuex/dist/vuex.esm.js");


function asyncGeneratorStep(gen, resolve, reject, _next, _throw, key, arg) { try { var info = gen[key](arg); var value = info.value; } catch (error) { reject(error); return; } if (info.done) { resolve(value); } else { Promise.resolve(value).then(_next, _throw); } }

function _asyncToGenerator(fn) { return function () { var self = this, args = arguments; return new Promise(function (resolve, reject) { var gen = fn.apply(self, args); function _next(value) { asyncGeneratorStep(gen, resolve, reject, _next, _throw, "next", value); } function _throw(err) { asyncGeneratorStep(gen, resolve, reject, _next, _throw, "throw", err); } _next(undefined); }); }; }

function ownKeys(object, enumerableOnly) { var keys = Object.keys(object); if (Object.getOwnPropertySymbols) { var symbols = Object.getOwnPropertySymbols(object); if (enumerableOnly) symbols = symbols.filter(function (sym) { return Object.getOwnPropertyDescriptor(object, sym).enumerable; }); keys.push.apply(keys, symbols); } return keys; }

function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; if (i % 2) { ownKeys(Object(source), true).forEach(function (key) { _defineProperty(target, key, source[key]); }); } else if (Object.getOwnPropertyDescriptors) { Object.defineProperties(target, Object.getOwnPropertyDescriptors(source)); } else { ownKeys(Object(source)).forEach(function (key) { Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key)); }); } } return target; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//


var success = function success() {
  return __webpack_require__.e(/*! import() */ 0).then(__webpack_require__.bind(null, /*! @views/Partials/web/popups/Success.vue */ "./resources/js/src/views/Partials/web/popups/Success.vue"));
};

/* harmony default export */ __webpack_exports__["default"] = ({
  props: {
    exclude: {
      type: Number,
      required: true,
      "default": null
    },
    property: {
      type: Number,
      required: false,
      "default": null
    }
  },
  data: function data() {
    return {
      properties: {},
      selectedProperty: 0,
      assignTo: ''
    };
  },
  components: {
    success: success
  },
  computed: _objectSpread({}, Object(vuex__WEBPACK_IMPORTED_MODULE_1__["mapState"])('employee', ['all_employees'])),
  created: function created() {
    this.fetch();
  },
  mounted: function mounted() {
    this.$nextTick(function () {});
  },
  methods: _objectSpread(_objectSpread({}, Object(vuex__WEBPACK_IMPORTED_MODULE_1__["mapActions"])('employee', ['getAll', 'store'])), {}, {
    fetch: function fetch() {
      var _arguments = arguments,
          _this = this;

      return _asyncToGenerator( /*#__PURE__*/_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.mark(function _callee() {
        var page, employee, params, _yield$_this$getAll, data;

        return _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.wrap(function _callee$(_context) {
          while (1) {
            switch (_context.prev = _context.next) {
              case 0:
                page = _arguments.length > 0 && _arguments[0] !== undefined ? _arguments[0] : 1;
                employee = _this.$route.params.employee;
                params = {
                  route: route('employee.index'),
                  mutation: 'set_employees',
                  data: {
                    page: page,
                    exclude: _this.exclude
                  }
                };
                _context.next = 5;
                return _this.getAll(params);

              case 5:
                _yield$_this$getAll = _context.sent;
                data = _yield$_this$getAll.data;
                _this.properties = data.properties;

              case 8:
              case "end":
                return _context.stop();
            }
          }
        }, _callee);
      }))();
    },
    assign: function assign() {
      var _this2 = this;

      return _asyncToGenerator( /*#__PURE__*/_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.mark(function _callee2() {
        var employee, fd, params, _yield$_this2$store, data, employeeId, index;

        return _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.wrap(function _callee2$(_context2) {
          while (1) {
            switch (_context2.prev = _context2.next) {
              case 0:
                employee = _this2.$route.params.employee;

                if (!(_this2.assignTo == '')) {
                  _context2.next = 4;
                  break;
                }

                _this2.$snotify.error('please select a Employee first');

                return _context2.abrupt("return");

              case 4:
                fd = new FormData();
                fd.append('assign_to', _this2.assignTo);
                fd.append('employee', _this2.exclude);
                if (_this2.property) fd.append('property', _this2.property);
                params = {
                  route: route('task.handover'),
                  data: fd,
                  method: 'POST'
                };
                _context2.next = 11;
                return _this2.store(params);

              case 11:
                _yield$_this2$store = _context2.sent;
                data = _yield$_this2$store.data;

                if (data.status) {
                  window.$('#assign-task').modal('hide');
                  employeeId = parseInt(_this2.assignTo);
                  index = _.findIndex(_this2.all_employees.data, function (employee) {
                    return employee.id == employeeId;
                  }); // let property = this.properties.data[index];

                  _this2.$emit('task-assigned');

                  _this2.all_employees.data.splice(index, 1);
                }

              case 14:
              case "end":
                return _context2.stop();
            }
          }
        }, _callee2);
      }))();
    }
  })
});

/***/ }),

/***/ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/src/views/Pages/web/Employee/components/AssignTask.vue?vue&type=template&id=229299bc&":
/*!******************************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/src/views/Pages/web/Employee/components/AssignTask.vue?vue&type=template&id=229299bc& ***!
  \******************************************************************************************************************************************************************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "render", function() { return render; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return staticRenderFns; });
var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c(
    "div",
    {
      staticClass: "modal fade",
      attrs: {
        id: "assign-task",
        tabindex: "-1",
        role: "dialog",
        "aria-labelledby": "exampleModalCenterTitle",
        "aria-hidden": "true"
      }
    },
    [
      _c(
        "div",
        {
          staticClass: "modal-dialog modal-dialog-centered",
          attrs: { role: "document" }
        },
        [
          _c(
            "div",
            { staticClass: "modal-content" },
            [
              _c("div", { staticClass: "my-modal-header" }, [
                _vm._m(0),
                _vm._v(" "),
                !_vm.$_.isUndefined(_vm.all_employees.data) &&
                _vm.all_employees.data.length > 0
                  ? _c(
                      "h4",
                      {
                        staticClass: "modal-title",
                        attrs: { id: "exampleModalLongTitle" }
                      },
                      [_vm._v("Assign Employee")]
                    )
                  : _vm._e(),
                _vm._v(" "),
                !_vm.$_.isUndefined(_vm.all_employees.data) &&
                _vm.all_employees.data.length > 0
                  ? _c(
                      "div",
                      {
                        staticClass:
                          "input-group md-form form-sm form-2 pl-0 my-search"
                      },
                      [
                        _c("input", {
                          staticClass: "form-control my-0 py-1 lime-border",
                          attrs: {
                            type: "text",
                            placeholder: "Search",
                            "aria-label": "Search"
                          }
                        }),
                        _vm._v(" "),
                        _vm._m(1)
                      ]
                    )
                  : _vm._e()
              ]),
              _vm._v(" "),
              _c("ValidationObserver", {
                ref: "assignemployeeObserver",
                attrs: { tag: "div" },
                scopedSlots: _vm._u([
                  {
                    key: "default",
                    fn: function(ref) {
                      var handleSubmit = ref.handleSubmit
                      return [
                        _c(
                          "form",
                          {
                            on: {
                              submit: function($event) {
                                $event.preventDefault()
                                return handleSubmit(_vm.assign)
                              }
                            }
                          },
                          [
                            _c("div", { staticClass: "modal-body" }, [
                              !_vm.$_.isUndefined(_vm.all_employees.data)
                                ? _c(
                                    "ul",
                                    { staticClass: "list-unstyled" },
                                    [
                                      _vm._l(_vm.all_employees.data, function(
                                        employee
                                      ) {
                                        return _c(
                                          "li",
                                          {
                                            key: employee.id,
                                            staticClass: "media"
                                          },
                                          [
                                            _c(
                                              "a",
                                              {
                                                attrs: {
                                                  href: "#",
                                                  "data-toggle": "modal",
                                                  "data-target":
                                                    "#delete-employee"
                                                }
                                              },
                                              [
                                                _c("i", {
                                                  staticClass:
                                                    "fas fa-times-circle"
                                                })
                                              ]
                                            ),
                                            _vm._v(" "),
                                            _c("img", {
                                              staticClass: "mr-3 w-25",
                                              attrs: {
                                                src:
                                                  "assets/images/download.png",
                                                alt: "...",
                                                width: "14%"
                                              }
                                            }),
                                            _vm._v(" "),
                                            _c(
                                              "div",
                                              { staticClass: "media-body" },
                                              [
                                                _c(
                                                  "h6",
                                                  { staticClass: "mt-0 mb-1" },
                                                  [
                                                    _vm._v(
                                                      _vm._s(employee.name)
                                                    )
                                                  ]
                                                ),
                                                _vm._v(" "),
                                                _c(
                                                  "h6",
                                                  { staticClass: "mt-0 mb-1" },
                                                  [
                                                    _vm._v(
                                                      "Employee_Id " +
                                                        _vm._s(employee.id)
                                                    )
                                                  ]
                                                ),
                                                _vm._v(" "),
                                                _c(
                                                  "span",
                                                  {
                                                    staticClass: "modal-gr-clr"
                                                  },
                                                  [
                                                    _vm._v(
                                                      "Assigned Properties: " +
                                                        _vm._s(
                                                          employee.properties_count
                                                        ) +
                                                        ", Assigned Tasks: " +
                                                        _vm._s(
                                                          employee.pending_tasks
                                                        )
                                                    )
                                                  ]
                                                ),
                                                _vm._v(" "),
                                                _c("ValidationProvider", {
                                                  staticClass:
                                                    "custom-control custom-radio",
                                                  attrs: {
                                                    rules: "required",
                                                    name: "Employee"
                                                  },
                                                  scopedSlots: _vm._u(
                                                    [
                                                      {
                                                        key: "default",
                                                        fn: function(ref) {
                                                          var errors =
                                                            ref.errors
                                                          return [
                                                            _c("input", {
                                                              directives: [
                                                                {
                                                                  name: "model",
                                                                  rawName:
                                                                    "v-model",
                                                                  value:
                                                                    _vm.assignTo,
                                                                  expression:
                                                                    "assignTo"
                                                                }
                                                              ],
                                                              staticClass:
                                                                "custom-control-input",
                                                              attrs: {
                                                                type: "radio",
                                                                id:
                                                                  "employee" +
                                                                  employee.id,
                                                                name: "employee"
                                                              },
                                                              domProps: {
                                                                value:
                                                                  employee.id,
                                                                checked: _vm._q(
                                                                  _vm.assignTo,
                                                                  employee.id
                                                                )
                                                              },
                                                              on: {
                                                                change: function(
                                                                  $event
                                                                ) {
                                                                  _vm.assignTo =
                                                                    employee.id
                                                                }
                                                              }
                                                            }),
                                                            _vm._v(" "),
                                                            _c("label", {
                                                              staticClass:
                                                                "custom-control-label",
                                                              attrs: {
                                                                for:
                                                                  "employee" +
                                                                  employee.id
                                                              }
                                                            }),
                                                            _vm._v(" "),
                                                            _c("span", [
                                                              _vm._v(
                                                                _vm._s(
                                                                  errors[0]
                                                                )
                                                              )
                                                            ])
                                                          ]
                                                        }
                                                      }
                                                    ],
                                                    null,
                                                    true
                                                  )
                                                })
                                              ],
                                              1
                                            )
                                          ]
                                        )
                                      }),
                                      _vm._v(" "),
                                      !_vm.$_.isUndefined(
                                        _vm.all_employees.data
                                      ) && _vm.all_employees.data.length == 0
                                        ? _c("li", [
                                            _c(
                                              "h4",
                                              { staticClass: "text-center" },
                                              [
                                                _vm._v(
                                                  "Another employee not found"
                                                )
                                              ]
                                            )
                                          ])
                                        : _vm._e()
                                    ],
                                    2
                                  )
                                : _vm._e()
                            ]),
                            _vm._v(" "),
                            !_vm.$_.isUndefined(_vm.all_employees.data) &&
                            _vm.all_employees.data.length > 0
                              ? _c("div", { staticClass: "modal-footer" }, [
                                  _c(
                                    "button",
                                    {
                                      staticClass: "modal-add-btn",
                                      attrs: {
                                        type: "submit",
                                        "data-toggle": "modal",
                                        "data-target": "#exampleModal"
                                      }
                                    },
                                    [_vm._v("Assign")]
                                  )
                                ])
                              : _vm._e()
                          ]
                        )
                      ]
                    }
                  }
                ])
              })
            ],
            1
          )
        ]
      )
    ]
  )
}
var staticRenderFns = [
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c(
      "button",
      {
        staticClass: "close",
        attrs: {
          type: "button",
          "data-dismiss": "modal",
          "aria-label": "Close"
        }
      },
      [_c("span", { attrs: { "aria-hidden": "true" } }, [_vm._v("×")])]
    )
  },
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("div", { staticClass: "input-group-append" }, [
      _c(
        "span",
        {
          staticClass: "input-group-text lime lighten-2",
          attrs: { id: "basic-text1" }
        },
        [
          _c("i", {
            staticClass: "fas fa-search text-grey",
            attrs: { "aria-hidden": "true" }
          })
        ]
      )
    ])
  }
]
render._withStripped = true



/***/ }),

/***/ "./resources/js/src/views/Pages/web/Employee/components/AssignTask.vue":
/*!*****************************************************************************!*\
  !*** ./resources/js/src/views/Pages/web/Employee/components/AssignTask.vue ***!
  \*****************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _AssignTask_vue_vue_type_template_id_229299bc___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./AssignTask.vue?vue&type=template&id=229299bc& */ "./resources/js/src/views/Pages/web/Employee/components/AssignTask.vue?vue&type=template&id=229299bc&");
/* harmony import */ var _AssignTask_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./AssignTask.vue?vue&type=script&lang=js& */ "./resources/js/src/views/Pages/web/Employee/components/AssignTask.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport *//* harmony import */ var _node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../../../../../../../node_modules/vue-loader/lib/runtime/componentNormalizer.js */ "./node_modules/vue-loader/lib/runtime/componentNormalizer.js");





/* normalize component */

var component = Object(_node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__["default"])(
  _AssignTask_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__["default"],
  _AssignTask_vue_vue_type_template_id_229299bc___WEBPACK_IMPORTED_MODULE_0__["render"],
  _AssignTask_vue_vue_type_template_id_229299bc___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"],
  false,
  null,
  null,
  null
  
)

/* hot reload */
if (false) { var api; }
component.options.__file = "resources/js/src/views/Pages/web/Employee/components/AssignTask.vue"
/* harmony default export */ __webpack_exports__["default"] = (component.exports);

/***/ }),

/***/ "./resources/js/src/views/Pages/web/Employee/components/AssignTask.vue?vue&type=script&lang=js&":
/*!******************************************************************************************************!*\
  !*** ./resources/js/src/views/Pages/web/Employee/components/AssignTask.vue?vue&type=script&lang=js& ***!
  \******************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_AssignTask_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../../../../../node_modules/babel-loader/lib??ref--4-0!../../../../../../../../node_modules/vue-loader/lib??vue-loader-options!./AssignTask.vue?vue&type=script&lang=js& */ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/src/views/Pages/web/Employee/components/AssignTask.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport */ /* harmony default export */ __webpack_exports__["default"] = (_node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_AssignTask_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__["default"]); 

/***/ }),

/***/ "./resources/js/src/views/Pages/web/Employee/components/AssignTask.vue?vue&type=template&id=229299bc&":
/*!************************************************************************************************************!*\
  !*** ./resources/js/src/views/Pages/web/Employee/components/AssignTask.vue?vue&type=template&id=229299bc& ***!
  \************************************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_AssignTask_vue_vue_type_template_id_229299bc___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../../../../../node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!../../../../../../../../node_modules/vue-loader/lib??vue-loader-options!./AssignTask.vue?vue&type=template&id=229299bc& */ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/src/views/Pages/web/Employee/components/AssignTask.vue?vue&type=template&id=229299bc&");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "render", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_AssignTask_vue_vue_type_template_id_229299bc___WEBPACK_IMPORTED_MODULE_0__["render"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_AssignTask_vue_vue_type_template_id_229299bc___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"]; });



/***/ })

}]);