(window["webpackJsonp"] = window["webpackJsonp"] || []).push([[2],{

/***/ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/src/views/Pages/web/Task/components/Logs.vue?vue&type=script&lang=js&":
/*!****************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/babel-loader/lib??ref--4-0!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/src/views/Pages/web/Task/components/Logs.vue?vue&type=script&lang=js& ***!
  \****************************************************************************************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @babel/runtime/regenerator */ "./node_modules/@babel/runtime/regenerator/index.js");
/* harmony import */ var _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var vuex__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! vuex */ "./node_modules/vuex/dist/vuex.esm.js");
/* harmony import */ var vuedraggable__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! vuedraggable */ "./node_modules/vuedraggable/dist/vuedraggable.umd.js");
/* harmony import */ var vuedraggable__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(vuedraggable__WEBPACK_IMPORTED_MODULE_2__);


function asyncGeneratorStep(gen, resolve, reject, _next, _throw, key, arg) { try { var info = gen[key](arg); var value = info.value; } catch (error) { reject(error); return; } if (info.done) { resolve(value); } else { Promise.resolve(value).then(_next, _throw); } }

function _asyncToGenerator(fn) { return function () { var self = this, args = arguments; return new Promise(function (resolve, reject) { var gen = fn.apply(self, args); function _next(value) { asyncGeneratorStep(gen, resolve, reject, _next, _throw, "next", value); } function _throw(err) { asyncGeneratorStep(gen, resolve, reject, _next, _throw, "throw", err); } _next(undefined); }); }; }

function ownKeys(object, enumerableOnly) { var keys = Object.keys(object); if (Object.getOwnPropertySymbols) { var symbols = Object.getOwnPropertySymbols(object); if (enumerableOnly) symbols = symbols.filter(function (sym) { return Object.getOwnPropertyDescriptor(object, sym).enumerable; }); keys.push.apply(keys, symbols); } return keys; }

function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; if (i % 2) { ownKeys(Object(source), true).forEach(function (key) { _defineProperty(target, key, source[key]); }); } else if (Object.getOwnPropertyDescriptors) { Object.defineProperties(target, Object.getOwnPropertyDescriptors(source)); } else { ownKeys(Object(source)).forEach(function (key) { Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key)); }); } } return target; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//


var DeleteTaskConfirm = function DeleteTaskConfirm() {
  return __webpack_require__.e(/*! import() */ 0).then(__webpack_require__.bind(null, /*! @views/Partials/web/popups/Success.vue */ "./resources/js/src/views/Partials/web/popups/Success.vue"));
};

var ViewTask = function ViewTask() {
  return __webpack_require__.e(/*! import() */ 10).then(__webpack_require__.bind(null, /*! ./View.vue */ "./resources/js/src/views/Pages/web/Task/components/View.vue"));
};

var ReportTask = function ReportTask() {
  return __webpack_require__.e(/*! import() */ 100).then(__webpack_require__.bind(null, /*! ./Report.vue */ "./resources/js/src/views/Pages/web/Task/components/Report.vue"));
};


/* harmony default export */ __webpack_exports__["default"] = ({
  props: {
    title: {
      type: String,
      // String, Number, Boolean, Function, Object, Array
      required: false,
      "default": 'Task Log'
    },
    status: {
      type: String,
      // String, Number, Boolean, Function, Object, Array
      required: false,
      "default": ''
    },
    route: {
      type: String,
      // String, Number, Boolean, Function, Object, Array
      required: false,
      "default": 'task.board'
    },
    variable: {
      type: String,
      // String, Number, Boolean, Function, Object, Array
      required: false,
      "default": 'tasks'
    },
    mutation: {
      type: String,
      // String, Number, Boolean, Function, Object, Array
      required: false,
      "default": 'index'
    },
    property: {
      type: Number,
      // String, Number, Boolean, Function, Object, Array
      required: false,
      "default": null
    },
    search: {
      type: String,
      // String, Number, Boolean, Function, Object, Array
      required: false,
      "default": null
    }
  },
  data: function data() {
    return {
      taskId: 0,
      newStatus: null,
      reportPreview: 0
    };
  },
  computed: _objectSpread(_objectSpread({}, Object(vuex__WEBPACK_IMPORTED_MODULE_1__["mapState"])('task', ['task'])), {}, {
    tasks: function tasks() {
      var tasks = this.$store.state.task[this.variable];
      return tasks;
    },
    dragOptions: function dragOptions() {
      return {
        animation: 300,
        group: "description",
        disabled: false,
        ghostClass: "ghost"
      };
    }
  }),
  created: function created() {
    var _this = this;

    return _asyncToGenerator( /*#__PURE__*/_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.mark(function _callee() {
      return _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.wrap(function _callee$(_context) {
        while (1) {
          switch (_context.prev = _context.next) {
            case 0:
              _context.next = 2;
              return _this.fetch();

            case 2:
            case "end":
              return _context.stop();
          }
        }
      }, _callee);
    }))();
  },
  components: {
    DeleteTaskConfirm: DeleteTaskConfirm,
    ViewTask: ViewTask,
    ReportTask: ReportTask,
    draggable: vuedraggable__WEBPACK_IMPORTED_MODULE_2___default.a
  },
  watch: {
    taskId: function taskId(val, oldVal) {
      var _this2 = this;

      return _asyncToGenerator( /*#__PURE__*/_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.mark(function _callee2() {
        return _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.wrap(function _callee2$(_context2) {
          while (1) {
            switch (_context2.prev = _context2.next) {
              case 0:
                _this2.updateStatus(val);

              case 1:
              case "end":
                return _context2.stop();
            }
          }
        }, _callee2);
      }))();
    }
  },
  methods: _objectSpread(_objectSpread(_objectSpread({}, Object(vuex__WEBPACK_IMPORTED_MODULE_1__["mapActions"])('task', ['getAll', 'get', 'delete', 'store'])), Object(vuex__WEBPACK_IMPORTED_MODULE_1__["mapMutations"])('task', ['set_task_status', 'set_status'])), {}, {
    handleEnd: function handleEnd(info) {// console.log(id);
    },
    handleDrag: function handleDrag(e, title) {
      var _this3 = this;

      return _asyncToGenerator( /*#__PURE__*/_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.mark(function _callee3() {
        return _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.wrap(function _callee3$(_context3) {
          while (1) {
            switch (_context3.prev = _context3.next) {
              case 0:
                // let newStatus;
                _this3.taskId = e.item.getAttribute('id');

                if (title == 'tasks') {
                  _this3.newStatus = -1;
                } else if (title == 'in_progress') {
                  _this3.newStatus = 0;
                } else {
                  _this3.newStatus = 1;
                }

              case 2:
              case "end":
                return _context3.stop();
            }
          }
        }, _callee3);
      }))();
    },
    statusUpdated: function statusUpdated(data) {
      this.set_status(data.status);
      $('#view-task').modal('hide');
      this.$emit('update');
    },
    fetch: function fetch() {
      var _arguments = arguments,
          _this4 = this;

      return _asyncToGenerator( /*#__PURE__*/_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.mark(function _callee4() {
        var page, params, _yield$_this4$getAll, data;

        return _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.wrap(function _callee4$(_context4) {
          while (1) {
            switch (_context4.prev = _context4.next) {
              case 0:
                page = _arguments.length > 0 && _arguments[0] !== undefined ? _arguments[0] : 1;
                params = {
                  route: route(_this4.route),
                  mutation: _this4.mutation,
                  data: {
                    page: page,
                    status: _this4.status,
                    property_id: _this4.property,
                    search: _this4.search
                  }
                };
                _context4.next = 4;
                return _this4.getAll(params);

              case 4:
                _yield$_this4$getAll = _context4.sent;
                data = _yield$_this4$getAll.data;

              case 6:
              case "end":
                return _context4.stop();
            }
          }
        }, _callee4);
      }))();
    },
    previewTask: function previewTask(taskId) {
      var _this5 = this;

      return _asyncToGenerator( /*#__PURE__*/_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.mark(function _callee5() {
        var params;
        return _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.wrap(function _callee5$(_context5) {
          while (1) {
            switch (_context5.prev = _context5.next) {
              case 0:
                params = {
                  route: route('task.board-show', {
                    task: taskId
                  }),
                  data: {}
                };
                _context5.next = 3;
                return _this5.get(params);

              case 3:
                $('#view-task').modal('show');

              case 4:
              case "end":
                return _context5.stop();
            }
          }
        }, _callee5);
      }))();
    },
    updateStatus: function updateStatus(val) {
      var _this6 = this;

      return _asyncToGenerator( /*#__PURE__*/_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.mark(function _callee6() {
        var params, _yield$axios$post, data;

        return _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.wrap(function _callee6$(_context6) {
          while (1) {
            switch (_context6.prev = _context6.next) {
              case 0:
                params = {
                  action: route('task.status', {
                    task: val
                  }),
                  method: 'POST',
                  data: {
                    status: _this6.newStatus
                  }
                };
                _context6.prev = 1;
                _context6.next = 4;
                return axios.post(route('task.status', {
                  task: val
                }), {
                  status: _this6.newStatus
                });

              case 4:
                _yield$axios$post = _context6.sent;
                data = _yield$axios$post.data;

                if (data.status) {
                  _this6.$snotify.success(data.msg);
                }

                _context6.next = 12;
                break;

              case 9:
                _context6.prev = 9;
                _context6.t0 = _context6["catch"](1);
                // statements
                console.log(_context6.t0);

              case 12:
              case "end":
                return _context6.stop();
            }
          }
        }, _callee6, null, [[1, 9]]);
      }))();
    },
    reported: function reported() {
      $('#report-task').modal('hide');
      this.reportPreview++;
      this.$emit('reported');
    },
    emitStatus: function emitStatus() {
      var title = this.variable;
      var newStatus;

      if (title == 'tasks') {
        newStatus = -1;
      } else if (title == 'in_progress') {
        newStatus = 0;
      } else {
        newStatus = 1;
      }

      this.set_task_status(newStatus);
    },
    show_report_form: function show_report_form(taskId) {
      var _this7 = this;

      return _asyncToGenerator( /*#__PURE__*/_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.mark(function _callee7() {
        var params;
        return _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.wrap(function _callee7$(_context7) {
          while (1) {
            switch (_context7.prev = _context7.next) {
              case 0:
                _this7.reportPreview++;
                params = {
                  route: route('task.board-show', {
                    task: taskId
                  }),
                  data: {}
                };
                _context7.next = 4;
                return _this7.get(params);

              case 4:
                $('#report-task').modal('show');
                /*setTimeout(function(){
                  },1000);*/

              case 5:
              case "end":
                return _context7.stop();
            }
          }
        }, _callee7);
      }))();
    },
    deleteTask: function deleteTask() {
      var _this8 = this;

      return _asyncToGenerator( /*#__PURE__*/_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.mark(function _callee8() {
        var params, _yield$_this8$delete, data;

        return _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.wrap(function _callee8$(_context8) {
          while (1) {
            switch (_context8.prev = _context8.next) {
              case 0:
                params = {
                  route: route('task.destroy', {
                    task: _this8.taskId
                  }),
                  method: 'DELETE',
                  mutation: '',
                  data: {}
                };
                _context8.next = 3;
                return _this8["delete"](params);

              case 3:
                _yield$_this8$delete = _context8.sent;
                data = _yield$_this8$delete.data;

                if (data.status) {
                  $('#delete-task-confirm').modal('hide');

                  _this8.$snotify.success(data.msg);
                }

              case 6:
              case "end":
                return _context8.stop();
            }
          }
        }, _callee8);
      }))();
    }
  })
});

/***/ }),

/***/ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/src/views/Pages/web/Task/components/Logs.vue?vue&type=template&id=0887842e&":
/*!********************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/src/views/Pages/web/Task/components/Logs.vue?vue&type=template&id=0887842e& ***!
  \********************************************************************************************************************************************************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "render", function() { return render; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return staticRenderFns; });
var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c(
    "div",
    { staticClass: "col-md-4" },
    [
      _c("div", { staticClass: "task-box-hd-text" }, [
        _c("span", [_vm._v(_vm._s(_vm.title))])
      ]),
      _vm._v(" "),
      _c("div", { staticClass: "create-task-btn-box" }, [
        !_vm.isEmployee
          ? _c(
              "a",
              {
                attrs: {
                  href: "javascript:void(0)",
                  "data-toggle": "modal",
                  "data-target": "#create-task"
                },
                on: { click: _vm.emitStatus }
              },
              [_c("i", { staticClass: "fas fa-plus-circle" })]
            )
          : _c("a", { attrs: { href: "javascript:void(0)" } }, [
              _c("i", { staticClass: "fas fa-plus-circle" })
            ])
      ]),
      _vm._v(" "),
      _c(
        "draggable",
        _vm._b(
          {
            attrs: {
              draggable: ".task-boxes",
              sort: false,
              direction: "horizontal",
              group: "people",
              list: _vm.tasks.data
            },
            on: {
              start: function($event) {
                _vm.drag = true
              },
              add: function($event) {
                return _vm.handleDrag($event, _vm.variable)
              },
              end: function($event) {
                return _vm.handleEnd($event)
              }
            }
          },
          "draggable",
          _vm.dragOptions,
          false
        ),
        _vm._l(_vm.tasks.data, function(task) {
          return _c(
            "div",
            {
              key: task.id,
              staticClass: "task-boxes",
              attrs: { "data-title": _vm.variable, id: task.id }
            },
            [
              _c("div", { staticClass: "dropdown" }, [
                _c(
                  "a",
                  {
                    attrs: {
                      href: "javascript:void(0)",
                      role: "button",
                      id: "dropdownMenuLink",
                      "data-toggle": "dropdown",
                      "aria-haspopup": "true",
                      "aria-expanded": "false"
                    }
                  },
                  [_c("i", { staticClass: "fas fa-ellipsis-v" })]
                ),
                _vm._v(" "),
                _c(
                  "div",
                  {
                    staticClass: "dropdown-menu",
                    attrs: { "aria-labelledby": "dropdownMenuLink" }
                  },
                  [
                    _c(
                      "a",
                      {
                        staticClass: "dropdown-item",
                        attrs: { href: "javascript:void(0)" },
                        on: {
                          click: function($event) {
                            return _vm.previewTask(task.id)
                          }
                        }
                      },
                      [_c("i", { staticClass: "fas fa-eye" }), _vm._v(" VIEW")]
                    ),
                    _vm._v(" "),
                    !_vm.isEmployee
                      ? _c(
                          "a",
                          {
                            staticClass: "dropdown-item",
                            attrs: {
                              href: "javascript:void(0)",
                              "data-toggle": "modal",
                              "data-target": "#delete-task-confirm"
                            },
                            on: {
                              click: function($event) {
                                _vm.taskId = task.id
                              }
                            }
                          },
                          [
                            _c("i", { staticClass: "fas fa-trash" }),
                            _vm._v(" DELETE")
                          ]
                        )
                      : _c(
                          "a",
                          {
                            staticClass: "dropdown-item",
                            attrs: { href: "javascript:void(0)" },
                            on: {
                              click: function($event) {
                                return _vm.show_report_form(task.id)
                              }
                            }
                          },
                          [
                            _c("i", { staticClass: "fas fa-file-alt" }),
                            _vm._v(" REPORT")
                          ]
                        )
                  ]
                )
              ]),
              _vm._v(" "),
              _c("i", { staticClass: "fas fa-check-circle" }),
              _vm._v(" "),
              _c("span", [_vm._v(_vm._s(task.name))]),
              _c("br"),
              _vm._v(" "),
              _c("i", { staticClass: "fas fa-user-circle" }),
              _vm._v(" "),
              _c("span", [
                _vm._v(_vm._s(_vm.formatDate(task.created_at, "DD MMMM")))
              ])
            ]
          )
        }),
        0
      ),
      _vm._v(" "),
      _c("pagination", {
        attrs: { data: _vm.tasks },
        on: { "pagination-change-page": _vm.fetch }
      }),
      _vm._v(" "),
      typeof _vm.tasks.data != "undefined" && _vm.tasks.data.length == 0
        ? _c("div", { staticClass: "task-boxes" }, [
            _c("p", { staticClass: "text-center" }, [
              _vm._v("No Data Available")
            ])
          ])
        : _vm._e(),
      _vm._v(" "),
      _c(
        "delete-task-confirm",
        {
          attrs: {
            el: "delete-task-confirm",
            title: "Delete Task",
            subtitle: "Are you sure you want to delete <br> this task?"
          }
        },
        [
          _c("template", { slot: "footer" }, [
            _c(
              "a",
              {
                staticClass: "emplyee-add-btn",
                attrs: {
                  href: "javascript:void(0)",
                  "data-toggle": "modal",
                  "data-target": "#assign-task"
                },
                on: {
                  click: function($event) {
                    return _vm.deleteTask()
                  }
                }
              },
              [_vm._v("Yes")]
            ),
            _vm._v(" "),
            _c(
              "a",
              {
                staticClass: "emplyee-add-btn",
                attrs: { href: "javascript:void(0)", "data-dismiss": "modal" }
              },
              [_vm._v("No")]
            )
          ])
        ],
        2
      ),
      _vm._v(" "),
      _c("view-task", {
        attrs: { task: _vm.task },
        on: { "status-change": _vm.statusUpdated }
      }),
      _vm._v(" "),
      _vm.reportPreview > 0
        ? _c("report-task", {
            attrs: { task: _vm.task },
            on: { reported: _vm.reported }
          })
        : _vm._e()
    ],
    1
  )
}
var staticRenderFns = []
render._withStripped = true



/***/ }),

/***/ "./resources/js/src/views/Pages/web/Task/components/Logs.vue":
/*!*******************************************************************!*\
  !*** ./resources/js/src/views/Pages/web/Task/components/Logs.vue ***!
  \*******************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _Logs_vue_vue_type_template_id_0887842e___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./Logs.vue?vue&type=template&id=0887842e& */ "./resources/js/src/views/Pages/web/Task/components/Logs.vue?vue&type=template&id=0887842e&");
/* harmony import */ var _Logs_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./Logs.vue?vue&type=script&lang=js& */ "./resources/js/src/views/Pages/web/Task/components/Logs.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport *//* harmony import */ var _node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../../../../../../../node_modules/vue-loader/lib/runtime/componentNormalizer.js */ "./node_modules/vue-loader/lib/runtime/componentNormalizer.js");





/* normalize component */

var component = Object(_node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__["default"])(
  _Logs_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__["default"],
  _Logs_vue_vue_type_template_id_0887842e___WEBPACK_IMPORTED_MODULE_0__["render"],
  _Logs_vue_vue_type_template_id_0887842e___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"],
  false,
  null,
  null,
  null
  
)

/* hot reload */
if (false) { var api; }
component.options.__file = "resources/js/src/views/Pages/web/Task/components/Logs.vue"
/* harmony default export */ __webpack_exports__["default"] = (component.exports);

/***/ }),

/***/ "./resources/js/src/views/Pages/web/Task/components/Logs.vue?vue&type=script&lang=js&":
/*!********************************************************************************************!*\
  !*** ./resources/js/src/views/Pages/web/Task/components/Logs.vue?vue&type=script&lang=js& ***!
  \********************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_Logs_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../../../../../node_modules/babel-loader/lib??ref--4-0!../../../../../../../../node_modules/vue-loader/lib??vue-loader-options!./Logs.vue?vue&type=script&lang=js& */ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/src/views/Pages/web/Task/components/Logs.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport */ /* harmony default export */ __webpack_exports__["default"] = (_node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_Logs_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__["default"]); 

/***/ }),

/***/ "./resources/js/src/views/Pages/web/Task/components/Logs.vue?vue&type=template&id=0887842e&":
/*!**************************************************************************************************!*\
  !*** ./resources/js/src/views/Pages/web/Task/components/Logs.vue?vue&type=template&id=0887842e& ***!
  \**************************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_Logs_vue_vue_type_template_id_0887842e___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../../../../../node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!../../../../../../../../node_modules/vue-loader/lib??vue-loader-options!./Logs.vue?vue&type=template&id=0887842e& */ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/src/views/Pages/web/Task/components/Logs.vue?vue&type=template&id=0887842e&");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "render", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_Logs_vue_vue_type_template_id_0887842e___WEBPACK_IMPORTED_MODULE_0__["render"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_Logs_vue_vue_type_template_id_0887842e___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"]; });



/***/ })

}]);