(window["webpackJsonp"] = window["webpackJsonp"] || []).push([[88],{

/***/ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/src/views/Pages/web/Property/Index.vue?vue&type=script&lang=js&":
/*!**********************************************************************************************************************************************************************************!*\
  !*** ./node_modules/babel-loader/lib??ref--4-0!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/src/views/Pages/web/Property/Index.vue?vue&type=script&lang=js& ***!
  \**********************************************************************************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @babel/runtime/regenerator */ "./node_modules/@babel/runtime/regenerator/index.js");
/* harmony import */ var _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var vuex__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! vuex */ "./node_modules/vuex/dist/vuex.esm.js");


function ownKeys(object, enumerableOnly) { var keys = Object.keys(object); if (Object.getOwnPropertySymbols) { var symbols = Object.getOwnPropertySymbols(object); if (enumerableOnly) symbols = symbols.filter(function (sym) { return Object.getOwnPropertyDescriptor(object, sym).enumerable; }); keys.push.apply(keys, symbols); } return keys; }

function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; if (i % 2) { ownKeys(Object(source), true).forEach(function (key) { _defineProperty(target, key, source[key]); }); } else if (Object.getOwnPropertyDescriptors) { Object.defineProperties(target, Object.getOwnPropertyDescriptors(source)); } else { ownKeys(Object(source)).forEach(function (key) { Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key)); }); } } return target; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

function asyncGeneratorStep(gen, resolve, reject, _next, _throw, key, arg) { try { var info = gen[key](arg); var value = info.value; } catch (error) { reject(error); return; } if (info.done) { resolve(value); } else { Promise.resolve(value).then(_next, _throw); } }

function _asyncToGenerator(fn) { return function () { var self = this, args = arguments; return new Promise(function (resolve, reject) { var gen = fn.apply(self, args); function _next(value) { asyncGeneratorStep(gen, resolve, reject, _next, _throw, "next", value); } function _throw(err) { asyncGeneratorStep(gen, resolve, reject, _next, _throw, "throw", err); } _next(undefined); }); }; }

//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//


var DeletePropertyConfirm = function DeletePropertyConfirm() {
  return __webpack_require__.e(/*! import() */ 0).then(__webpack_require__.bind(null, /*! @views/Partials/web/popups/Success.vue */ "./resources/js/src/views/Partials/web/popups/Success.vue"));
};

/* harmony default export */ __webpack_exports__["default"] = ({
  created: function created() {
    var _this = this;

    return _asyncToGenerator( /*#__PURE__*/_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.mark(function _callee() {
      return _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.wrap(function _callee$(_context) {
        while (1) {
          switch (_context.prev = _context.next) {
            case 0:
              _context.next = 2;
              return _this.fetch();

            case 2:
              _this.fetch = _.debounce(_this.fetch, 500);

            case 3:
            case "end":
              return _context.stop();
          }
        }
      }, _callee);
    }))();
  },
  data: function data() {
    return {
      search: '',
      propertyId: ''
    };
  },
  components: {
    DeletePropertyConfirm: DeletePropertyConfirm
  },
  computed: _objectSpread({}, Object(vuex__WEBPACK_IMPORTED_MODULE_1__["mapState"])('property', ['properties'])),
  watch: {
    search: function search(val, oldVal) {
      this.fetch();
    }
  },
  methods: _objectSpread(_objectSpread({}, Object(vuex__WEBPACK_IMPORTED_MODULE_1__["mapActions"])('property', ['getAll', 'delete'])), {}, {
    fetch: function fetch() {
      var _arguments = arguments,
          _this2 = this;

      return _asyncToGenerator( /*#__PURE__*/_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.mark(function _callee2() {
        var page, params;
        return _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.wrap(function _callee2$(_context2) {
          while (1) {
            switch (_context2.prev = _context2.next) {
              case 0:
                page = _arguments.length > 0 && _arguments[0] !== undefined ? _arguments[0] : 1;
                params = {
                  route: route('property.index'),
                  mutation: 'index',
                  data: {
                    page: page,
                    search: _this2.search
                  }
                };
                _context2.next = 4;
                return _this2.getAll(params);

              case 4:
              case "end":
                return _context2.stop();
            }
          }
        }, _callee2);
      }))();
    },
    deleteProperty: function deleteProperty() {
      var _this3 = this;

      return _asyncToGenerator( /*#__PURE__*/_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.mark(function _callee3() {
        var params, _yield$_this3$delete, data;

        return _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.wrap(function _callee3$(_context3) {
          while (1) {
            switch (_context3.prev = _context3.next) {
              case 0:
                params = {
                  route: route('property.destroy', {
                    property: _this3.propertyId
                  }),
                  method: 'DELETE',
                  mutation: '',
                  data: {}
                };
                _context3.next = 3;
                return _this3["delete"](params);

              case 3:
                _yield$_this3$delete = _context3.sent;
                data = _yield$_this3$delete.data;

                if (data.status) {
                  $('#delete-property-confirm').modal('hide');

                  _this3.$snotify.success(data.msg);
                }

              case 6:
              case "end":
                return _context3.stop();
            }
          }
        }, _callee3);
      }))();
    }
  })
});

/***/ }),

/***/ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/src/views/Pages/web/Property/Index.vue?vue&type=template&id=441186f0&":
/*!**************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/src/views/Pages/web/Property/Index.vue?vue&type=template&id=441186f0& ***!
  \**************************************************************************************************************************************************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "render", function() { return render; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return staticRenderFns; });
var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c(
    "div",
    [
      _c("section", { staticClass: "searchbar-box" }, [
        _c("div", { staticClass: "container" }, [
          _c("div", { staticClass: "row" }, [
            _c("div", { staticClass: "col-md-6" }, [
              _c(
                "div",
                { staticClass: "input-group md-form form-sm form-2 pl-0" },
                [
                  _c("input", {
                    directives: [
                      {
                        name: "model",
                        rawName: "v-model",
                        value: _vm.search,
                        expression: "search"
                      }
                    ],
                    staticClass: "form-control my-0 py-1 lime-border",
                    attrs: {
                      type: "text",
                      placeholder: "Search",
                      "aria-label": "Search"
                    },
                    domProps: { value: _vm.search },
                    on: {
                      input: function($event) {
                        if ($event.target.composing) {
                          return
                        }
                        _vm.search = $event.target.value
                      }
                    }
                  }),
                  _vm._v(" "),
                  _vm._m(0)
                ]
              )
            ]),
            _vm._v(" "),
            _c(
              "div",
              { staticClass: "col-md-6" },
              [
                _c(
                  "router-link",
                  {
                    staticClass: "search-bar-btn",
                    attrs: { to: { name: "web.properties.create" } }
                  },
                  [_vm._v("ADD PROPERTY")]
                )
              ],
              1
            )
          ])
        ])
      ]),
      _vm._v(" "),
      _c("section", { staticClass: "property-listing-cards" }, [
        _c("div", { staticClass: "container" }, [
          _c("div", { staticClass: "row" }, [
            _c(
              "div",
              { staticClass: "card-deck" },
              [
                _vm._l(_vm.properties.data, function(property, index) {
                  return _c("div", { key: index, staticClass: "col-md-6" }, [
                    _c("div", { staticClass: "card" }, [
                      _c("img", {
                        staticClass: "card-img-top",
                        attrs: {
                          src: property.media
                            ? property.media.url
                            : "assets/images/placeholder.png",
                          alt: "..."
                        }
                      }),
                      _vm._v(" "),
                      _c("div", { staticClass: "dropdown" }, [
                        _vm._m(1, true),
                        _vm._v(" "),
                        _c(
                          "div",
                          {
                            staticClass: "dropdown-menu",
                            attrs: { "aria-labelledby": "dropdownMenuLink" }
                          },
                          [
                            _c(
                              "router-link",
                              {
                                staticClass: "dropdown-item",
                                attrs: {
                                  to: {
                                    name: "web.properties.show",
                                    params: { id: property.property_id }
                                  }
                                }
                              },
                              [
                                _c("i", { staticClass: "fas fa-eye" }),
                                _vm._v(
                                  " VIEW\n                                        PROPERTY"
                                )
                              ]
                            ),
                            _vm._v(" "),
                            _c(
                              "a",
                              {
                                staticClass: "dropdown-item",
                                attrs: {
                                  href: "#",
                                  "data-toggle": "modal",
                                  "data-target": "#delete-property-confirm"
                                },
                                on: {
                                  click: function($event) {
                                    _vm.propertyId = property.id
                                  }
                                }
                              },
                              [
                                _c("i", { staticClass: "fas fa-trash" }),
                                _vm._v(" DELETE PROPERTY")
                              ]
                            )
                          ],
                          1
                        )
                      ]),
                      _vm._v(" "),
                      _c("div", { staticClass: "card-body" }, [
                        _c("strong", [_vm._v(_vm._s(property.name))]),
                        _vm._v(" "),
                        _c("span", { staticClass: "card-green-text" }, [
                          _vm._v("Property_ID: P_" + _vm._s(property.id))
                        ]),
                        _vm._v(" "),
                        _c("h5", { staticClass: "card-title" }, [
                          _vm._v("Address:")
                        ]),
                        _vm._v(" "),
                        _c("small", [
                          _vm._v(
                            _vm._s(property.address) +
                              " " +
                              _vm._s(property.address_1)
                          )
                        ]),
                        _vm._v(" "),
                        _c("p", { staticClass: "card-text text-muted" }, [
                          _c("small", [_vm._v(_vm._s(property.excerpt))])
                        ])
                      ]),
                      _vm._v(" "),
                      _c("div", { staticClass: "card-footer" }, [
                        _c("small", { staticClass: "card-footer-left" }, [
                          _vm._v(
                            _vm._s(property.bedrooms) +
                              " Bedrooms | " +
                              _vm._s(property.bathrooms) +
                              " bathrooms | " +
                              _vm._s(property.kitchens) +
                              " kitchen |"
                          )
                        ]),
                        _vm._v(" "),
                        _c("small", { staticClass: "card-footer-right" }, [
                          _vm._v(
                            "Total Employees: " +
                              _vm._s(property.employees_count)
                          )
                        ])
                      ])
                    ])
                  ])
                }),
                _vm._v(" "),
                !_vm.$_.isUndefined(_vm.properties.data) &&
                _vm.properties.data.length == 0
                  ? _c("div", { staticClass: "col-md-12 " }, [
                      _c("h3", { staticClass: "text-center" }, [
                        _vm._v("No Property Found")
                      ])
                    ])
                  : _vm._e()
              ],
              2
            )
          ])
        ])
      ]),
      _vm._v(" "),
      _c("section", { staticClass: "pagination-box" }, [
        _c("div", { staticClass: "container" }, [
          _c("div", { staticClass: "row" }, [
            _c("div", { staticClass: "col-md-6" }, [
              _c("strong", [
                _vm._v(
                  "SHOWING " +
                    _vm._s(_vm.properties.per_page) +
                    " OUT OF " +
                    _vm._s(_vm.properties.total) +
                    " ENTRIES"
                )
              ])
            ]),
            _vm._v(" "),
            _c(
              "div",
              { staticClass: "col-md-6" },
              [
                _c("pagination", {
                  attrs: { data: _vm.properties },
                  on: { "pagination-change-page": _vm.fetch }
                })
              ],
              1
            )
          ])
        ])
      ]),
      _vm._v(" "),
      _c(
        "delete-property-confirm",
        {
          attrs: {
            el: "delete-property-confirm",
            title: "Delete Property",
            subtitle: "Are you sure you want to delete <br> this Property?"
          }
        },
        [
          _c("template", { slot: "footer" }, [
            _c(
              "a",
              {
                staticClass: "emplyee-add-btn",
                attrs: {
                  href: "javascript:void(0)",
                  "data-toggle": "modal",
                  "data-target": "#assign-task"
                },
                on: {
                  click: function($event) {
                    return _vm.deleteProperty()
                  }
                }
              },
              [_vm._v("Yes")]
            ),
            _vm._v(" "),
            _c(
              "a",
              {
                staticClass: "emplyee-add-btn",
                attrs: { href: "javascript:void(0)", "data-dismiss": "modal" }
              },
              [_vm._v("No")]
            )
          ])
        ],
        2
      )
    ],
    1
  )
}
var staticRenderFns = [
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("div", { staticClass: "input-group-append" }, [
      _c(
        "span",
        {
          staticClass: "input-group-text lime lighten-2",
          attrs: { id: "basic-text1" }
        },
        [
          _c("i", {
            staticClass: "fas fa-search text-grey",
            attrs: { "aria-hidden": "true" }
          })
        ]
      )
    ])
  },
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c(
      "a",
      {
        attrs: {
          href: "#",
          role: "button",
          id: "dropdownMenuLink",
          "data-toggle": "dropdown",
          "aria-haspopup": "true",
          "aria-expanded": "false"
        }
      },
      [_c("i", { staticClass: "fas fa-ellipsis-v" })]
    )
  }
]
render._withStripped = true



/***/ }),

/***/ "./resources/js/src/views/Pages/web/Property/Index.vue":
/*!*************************************************************!*\
  !*** ./resources/js/src/views/Pages/web/Property/Index.vue ***!
  \*************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _Index_vue_vue_type_template_id_441186f0___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./Index.vue?vue&type=template&id=441186f0& */ "./resources/js/src/views/Pages/web/Property/Index.vue?vue&type=template&id=441186f0&");
/* harmony import */ var _Index_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./Index.vue?vue&type=script&lang=js& */ "./resources/js/src/views/Pages/web/Property/Index.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport *//* harmony import */ var _node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../../../../../../node_modules/vue-loader/lib/runtime/componentNormalizer.js */ "./node_modules/vue-loader/lib/runtime/componentNormalizer.js");





/* normalize component */

var component = Object(_node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__["default"])(
  _Index_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__["default"],
  _Index_vue_vue_type_template_id_441186f0___WEBPACK_IMPORTED_MODULE_0__["render"],
  _Index_vue_vue_type_template_id_441186f0___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"],
  false,
  null,
  null,
  null
  
)

/* hot reload */
if (false) { var api; }
component.options.__file = "resources/js/src/views/Pages/web/Property/Index.vue"
/* harmony default export */ __webpack_exports__["default"] = (component.exports);

/***/ }),

/***/ "./resources/js/src/views/Pages/web/Property/Index.vue?vue&type=script&lang=js&":
/*!**************************************************************************************!*\
  !*** ./resources/js/src/views/Pages/web/Property/Index.vue?vue&type=script&lang=js& ***!
  \**************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_Index_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../../../../node_modules/babel-loader/lib??ref--4-0!../../../../../../../node_modules/vue-loader/lib??vue-loader-options!./Index.vue?vue&type=script&lang=js& */ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/src/views/Pages/web/Property/Index.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport */ /* harmony default export */ __webpack_exports__["default"] = (_node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_Index_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__["default"]); 

/***/ }),

/***/ "./resources/js/src/views/Pages/web/Property/Index.vue?vue&type=template&id=441186f0&":
/*!********************************************************************************************!*\
  !*** ./resources/js/src/views/Pages/web/Property/Index.vue?vue&type=template&id=441186f0& ***!
  \********************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_Index_vue_vue_type_template_id_441186f0___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../../../../node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!../../../../../../../node_modules/vue-loader/lib??vue-loader-options!./Index.vue?vue&type=template&id=441186f0& */ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/src/views/Pages/web/Property/Index.vue?vue&type=template&id=441186f0&");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "render", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_Index_vue_vue_type_template_id_441186f0___WEBPACK_IMPORTED_MODULE_0__["render"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_Index_vue_vue_type_template_id_441186f0___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"]; });



/***/ })

}]);