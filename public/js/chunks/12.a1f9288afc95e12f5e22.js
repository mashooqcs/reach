(window["webpackJsonp"] = window["webpackJsonp"] || []).push([[12],{

/***/ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/src/views/Partials/web/GreenBanner.vue?vue&type=script&lang=js&":
/*!**********************************************************************************************************************************************************************************!*\
  !*** ./node_modules/babel-loader/lib??ref--4-0!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/src/views/Partials/web/GreenBanner.vue?vue&type=script&lang=js& ***!
  \**********************************************************************************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
/* harmony default export */ __webpack_exports__["default"] = ({});

/***/ }),

/***/ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/src/views/Partials/web/GreenBanner.vue?vue&type=template&id=4c97af3e&":
/*!**************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/src/views/Partials/web/GreenBanner.vue?vue&type=template&id=4c97af3e& ***!
  \**************************************************************************************************************************************************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "render", function() { return render; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return staticRenderFns; });
var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _vm._m(0)
}
var staticRenderFns = [
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("section", { staticClass: "third-box" }, [
      _c("div", { staticClass: "white-rectangle" }, [
        _c("img", { attrs: { src: "assets/images/Layer 7.png", alt: "" } })
      ]),
      _vm._v(" "),
      _c("div", { staticClass: "container" }, [
        _c("div", { staticClass: "row" }, [
          _c("div", { staticClass: "col-md-2" }, [
            _c("small", [_vm._v("work Process")])
          ]),
          _vm._v(" "),
          _c("div", { staticClass: "col-md-10" }, [
            _c("span", { staticClass: "third-box-haed" }, [
              _vm._v("This is How We\n                    "),
              _c("br"),
              _vm._v(" Streameline Our Process")
            ])
          ])
        ]),
        _vm._v(" "),
        _c("div", { staticClass: "row" }, [
          _c("div", { staticClass: "col-md-4" }, [
            _c("div", { staticClass: "home-icon" }, [
              _c("img", {
                attrs: { src: "assets/images/602275.png", alt: "" }
              }),
              _vm._v(" "),
              _c("h6", [_vm._v("Add Your Property")]),
              _vm._v(" "),
              _c("p", [
                _vm._v(
                  "Lorem ipsum dolor sit amet, consectetur\n                        "
                ),
                _c("br"),
                _vm._v(
                  " adipiscing elit, sed do eiusmod tempor\n                        "
                ),
                _c("br"),
                _vm._v(
                  " incididunt ut labore et dolore magna aliqua.\n                        "
                ),
                _c("br"),
                _vm._v(" minim.enim ad minim veniam...")
              ])
            ])
          ]),
          _vm._v(" "),
          _c("div", { staticClass: "col-md-4" }, [
            _c("div", { staticClass: "home-icon" }, [
              _c("img", {
                attrs: { src: "assets/images/3094964.png", alt: "" }
              }),
              _vm._v(" "),
              _c("h6", [_vm._v("Add Your Employees")]),
              _vm._v(" "),
              _c("p", [
                _vm._v(
                  "Lorem ipsum dolor sit amet, consectetur\n                        "
                ),
                _c("br"),
                _vm._v(
                  " adipiscing elit, sed do eiusmod tempor\n                        "
                ),
                _c("br"),
                _vm._v(
                  " incididunt ut labore et dolore magna aliqua.\n                        "
                ),
                _c("br"),
                _vm._v(" minim.enim ad minim veniam...")
              ])
            ])
          ]),
          _vm._v(" "),
          _c("div", { staticClass: "col-md-4" }, [
            _c("div", { staticClass: "home-icon" }, [
              _c("img", {
                attrs: { src: "assets/images/2145788.png", alt: "" }
              }),
              _vm._v(" "),
              _c("h6", [_vm._v("Assign task")]),
              _vm._v(" "),
              _c("p", [
                _vm._v(
                  "Lorem ipsum dolor sit amet, consectetur\n                        "
                ),
                _c("br"),
                _vm._v(
                  " adipiscing elit, sed do eiusmod tempor\n                        "
                ),
                _c("br"),
                _vm._v(
                  " incididunt ut labore et dolore magna aliqua.\n                        "
                ),
                _c("br"),
                _vm._v(" minim.enim ad minim veniam...")
              ])
            ])
          ])
        ])
      ]),
      _vm._v(" "),
      _c("div", { staticClass: "green-rectangle" }, [
        _c("img", { attrs: { src: "assets/images/Layer 9.png", alt: "" } })
      ])
    ])
  }
]
render._withStripped = true



/***/ }),

/***/ "./resources/js/src/views/Partials/web/GreenBanner.vue":
/*!*************************************************************!*\
  !*** ./resources/js/src/views/Partials/web/GreenBanner.vue ***!
  \*************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _GreenBanner_vue_vue_type_template_id_4c97af3e___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./GreenBanner.vue?vue&type=template&id=4c97af3e& */ "./resources/js/src/views/Partials/web/GreenBanner.vue?vue&type=template&id=4c97af3e&");
/* harmony import */ var _GreenBanner_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./GreenBanner.vue?vue&type=script&lang=js& */ "./resources/js/src/views/Partials/web/GreenBanner.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport *//* harmony import */ var _node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../../../../../node_modules/vue-loader/lib/runtime/componentNormalizer.js */ "./node_modules/vue-loader/lib/runtime/componentNormalizer.js");





/* normalize component */

var component = Object(_node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__["default"])(
  _GreenBanner_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__["default"],
  _GreenBanner_vue_vue_type_template_id_4c97af3e___WEBPACK_IMPORTED_MODULE_0__["render"],
  _GreenBanner_vue_vue_type_template_id_4c97af3e___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"],
  false,
  null,
  null,
  null
  
)

/* hot reload */
if (false) { var api; }
component.options.__file = "resources/js/src/views/Partials/web/GreenBanner.vue"
/* harmony default export */ __webpack_exports__["default"] = (component.exports);

/***/ }),

/***/ "./resources/js/src/views/Partials/web/GreenBanner.vue?vue&type=script&lang=js&":
/*!**************************************************************************************!*\
  !*** ./resources/js/src/views/Partials/web/GreenBanner.vue?vue&type=script&lang=js& ***!
  \**************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_GreenBanner_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../../../node_modules/babel-loader/lib??ref--4-0!../../../../../../node_modules/vue-loader/lib??vue-loader-options!./GreenBanner.vue?vue&type=script&lang=js& */ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/src/views/Partials/web/GreenBanner.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport */ /* harmony default export */ __webpack_exports__["default"] = (_node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_GreenBanner_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__["default"]); 

/***/ }),

/***/ "./resources/js/src/views/Partials/web/GreenBanner.vue?vue&type=template&id=4c97af3e&":
/*!********************************************************************************************!*\
  !*** ./resources/js/src/views/Partials/web/GreenBanner.vue?vue&type=template&id=4c97af3e& ***!
  \********************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_GreenBanner_vue_vue_type_template_id_4c97af3e___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../../../node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!../../../../../../node_modules/vue-loader/lib??vue-loader-options!./GreenBanner.vue?vue&type=template&id=4c97af3e& */ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/src/views/Partials/web/GreenBanner.vue?vue&type=template&id=4c97af3e&");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "render", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_GreenBanner_vue_vue_type_template_id_4c97af3e___WEBPACK_IMPORTED_MODULE_0__["render"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_GreenBanner_vue_vue_type_template_id_4c97af3e___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"]; });



/***/ })

}]);