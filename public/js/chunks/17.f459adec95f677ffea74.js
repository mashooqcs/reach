(window["webpackJsonp"] = window["webpackJsonp"] || []).push([[17],{

/***/ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/src/views/Pages/admin/Dashboard.vue?vue&type=script&lang=js&":
/*!*******************************************************************************************************************************************************************************!*\
  !*** ./node_modules/babel-loader/lib??ref--4-0!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/src/views/Pages/admin/Dashboard.vue?vue&type=script&lang=js& ***!
  \*******************************************************************************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @babel/runtime/regenerator */ "./node_modules/@babel/runtime/regenerator/index.js");
/* harmony import */ var _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var vuex__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! vuex */ "./node_modules/vuex/dist/vuex.esm.js");
/* harmony import */ var _BarChart_js__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./BarChart.js */ "./resources/js/src/views/Pages/admin/BarChart.js");
/* harmony import */ var _core_months_js__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @core/months.js */ "./resources/js/src/Core/months.js");


function asyncGeneratorStep(gen, resolve, reject, _next, _throw, key, arg) { try { var info = gen[key](arg); var value = info.value; } catch (error) { reject(error); return; } if (info.done) { resolve(value); } else { Promise.resolve(value).then(_next, _throw); } }

function _asyncToGenerator(fn) { return function () { var self = this, args = arguments; return new Promise(function (resolve, reject) { var gen = fn.apply(self, args); function _next(value) { asyncGeneratorStep(gen, resolve, reject, _next, _throw, "next", value); } function _throw(err) { asyncGeneratorStep(gen, resolve, reject, _next, _throw, "throw", err); } _next(undefined); }); }; }

function ownKeys(object, enumerableOnly) { var keys = Object.keys(object); if (Object.getOwnPropertySymbols) { var symbols = Object.getOwnPropertySymbols(object); if (enumerableOnly) symbols = symbols.filter(function (sym) { return Object.getOwnPropertyDescriptor(object, sym).enumerable; }); keys.push.apply(keys, symbols); } return keys; }

function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; if (i % 2) { ownKeys(Object(source), true).forEach(function (key) { _defineProperty(target, key, source[key]); }); } else if (Object.getOwnPropertyDescriptors) { Object.defineProperties(target, Object.getOwnPropertyDescriptors(source)); } else { ownKeys(Object(source)).forEach(function (key) { Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key)); }); } } return target; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

function _toConsumableArray(arr) { return _arrayWithoutHoles(arr) || _iterableToArray(arr) || _unsupportedIterableToArray(arr) || _nonIterableSpread(); }

function _nonIterableSpread() { throw new TypeError("Invalid attempt to spread non-iterable instance.\nIn order to be iterable, non-array objects must have a [Symbol.iterator]() method."); }

function _unsupportedIterableToArray(o, minLen) { if (!o) return; if (typeof o === "string") return _arrayLikeToArray(o, minLen); var n = Object.prototype.toString.call(o).slice(8, -1); if (n === "Object" && o.constructor) n = o.constructor.name; if (n === "Map" || n === "Set") return Array.from(o); if (n === "Arguments" || /^(?:Ui|I)nt(?:8|16|32)(?:Clamped)?Array$/.test(n)) return _arrayLikeToArray(o, minLen); }

function _iterableToArray(iter) { if (typeof Symbol !== "undefined" && Symbol.iterator in Object(iter)) return Array.from(iter); }

function _arrayWithoutHoles(arr) { if (Array.isArray(arr)) return _arrayLikeToArray(arr); }

function _arrayLikeToArray(arr, len) { if (len == null || len > arr.length) len = arr.length; for (var i = 0, arr2 = new Array(len); i < len; i++) { arr2[i] = arr[i]; } return arr2; }

//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
 // const EmployeeTable = ()=> import('./components/EmployeeTable.vue');


 // console.log(allMonths);
// register component to use

/* harmony default export */ __webpack_exports__["default"] = ({
  components: {
    BarChart: _BarChart_js__WEBPACK_IMPORTED_MODULE_2__["default"]
  },
  data: function data() {
    return {
      selectedYear: '2021',
      months: [],
      loaded: false,
      chartData: {
        labels: _core_months_js__WEBPACK_IMPORTED_MODULE_3__["default"],
        datasets: [{
          label: 'Payments',
          data: [],
          backgroundColor: ['rgba(255, 99, 132, 0.2)'],
          borderColor: ['rgba(255, 99, 132, 1)'],
          borderWidth: 0.5,
          minBarLength: 2
        }]
      },
      options: {
        scales: {
          yAxes: [{
            ticks: {
              beginAtZero: true
            }
          }]
        }
      },
      chartOptionsBar: {
        xAxis: {
          data: _toConsumableArray(_core_months_js__WEBPACK_IMPORTED_MODULE_3__["default"])
        },
        yAxis: {
          type: 'value'
        },
        series: [{
          type: 'bar',
          data: []
        }]
      }
    };
  },
  computed: _objectSpread(_objectSpread({}, Object(vuex__WEBPACK_IMPORTED_MODULE_1__["mapState"])('admin', ['home'])), {}, {
    years: function years() {
      var today = new Date();
      var currentYear = today.getFullYear();
      var last10Years = new Date(today.setFullYear(today.getFullYear() - 10)).getFullYear();
      var years = [];

      for (var i = currentYear; i >= last10Years; i--) {
        years.push(i);
      }

      return years;
    }
  }),
  created: function created() {
    var self = this;
    this.fetch().then(function (res) {
      self.loaded = true;
    });
  },
  beforeMount: function beforeMount() {// console.log(this.home);
  },
  watch: {
    search: function search(val, oldVal) {
      this.fetch();
    },
    selectedYear: function selectedYear() {
      this.fetch();
    },
    chartData: function chartData(val) {
      alert();
    }
  },
  methods: _objectSpread(_objectSpread({}, Object(vuex__WEBPACK_IMPORTED_MODULE_1__["mapActions"])('admin', ['getAll'])), {}, {
    check: function check() {
      alert();
    },
    fetch: function fetch() {
      var _this = this;

      return _asyncToGenerator( /*#__PURE__*/_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.mark(function _callee() {
        var params, _yield$_this$getAll, data;

        return _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.wrap(function _callee$(_context) {
          while (1) {
            switch (_context.prev = _context.next) {
              case 0:
                _this.loaded = false;
                params = {
                  route: route('admin.home'),
                  mutation: 'SET_HOME_DATA',
                  variable: 'data',
                  data: {
                    search: _this.search,
                    year: _this.selectedYear
                  }
                };
                _context.next = 4;
                return _this.getAll(params);

              case 4:
                _yield$_this$getAll = _context.sent;
                data = _yield$_this$getAll.data;
                _this.months = data.data.months;
                _this.loaded = true;
                _this.chartData.datasets[0].data = _this.months;
                return _context.abrupt("return", data);

              case 10:
              case "end":
                return _context.stop();
            }
          }
        }, _callee);
      }))();
    }
  })
});

/***/ }),

/***/ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/src/views/Pages/admin/Dashboard.vue?vue&type=template&id=3f00822f&":
/*!***********************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/src/views/Pages/admin/Dashboard.vue?vue&type=template&id=3f00822f& ***!
  \***********************************************************************************************************************************************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "render", function() { return render; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return staticRenderFns; });
var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c("div", { staticClass: "content-wrapper" }, [
    _c("div", { staticClass: "content-body" }, [
      _c(
        "section",
        {
          staticClass: "search view-cause dashboard",
          attrs: { id: "configuration" }
        },
        [
          _c("div", { staticClass: "row" }, [
            _c("div", { staticClass: "col-12" }, [
              _c("h1", [_vm._v("ADMIN DASHBOARD")]),
              _vm._v(" "),
              _c("div", { staticClass: "row mt-2 cat-cards" }, [
                _c("div", { staticClass: "col-xl-12 col-12" }, [
                  _c("div", { staticClass: "row" }, [
                    _c(
                      "div",
                      { staticClass: "col-xl-3 col-md-6 col-12 d-flex" },
                      [
                        _c("div", { staticClass: "card rounded w-100" }, [
                          _c("div", { staticClass: "card-content" }, [
                            _c("div", { staticClass: "card-body cleartfix" }, [
                              _c("div", { staticClass: "media" }, [
                                _vm._m(0),
                                _vm._v(" "),
                                _c(
                                  "div",
                                  { staticClass: "align-self-center" },
                                  [
                                    _c("h2", [
                                      _vm._v(
                                        _vm._s(_vm.home.property_owners) + " "
                                      ),
                                      _c("i", { staticClass: "fas fa-users" })
                                    ])
                                  ]
                                )
                              ])
                            ])
                          ])
                        ])
                      ]
                    ),
                    _vm._v(" "),
                    _c(
                      "div",
                      { staticClass: "col-xl-3 col-md-6 col-12 d-flex" },
                      [
                        _c("div", { staticClass: "card rounded w-100" }, [
                          _c("div", { staticClass: "card-content" }, [
                            _c("div", { staticClass: "card-body cleartfix" }, [
                              _c("div", { staticClass: "media" }, [
                                _vm._m(1),
                                _vm._v(" "),
                                _c(
                                  "div",
                                  { staticClass: "align-self-center" },
                                  [
                                    _c("h2", [
                                      _vm._v(
                                        _vm._s(_vm.home.registered_employees) +
                                          " "
                                      ),
                                      _c("i", { staticClass: "fas fa-users" })
                                    ])
                                  ]
                                )
                              ])
                            ])
                          ])
                        ])
                      ]
                    ),
                    _vm._v(" "),
                    _c(
                      "div",
                      { staticClass: "col-xl-3 col-md-6 col-12 d-flex" },
                      [
                        _c("div", { staticClass: "card rounded w-100" }, [
                          _c("div", { staticClass: "card-content" }, [
                            _c("div", { staticClass: "card-body cleartfix" }, [
                              _c("div", { staticClass: "media" }, [
                                _vm._m(2),
                                _vm._v(" "),
                                _c(
                                  "div",
                                  { staticClass: "align-self-center" },
                                  [
                                    _c("h2", [
                                      _vm._v(_vm._s(_vm.home.properties) + " "),
                                      _c("i", { staticClass: "fas fa-users" })
                                    ])
                                  ]
                                )
                              ])
                            ])
                          ])
                        ])
                      ]
                    ),
                    _vm._v(" "),
                    _c(
                      "div",
                      { staticClass: "col-xl-3 col-md-6 col-12 d-flex" },
                      [
                        _c("div", { staticClass: "card rounded w-100" }, [
                          _c("div", { staticClass: "card-content" }, [
                            _c("div", { staticClass: "card-body cleartfix" }, [
                              _c("div", { staticClass: "media" }, [
                                _vm._m(3),
                                _vm._v(" "),
                                _c(
                                  "div",
                                  { staticClass: "align-self-center" },
                                  [
                                    _c("h2", [
                                      _vm._v(_vm._s(_vm.home.sub_admins) + " "),
                                      _c("i", { staticClass: "fas fa-users" })
                                    ])
                                  ]
                                )
                              ])
                            ])
                          ])
                        ])
                      ]
                    )
                  ]),
                  _vm._v(" "),
                  _c("div", { staticClass: "row stats" }, [
                    _c("div", { staticClass: "col-12" }, [
                      _c("div", { staticClass: "card rounded w-100" }, [
                        _c("div", { staticClass: "card-content" }, [
                          _c("div", { staticClass: "card-body cleartfix" }, [
                            _vm._m(4),
                            _vm._v(" "),
                            _c("div", { staticClass: "row" }, [
                              _c(
                                "div",
                                {
                                  staticClass: "col-md-6 col-12 text-center b-r"
                                },
                                [
                                  _c("div", { staticClass: "c100 p40 big" }, [
                                    _c("span", [
                                      _vm._v(
                                        "$" +
                                          _vm._s(_vm.home.avg_monthly_income)
                                      )
                                    ]),
                                    _vm._v(" "),
                                    _vm._m(5)
                                  ]),
                                  _vm._v(" "),
                                  _c("h5", [_vm._v("Average Monthly Income")])
                                ]
                              ),
                              _vm._v(" "),
                              _c("hr"),
                              _vm._v(" "),
                              _c(
                                "div",
                                { staticClass: "col-md-6 col-12 text-center" },
                                [
                                  _c("div", { staticClass: "c100 p60 big" }, [
                                    _c("span", [
                                      _vm._v(
                                        "$" + _vm._s(_vm.home.avg_yearly_income)
                                      )
                                    ]),
                                    _vm._v(" "),
                                    _vm._m(6)
                                  ]),
                                  _vm._v(" "),
                                  _c("h5", [_vm._v("Average Yearly Income")])
                                ]
                              )
                            ])
                          ])
                        ])
                      ])
                    ])
                  ])
                ])
              ]),
              _vm._v(" "),
              _c("div", { staticClass: "card rounded pad-20" }, [
                _c("div", { staticClass: "card-content collapse show" }, [
                  _c(
                    "div",
                    {
                      staticClass: "card-body table-responsive card-dashboard"
                    },
                    [
                      _c("div", { staticClass: "row" }, [
                        _c(
                          "div",
                          { staticClass: "col-xl-12 col-lg-12 col-md-12" },
                          [
                            _c(
                              "div",
                              { staticClass: "row justify-content-between" },
                              [
                                _vm._m(7),
                                _vm._v(" "),
                                _c("div", { staticClass: "col-md-3 col-12" }, [
                                  _c(
                                    "select",
                                    {
                                      directives: [
                                        {
                                          name: "model",
                                          rawName: "v-model",
                                          value: _vm.selectedYear,
                                          expression: "selectedYear"
                                        }
                                      ],
                                      on: {
                                        change: function($event) {
                                          var $$selectedVal = Array.prototype.filter
                                            .call(
                                              $event.target.options,
                                              function(o) {
                                                return o.selected
                                              }
                                            )
                                            .map(function(o) {
                                              var val =
                                                "_value" in o
                                                  ? o._value
                                                  : o.value
                                              return val
                                            })
                                          _vm.selectedYear = $event.target
                                            .multiple
                                            ? $$selectedVal
                                            : $$selectedVal[0]
                                        }
                                      }
                                    },
                                    _vm._l(_vm.years, function(year) {
                                      return _c(
                                        "option",
                                        { domProps: { value: year } },
                                        [_vm._v(_vm._s(year))]
                                      )
                                    }),
                                    0
                                  )
                                ])
                              ]
                            ),
                            _vm._v(" "),
                            _c(
                              "div",
                              {
                                staticClass:
                                  "media home-chart align-items-center mt-2"
                              },
                              [
                                _c("h5", { staticClass: "text-center" }, [
                                  _vm._v("Revenue")
                                ]),
                                _vm._v(" "),
                                _c(
                                  "div",
                                  { staticClass: "media-body" },
                                  [
                                    _vm.loaded
                                      ? _c("bar-chart", {
                                          attrs: {
                                            "chart:render": "check",
                                            chartData: _vm.chartData,
                                            options: _vm.options
                                          }
                                        })
                                      : _vm._e(),
                                    _vm._v(" "),
                                    _c(
                                      "h5",
                                      { staticClass: "text-center month-text" },
                                      [_vm._v("Months")]
                                    )
                                  ],
                                  1
                                )
                              ]
                            )
                          ]
                        )
                      ])
                    ]
                  )
                ])
              ])
            ])
          ])
        ]
      )
    ])
  ])
}
var staticRenderFns = [
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("div", { staticClass: "media-body align-self-center" }, [
      _c("p", { staticClass: "m-0" }, [_vm._v("REGISTERED PROPERTY OWNERS")])
    ])
  },
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("div", { staticClass: "media-body align-self-center" }, [
      _c("p", { staticClass: "m-0" }, [_vm._v("REGISTERED EMPLOYEES")])
    ])
  },
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("div", { staticClass: "media-body align-self-center" }, [
      _c("p", { staticClass: "m-0" }, [_vm._v("REGISTERED PROPERTIES")])
    ])
  },
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("div", { staticClass: "media-body align-self-center" }, [
      _c("p", { staticClass: "m-0" }, [_vm._v("REGISTERED SUB-ADMINS")])
    ])
  },
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("div", { staticClass: "text-center" }, [
      _c("h6", [_vm._v("Quick Stats")])
    ])
  },
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("div", { staticClass: "slice" }, [
      _c("div", { staticClass: "bar" }),
      _vm._v(" "),
      _c("div", { staticClass: "fill" })
    ])
  },
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("div", { staticClass: "slice" }, [
      _c("div", { staticClass: "bar" }),
      _vm._v(" "),
      _c("div", { staticClass: "fill" })
    ])
  },
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("div", { staticClass: "col-md-9 col-12" }, [
      _c("h1", [_vm._v("Revenue Generated Per Month")])
    ])
  }
]
render._withStripped = true



/***/ }),

/***/ "./resources/js/src/Core/months.js":
/*!*****************************************!*\
  !*** ./resources/js/src/Core/months.js ***!
  \*****************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
var months = ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'];
/* harmony default export */ __webpack_exports__["default"] = (months);

/***/ }),

/***/ "./resources/js/src/views/Pages/admin/BarChart.js":
/*!********************************************************!*\
  !*** ./resources/js/src/views/Pages/admin/BarChart.js ***!
  \********************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var vue_chartjs__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! vue-chartjs */ "./node_modules/vue-chartjs/es/index.js");

/* harmony default export */ __webpack_exports__["default"] = ({
  "extends": vue_chartjs__WEBPACK_IMPORTED_MODULE_0__["Bar"],
  props: ['chartData', 'options'],
  data: function data() {
    return {};
  },
  mounted: function mounted() {
    this.renderChart(this.chartData, this.options);
  }
});

/***/ }),

/***/ "./resources/js/src/views/Pages/admin/Dashboard.vue":
/*!**********************************************************!*\
  !*** ./resources/js/src/views/Pages/admin/Dashboard.vue ***!
  \**********************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _Dashboard_vue_vue_type_template_id_3f00822f___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./Dashboard.vue?vue&type=template&id=3f00822f& */ "./resources/js/src/views/Pages/admin/Dashboard.vue?vue&type=template&id=3f00822f&");
/* harmony import */ var _Dashboard_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./Dashboard.vue?vue&type=script&lang=js& */ "./resources/js/src/views/Pages/admin/Dashboard.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport *//* harmony import */ var _node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../../../../../node_modules/vue-loader/lib/runtime/componentNormalizer.js */ "./node_modules/vue-loader/lib/runtime/componentNormalizer.js");





/* normalize component */

var component = Object(_node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__["default"])(
  _Dashboard_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__["default"],
  _Dashboard_vue_vue_type_template_id_3f00822f___WEBPACK_IMPORTED_MODULE_0__["render"],
  _Dashboard_vue_vue_type_template_id_3f00822f___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"],
  false,
  null,
  null,
  null
  
)

/* hot reload */
if (false) { var api; }
component.options.__file = "resources/js/src/views/Pages/admin/Dashboard.vue"
/* harmony default export */ __webpack_exports__["default"] = (component.exports);

/***/ }),

/***/ "./resources/js/src/views/Pages/admin/Dashboard.vue?vue&type=script&lang=js&":
/*!***********************************************************************************!*\
  !*** ./resources/js/src/views/Pages/admin/Dashboard.vue?vue&type=script&lang=js& ***!
  \***********************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_Dashboard_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../../../node_modules/babel-loader/lib??ref--4-0!../../../../../../node_modules/vue-loader/lib??vue-loader-options!./Dashboard.vue?vue&type=script&lang=js& */ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/src/views/Pages/admin/Dashboard.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport */ /* harmony default export */ __webpack_exports__["default"] = (_node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_Dashboard_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__["default"]); 

/***/ }),

/***/ "./resources/js/src/views/Pages/admin/Dashboard.vue?vue&type=template&id=3f00822f&":
/*!*****************************************************************************************!*\
  !*** ./resources/js/src/views/Pages/admin/Dashboard.vue?vue&type=template&id=3f00822f& ***!
  \*****************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_Dashboard_vue_vue_type_template_id_3f00822f___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../../../node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!../../../../../../node_modules/vue-loader/lib??vue-loader-options!./Dashboard.vue?vue&type=template&id=3f00822f& */ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/src/views/Pages/admin/Dashboard.vue?vue&type=template&id=3f00822f&");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "render", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_Dashboard_vue_vue_type_template_id_3f00822f___WEBPACK_IMPORTED_MODULE_0__["render"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_Dashboard_vue_vue_type_template_id_3f00822f___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"]; });



/***/ })

}]);