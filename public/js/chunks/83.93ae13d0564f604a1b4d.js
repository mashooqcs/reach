(window["webpackJsonp"] = window["webpackJsonp"] || []).push([[83],{

/***/ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/src/views/Pages/web/Home.vue?vue&type=script&lang=js&":
/*!************************************************************************************************************************************************************************!*\
  !*** ./node_modules/babel-loader/lib??ref--4-0!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/src/views/Pages/web/Home.vue?vue&type=script&lang=js& ***!
  \************************************************************************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
var About = function About() {
  return __webpack_require__.e(/*! import() */ 11).then(__webpack_require__.bind(null, /*! @views/Partials/web/About.vue */ "./resources/js/src/views/Partials/web/About.vue"));
};

var GreenBanner = function GreenBanner() {
  return __webpack_require__.e(/*! import() */ 12).then(__webpack_require__.bind(null, /*! @views/Partials/web/GreenBanner.vue */ "./resources/js/src/views/Partials/web/GreenBanner.vue"));
};

/* harmony default export */ __webpack_exports__["default"] = ({
  components: {
    About: About,
    GreenBanner: GreenBanner
  },
  mounted: function mounted() {
    this.$nextTick(function () {
      $('#home-slider').on('initialized.owl.carousel changed.owl.carousel', function (e) {
        if (!e.namespace) {
          return;
        }

        var carousel = e.relatedTarget;
        $('.slider-counter').text('0' + carousel.relative(carousel.current() + 1));
      }).owlCarousel({
        items: 1,
        loop: true,
        margin: 0,
        nav: true,
        dots: false
      });
    });
  }
});

/***/ }),

/***/ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/src/views/Pages/web/Home.vue?vue&type=template&id=6f9ed822&":
/*!****************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/src/views/Pages/web/Home.vue?vue&type=template&id=6f9ed822& ***!
  \****************************************************************************************************************************************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "render", function() { return render; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return staticRenderFns; });
var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c(
    "div",
    [
      _vm._m(0),
      _vm._v(" "),
      _c("About"),
      _vm._v(" "),
      _c("green-banner"),
      _vm._v(" "),
      _vm._m(1)
    ],
    1
  )
}
var staticRenderFns = [
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("div", { staticClass: "main-page-slider" }, [
      _c("div", { staticClass: "for-slider" }, [
        _c("div", { staticClass: "container" }, [
          _c(
            "div",
            {
              staticClass: "owl-carousel owl-theme ",
              attrs: { id: "home-slider" }
            },
            [
              _c("div", { staticClass: "item" }, [
                _c("h1", [
                  _vm._v(
                    "The Best way to Manage\n                            "
                  ),
                  _c("br"),
                  _vm._v(" your Employee’s Task\n                        ")
                ]),
                _vm._v(" "),
                _c("span", [
                  _vm._v(
                    "With over 700,000 active listings, Realtyspace has the largest\n                            "
                  ),
                  _c("br"),
                  _vm._v(" inventory of apartments in the United States.")
                ]),
                _vm._v(" "),
                _c("br"),
                _vm._v(" "),
                _c(
                  "button",
                  {
                    staticClass: "btn btn-secondary",
                    attrs: { type: "button" }
                  },
                  [
                    _vm._v("Get Listed Now\n                            "),
                    _c("i", { staticClass: "fas fa-arrow-right" })
                  ]
                )
              ]),
              _vm._v(" "),
              _c("div", { staticClass: "item" }, [
                _c("h1", [
                  _vm._v(
                    "The Best way to Manage\n                            "
                  ),
                  _c("br"),
                  _vm._v(" your Employee’s Task\n                        ")
                ]),
                _vm._v(" "),
                _c("span", [
                  _vm._v(
                    "With over 700,000 active listings, Realtyspace has the largest\n                            "
                  ),
                  _c("br"),
                  _vm._v(" inventory of apartments in the United States.")
                ]),
                _vm._v(" "),
                _c("br"),
                _vm._v(" "),
                _c(
                  "button",
                  {
                    staticClass: "btn btn-secondary",
                    attrs: { type: "button" }
                  },
                  [
                    _vm._v("Get Listed Now\n                            "),
                    _c("i", { staticClass: "fas fa-arrow-right" })
                  ]
                )
              ])
            ]
          )
        ]),
        _vm._v(" "),
        _c("h2", { staticClass: "slider-counter" }),
        _vm._v(" "),
        _c("div", { staticClass: "social-icons" }, [
          _c("ul", [
            _c("li", [
              _c("a", { attrs: { href: "#" } }, [
                _c("i", { staticClass: "fab fa-facebook-f" })
              ])
            ]),
            _vm._v(" "),
            _c("li", [
              _c("a", { attrs: { href: "#" } }, [
                _c("i", { staticClass: "fab fa-google-plus-g" })
              ])
            ]),
            _vm._v(" "),
            _c("li", [
              _c("a", { attrs: { href: "#" } }, [
                _c("i", { staticClass: "fab fa-twitter" })
              ])
            ]),
            _vm._v(" "),
            _c("li", [
              _c("a", { attrs: { href: "#" } }, [
                _c("i", { staticClass: "fab fa-instagram" })
              ])
            ])
          ])
        ]),
        _vm._v(" "),
        _c("div", { staticClass: "for-rotate" }, [
          _c("p", [_vm._v("SCROLL DOWN")])
        ])
      ])
    ])
  },
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("section", { staticClass: "fourth-box" }, [
      _c("div", { staticClass: "container" }, [
        _c("div", { staticClass: "row" }, [
          _c("div", { staticClass: "col-md-7" }, [
            _c("div", { staticClass: "green-back" }, [
              _c("h5", [_vm._v("SCNELL")]),
              _vm._v(" "),
              _c("h2", [_vm._v("Manage Property")]),
              _vm._v(" "),
              _c("hr", { staticClass: "solid" }),
              _vm._v(" "),
              _c("p", [
                _vm._v(
                  "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim.\n                            "
                ),
                _c("br"),
                _vm._v(" "),
                _c("br"),
                _vm._v(
                  " Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo\n                            consequat.\n                            "
                ),
                _c("br"),
                _vm._v(" "),
                _c("br"),
                _vm._v(
                  " Duis aute irure dolor in reprehenderit in voluptate velit esse cillum."
                )
              ]),
              _vm._v(" "),
              _c("br"),
              _vm._v(" "),
              _c("a", { staticClass: "green-button", attrs: { href: "#" } }, [
                _vm._v("Read More  \n                            "),
                _c("i", { staticClass: "fas fa-arrow-right" })
              ])
            ])
          ]),
          _vm._v(" "),
          _c("div", { staticClass: "col-md-5" }, [
            _c("div", { staticClass: "img-box" }, [
              _c("img", {
                attrs: {
                  src: "assets/images/shutterstock_641178736.jpg",
                  alt: ""
                }
              })
            ]),
            _vm._v(" "),
            _c("div", { staticClass: "green-rectangle" }, [
              _c("img", {
                attrs: { src: "assets/images/Layer 11.png", alt: "" }
              })
            ])
          ])
        ])
      ])
    ])
  }
]
render._withStripped = true



/***/ }),

/***/ "./resources/js/src/views/Pages/web/Home.vue":
/*!***************************************************!*\
  !*** ./resources/js/src/views/Pages/web/Home.vue ***!
  \***************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _Home_vue_vue_type_template_id_6f9ed822___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./Home.vue?vue&type=template&id=6f9ed822& */ "./resources/js/src/views/Pages/web/Home.vue?vue&type=template&id=6f9ed822&");
/* harmony import */ var _Home_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./Home.vue?vue&type=script&lang=js& */ "./resources/js/src/views/Pages/web/Home.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport *//* harmony import */ var _node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../../../../../node_modules/vue-loader/lib/runtime/componentNormalizer.js */ "./node_modules/vue-loader/lib/runtime/componentNormalizer.js");





/* normalize component */

var component = Object(_node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__["default"])(
  _Home_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__["default"],
  _Home_vue_vue_type_template_id_6f9ed822___WEBPACK_IMPORTED_MODULE_0__["render"],
  _Home_vue_vue_type_template_id_6f9ed822___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"],
  false,
  null,
  null,
  null
  
)

/* hot reload */
if (false) { var api; }
component.options.__file = "resources/js/src/views/Pages/web/Home.vue"
/* harmony default export */ __webpack_exports__["default"] = (component.exports);

/***/ }),

/***/ "./resources/js/src/views/Pages/web/Home.vue?vue&type=script&lang=js&":
/*!****************************************************************************!*\
  !*** ./resources/js/src/views/Pages/web/Home.vue?vue&type=script&lang=js& ***!
  \****************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_Home_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../../../node_modules/babel-loader/lib??ref--4-0!../../../../../../node_modules/vue-loader/lib??vue-loader-options!./Home.vue?vue&type=script&lang=js& */ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/src/views/Pages/web/Home.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport */ /* harmony default export */ __webpack_exports__["default"] = (_node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_Home_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__["default"]); 

/***/ }),

/***/ "./resources/js/src/views/Pages/web/Home.vue?vue&type=template&id=6f9ed822&":
/*!**********************************************************************************!*\
  !*** ./resources/js/src/views/Pages/web/Home.vue?vue&type=template&id=6f9ed822& ***!
  \**********************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_Home_vue_vue_type_template_id_6f9ed822___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../../../node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!../../../../../../node_modules/vue-loader/lib??vue-loader-options!./Home.vue?vue&type=template&id=6f9ed822& */ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/src/views/Pages/web/Home.vue?vue&type=template&id=6f9ed822&");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "render", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_Home_vue_vue_type_template_id_6f9ed822___WEBPACK_IMPORTED_MODULE_0__["render"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_Home_vue_vue_type_template_id_6f9ed822___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"]; });



/***/ })

}]);