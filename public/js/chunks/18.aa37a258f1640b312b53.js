(window["webpackJsonp"] = window["webpackJsonp"] || []).push([[18],{

/***/ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/src/Core/components/Sidebar/Sidebar.vue?vue&type=script&lang=js&":
/*!***********************************************************************************************************************************************************************************!*\
  !*** ./node_modules/babel-loader/lib??ref--4-0!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/src/Core/components/Sidebar/Sidebar.vue?vue&type=script&lang=js& ***!
  \***********************************************************************************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _sideBarItems_js__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./sideBarItems.js */ "./resources/js/src/Core/components/Sidebar/sideBarItems.js");
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//


var sidebarItem = function sidebarItem() {
  return __webpack_require__.e(/*! import() */ 27).then(__webpack_require__.bind(null, /*! ./SidebarItem.vue */ "./resources/js/src/Core/components/Sidebar/SidebarItem.vue"));
};

var SidebarGroup = function SidebarGroup() {
  return __webpack_require__.e(/*! import() */ 26).then(__webpack_require__.bind(null, /*! ./SidebarGroup.vue */ "./resources/js/src/Core/components/Sidebar/SidebarGroup.vue"));
};

/* harmony default export */ __webpack_exports__["default"] = ({
  data: function data() {
    return {
      clickedIndex: -1,
      sidebarItems: _sideBarItems_js__WEBPACK_IMPORTED_MODULE_0__["default"]
    };
  },
  components: {
    sidebarItem: sidebarItem,
    SidebarGroup: SidebarGroup
  },
  methods: {
    showChildren: function showChildren(index) {
      if (this.clickedIndex == index) {
        this.clickedIndex = -1;
      } else {
        this.clickedIndex = index;
      }
    }
  }
});

/***/ }),

/***/ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/src/Core/components/Sidebar/Sidebar.vue?vue&type=template&id=5ece29f2&":
/*!***************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/src/Core/components/Sidebar/Sidebar.vue?vue&type=template&id=5ece29f2& ***!
  \***************************************************************************************************************************************************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "render", function() { return render; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return staticRenderFns; });
var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c(
    "div",
    {
      staticClass: "main-menu menu-fixed menu-light menu-accordion",
      attrs: { "data-scroll-to-active": "true" }
    },
    [
      _c("div", { staticClass: "main-menu-content" }, [
        _c(
          "ul",
          {
            staticClass: "navigation navigation-main",
            attrs: {
              id: "main-menu-navigation",
              "data-menu": "menu-navigation"
            }
          },
          _vm._l(_vm.sidebarItems, function(item, index) {
            return _c(
              "li",
              {
                key: index,
                staticClass: "nav-item",
                class: {
                  "has-sub":
                    typeof item.children != "undefined" &&
                    item.children.length > 0,
                  open: _vm.clickedIndex == index
                },
                on: {
                  click: function($event) {
                    return _vm.showChildren(index)
                  }
                }
              },
              [
                _vm.$user.role == "sub_admin" &&
                (_vm.userPermissions.indexOf(item.permission) !== -1 ||
                  item.permission == "admin.dashboard")
                  ? [
                      _c("sidebar-item", { attrs: { item: item } }),
                      _vm._v(" "),
                      _c("sidebar-group", { attrs: { item: item } })
                    ]
                  : _vm.$user.role == "super_admin"
                  ? [
                      _c("sidebar-item", { attrs: { item: item } }),
                      _vm._v(" "),
                      _c("sidebar-group", { attrs: { item: item } })
                    ]
                  : _vm._e()
              ],
              2
            )
          }),
          0
        )
      ])
    ]
  )
}
var staticRenderFns = []
render._withStripped = true



/***/ }),

/***/ "./resources/js/src/Core/components/Sidebar/Sidebar.vue":
/*!**************************************************************!*\
  !*** ./resources/js/src/Core/components/Sidebar/Sidebar.vue ***!
  \**************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _Sidebar_vue_vue_type_template_id_5ece29f2___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./Sidebar.vue?vue&type=template&id=5ece29f2& */ "./resources/js/src/Core/components/Sidebar/Sidebar.vue?vue&type=template&id=5ece29f2&");
/* harmony import */ var _Sidebar_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./Sidebar.vue?vue&type=script&lang=js& */ "./resources/js/src/Core/components/Sidebar/Sidebar.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport *//* harmony import */ var _node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../../../../../node_modules/vue-loader/lib/runtime/componentNormalizer.js */ "./node_modules/vue-loader/lib/runtime/componentNormalizer.js");





/* normalize component */

var component = Object(_node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__["default"])(
  _Sidebar_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__["default"],
  _Sidebar_vue_vue_type_template_id_5ece29f2___WEBPACK_IMPORTED_MODULE_0__["render"],
  _Sidebar_vue_vue_type_template_id_5ece29f2___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"],
  false,
  null,
  null,
  null
  
)

/* hot reload */
if (false) { var api; }
component.options.__file = "resources/js/src/Core/components/Sidebar/Sidebar.vue"
/* harmony default export */ __webpack_exports__["default"] = (component.exports);

/***/ }),

/***/ "./resources/js/src/Core/components/Sidebar/Sidebar.vue?vue&type=script&lang=js&":
/*!***************************************************************************************!*\
  !*** ./resources/js/src/Core/components/Sidebar/Sidebar.vue?vue&type=script&lang=js& ***!
  \***************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_Sidebar_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../../../node_modules/babel-loader/lib??ref--4-0!../../../../../../node_modules/vue-loader/lib??vue-loader-options!./Sidebar.vue?vue&type=script&lang=js& */ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/src/Core/components/Sidebar/Sidebar.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport */ /* harmony default export */ __webpack_exports__["default"] = (_node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_Sidebar_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__["default"]); 

/***/ }),

/***/ "./resources/js/src/Core/components/Sidebar/Sidebar.vue?vue&type=template&id=5ece29f2&":
/*!*********************************************************************************************!*\
  !*** ./resources/js/src/Core/components/Sidebar/Sidebar.vue?vue&type=template&id=5ece29f2& ***!
  \*********************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_Sidebar_vue_vue_type_template_id_5ece29f2___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../../../node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!../../../../../../node_modules/vue-loader/lib??vue-loader-options!./Sidebar.vue?vue&type=template&id=5ece29f2& */ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/src/Core/components/Sidebar/Sidebar.vue?vue&type=template&id=5ece29f2&");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "render", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_Sidebar_vue_vue_type_template_id_5ece29f2___WEBPACK_IMPORTED_MODULE_0__["render"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_Sidebar_vue_vue_type_template_id_5ece29f2___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"]; });



/***/ }),

/***/ "./resources/js/src/Core/components/Sidebar/sideBarItems.js":
/*!******************************************************************!*\
  !*** ./resources/js/src/Core/components/Sidebar/sideBarItems.js ***!
  \******************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
var sidebarItems = [{
  componentName: 'admin.dashboard',
  iconClasses: 'fas fa-area-chart',
  name: 'Dashboard',
  permission: 'admin.dashboard'
}, {
  iconClasses: 'fas fa-users',
  name: 'User Management',
  permission: 'admin.users',
  children: [{
    componentName: 'admin.users.index',
    defaultParams: {
      type: 'employee'
    },
    name: 'Employee'
  }, {
    componentName: 'admin.users.index',
    defaultParams: {
      type: 'property-owner'
    },
    name: 'Property Owner'
  }]
}, {
  componentName: 'admin.properties.index',
  permission: 'admin.properties',
  iconClasses: 'fas fa-building',
  name: 'Property Management'
}, {
  componentName: 'admin.subscriptions.index',
  iconClasses: 'fas fa-money-bill',
  name: 'Subscription Log',
  permission: 'admin.subscriptions'
}, {
  componentName: 'admin.logs.index',
  iconClasses: 'fas fa-file-alt',
  name: 'Task Log',
  permission: 'admin.logs'
}, {
  componentName: 'admin.feedback.index',
  iconClasses: 'fas fa-comments',
  name: 'Feedback',
  permission: 'admin.feedbacks'
}, {
  componentName: 'admin.packages.index',
  iconClasses: 'fas fa-clipboard-list',
  name: 'Package Management',
  permission: 'admin.packages'
}, {
  componentName: 'admin.admins.index',
  iconClasses: 'fas fa-user',
  name: 'Sub-Admin Management',
  permission: 'admin.admins'
}
/*{
    componentName: 'AdminHome',
    iconClasses: 'fa fa-th-large',
    name: 'Product Management',
    children: [
    	{
            componentName: 'Users',
            iconClasses: '',
            name: 'Inventory Management',
        },
        {
            componentName: 'Users',
            iconClasses: '',
            name: 'Category Management',
        },
        {
            componentName: 'Users',
            iconClasses: '',
            name: 'Attribute Management',
        },
      ],
},*/
];
/* harmony default export */ __webpack_exports__["default"] = (sidebarItems);

/***/ })

}]);