(window["webpackJsonp"] = window["webpackJsonp"] || []).push([[96],{

/***/ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/src/views/Pages/web/Task/components/BoardView.vue?vue&type=script&lang=js&":
/*!*********************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/babel-loader/lib??ref--4-0!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/src/views/Pages/web/Task/components/BoardView.vue?vue&type=script&lang=js& ***!
  \*********************************************************************************************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
var AllLogs = function AllLogs() {
  return Promise.all(/*! import() */[__webpack_require__.e(3), __webpack_require__.e(2)]).then(__webpack_require__.bind(null, /*! ./Logs.vue */ "./resources/js/src/views/Pages/web/Task/components/Logs.vue"));
};

var InprogressTasks = function InprogressTasks() {
  return Promise.all(/*! import() */[__webpack_require__.e(3), __webpack_require__.e(2)]).then(__webpack_require__.bind(null, /*! ./Logs.vue */ "./resources/js/src/views/Pages/web/Task/components/Logs.vue"));
};

var CompletedTasks = function CompletedTasks() {
  return Promise.all(/*! import() */[__webpack_require__.e(3), __webpack_require__.e(2)]).then(__webpack_require__.bind(null, /*! ./Logs.vue */ "./resources/js/src/views/Pages/web/Task/components/Logs.vue"));
};

/* harmony default export */ __webpack_exports__["default"] = ({
  props: {
    selectedProperty: {
      type: Number,
      "default": null
    },
    search: {
      type: String,
      "default": null
    },
    allLogsKey: {
      type: Number,
      "default": 0
    },
    inProgressKey: {
      type: Number,
      "default": 0
    },
    completedKey: {
      type: Number,
      "default": 0
    }
  },
  inheritAttrs: false,
  components: {
    AllLogs: AllLogs,
    InprogressTasks: InprogressTasks,
    CompletedTasks: CompletedTasks
  }
});

/***/ }),

/***/ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/src/views/Pages/web/Task/components/BoardView.vue?vue&type=template&id=552ea688&":
/*!*************************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/src/views/Pages/web/Task/components/BoardView.vue?vue&type=template&id=552ea688& ***!
  \*************************************************************************************************************************************************************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "render", function() { return render; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return staticRenderFns; });
var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c("div", { staticClass: "main-board" }, [
    _c(
      "div",
      { staticClass: "row" },
      [
        _c("all-logs", {
          key: "all" + _vm.allLogsKey,
          attrs: { search: _vm.search, property: _vm.selectedProperty },
          on: {
            update: function($event) {
              return _vm.$emit("update")
            }
          }
        }),
        _vm._v(" "),
        _c("inprogress-tasks", {
          key: "in_progress" + _vm.inProgressKey,
          attrs: {
            search: _vm.search,
            property: _vm.selectedProperty,
            variable: "in_progress",
            mutation: "set_in_progress",
            status: "in_progress",
            title: "In Progress"
          },
          on: {
            update: function($event) {
              return _vm.$emit("update")
            }
          }
        }),
        _vm._v(" "),
        _c("completed-tasks", {
          key: "completed" + _vm.completedKey,
          attrs: {
            search: _vm.search,
            property: _vm.selectedProperty,
            variable: "completed",
            mutation: "set_completed",
            status: "completed",
            title: "Done"
          },
          on: {
            update: function($event) {
              return _vm.$emit("update")
            }
          }
        })
      ],
      1
    )
  ])
}
var staticRenderFns = []
render._withStripped = true



/***/ }),

/***/ "./resources/js/src/views/Pages/web/Task/components/BoardView.vue":
/*!************************************************************************!*\
  !*** ./resources/js/src/views/Pages/web/Task/components/BoardView.vue ***!
  \************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _BoardView_vue_vue_type_template_id_552ea688___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./BoardView.vue?vue&type=template&id=552ea688& */ "./resources/js/src/views/Pages/web/Task/components/BoardView.vue?vue&type=template&id=552ea688&");
/* harmony import */ var _BoardView_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./BoardView.vue?vue&type=script&lang=js& */ "./resources/js/src/views/Pages/web/Task/components/BoardView.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport *//* harmony import */ var _node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../../../../../../../node_modules/vue-loader/lib/runtime/componentNormalizer.js */ "./node_modules/vue-loader/lib/runtime/componentNormalizer.js");





/* normalize component */

var component = Object(_node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__["default"])(
  _BoardView_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__["default"],
  _BoardView_vue_vue_type_template_id_552ea688___WEBPACK_IMPORTED_MODULE_0__["render"],
  _BoardView_vue_vue_type_template_id_552ea688___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"],
  false,
  null,
  null,
  null
  
)

/* hot reload */
if (false) { var api; }
component.options.__file = "resources/js/src/views/Pages/web/Task/components/BoardView.vue"
/* harmony default export */ __webpack_exports__["default"] = (component.exports);

/***/ }),

/***/ "./resources/js/src/views/Pages/web/Task/components/BoardView.vue?vue&type=script&lang=js&":
/*!*************************************************************************************************!*\
  !*** ./resources/js/src/views/Pages/web/Task/components/BoardView.vue?vue&type=script&lang=js& ***!
  \*************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_BoardView_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../../../../../node_modules/babel-loader/lib??ref--4-0!../../../../../../../../node_modules/vue-loader/lib??vue-loader-options!./BoardView.vue?vue&type=script&lang=js& */ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/src/views/Pages/web/Task/components/BoardView.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport */ /* harmony default export */ __webpack_exports__["default"] = (_node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_BoardView_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__["default"]); 

/***/ }),

/***/ "./resources/js/src/views/Pages/web/Task/components/BoardView.vue?vue&type=template&id=552ea688&":
/*!*******************************************************************************************************!*\
  !*** ./resources/js/src/views/Pages/web/Task/components/BoardView.vue?vue&type=template&id=552ea688& ***!
  \*******************************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_BoardView_vue_vue_type_template_id_552ea688___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../../../../../node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!../../../../../../../../node_modules/vue-loader/lib??vue-loader-options!./BoardView.vue?vue&type=template&id=552ea688& */ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/src/views/Pages/web/Task/components/BoardView.vue?vue&type=template&id=552ea688&");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "render", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_BoardView_vue_vue_type_template_id_552ea688___WEBPACK_IMPORTED_MODULE_0__["render"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_BoardView_vue_vue_type_template_id_552ea688___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"]; });



/***/ })

}]);