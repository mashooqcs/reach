(window["webpackJsonp"] = window["webpackJsonp"] || []).push([[76],{

/***/ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/src/views/Pages/web/Auth/Register/Owner.vue?vue&type=script&lang=js&":
/*!***************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/babel-loader/lib??ref--4-0!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/src/views/Pages/web/Auth/Register/Owner.vue?vue&type=script&lang=js& ***!
  \***************************************************************************************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @babel/runtime/regenerator */ "./node_modules/@babel/runtime/regenerator/index.js");
/* harmony import */ var _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var vue_form_wizard__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! vue-form-wizard */ "./node_modules/vue-form-wizard/dist/vue-form-wizard.js");
/* harmony import */ var vue_form_wizard__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(vue_form_wizard__WEBPACK_IMPORTED_MODULE_1__);


function asyncGeneratorStep(gen, resolve, reject, _next, _throw, key, arg) { try { var info = gen[key](arg); var value = info.value; } catch (error) { reject(error); return; } if (info.done) { resolve(value); } else { Promise.resolve(value).then(_next, _throw); } }

function _asyncToGenerator(fn) { return function () { var self = this, args = arguments; return new Promise(function (resolve, reject) { var gen = fn.apply(self, args); function _next(value) { asyncGeneratorStep(gen, resolve, reject, _next, _throw, "next", value); } function _throw(err) { asyncGeneratorStep(gen, resolve, reject, _next, _throw, "throw", err); } _next(undefined); }); }; }

//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//


var success = function success() {
  return __webpack_require__.e(/*! import() */ 0).then(__webpack_require__.bind(null, /*! @views/Partials/web/popups/Success.vue */ "./resources/js/src/views/Partials/web/popups/Success.vue"));
}; //component code


/* harmony default export */ __webpack_exports__["default"] = ({
  components: {
    FormWizard: vue_form_wizard__WEBPACK_IMPORTED_MODULE_1__["FormWizard"],
    TabContent: vue_form_wizard__WEBPACK_IMPORTED_MODULE_1__["TabContent"],
    WizardStep: vue_form_wizard__WEBPACK_IMPORTED_MODULE_1__["WizardStep"],
    success: success
  },
  data: function data() {
    return {
      currentTab: 1,
      form: {
        plan_id: 0
      },
      subscriptions: []
    };
  },
  created: function created() {
    var _this = this;

    return _asyncToGenerator( /*#__PURE__*/_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.mark(function _callee() {
      return _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.wrap(function _callee$(_context) {
        while (1) {
          switch (_context.prev = _context.next) {
            case 0:
              _context.next = 2;
              return _this.getSubscriptions();

            case 2:
            case "end":
              return _context.stop();
          }
        }
      }, _callee);
    }))();
  },
  mounted: function mounted() {
    var _this2 = this;

    return _asyncToGenerator( /*#__PURE__*/_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.mark(function _callee2() {
      return _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.wrap(function _callee2$(_context2) {
        while (1) {
          switch (_context2.prev = _context2.next) {
            case 0:
              _this2.currentTab = _this2.$refs.wizard.activeTabIndex + 1;

            case 1:
            case "end":
              return _context2.stop();
          }
        }
      }, _callee2);
    }))();
  },
  watch: {
    'form.card_number': function formCard_number(val, oldVal) {
      val = val.replace(/[^\dA-Z]/g, '').replace(/(.{4})/g, '$1-').trim('-').slice(0, 19);
      this.form.card_number = val;
      /*if (val.length == 21) {
          this.form.card_number = oldVal;
      } else {
          // adding spaces
      }*/
    }
  },
  methods: {
    getSubscriptions: function getSubscriptions() {
      var _this3 = this;

      return _asyncToGenerator( /*#__PURE__*/_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.mark(function _callee3() {
        var _yield$axios$get, data;

        return _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.wrap(function _callee3$(_context3) {
          while (1) {
            switch (_context3.prev = _context3.next) {
              case 0:
                _context3.prev = 0;
                _context3.next = 3;
                return axios.get(route('app.subscriptions'));

              case 3:
                _yield$axios$get = _context3.sent;
                data = _yield$axios$get.data;
                _this3.subscriptions = data.subscriptions; // statements

                _context3.next = 10;
                break;

              case 8:
                _context3.prev = 8;
                _context3.t0 = _context3["catch"](0);

              case 10:
              case "end":
                return _context3.stop();
            }
          }
        }, _callee3, null, [[0, 8]]);
      }))();
    },
    create: function create() {
      var _this4 = this;

      return _asyncToGenerator( /*#__PURE__*/_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.mark(function _callee4() {
        var fd, _yield$axios$post, data, message;

        return _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.wrap(function _callee4$(_context4) {
          while (1) {
            switch (_context4.prev = _context4.next) {
              case 0:
                _context4.prev = 0;
                // statements
                fd = new FormData();

                _this4.buildFormData(fd, _this4.form);

                _context4.next = 5;
                return axios.post(route('register'), fd);

              case 5:
                _yield$axios$post = _context4.sent;
                data = _yield$axios$post.data;

                if (data.status) {
                  window.$('#successPopup').modal('show');
                  _this4.form = {
                    plan_id: 0
                  };

                  _this4.$refs.wizardobserver.reset();
                }

                _context4.next = 14;
                break;

              case 10:
                _context4.prev = 10;
                _context4.t0 = _context4["catch"](0);

                if (_context4.t0.response) {
                  _this4.$refs.wizardobserver.setErrors(_context4.t0.response.data.errors);

                  message = _context4.t0.response.data.errors.message;

                  if (message) {
                    _this4.$snotify.error(message);
                  }
                }

                console.log(_context4.t0); // statements

              case 14:
              case "end":
                return _context4.stop();
            }
          }
        }, _callee4, null, [[0, 10]]);
      }))();
    },
    verifyEmail: function verifyEmail() {
      var _this5 = this;

      return _asyncToGenerator( /*#__PURE__*/_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.mark(function _callee5() {
        var _yield$axios$post2, data;

        return _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.wrap(function _callee5$(_context5) {
          while (1) {
            switch (_context5.prev = _context5.next) {
              case 0:
                if (!(_this5.currentTab == 1)) {
                  _context5.next = 15;
                  break;
                }

                _context5.prev = 1;
                _context5.next = 4;
                return axios.post(route('register.check-email'), {
                  email: _this5.form.email
                });

              case 4:
                _yield$axios$post2 = _context5.sent;
                data = _yield$axios$post2.data;
                return _context5.abrupt("return", true);

              case 9:
                _context5.prev = 9;
                _context5.t0 = _context5["catch"](1);

                if (!_context5.t0.response) {
                  _context5.next = 14;
                  break;
                }

                _this5.$refs.wizardobserver.setErrors(_context5.t0.response.data.errors);

                return _context5.abrupt("return", false);

              case 14:
                console.log(_context5.t0);

              case 15:
              case "end":
                return _context5.stop();
            }
          }
        }, _callee5, null, [[1, 9]]);
      }))();
    },
    changeTab: function changeTab(prev, next) {
      console.log();
      this.currentTab = next + 1;
    },
    changingStep: function changingStep(index) {
      var _this6 = this;

      return _asyncToGenerator( /*#__PURE__*/_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.mark(function _callee6() {
        var valid;
        return _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.wrap(function _callee6$(_context6) {
          while (1) {
            switch (_context6.prev = _context6.next) {
              case 0:
                _context6.next = 2;
                return _this6.$refs.wizardobserver.validate(_this6.$event);

              case 2:
                valid = _context6.sent;

                if (_this6.verifyEmail()) {
                  _context6.next = 5;
                  break;
                }

                return _context6.abrupt("return", false);

              case 5:
                _this6.$emit('on-validate', valid, _this6.activeTabIndex);

                return _context6.abrupt("return", valid);

              case 7:
              case "end":
                return _context6.stop();
            }
          }
        }, _callee6);
      }))();
    }
  }
});

/***/ }),

/***/ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/src/views/Pages/web/Auth/Register/Owner.vue?vue&type=template&id=49c3f42e&":
/*!*******************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/src/views/Pages/web/Auth/Register/Owner.vue?vue&type=template&id=49c3f42e& ***!
  \*******************************************************************************************************************************************************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "render", function() { return render; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return staticRenderFns; });
var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c("section", { staticClass: "join-user" }, [
    _c(
      "div",
      { staticClass: "container" },
      [
        _c(
          "div",
          { staticClass: "for-user-bg" },
          [
            _c("ValidationObserver", {
              ref: "wizardobserver",
              scopedSlots: _vm._u([
                {
                  key: "default",
                  fn: function(ref) {
                    var handleSubmit = ref.handleSubmit
                    var validate = ref.validate
                    return [
                      _c(
                        "form",
                        {
                          on: {
                            submit: function($event) {
                              $event.preventDefault()
                              return handleSubmit(_vm.create)
                            }
                          }
                        },
                        [
                          _c(
                            "form-wizard",
                            {
                              ref: "wizard",
                              attrs: {
                                stepsClasses: "d-flex justify-content-around"
                              },
                              on: { "on-change": _vm.changeTab },
                              scopedSlots: _vm._u(
                                [
                                  {
                                    key: "step",
                                    fn: function(tabs) {
                                      return _c("div", { staticClass: "row" }, [
                                        _c(
                                          "div",
                                          {
                                            staticClass: "col-md-4 text-center"
                                          },
                                          [
                                            _c(
                                              "div",
                                              { staticClass: "home-icon" },
                                              [
                                                _c("span", [
                                                  _vm._v(_vm._s(tabs.index + 1))
                                                ]),
                                                _vm._v(" "),
                                                _c(
                                                  "h6",
                                                  {
                                                    staticClass: "text-center"
                                                  },
                                                  [_vm._v("Step")]
                                                )
                                              ]
                                            )
                                          ]
                                        )
                                      ])
                                    }
                                  },
                                  {
                                    key: "footer",
                                    fn: function(props) {
                                      return _c("div", { staticClass: "row" }, [
                                        _c("div", { staticClass: "col-md-6" }, [
                                          props.activeTabIndex != 0
                                            ? _c(
                                                "button",
                                                {
                                                  staticClass:
                                                    "veri-green-button",
                                                  attrs: { type: "button" },
                                                  on: {
                                                    click: function($event) {
                                                      return props.prevTab()
                                                    }
                                                  }
                                                },
                                                [_vm._v("BACK")]
                                              )
                                            : _vm._e()
                                        ]),
                                        _vm._v(" "),
                                        _c("div", { staticClass: "col-md-6" }, [
                                          !props.isLastStep
                                            ? _c(
                                                "button",
                                                {
                                                  staticClass:
                                                    "veri-green-button",
                                                  attrs: { type: "button" },
                                                  on: {
                                                    click: function($event) {
                                                      return props.nextTab()
                                                    }
                                                  }
                                                },
                                                [_vm._v("NEXT")]
                                              )
                                            : _c(
                                                "button",
                                                {
                                                  staticClass:
                                                    "veri-green-button",
                                                  attrs: { type: "submit" }
                                                },
                                                [_vm._v("Complete")]
                                              )
                                        ])
                                      ])
                                    }
                                  }
                                ],
                                null,
                                true
                              )
                            },
                            [
                              _c("template", { slot: "title" }, [
                                _c("h1", [
                                  _vm._v(
                                    "Step " + _vm._s(_vm.currentTab) + " of 3"
                                  )
                                ])
                              ]),
                              _vm._v(" "),
                              _c("hr"),
                              _vm._v(" "),
                              _vm._v(" "),
                              _c(
                                "tab-content",
                                {
                                  attrs: {
                                    title: "Step",
                                    "before-change": _vm.changingStep
                                  }
                                },
                                [
                                  _c("h4", [
                                    _vm._v(
                                      "Create you account to start your membership"
                                    )
                                  ]),
                                  _vm._v(" "),
                                  _c("ValidationProvider", {
                                    staticClass: "form-group",
                                    attrs: { rules: "required", name: "Name" },
                                    scopedSlots: _vm._u(
                                      [
                                        {
                                          key: "default",
                                          fn: function(ref) {
                                            var errors = ref.errors
                                            return [
                                              _c(
                                                "label",
                                                { attrs: { for: "name" } },
                                                [_vm._v("Name")]
                                              ),
                                              _vm._v(" "),
                                              _c("input", {
                                                directives: [
                                                  {
                                                    name: "model",
                                                    rawName: "v-model",
                                                    value: _vm.form.name,
                                                    expression: "form.name"
                                                  }
                                                ],
                                                staticClass: "form-control",
                                                attrs: {
                                                  type: "name",
                                                  id: "name",
                                                  "aria-describedby":
                                                    "emailHelp",
                                                  placeholder: "Enter Name"
                                                },
                                                domProps: {
                                                  value: _vm.form.name
                                                },
                                                on: {
                                                  input: function($event) {
                                                    if (
                                                      $event.target.composing
                                                    ) {
                                                      return
                                                    }
                                                    _vm.$set(
                                                      _vm.form,
                                                      "name",
                                                      $event.target.value
                                                    )
                                                  }
                                                }
                                              }),
                                              _vm._v(" "),
                                              _c(
                                                "div",
                                                { staticClass: "text-danger" },
                                                [_vm._v(_vm._s(errors[0]))]
                                              )
                                            ]
                                          }
                                        }
                                      ],
                                      null,
                                      true
                                    )
                                  }),
                                  _vm._v(" "),
                                  _c("ValidationProvider", {
                                    staticClass: "form-group",
                                    attrs: {
                                      vid: "email",
                                      rules: "required",
                                      name: "Email"
                                    },
                                    scopedSlots: _vm._u(
                                      [
                                        {
                                          key: "default",
                                          fn: function(ref) {
                                            var errors = ref.errors
                                            return [
                                              _c(
                                                "label",
                                                {
                                                  attrs: {
                                                    for: "exampleInputEmail1"
                                                  }
                                                },
                                                [_vm._v("Email")]
                                              ),
                                              _vm._v(" "),
                                              _c("input", {
                                                directives: [
                                                  {
                                                    name: "model",
                                                    rawName: "v-model",
                                                    value: _vm.form.email,
                                                    expression: "form.email"
                                                  }
                                                ],
                                                staticClass: "form-control",
                                                attrs: {
                                                  type: "email",
                                                  id: "exampleInputEmail1",
                                                  "aria-describedby":
                                                    "emailHelp",
                                                  placeholder: "Enter Email"
                                                },
                                                domProps: {
                                                  value: _vm.form.email
                                                },
                                                on: {
                                                  blur: _vm.verifyEmail,
                                                  input: function($event) {
                                                    if (
                                                      $event.target.composing
                                                    ) {
                                                      return
                                                    }
                                                    _vm.$set(
                                                      _vm.form,
                                                      "email",
                                                      $event.target.value
                                                    )
                                                  }
                                                }
                                              }),
                                              _vm._v(" "),
                                              _c(
                                                "div",
                                                { staticClass: "text-danger" },
                                                [_vm._v(_vm._s(errors[0]))]
                                              )
                                            ]
                                          }
                                        }
                                      ],
                                      null,
                                      true
                                    )
                                  }),
                                  _vm._v(" "),
                                  _c("ValidationProvider", {
                                    staticClass: "form-group",
                                    attrs: {
                                      rules:
                                        "required|confirmed:password_confirmation",
                                      name: "Password"
                                    },
                                    scopedSlots: _vm._u(
                                      [
                                        {
                                          key: "default",
                                          fn: function(ref) {
                                            var errors = ref.errors
                                            return [
                                              _c(
                                                "label",
                                                { attrs: { for: "password" } },
                                                [_vm._v("Password")]
                                              ),
                                              _vm._v(" "),
                                              _c("input", {
                                                directives: [
                                                  {
                                                    name: "model",
                                                    rawName: "v-model",
                                                    value: _vm.form.password,
                                                    expression: "form.password"
                                                  }
                                                ],
                                                staticClass: "form-control",
                                                attrs: {
                                                  type: "password",
                                                  id: "password",
                                                  "aria-describedby":
                                                    "emailHelp",
                                                  placeholder:
                                                    "Enter Your Password"
                                                },
                                                domProps: {
                                                  value: _vm.form.password
                                                },
                                                on: {
                                                  input: function($event) {
                                                    if (
                                                      $event.target.composing
                                                    ) {
                                                      return
                                                    }
                                                    _vm.$set(
                                                      _vm.form,
                                                      "password",
                                                      $event.target.value
                                                    )
                                                  }
                                                }
                                              }),
                                              _vm._v(" "),
                                              _c(
                                                "div",
                                                { staticClass: "text-danger" },
                                                [_vm._v(_vm._s(errors[0]))]
                                              )
                                            ]
                                          }
                                        }
                                      ],
                                      null,
                                      true
                                    )
                                  }),
                                  _vm._v(" "),
                                  _c("ValidationProvider", {
                                    staticClass: "form-group",
                                    attrs: {
                                      vid: "password_confirmation",
                                      rules: "required",
                                      name: "Confirm Password"
                                    },
                                    scopedSlots: _vm._u(
                                      [
                                        {
                                          key: "default",
                                          fn: function(ref) {
                                            var errors = ref.errors
                                            return [
                                              _c(
                                                "label",
                                                {
                                                  attrs: {
                                                    for: "password_confirmation"
                                                  }
                                                },
                                                [_vm._v("Confirm Password")]
                                              ),
                                              _vm._v(" "),
                                              _c("input", {
                                                directives: [
                                                  {
                                                    name: "model",
                                                    rawName: "v-model",
                                                    value:
                                                      _vm.form
                                                        .password_confirmation,
                                                    expression:
                                                      "form.password_confirmation"
                                                  }
                                                ],
                                                staticClass: "form-control",
                                                attrs: {
                                                  type: "password",
                                                  id: "password_confirmation",
                                                  "aria-describedby":
                                                    "emailHelp",
                                                  placeholder:
                                                    "Enter Your Password"
                                                },
                                                domProps: {
                                                  value:
                                                    _vm.form
                                                      .password_confirmation
                                                },
                                                on: {
                                                  input: function($event) {
                                                    if (
                                                      $event.target.composing
                                                    ) {
                                                      return
                                                    }
                                                    _vm.$set(
                                                      _vm.form,
                                                      "password_confirmation",
                                                      $event.target.value
                                                    )
                                                  }
                                                }
                                              }),
                                              _vm._v(" "),
                                              _c(
                                                "div",
                                                { staticClass: "text-danger" },
                                                [_vm._v(_vm._s(errors[0]))]
                                              )
                                            ]
                                          }
                                        }
                                      ],
                                      null,
                                      true
                                    )
                                  })
                                ],
                                1
                              ),
                              _vm._v(" "),
                              _c(
                                "tab-content",
                                {
                                  attrs: {
                                    title: "Step",
                                    "before-change": _vm.changingStep
                                  }
                                },
                                [
                                  _vm.currentTab == 2
                                    ? _c("ValidationProvider", {
                                        staticClass: "price-card",
                                        attrs: {
                                          tag: "div",
                                          rules: "min_value:1"
                                        },
                                        scopedSlots: _vm._u(
                                          [
                                            {
                                              key: "default",
                                              fn: function(ref) {
                                                var errors = ref.errors
                                                return [
                                                  _c(
                                                    "div",
                                                    {
                                                      staticClass:
                                                        "row justify-content-center mb-4"
                                                    },
                                                    [
                                                      _c("input", {
                                                        directives: [
                                                          {
                                                            name: "model",
                                                            rawName: "v-model",
                                                            value:
                                                              _vm.form.plan_id,
                                                            expression:
                                                              "form.plan_id"
                                                          }
                                                        ],
                                                        attrs: {
                                                          type: "hidden"
                                                        },
                                                        domProps: {
                                                          value:
                                                            _vm.form.plan_id
                                                        },
                                                        on: {
                                                          input: function(
                                                            $event
                                                          ) {
                                                            if (
                                                              $event.target
                                                                .composing
                                                            ) {
                                                              return
                                                            }
                                                            _vm.$set(
                                                              _vm.form,
                                                              "plan_id",
                                                              $event.target
                                                                .value
                                                            )
                                                          }
                                                        }
                                                      }),
                                                      _vm._v(" "),
                                                      _vm._l(
                                                        _vm.subscriptions,
                                                        function(plan) {
                                                          return _c(
                                                            "div",
                                                            {
                                                              staticClass:
                                                                "col-xl-3 col-lg-4 col-md-6 col-12 cursor-pointer"
                                                            },
                                                            [
                                                              _c(
                                                                "div",
                                                                {
                                                                  staticClass:
                                                                    "columns",
                                                                  class: {
                                                                    "border border-dark":
                                                                      _vm.form
                                                                        .plan_id ==
                                                                      plan.id
                                                                  },
                                                                  on: {
                                                                    click: function(
                                                                      $event
                                                                    ) {
                                                                      _vm.form.plan_id =
                                                                        plan.id
                                                                    }
                                                                  }
                                                                },
                                                                [
                                                                  _c(
                                                                    "ul",
                                                                    {
                                                                      staticClass:
                                                                        "price"
                                                                    },
                                                                    [
                                                                      _c(
                                                                        "li",
                                                                        {
                                                                          staticClass:
                                                                            "header"
                                                                        },
                                                                        [
                                                                          _vm._v(
                                                                            _vm._s(
                                                                              plan.title
                                                                            )
                                                                          )
                                                                        ]
                                                                      ),
                                                                      _vm._v(
                                                                        " "
                                                                      ),
                                                                      _c("li", [
                                                                        _vm._v(
                                                                          "$\n                                                    "
                                                                        ),
                                                                        _c(
                                                                          "span",
                                                                          {
                                                                            staticClass:
                                                                              "for-big"
                                                                          },
                                                                          [
                                                                            _vm._v(
                                                                              _vm._s(
                                                                                plan.amount
                                                                              )
                                                                            )
                                                                          ]
                                                                        ),
                                                                        _vm._v(
                                                                          " "
                                                                        ),
                                                                        _c(
                                                                          "sub",
                                                                          [
                                                                            _vm._v(
                                                                              _vm._s(
                                                                                plan.duration
                                                                              ) +
                                                                                " month"
                                                                            )
                                                                          ]
                                                                        )
                                                                      ]),
                                                                      _vm._v(
                                                                        " "
                                                                      ),
                                                                      _c("li", [
                                                                        _c(
                                                                          "p",
                                                                          [
                                                                            _vm._v(
                                                                              _vm._s(
                                                                                plan.description
                                                                              )
                                                                            )
                                                                          ]
                                                                        )
                                                                      ])
                                                                    ]
                                                                  )
                                                                ]
                                                              )
                                                            ]
                                                          )
                                                        }
                                                      )
                                                    ],
                                                    2
                                                  ),
                                                  _vm._v(" "),
                                                  errors[0]
                                                    ? _c(
                                                        "div",
                                                        {
                                                          staticClass:
                                                            "text-danger justify-content-center text-center"
                                                        },
                                                        [
                                                          _vm._v(
                                                            "please select a package to continue"
                                                          )
                                                        ]
                                                      )
                                                    : _vm._e()
                                                ]
                                              }
                                            }
                                          ],
                                          null,
                                          true
                                        )
                                      })
                                    : _vm._e()
                                ],
                                1
                              ),
                              _vm._v(" "),
                              _c("tab-content", { attrs: { title: "Step" } }, [
                                _vm.currentTab == 3
                                  ? _c(
                                      "div",
                                      [
                                        _c("ValidationProvider", {
                                          staticClass: "form-group",
                                          attrs: {
                                            tag: "div",
                                            rules: "required",
                                            name: "Card Holder Name"
                                          },
                                          scopedSlots: _vm._u(
                                            [
                                              {
                                                key: "default",
                                                fn: function(ref) {
                                                  var errors = ref.errors
                                                  return [
                                                    _c(
                                                      "label",
                                                      {
                                                        attrs: {
                                                          for:
                                                            "card_holder_name"
                                                        }
                                                      },
                                                      [
                                                        _vm._v(
                                                          "Card Holder Name *"
                                                        )
                                                      ]
                                                    ),
                                                    _vm._v(" "),
                                                    _c("input", {
                                                      directives: [
                                                        {
                                                          name: "model",
                                                          rawName: "v-model",
                                                          value:
                                                            _vm.form
                                                              .card_holder_name,
                                                          expression:
                                                            "form.card_holder_name"
                                                        }
                                                      ],
                                                      staticClass:
                                                        "form-control",
                                                      attrs: {
                                                        type: "text",
                                                        id: "card_holder_name",
                                                        "aria-describedby":
                                                          "emailHelp",
                                                        placeholder:
                                                          "Enter Card Holder Name"
                                                      },
                                                      domProps: {
                                                        value:
                                                          _vm.form
                                                            .card_holder_name
                                                      },
                                                      on: {
                                                        input: function(
                                                          $event
                                                        ) {
                                                          if (
                                                            $event.target
                                                              .composing
                                                          ) {
                                                            return
                                                          }
                                                          _vm.$set(
                                                            _vm.form,
                                                            "card_holder_name",
                                                            $event.target.value
                                                          )
                                                        }
                                                      }
                                                    }),
                                                    _vm._v(" "),
                                                    _c(
                                                      "div",
                                                      {
                                                        staticClass:
                                                          "text-danger"
                                                      },
                                                      [
                                                        _vm._v(
                                                          _vm._s(errors[0])
                                                        )
                                                      ]
                                                    )
                                                  ]
                                                }
                                              }
                                            ],
                                            null,
                                            true
                                          )
                                        }),
                                        _vm._v(" "),
                                        _c("ValidationProvider", {
                                          staticClass: "form-group",
                                          attrs: {
                                            vid: "card_number",
                                            tag: "div",
                                            rules: "required",
                                            name: "Card Number"
                                          },
                                          scopedSlots: _vm._u(
                                            [
                                              {
                                                key: "default",
                                                fn: function(ref) {
                                                  var errors = ref.errors
                                                  return [
                                                    _c(
                                                      "label",
                                                      {
                                                        attrs: {
                                                          for: "card_number"
                                                        }
                                                      },
                                                      [_vm._v("Card Number *")]
                                                    ),
                                                    _vm._v(" "),
                                                    _c("input", {
                                                      directives: [
                                                        {
                                                          name: "model",
                                                          rawName: "v-model",
                                                          value:
                                                            _vm.form
                                                              .card_number,
                                                          expression:
                                                            "form.card_number"
                                                        }
                                                      ],
                                                      staticClass:
                                                        "form-control",
                                                      attrs: {
                                                        type: "text",
                                                        id: "card_number",
                                                        "aria-describedby":
                                                          "emailHelp",
                                                        placeholder:
                                                          "Enter Card Number"
                                                      },
                                                      domProps: {
                                                        value:
                                                          _vm.form.card_number
                                                      },
                                                      on: {
                                                        input: function(
                                                          $event
                                                        ) {
                                                          if (
                                                            $event.target
                                                              .composing
                                                          ) {
                                                            return
                                                          }
                                                          _vm.$set(
                                                            _vm.form,
                                                            "card_number",
                                                            $event.target.value
                                                          )
                                                        }
                                                      }
                                                    }),
                                                    _vm._v(" "),
                                                    _c(
                                                      "div",
                                                      {
                                                        staticClass:
                                                          "text-danger"
                                                      },
                                                      [
                                                        _vm._v(
                                                          _vm._s(errors[0])
                                                        )
                                                      ]
                                                    )
                                                  ]
                                                }
                                              }
                                            ],
                                            null,
                                            true
                                          )
                                        }),
                                        _vm._v(" "),
                                        _c("ValidationProvider", {
                                          staticClass: "form-group",
                                          attrs: {
                                            vid: "cvv",
                                            tag: "div",
                                            rules: "required|min:3|max:4",
                                            name: "CVV Number"
                                          },
                                          scopedSlots: _vm._u(
                                            [
                                              {
                                                key: "default",
                                                fn: function(ref) {
                                                  var errors = ref.errors
                                                  return [
                                                    _c(
                                                      "label",
                                                      {
                                                        attrs: {
                                                          for: "cvv_number"
                                                        }
                                                      },
                                                      [_vm._v("CVV Number *")]
                                                    ),
                                                    _vm._v(" "),
                                                    _c("input", {
                                                      directives: [
                                                        {
                                                          name: "model",
                                                          rawName: "v-model",
                                                          value: _vm.form.cvv,
                                                          expression: "form.cvv"
                                                        }
                                                      ],
                                                      staticClass:
                                                        "form-control",
                                                      attrs: {
                                                        type: "text",
                                                        id: "cvv_number",
                                                        "aria-describedby":
                                                          "emailHelp",
                                                        placeholder:
                                                          "Enter CVV Number"
                                                      },
                                                      domProps: {
                                                        value: _vm.form.cvv
                                                      },
                                                      on: {
                                                        input: function(
                                                          $event
                                                        ) {
                                                          if (
                                                            $event.target
                                                              .composing
                                                          ) {
                                                            return
                                                          }
                                                          _vm.$set(
                                                            _vm.form,
                                                            "cvv",
                                                            $event.target.value
                                                          )
                                                        }
                                                      }
                                                    }),
                                                    _vm._v(" "),
                                                    _c(
                                                      "div",
                                                      {
                                                        staticClass:
                                                          "text-danger"
                                                      },
                                                      [
                                                        _vm._v(
                                                          _vm._s(errors[0])
                                                        )
                                                      ]
                                                    )
                                                  ]
                                                }
                                              }
                                            ],
                                            null,
                                            true
                                          )
                                        }),
                                        _vm._v(" "),
                                        _c(
                                          "div",
                                          { staticClass: "row" },
                                          [
                                            _c("ValidationProvider", {
                                              staticClass:
                                                "form-group col-md-6",
                                              attrs: {
                                                vid: "expiry_month",
                                                tag: "div",
                                                rules: "required",
                                                name: "Expiry Month"
                                              },
                                              scopedSlots: _vm._u(
                                                [
                                                  {
                                                    key: "default",
                                                    fn: function(ref) {
                                                      var errors = ref.errors
                                                      return [
                                                        _c(
                                                          "label",
                                                          {
                                                            attrs: {
                                                              for: "expiry"
                                                            }
                                                          },
                                                          [
                                                            _vm._v(
                                                              "Expiry Month *"
                                                            )
                                                          ]
                                                        ),
                                                        _vm._v(" "),
                                                        _c("input", {
                                                          directives: [
                                                            {
                                                              name: "model",
                                                              rawName:
                                                                "v-model",
                                                              value:
                                                                _vm.form
                                                                  .expiry_month,
                                                              expression:
                                                                "form.expiry_month"
                                                            }
                                                          ],
                                                          staticClass:
                                                            "form-control",
                                                          attrs: {
                                                            type: "text",
                                                            id: "expiry",
                                                            "aria-describedby":
                                                              "emailHelp",
                                                            placeholder:
                                                              "Enter Expiry"
                                                          },
                                                          domProps: {
                                                            value:
                                                              _vm.form
                                                                .expiry_month
                                                          },
                                                          on: {
                                                            input: function(
                                                              $event
                                                            ) {
                                                              if (
                                                                $event.target
                                                                  .composing
                                                              ) {
                                                                return
                                                              }
                                                              _vm.$set(
                                                                _vm.form,
                                                                "expiry_month",
                                                                $event.target
                                                                  .value
                                                              )
                                                            }
                                                          }
                                                        }),
                                                        _vm._v(" "),
                                                        _c(
                                                          "div",
                                                          {
                                                            staticClass:
                                                              "text-danger"
                                                          },
                                                          [
                                                            _vm._v(
                                                              _vm._s(errors[0])
                                                            )
                                                          ]
                                                        )
                                                      ]
                                                    }
                                                  }
                                                ],
                                                null,
                                                true
                                              )
                                            }),
                                            _vm._v(" "),
                                            _c("ValidationProvider", {
                                              staticClass:
                                                "form-group col-md-6",
                                              attrs: {
                                                vid: "expiry_year",
                                                tag: "div",
                                                rules: "required",
                                                name: "Expiry Year"
                                              },
                                              scopedSlots: _vm._u(
                                                [
                                                  {
                                                    key: "default",
                                                    fn: function(ref) {
                                                      var errors = ref.errors
                                                      return [
                                                        _c(
                                                          "label",
                                                          {
                                                            attrs: {
                                                              for: "expiry"
                                                            }
                                                          },
                                                          [
                                                            _vm._v(
                                                              "Expiry Year *"
                                                            )
                                                          ]
                                                        ),
                                                        _vm._v(" "),
                                                        _c("input", {
                                                          directives: [
                                                            {
                                                              name: "model",
                                                              rawName:
                                                                "v-model",
                                                              value:
                                                                _vm.form
                                                                  .expiry_year,
                                                              expression:
                                                                "form.expiry_year"
                                                            }
                                                          ],
                                                          staticClass:
                                                            "form-control",
                                                          attrs: {
                                                            type: "text",
                                                            id: "expiry",
                                                            "aria-describedby":
                                                              "emailHelp",
                                                            placeholder:
                                                              "Enter Expiry"
                                                          },
                                                          domProps: {
                                                            value:
                                                              _vm.form
                                                                .expiry_year
                                                          },
                                                          on: {
                                                            input: function(
                                                              $event
                                                            ) {
                                                              if (
                                                                $event.target
                                                                  .composing
                                                              ) {
                                                                return
                                                              }
                                                              _vm.$set(
                                                                _vm.form,
                                                                "expiry_year",
                                                                $event.target
                                                                  .value
                                                              )
                                                            }
                                                          }
                                                        }),
                                                        _vm._v(" "),
                                                        _c(
                                                          "div",
                                                          {
                                                            staticClass:
                                                              "text-danger"
                                                          },
                                                          [
                                                            _vm._v(
                                                              _vm._s(errors[0])
                                                            )
                                                          ]
                                                        )
                                                      ]
                                                    }
                                                  }
                                                ],
                                                null,
                                                true
                                              )
                                            })
                                          ],
                                          1
                                        )
                                      ],
                                      1
                                    )
                                  : _vm._e()
                              ])
                            ],
                            2
                          )
                        ],
                        1
                      )
                    ]
                  }
                }
              ])
            })
          ],
          1
        ),
        _vm._v(" "),
        _c(
          "success",
          { attrs: { title: "Sign Up Successful!" } },
          [
            _c(
              "router-link",
              {
                staticClass: "green-small",
                attrs: {
                  "data-dismiss": "modal",
                  to: { name: "web.auth.login" }
                }
              },
              [
                _c("small", { attrs: { id: "emailHelp" } }, [
                  _vm._v("Proceed to Login")
                ])
              ]
            )
          ],
          1
        ),
        _vm._v(" "),
        _vm._m(0),
        _vm._v(" "),
        _vm._m(1)
      ],
      1
    )
  ])
}
var staticRenderFns = [
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c(
      "div",
      {
        staticClass: "modal fade",
        attrs: {
          id: "verify-email",
          tabindex: "-1",
          "aria-labelledby": "exampleModalLabel",
          "aria-hidden": "true"
        }
      },
      [
        _c("div", { staticClass: "modal-dialog modal-dialog-centered" }, [
          _c("div", { staticClass: "modal-content" }, [
            _c("div", { staticClass: "modal-header" }, [
              _c(
                "h2",
                {
                  staticClass: "modal-title",
                  attrs: { id: "exampleModalLabel" }
                },
                [_vm._v("Email Verification")]
              )
            ]),
            _vm._v(" "),
            _c("span", { staticClass: "modal-span" }, [
              _vm._v(
                "A verification code has been sent to your\n                        "
              ),
              _c("br"),
              _vm._v(" Email Address :abc@xyz.com")
            ]),
            _vm._v(" "),
            _c("div", { staticClass: "modal-body" }, [
              _c("form", [
                _c("div", { staticClass: "form-group" }, [
                  _c("label", { attrs: { for: "exampleInputEmail1" } }, [
                    _vm._v("Verification Code")
                  ]),
                  _vm._v(" "),
                  _c("input", {
                    staticClass: "form-control",
                    attrs: {
                      type: "text",
                      id: "exampleInputEmail1",
                      "aria-describedby": "emailHelp",
                      placeholder: "Verification Code"
                    }
                  }),
                  _vm._v(" "),
                  _c(
                    "a",
                    {
                      attrs: {
                        href: "#",
                        "data-toggle": "modal",
                        "data-target": "#exampleModal-2"
                      }
                    },
                    [
                      _c(
                        "small",
                        {
                          staticClass: "green-small",
                          attrs: { id: "emailHelp" }
                        },
                        [
                          _vm._v(
                            "Don't want to use email?\n                                        Verify From Phone"
                          )
                        ]
                      )
                    ]
                  ),
                  _vm._v(" "),
                  _c("a", { attrs: { href: "#" } }, [
                    _c(
                      "small",
                      {
                        staticClass: "green-small-right",
                        attrs: { id: "emailHelp" }
                      },
                      [
                        _vm._v(
                          "Resend\n                                        Code"
                        )
                      ]
                    )
                  ])
                ])
              ])
            ]),
            _vm._v(" "),
            _c("div", { staticClass: "modal-footer" }, [
              _c(
                "button",
                { staticClass: "modal-btn", attrs: { type: "button" } },
                [_vm._v("Continue")]
              )
            ])
          ])
        ])
      ]
    )
  },
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c(
      "div",
      {
        staticClass: "modal fade",
        attrs: {
          id: "exampleModal-2",
          tabindex: "-1",
          "aria-labelledby": "exampleModalLabel",
          "aria-hidden": "true"
        }
      },
      [
        _c("div", { staticClass: "modal-dialog modal-dialog-centered" }, [
          _c("div", { staticClass: "modal-content" }, [
            _c("div", { staticClass: "modal-header" }, [
              _c(
                "h2",
                {
                  staticClass: "modal-title",
                  attrs: { id: "exampleModalLabel" }
                },
                [_vm._v("Email Verification")]
              )
            ]),
            _vm._v(" "),
            _c("span", { staticClass: "modal-span" }, [
              _vm._v("Enter Verification Code You Have Received "),
              _c("br"),
              _vm._v(" On Your Phone\n                        Number")
            ]),
            _vm._v(" "),
            _c("div", { staticClass: "modal-body" }, [
              _c("form", [
                _c("div", { staticClass: "form-group" }, [
                  _c("label", { attrs: { for: "exampleInputEmail1" } }, [
                    _vm._v("Verification Code")
                  ]),
                  _vm._v(" "),
                  _c("input", {
                    staticClass: "form-control",
                    attrs: {
                      type: "text",
                      id: "exampleInputEmail1",
                      "aria-describedby": "emailHelp",
                      placeholder: "Verification Code"
                    }
                  }),
                  _vm._v(" "),
                  _c(
                    "a",
                    {
                      attrs: {
                        href: "#",
                        "data-toggle": "modal",
                        "data-target": "#exampleModal"
                      }
                    },
                    [
                      _c(
                        "small",
                        {
                          staticClass: "green-small",
                          attrs: { id: "emailHelp" }
                        },
                        [
                          _vm._v(
                            "Don't want to use your number?\n                                        Verify From Email"
                          )
                        ]
                      )
                    ]
                  ),
                  _vm._v(" "),
                  _c("a", { attrs: { href: "#" } }, [
                    _c(
                      "small",
                      {
                        staticClass: "green-small-right",
                        attrs: { id: "emailHelp" }
                      },
                      [
                        _vm._v(
                          "Resend\n                                        Code"
                        )
                      ]
                    )
                  ])
                ])
              ])
            ]),
            _vm._v(" "),
            _c("div", { staticClass: "modal-footer" }, [
              _c(
                "button",
                { staticClass: "modal-btn", attrs: { type: "button" } },
                [_vm._v("Continue")]
              )
            ])
          ])
        ])
      ]
    )
  }
]
render._withStripped = true



/***/ }),

/***/ "./resources/js/src/views/Pages/web/Auth/Register/Owner.vue":
/*!******************************************************************!*\
  !*** ./resources/js/src/views/Pages/web/Auth/Register/Owner.vue ***!
  \******************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _Owner_vue_vue_type_template_id_49c3f42e___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./Owner.vue?vue&type=template&id=49c3f42e& */ "./resources/js/src/views/Pages/web/Auth/Register/Owner.vue?vue&type=template&id=49c3f42e&");
/* harmony import */ var _Owner_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./Owner.vue?vue&type=script&lang=js& */ "./resources/js/src/views/Pages/web/Auth/Register/Owner.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport *//* harmony import */ var _node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../../../../../../../node_modules/vue-loader/lib/runtime/componentNormalizer.js */ "./node_modules/vue-loader/lib/runtime/componentNormalizer.js");





/* normalize component */

var component = Object(_node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__["default"])(
  _Owner_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__["default"],
  _Owner_vue_vue_type_template_id_49c3f42e___WEBPACK_IMPORTED_MODULE_0__["render"],
  _Owner_vue_vue_type_template_id_49c3f42e___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"],
  false,
  null,
  null,
  null
  
)

/* hot reload */
if (false) { var api; }
component.options.__file = "resources/js/src/views/Pages/web/Auth/Register/Owner.vue"
/* harmony default export */ __webpack_exports__["default"] = (component.exports);

/***/ }),

/***/ "./resources/js/src/views/Pages/web/Auth/Register/Owner.vue?vue&type=script&lang=js&":
/*!*******************************************************************************************!*\
  !*** ./resources/js/src/views/Pages/web/Auth/Register/Owner.vue?vue&type=script&lang=js& ***!
  \*******************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_Owner_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../../../../../node_modules/babel-loader/lib??ref--4-0!../../../../../../../../node_modules/vue-loader/lib??vue-loader-options!./Owner.vue?vue&type=script&lang=js& */ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/src/views/Pages/web/Auth/Register/Owner.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport */ /* harmony default export */ __webpack_exports__["default"] = (_node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_Owner_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__["default"]); 

/***/ }),

/***/ "./resources/js/src/views/Pages/web/Auth/Register/Owner.vue?vue&type=template&id=49c3f42e&":
/*!*************************************************************************************************!*\
  !*** ./resources/js/src/views/Pages/web/Auth/Register/Owner.vue?vue&type=template&id=49c3f42e& ***!
  \*************************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_Owner_vue_vue_type_template_id_49c3f42e___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../../../../../node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!../../../../../../../../node_modules/vue-loader/lib??vue-loader-options!./Owner.vue?vue&type=template&id=49c3f42e& */ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/src/views/Pages/web/Auth/Register/Owner.vue?vue&type=template&id=49c3f42e&");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "render", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_Owner_vue_vue_type_template_id_49c3f42e___WEBPACK_IMPORTED_MODULE_0__["render"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_Owner_vue_vue_type_template_id_49c3f42e___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"]; });



/***/ })

}]);