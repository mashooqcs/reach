(window["webpackJsonp"] = window["webpackJsonp"] || []).push([[42],{

/***/ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/src/views/Pages/admin/Agent/components/OwnerTable.vue?vue&type=script&lang=js&":
/*!*************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/babel-loader/lib??ref--4-0!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/src/views/Pages/admin/Agent/components/OwnerTable.vue?vue&type=script&lang=js& ***!
  \*************************************************************************************************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @babel/runtime/regenerator */ "./node_modules/@babel/runtime/regenerator/index.js");
/* harmony import */ var _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var vuex__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! vuex */ "./node_modules/vuex/dist/vuex.esm.js");


function asyncGeneratorStep(gen, resolve, reject, _next, _throw, key, arg) { try { var info = gen[key](arg); var value = info.value; } catch (error) { reject(error); return; } if (info.done) { resolve(value); } else { Promise.resolve(value).then(_next, _throw); } }

function _asyncToGenerator(fn) { return function () { var self = this, args = arguments; return new Promise(function (resolve, reject) { var gen = fn.apply(self, args); function _next(value) { asyncGeneratorStep(gen, resolve, reject, _next, _throw, "next", value); } function _throw(err) { asyncGeneratorStep(gen, resolve, reject, _next, _throw, "throw", err); } _next(undefined); }); }; }

function ownKeys(object, enumerableOnly) { var keys = Object.keys(object); if (Object.getOwnPropertySymbols) { var symbols = Object.getOwnPropertySymbols(object); if (enumerableOnly) symbols = symbols.filter(function (sym) { return Object.getOwnPropertyDescriptor(object, sym).enumerable; }); keys.push.apply(keys, symbols); } return keys; }

function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; if (i % 2) { ownKeys(Object(source), true).forEach(function (key) { _defineProperty(target, key, source[key]); }); } else if (Object.getOwnPropertyDescriptors) { Object.defineProperties(target, Object.getOwnPropertyDescriptors(source)); } else { ownKeys(Object(source)).forEach(function (key) { Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key)); }); } } return target; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//

/* harmony default export */ __webpack_exports__["default"] = ({
  data: function data() {
    return {
      userId: 0
    };
  },
  computed: _objectSpread(_objectSpread({}, Object(vuex__WEBPACK_IMPORTED_MODULE_1__["mapState"])('admin', ['users'])), {}, {
    status: function status() {
      return this.$route.params.status;
    }
  }),
  methods: _objectSpread(_objectSpread({}, Object(vuex__WEBPACK_IMPORTED_MODULE_1__["mapActions"])('admin', ['store'])), {}, {
    blockEntity: function blockEntity() {
      var _this = this;

      return _asyncToGenerator( /*#__PURE__*/_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.mark(function _callee() {
        var type, params, _yield$_this$store, data;

        return _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.wrap(function _callee$(_context) {
          while (1) {
            switch (_context.prev = _context.next) {
              case 0:
                type = _this.$route.params.type;
                params = {
                  route: route('admin.user.block', {
                    user: _this.userId
                  }),
                  method: 'POST',
                  mutation: 'CHANGE_USER_STATUS',
                  variable: 'data',
                  data: {
                    type: type
                  }
                };
                _context.next = 4;
                return _this.store(params);

              case 4:
                _yield$_this$store = _context.sent;
                data = _yield$_this$store.data;

                if (data.status) {
                  _this.$snotify.success(data.msg);
                } else {
                  _this.$snotify.error(data.msg);
                }

              case 7:
              case "end":
                return _context.stop();
            }
          }
        }, _callee);
      }))();
    }
  })
});

/***/ }),

/***/ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/src/views/Pages/admin/Agent/components/OwnerTable.vue?vue&type=template&id=710b27e5&":
/*!*****************************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/src/views/Pages/admin/Agent/components/OwnerTable.vue?vue&type=template&id=710b27e5& ***!
  \*****************************************************************************************************************************************************************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "render", function() { return render; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return staticRenderFns; });
var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c(
    "table",
    { staticClass: "table table-striped table-bordered zero-configuration" },
    [
      _vm._m(0),
      _vm._v(" "),
      _c(
        "tbody",
        [
          _vm._l(_vm.users.data, function(user, index) {
            return _c("tr", { key: index }, [
              _c("td", [_vm._v(_vm._s(_vm.serialNumber("users", index)))]),
              _vm._v(" "),
              _c("td", [_vm._v(_vm._s(user.id))]),
              _vm._v(" "),
              _c("td", [_vm._v(_vm._s(user.name))]),
              _vm._v(" "),
              _c("td", [_vm._v(_vm._s(user.email))]),
              _vm._v(" "),
              _c("td", [_vm._v(_vm._s(_vm.formatDate(user.created_at)))]),
              _vm._v(" "),
              _c("td", [
                _c("div", { staticClass: "btn-group mr-1 mb-1" }, [
                  _vm._m(1, true),
                  _vm._v(" "),
                  _c(
                    "div",
                    {
                      staticClass: "dropdown-menu dropdown-menu-right",
                      staticStyle: {
                        position: "absolute",
                        transform: "translate3d(4px, 23px, 0px)",
                        top: "0px",
                        left: "0px",
                        "will-change": "transform"
                      },
                      attrs: { "x-placement": "bottom-start" }
                    },
                    [
                      _c(
                        "router-link",
                        {
                          staticClass: "dropdown-item",
                          attrs: {
                            to: {
                              name: "admin.users.show",
                              params: { type: "property-owner", user: user.id }
                            }
                          }
                        },
                        [
                          _c("i", { staticClass: "fa fa-eye" }),
                          _vm._v("VIEW PROFILE")
                        ]
                      ),
                      _vm._v(" "),
                      _c(
                        "router-link",
                        {
                          staticClass: "dropdown-item",
                          attrs: {
                            to: {
                              name: "admin.properties.users",
                              params: { user: user.id }
                            }
                          }
                        },
                        [
                          _c("i", {
                            staticClass: "fas fa-building",
                            attrs: { "aria-hidden": "true" }
                          }),
                          _vm._v("VIEW PROPERTIES")
                        ]
                      ),
                      _vm._v(" "),
                      _c(
                        "a",
                        {
                          staticClass: "dropdown-item",
                          attrs: {
                            href: "#",
                            "data-toggle": "modal",
                            "data-target": "#confirm-block"
                          },
                          on: {
                            click: function($event) {
                              _vm.userId = user.id
                            }
                          }
                        },
                        [
                          _c("i", {
                            staticClass: "fa",
                            class:
                              _vm.routeParams.status == "blocked"
                                ? "fa-restore"
                                : "fa-ban",
                            attrs: { "aria-hidden": "true" }
                          }),
                          _vm._v(
                            _vm._s(
                              _vm.routeParams.status == "blocked"
                                ? "UN BLOCK"
                                : "BLOCK"
                            ) + " PROPERTY OWNER"
                          )
                        ]
                      )
                    ],
                    1
                  )
                ])
              ])
            ])
          }),
          _vm._v(" "),
          _c("no-record", {
            staticClass: "col-md-3",
            attrs: { tag: "tr", colspan: 6, data: _vm.users.data }
          })
        ],
        2
      ),
      _vm._v(" "),
      _c("confirm", {
        attrs: {
          el: "confirm-block",
          title: "System Message",
          subtitle:
            "Are you sure you want to " +
            (_vm.routeParams.status == "blocked" ? "un block" : "block") +
            " this property owner? "
        },
        on: { confirmed: _vm.blockEntity }
      })
    ],
    1
  )
}
var staticRenderFns = [
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("thead", [
      _c("tr", [
        _c("th", [_vm._v("S.No")]),
        _vm._v(" "),
        _c("th", [_vm._v("PROPERTY OWNER_ID")]),
        _vm._v(" "),
        _c("th", [_vm._v("FULL NAME")]),
        _vm._v(" "),
        _c("th", [_vm._v("EMAIL")]),
        _vm._v(" "),
        _c("th", [_vm._v("REGISTERATION DATE")]),
        _vm._v(" "),
        _c("th", [_vm._v("ACTIONS")])
      ])
    ])
  },
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c(
      "button",
      {
        staticClass: "btn  btn-drop-table btn-sm",
        attrs: {
          type: "button",
          "data-toggle": "dropdown",
          "aria-haspopup": "true",
          "aria-expanded": "false"
        }
      },
      [_c("i", { staticClass: "fa fa-ellipsis-v" })]
    )
  }
]
render._withStripped = true



/***/ }),

/***/ "./resources/js/src/views/Pages/admin/Agent/components/OwnerTable.vue":
/*!****************************************************************************!*\
  !*** ./resources/js/src/views/Pages/admin/Agent/components/OwnerTable.vue ***!
  \****************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _OwnerTable_vue_vue_type_template_id_710b27e5___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./OwnerTable.vue?vue&type=template&id=710b27e5& */ "./resources/js/src/views/Pages/admin/Agent/components/OwnerTable.vue?vue&type=template&id=710b27e5&");
/* harmony import */ var _OwnerTable_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./OwnerTable.vue?vue&type=script&lang=js& */ "./resources/js/src/views/Pages/admin/Agent/components/OwnerTable.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport *//* harmony import */ var _node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../../../../../../../node_modules/vue-loader/lib/runtime/componentNormalizer.js */ "./node_modules/vue-loader/lib/runtime/componentNormalizer.js");





/* normalize component */

var component = Object(_node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__["default"])(
  _OwnerTable_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__["default"],
  _OwnerTable_vue_vue_type_template_id_710b27e5___WEBPACK_IMPORTED_MODULE_0__["render"],
  _OwnerTable_vue_vue_type_template_id_710b27e5___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"],
  false,
  null,
  null,
  null
  
)

/* hot reload */
if (false) { var api; }
component.options.__file = "resources/js/src/views/Pages/admin/Agent/components/OwnerTable.vue"
/* harmony default export */ __webpack_exports__["default"] = (component.exports);

/***/ }),

/***/ "./resources/js/src/views/Pages/admin/Agent/components/OwnerTable.vue?vue&type=script&lang=js&":
/*!*****************************************************************************************************!*\
  !*** ./resources/js/src/views/Pages/admin/Agent/components/OwnerTable.vue?vue&type=script&lang=js& ***!
  \*****************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_OwnerTable_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../../../../../node_modules/babel-loader/lib??ref--4-0!../../../../../../../../node_modules/vue-loader/lib??vue-loader-options!./OwnerTable.vue?vue&type=script&lang=js& */ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/src/views/Pages/admin/Agent/components/OwnerTable.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport */ /* harmony default export */ __webpack_exports__["default"] = (_node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_OwnerTable_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__["default"]); 

/***/ }),

/***/ "./resources/js/src/views/Pages/admin/Agent/components/OwnerTable.vue?vue&type=template&id=710b27e5&":
/*!***********************************************************************************************************!*\
  !*** ./resources/js/src/views/Pages/admin/Agent/components/OwnerTable.vue?vue&type=template&id=710b27e5& ***!
  \***********************************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_OwnerTable_vue_vue_type_template_id_710b27e5___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../../../../../node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!../../../../../../../../node_modules/vue-loader/lib??vue-loader-options!./OwnerTable.vue?vue&type=template&id=710b27e5& */ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/src/views/Pages/admin/Agent/components/OwnerTable.vue?vue&type=template&id=710b27e5&");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "render", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_OwnerTable_vue_vue_type_template_id_710b27e5___WEBPACK_IMPORTED_MODULE_0__["render"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_OwnerTable_vue_vue_type_template_id_710b27e5___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"]; });



/***/ })

}]);