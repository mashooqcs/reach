(window["webpackJsonp"] = window["webpackJsonp"] || []).push([[80],{

/***/ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/src/views/Pages/web/Employee/Show.vue?vue&type=script&lang=js&":
/*!*********************************************************************************************************************************************************************************!*\
  !*** ./node_modules/babel-loader/lib??ref--4-0!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/src/views/Pages/web/Employee/Show.vue?vue&type=script&lang=js& ***!
  \*********************************************************************************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @babel/runtime/regenerator */ "./node_modules/@babel/runtime/regenerator/index.js");
/* harmony import */ var _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var vuex__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! vuex */ "./node_modules/vuex/dist/vuex.esm.js");


function ownKeys(object, enumerableOnly) { var keys = Object.keys(object); if (Object.getOwnPropertySymbols) { var symbols = Object.getOwnPropertySymbols(object); if (enumerableOnly) symbols = symbols.filter(function (sym) { return Object.getOwnPropertyDescriptor(object, sym).enumerable; }); keys.push.apply(keys, symbols); } return keys; }

function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; if (i % 2) { ownKeys(Object(source), true).forEach(function (key) { _defineProperty(target, key, source[key]); }); } else if (Object.getOwnPropertyDescriptors) { Object.defineProperties(target, Object.getOwnPropertyDescriptors(source)); } else { ownKeys(Object(source)).forEach(function (key) { Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key)); }); } } return target; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

function asyncGeneratorStep(gen, resolve, reject, _next, _throw, key, arg) { try { var info = gen[key](arg); var value = info.value; } catch (error) { reject(error); return; } if (info.done) { resolve(value); } else { Promise.resolve(value).then(_next, _throw); } }

function _asyncToGenerator(fn) { return function () { var self = this, args = arguments; return new Promise(function (resolve, reject) { var gen = fn.apply(self, args); function _next(value) { asyncGeneratorStep(gen, resolve, reject, _next, _throw, "next", value); } function _throw(err) { asyncGeneratorStep(gen, resolve, reject, _next, _throw, "throw", err); } _next(undefined); }); }; }

//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//


var AssignProperty = function AssignProperty() {
  return __webpack_require__.e(/*! import() */ 82).then(__webpack_require__.bind(null, /*! ./components/AssignProperty.vue */ "./resources/js/src/views/Pages/web/Employee/components/AssignProperty.vue"));
};

/* harmony default export */ __webpack_exports__["default"] = ({
  created: function created() {
    var _this = this;

    return _asyncToGenerator( /*#__PURE__*/_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.mark(function _callee() {
      return _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.wrap(function _callee$(_context) {
        while (1) {
          switch (_context.prev = _context.next) {
            case 0:
              _context.next = 2;
              return _this.fetch();

            case 2:
            case "end":
              return _context.stop();
          }
        }
      }, _callee);
    }))();
  },
  data: function data() {
    return {
      assignPoperty: 0
    };
  },
  components: {
    AssignProperty: AssignProperty
  },
  computed: _objectSpread(_objectSpread({}, Object(vuex__WEBPACK_IMPORTED_MODULE_1__["mapState"])('employee', ['employee'])), Object(vuex__WEBPACK_IMPORTED_MODULE_1__["mapState"])('property', ['properties'])),
  methods: _objectSpread(_objectSpread(_objectSpread(_objectSpread({}, Object(vuex__WEBPACK_IMPORTED_MODULE_1__["mapActions"])('property', ['getAll'])), Object(vuex__WEBPACK_IMPORTED_MODULE_1__["mapActions"])('employee', ['get', 'store'])), Object(vuex__WEBPACK_IMPORTED_MODULE_1__["mapMutations"])('employee', ['set_properties'])), {}, {
    remove: function remove(property) {
      var _this2 = this;

      return _asyncToGenerator( /*#__PURE__*/_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.mark(function _callee2() {
        var _total_tasks_pending, id, employee, params, _yield$_this2$store, data;

        return _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.wrap(function _callee2$(_context2) {
          while (1) {
            switch (_context2.prev = _context2.next) {
              case 0:
                _total_tasks_pending = property._total_tasks_pending, id = property.id;
                employee = _this2.$route.params.employee; // alert(_total_tasks_pending);

                if (!(_total_tasks_pending == 0)) {
                  _context2.next = 9;
                  break;
                }

                params = {
                  route: route('employee.remove-property', {
                    employee: employee
                  }),
                  method: 'POST',
                  mutation: 'remove_property',
                  data: {
                    property: id
                  }
                };
                _context2.next = 6;
                return _this2.store(params);

              case 6:
                _yield$_this2$store = _context2.sent;
                data = _yield$_this2$store.data;

                if (data.status) {}

              case 9:
              case "end":
                return _context2.stop();
            }
          }
        }, _callee2);
      }))();
    },
    fetch: function fetch() {
      var _this3 = this;

      return _asyncToGenerator( /*#__PURE__*/_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.mark(function _callee3() {
        var employee, params;
        return _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.wrap(function _callee3$(_context3) {
          while (1) {
            switch (_context3.prev = _context3.next) {
              case 0:
                employee = _this3.$route.params.employee;
                params = {
                  route: route('employee.show', {
                    employee: employee
                  }),
                  data: {}
                };
                _context3.next = 4;
                return _this3.get(params);

              case 4:
              case "end":
                return _context3.stop();
            }
          }
        }, _callee3);
      }))();
    },
    popupClosed: function popupClosed() {
      this.assignPoperty++;
    },
    updateProperties: function updateProperties(property) {
      // console.log(property);
      this.set_properties(property);
    }
  })
});

/***/ }),

/***/ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/src/views/Pages/web/Employee/Show.vue?vue&type=template&id=9fe66704&":
/*!*************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/src/views/Pages/web/Employee/Show.vue?vue&type=template&id=9fe66704& ***!
  \*************************************************************************************************************************************************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "render", function() { return render; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return staticRenderFns; });
var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c(
    "section",
    { staticClass: "new-employee-formbox" },
    [
      _c("div", { staticClass: "container" }, [
        _c("span", { staticClass: "emp-green" }, [
          _vm._v("Emp_Id: E-" + _vm._s(_vm.employee.id))
        ]),
        _vm._v(" "),
        _c("h1", [_vm._v("View Employee")]),
        _vm._v(" "),
        _c("hr"),
        _vm._v(" "),
        _c("div", { staticClass: "row" }, [
          _vm._m(0),
          _vm._v(" "),
          _vm._m(1),
          _vm._v(" "),
          _c("div", { staticClass: "col-md-12" }, [
            _c("p", [_vm._v("Employee_Name:")]),
            _vm._v(" "),
            _c("p", { staticClass: "text-muted" }, [
              _vm._v(_vm._s(_vm.employee.name))
            ]),
            _vm._v(" "),
            _c("p", [_vm._v("Employee Email Address:")]),
            _vm._v(" "),
            _c("p", { staticClass: "text-muted" }, [
              _vm._v(_vm._s(_vm.employee.email))
            ]),
            _vm._v(" "),
            _c("p", [_vm._v("Employee Gender:")]),
            _vm._v(" "),
            _c("p", { staticClass: "text-muted" }, [
              _vm._v(_vm._s(_vm.employee.gender))
            ]),
            _vm._v(" "),
            _c("p", [_vm._v("Assigned Property:")]),
            _vm._v(" "),
            _c("p", { staticClass: "text-muted" }, [
              _vm._v(
                "Total Properties: " +
                  _vm._s(_vm.employee.properties_count || 0) +
                  ", Total Tasks :" +
                  _vm._s(_vm.employee.tasks_count || 0)
              )
            ])
          ])
        ]),
        _vm._v(" "),
        _c(
          "a",
          {
            staticClass: "assign-property-btn",
            attrs: {
              href: "javascript:void(0)",
              "data-toggle": "modal",
              "data-target": "#assign-property"
            },
            on: {
              click: function($event) {
                _vm.assignPoperty++
              }
            }
          },
          [_vm._v("Assign\n            Property")]
        ),
        _c("br"),
        _vm._v(" "),
        _c(
          "div",
          { staticClass: "card" },
          [
            _vm._m(2),
            _vm._v(" "),
            _vm._l(_vm.employee.properties, function(property) {
              return _c("div", { key: property.id, staticClass: "media" }, [
                _c("img", {
                  staticClass: "mr-3",
                  attrs: { src: "assets/images/view-emp-1.png", alt: "..." }
                }),
                _vm._v(" "),
                _c("div", { staticClass: "media-body" }, [
                  _c("span", [
                    _vm._v(_vm._s(property.name) + " "),
                    _c("br"),
                    _vm._v(" Property_ID: P-" + _vm._s(property.id))
                  ]),
                  _vm._v(" "),
                  _c(
                    "a",
                    {
                      staticClass: "remove-property-btn",
                      attrs: { href: "javascript:void(0)" },
                      on: {
                        click: function($event) {
                          return _vm.remove(property)
                        }
                      }
                    },
                    [_vm._v("Remove Property")]
                  )
                ])
              ])
            }),
            _vm._v(" "),
            typeof _vm.employee.properties != "undefined" &&
            _vm.employee.properties.length == 0
              ? _c("div", { staticClass: "media" }, [
                  _c("h4", { staticClass: "text-center" }, [
                    _vm._v("No Any Property Assigned")
                  ])
                ])
              : _vm._e()
          ],
          2
        ),
        _vm._v(" "),
        _vm._m(3),
        _vm._v(" "),
        _c("div", { staticClass: "col-md-12" }, [
          _c("p", [_vm._v("Email Address:")]),
          _vm._v(" "),
          _c("p", { staticClass: "text-muted" }, [
            _vm._v(_vm._s(_vm.employee.email) + " copy")
          ]),
          _vm._v(" "),
          _c("p", [_vm._v("Auto-generated Password:")]),
          _vm._v(" "),
          _c("p", { staticClass: "text-muted" }, [
            _vm._v(_vm._s(_vm.employee.generated_password || "NA"))
          ]),
          _vm._v(" "),
          _c("p", [_vm._v("Unique Code:")]),
          _vm._v(" "),
          _c("p", { staticClass: "text-muted" }, [
            _vm._v(_vm._s(_vm.employee.code || "NA"))
          ])
        ]),
        _vm._v(" "),
        _c(
          "a",
          {
            staticClass: "assign-property-btn",
            attrs: {
              href: "#",
              "data-toggle": "modal",
              "data-target": "#exampleModal-remove"
            }
          },
          [_vm._v("Send Login\n                Details To Employee")]
        ),
        _c("br"),
        _vm._v(" "),
        _c(
          "a",
          { staticClass: "assign-property-close-btn", attrs: { href: "#" } },
          [_vm._v("Close")]
        )
      ]),
      _vm._v(" "),
      _c("assign-property", {
        key: _vm.assignPoperty,
        on: {
          closed: _vm.popupClosed,
          "assigned-property": _vm.updateProperties
        }
      })
    ],
    1
  )
}
var staticRenderFns = [
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("div", { staticClass: "col-md-6" }, [
      _c("h6", [_vm._v(" Employee Personal Details:")])
    ])
  },
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("div", { staticClass: "col-md-6" }, [
      _c(
        "a",
        {
          staticClass: "assign-property-btn",
          attrs: { href: "view-employee-detail-page.php" }
        },
        [_vm._v("View Profile")]
      )
    ])
  },
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c(
      "div",
      { staticClass: "input-group md-form form-sm form-2 pl-0 my-search" },
      [
        _c("input", {
          staticClass: "form-control my-0 py-1 lime-border",
          attrs: { type: "text", placeholder: "Search", "aria-label": "Search" }
        }),
        _vm._v(" "),
        _c("div", { staticClass: "input-group-append" }, [
          _c(
            "span",
            {
              staticClass: "input-group-text lime lighten-2",
              attrs: { id: "basic-text1" }
            },
            [
              _c("i", {
                staticClass: "fas fa-search text-grey",
                attrs: { "aria-hidden": "true" }
              })
            ]
          )
        ])
      ]
    )
  },
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("div", { staticClass: "col-md-12" }, [
      _c("h6", [_vm._v("Login Details:")])
    ])
  }
]
render._withStripped = true



/***/ }),

/***/ "./resources/js/src/views/Pages/web/Employee/Show.vue":
/*!************************************************************!*\
  !*** ./resources/js/src/views/Pages/web/Employee/Show.vue ***!
  \************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _Show_vue_vue_type_template_id_9fe66704___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./Show.vue?vue&type=template&id=9fe66704& */ "./resources/js/src/views/Pages/web/Employee/Show.vue?vue&type=template&id=9fe66704&");
/* harmony import */ var _Show_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./Show.vue?vue&type=script&lang=js& */ "./resources/js/src/views/Pages/web/Employee/Show.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport *//* harmony import */ var _node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../../../../../../node_modules/vue-loader/lib/runtime/componentNormalizer.js */ "./node_modules/vue-loader/lib/runtime/componentNormalizer.js");





/* normalize component */

var component = Object(_node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__["default"])(
  _Show_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__["default"],
  _Show_vue_vue_type_template_id_9fe66704___WEBPACK_IMPORTED_MODULE_0__["render"],
  _Show_vue_vue_type_template_id_9fe66704___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"],
  false,
  null,
  null,
  null
  
)

/* hot reload */
if (false) { var api; }
component.options.__file = "resources/js/src/views/Pages/web/Employee/Show.vue"
/* harmony default export */ __webpack_exports__["default"] = (component.exports);

/***/ }),

/***/ "./resources/js/src/views/Pages/web/Employee/Show.vue?vue&type=script&lang=js&":
/*!*************************************************************************************!*\
  !*** ./resources/js/src/views/Pages/web/Employee/Show.vue?vue&type=script&lang=js& ***!
  \*************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_Show_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../../../../node_modules/babel-loader/lib??ref--4-0!../../../../../../../node_modules/vue-loader/lib??vue-loader-options!./Show.vue?vue&type=script&lang=js& */ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/src/views/Pages/web/Employee/Show.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport */ /* harmony default export */ __webpack_exports__["default"] = (_node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_Show_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__["default"]); 

/***/ }),

/***/ "./resources/js/src/views/Pages/web/Employee/Show.vue?vue&type=template&id=9fe66704&":
/*!*******************************************************************************************!*\
  !*** ./resources/js/src/views/Pages/web/Employee/Show.vue?vue&type=template&id=9fe66704& ***!
  \*******************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_Show_vue_vue_type_template_id_9fe66704___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../../../../node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!../../../../../../../node_modules/vue-loader/lib??vue-loader-options!./Show.vue?vue&type=template&id=9fe66704& */ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/src/views/Pages/web/Employee/Show.vue?vue&type=template&id=9fe66704&");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "render", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_Show_vue_vue_type_template_id_9fe66704___WEBPACK_IMPORTED_MODULE_0__["render"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_Show_vue_vue_type_template_id_9fe66704___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"]; });



/***/ })

}]);