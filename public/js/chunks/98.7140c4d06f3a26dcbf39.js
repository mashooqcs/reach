(window["webpackJsonp"] = window["webpackJsonp"] || []).push([[98],{

/***/ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/src/views/Pages/web/Task/components/Create.vue?vue&type=script&lang=js&":
/*!******************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/babel-loader/lib??ref--4-0!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/src/views/Pages/web/Task/components/Create.vue?vue&type=script&lang=js& ***!
  \******************************************************************************************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @babel/runtime/regenerator */ "./node_modules/@babel/runtime/regenerator/index.js");
/* harmony import */ var _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var vuex__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! vuex */ "./node_modules/vuex/dist/vuex.esm.js");
/* harmony import */ var vue_flatpickr_component__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! vue-flatpickr-component */ "./node_modules/vue-flatpickr-component/dist/vue-flatpickr.min.js");
/* harmony import */ var vue_flatpickr_component__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(vue_flatpickr_component__WEBPACK_IMPORTED_MODULE_2__);


function asyncGeneratorStep(gen, resolve, reject, _next, _throw, key, arg) { try { var info = gen[key](arg); var value = info.value; } catch (error) { reject(error); return; } if (info.done) { resolve(value); } else { Promise.resolve(value).then(_next, _throw); } }

function _asyncToGenerator(fn) { return function () { var self = this, args = arguments; return new Promise(function (resolve, reject) { var gen = fn.apply(self, args); function _next(value) { asyncGeneratorStep(gen, resolve, reject, _next, _throw, "next", value); } function _throw(err) { asyncGeneratorStep(gen, resolve, reject, _next, _throw, "throw", err); } _next(undefined); }); }; }

function ownKeys(object, enumerableOnly) { var keys = Object.keys(object); if (Object.getOwnPropertySymbols) { var symbols = Object.getOwnPropertySymbols(object); if (enumerableOnly) symbols = symbols.filter(function (sym) { return Object.getOwnPropertyDescriptor(object, sym).enumerable; }); keys.push.apply(keys, symbols); } return keys; }

function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; if (i % 2) { ownKeys(Object(source), true).forEach(function (key) { _defineProperty(target, key, source[key]); }); } else if (Object.getOwnPropertyDescriptors) { Object.defineProperties(target, Object.getOwnPropertyDescriptors(source)); } else { ownKeys(Object(source)).forEach(function (key) { Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key)); }); } } return target; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//


/* harmony default export */ __webpack_exports__["default"] = ({
  components: {
    flatPickr: vue_flatpickr_component__WEBPACK_IMPORTED_MODULE_2___default.a
  },
  props: {
    taskDate: {
      type: String,
      "default": null
    }
  },
  data: function data() {
    return {
      el: '#create-task',
      employees: [],
      employee: null,
      property: null,
      form: {
        property_id: '',
        employeeIds: '',
        files: []
      },
      reader: undefined,
      timeConfig: {
        enableTime: true,
        noCalendar: true,
        time_24hr: true,
        "static": true
      }
    };
  },
  watch: {
    taskDate: function taskDate(val, oldVal) {
      // console.log(val)
      this.form.due_date = val;
      this.$refs.due_date.value = val;
    }
  },
  created: function created() {
    this.fetch();
    this.fetchProperties(); // console.log(this.taskDate);
  },
  computed: _objectSpread(_objectSpread({}, Object(vuex__WEBPACK_IMPORTED_MODULE_1__["mapState"])('property', ['properties'])), Object(vuex__WEBPACK_IMPORTED_MODULE_1__["mapState"])('task', ['task_status'])),
  methods: _objectSpread(_objectSpread(_objectSpread({}, Object(vuex__WEBPACK_IMPORTED_MODULE_1__["mapActions"])({
    get: 'property/getAll',
    storeTask: 'task/store'
  })), Object(vuex__WEBPACK_IMPORTED_MODULE_1__["mapActions"])('employee', ['getAll'])), {}, {
    fetchProperties: function fetchProperties() {
      var _arguments = arguments,
          _this = this;

      return _asyncToGenerator( /*#__PURE__*/_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.mark(function _callee() {
        var page, params;
        return _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.wrap(function _callee$(_context) {
          while (1) {
            switch (_context.prev = _context.next) {
              case 0:
                page = _arguments.length > 0 && _arguments[0] !== undefined ? _arguments[0] : 1;
                params = {
                  route: route('property.index'),
                  mutation: 'index',
                  data: {
                    page: page
                  }
                };
                _context.next = 4;
                return _this.get(params);

              case 4:
              case "end":
                return _context.stop();
            }
          }
        }, _callee);
      }))();
    },
    fetch: function fetch() {
      var _arguments2 = arguments,
          _this2 = this;

      return _asyncToGenerator( /*#__PURE__*/_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.mark(function _callee2() {
        var page, params;
        return _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.wrap(function _callee2$(_context2) {
          while (1) {
            switch (_context2.prev = _context2.next) {
              case 0:
                page = _arguments2.length > 0 && _arguments2[0] !== undefined ? _arguments2[0] : 1;
                params = {
                  route: route('employee.index'),
                  data: {
                    page: page
                  }
                };
                _context2.next = 4;
                return _this2.getAll(params);

              case 4:
              case "end":
                return _context2.stop();
            }
          }
        }, _callee2);
      }))();
    },
    setBase64: function setBase64(e) {
      // console.log(e.target);
      return e.target.result;
    },
    getBase64: function getBase64(file) {
      return new Promise(function (resolve) {
        var reader = new FileReader();

        reader.onloadend = function () {
          resolve(reader.result);
        };

        reader.readAsDataURL(file);
      });
    },
    getFiles: function getFiles(e) {
      var _this3 = this;

      return _asyncToGenerator( /*#__PURE__*/_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.mark(function _callee4() {
        var _yield$_this3$$refs$u, valid, files, self;

        return _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.wrap(function _callee4$(_context4) {
          while (1) {
            switch (_context4.prev = _context4.next) {
              case 0:
                _context4.next = 2;
                return _this3.$refs.uploaderProvider.validate(e);

              case 2:
                _yield$_this3$$refs$u = _context4.sent;
                valid = _yield$_this3$$refs$u.valid;

                if (!valid) {
                  _context4.next = 11;
                  break;
                }

                files = _this3.$refs.files.files;
                self = _this3;
                _context4.next = 9;
                return _.each(files, /*#__PURE__*/function () {
                  var _ref = _asyncToGenerator( /*#__PURE__*/_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.mark(function _callee3(file) {
                    var base64;
                    return _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.wrap(function _callee3$(_context3) {
                      while (1) {
                        switch (_context3.prev = _context3.next) {
                          case 0:
                            _context3.next = 2;
                            return _this3.getBase64(file);

                          case 2:
                            base64 = _context3.sent;

                            if (self.form.files.length < 3) {
                              self.form.files.push({
                                file: file,
                                base64: base64
                              });
                            }

                          case 4:
                          case "end":
                            return _context3.stop();
                        }
                      }
                    }, _callee3);
                  }));

                  return function (_x) {
                    return _ref.apply(this, arguments);
                  };
                }());

              case 9:
                // console.log(this.form.files.length);
                if (_this3.form.files.length > 3) {
                  _this3.form.files.splice(2);

                  _this3.$snotify.error('you can not upload more than 3 files');
                }

                _this3.$refs.files.value = '';

              case 11:
              case "end":
                return _context4.stop();
            }
          }
        }, _callee4);
      }))();
    },
    setEmployee: function setEmployee(employee) {
      var _this4 = this;

      return _asyncToGenerator( /*#__PURE__*/_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.mark(function _callee5() {
        var ids;
        return _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.wrap(function _callee5$(_context5) {
          while (1) {
            switch (_context5.prev = _context5.next) {
              case 0:
                // console.log(employee)
                ids = _.map(employee, 'id');
                _this4.form.employeeIds = ids;

              case 2:
              case "end":
                return _context5.stop();
            }
          }
        }, _callee5);
      }))();
    },
    getPropertyEmployees: function getPropertyEmployees(property) {
      var _this5 = this;

      return _asyncToGenerator( /*#__PURE__*/_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.mark(function _callee6() {
        var id, params, _yield$_this5$get, data;

        return _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.wrap(function _callee6$(_context6) {
          while (1) {
            switch (_context6.prev = _context6.next) {
              case 0:
                if (!property) {
                  _context6.next = 11;
                  break;
                }

                id = property.id;
                _this5.form.property_id = id;
                _this5.form.employeeIds = [];
                _this5.employee = null;
                params = {
                  route: route('property.get-employees', {
                    property: id
                  })
                };
                _context6.next = 8;
                return _this5.get(params);

              case 8:
                _yield$_this5$get = _context6.sent;
                data = _yield$_this5$get.data;
                _this5.employees = data;

              case 11:
                ;

              case 12:
              case "end":
                return _context6.stop();
            }
          }
        }, _callee6);
      }))();
    },
    create: function create() {
      var _this6 = this;

      return _asyncToGenerator( /*#__PURE__*/_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.mark(function _callee7() {
        var fd, files, form, params, _yield$_this6$storeTa, data;

        return _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.wrap(function _callee7$(_context7) {
          while (1) {
            switch (_context7.prev = _context7.next) {
              case 0:
                fd = new FormData();
                files = _.map(_this6.form.files, 'file');
                _this6.form.file;
                form = _objectSpread(_objectSpread({}, _this6.form), {}, {
                  status: _this6.task_status,
                  files: files
                });

                _this6.buildFormData(fd, form);

                params = {
                  route: route('task.store'),
                  method: 'POST',
                  data: fd
                };
                _context7.next = 8;
                return _this6.storeTask(params);

              case 8:
                _yield$_this6$storeTa = _context7.sent;
                data = _yield$_this6$storeTa.data;

                if (data.status) {
                  _this6.$snotify.success(data.msg);

                  window.$(_this6.el).modal('hide');

                  _this6.$emit('created');
                }

              case 11:
              case "end":
                return _context7.stop();
            }
          }
        }, _callee7);
      }))();
    }
  })
});

/***/ }),

/***/ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/src/views/Pages/web/Task/components/Create.vue?vue&type=template&id=1960b33b&":
/*!**********************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/src/views/Pages/web/Task/components/Create.vue?vue&type=template&id=1960b33b& ***!
  \**********************************************************************************************************************************************************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "render", function() { return render; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return staticRenderFns; });
var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c("div", { staticClass: "create-task-modal" }, [
    _c(
      "div",
      {
        staticClass: "modal fade",
        attrs: {
          id: "create-task",
          tabindex: "-1",
          "aria-labelledby": "exampleModalLabel",
          "aria-hidden": "true"
        }
      },
      [
        _c("div", { staticClass: "modal-dialog modal-dialog-centered" }, [
          _c(
            "div",
            { staticClass: "modal-content" },
            [
              _vm._m(0),
              _vm._v(" "),
              _c("ValidationObserver", {
                ref: "createTaskObserver",
                attrs: { tag: "div" },
                scopedSlots: _vm._u([
                  {
                    key: "default",
                    fn: function(ref) {
                      var handleSubmit = ref.handleSubmit
                      return [
                        _c(
                          "form",
                          {
                            ref: "createTaskForm",
                            on: {
                              submit: function($event) {
                                $event.preventDefault()
                                return handleSubmit(_vm.create)
                              }
                            }
                          },
                          [
                            _c(
                              "div",
                              { staticClass: "modal-body" },
                              [
                                _c("h3", { staticClass: "text-center" }, [
                                  _vm._v("Creat Task")
                                ]),
                                _vm._v(" "),
                                _c("ValidationProvider", {
                                  staticClass: "form-group",
                                  attrs: {
                                    tag: "div",
                                    rules: "required",
                                    name: "Task Name"
                                  },
                                  scopedSlots: _vm._u(
                                    [
                                      {
                                        key: "default",
                                        fn: function(ref) {
                                          var errors = ref.errors
                                          return [
                                            _c(
                                              "label",
                                              {
                                                attrs: {
                                                  for: "exampleInputEmail1"
                                                }
                                              },
                                              [_vm._v("Task Name :")]
                                            ),
                                            _vm._v(" "),
                                            _c("input", {
                                              directives: [
                                                {
                                                  name: "model",
                                                  rawName: "v-model",
                                                  value: _vm.form.name,
                                                  expression: "form.name"
                                                }
                                              ],
                                              staticClass: "form-control",
                                              attrs: {
                                                type: "text",
                                                id: "exampleInputEmail1",
                                                "aria-describedby": "emailHelp",
                                                placeholder: "Enter Task Name"
                                              },
                                              domProps: {
                                                value: _vm.form.name
                                              },
                                              on: {
                                                input: function($event) {
                                                  if ($event.target.composing) {
                                                    return
                                                  }
                                                  _vm.$set(
                                                    _vm.form,
                                                    "name",
                                                    $event.target.value
                                                  )
                                                }
                                              }
                                            }),
                                            _vm._v(" "),
                                            _c(
                                              "div",
                                              { staticClass: "text-danger" },
                                              [_vm._v(_vm._s(errors[0]))]
                                            )
                                          ]
                                        }
                                      }
                                    ],
                                    null,
                                    true
                                  )
                                }),
                                _vm._v(" "),
                                _c("ValidationProvider", {
                                  staticClass: "form-group",
                                  attrs: {
                                    tag: "div",
                                    rules: "required",
                                    name: "Property Name"
                                  },
                                  scopedSlots: _vm._u(
                                    [
                                      {
                                        key: "default",
                                        fn: function(ref) {
                                          var errors = ref.errors
                                          return [
                                            _c(
                                              "label",
                                              {
                                                attrs: {
                                                  for: "exampleInputEmail1"
                                                }
                                              },
                                              [_vm._v("Property Name:")]
                                            ),
                                            _vm._v(" "),
                                            _c("v-select", {
                                              attrs: {
                                                options: _vm.properties.data,
                                                label: "name"
                                              },
                                              on: {
                                                input: _vm.getPropertyEmployees
                                              },
                                              model: {
                                                value: _vm.property,
                                                callback: function($$v) {
                                                  _vm.property = $$v
                                                },
                                                expression: "property"
                                              }
                                            }),
                                            _vm._v(" "),
                                            _c(
                                              "div",
                                              { staticClass: "text-danger" },
                                              [_vm._v(_vm._s(errors[0]))]
                                            )
                                          ]
                                        }
                                      }
                                    ],
                                    null,
                                    true
                                  )
                                }),
                                _vm._v(" "),
                                _c("div", { staticClass: "row" }, [
                                  _c(
                                    "div",
                                    { staticClass: "col-md-5" },
                                    [
                                      _c("ValidationProvider", {
                                        staticClass: "form-group",
                                        attrs: {
                                          tag: "div",
                                          rules: "required",
                                          name: "Time From"
                                        },
                                        scopedSlots: _vm._u(
                                          [
                                            {
                                              key: "default",
                                              fn: function(ref) {
                                                var errors = ref.errors
                                                return [
                                                  _c(
                                                    "label",
                                                    {
                                                      attrs: {
                                                        for:
                                                          "exampleInputEmail1"
                                                      }
                                                    },
                                                    [_vm._v("Time:")]
                                                  ),
                                                  _vm._v(" "),
                                                  _c("flat-pickr", {
                                                    staticClass: "form-control",
                                                    attrs: {
                                                      config: _vm.timeConfig
                                                    },
                                                    model: {
                                                      value: _vm.form.start,
                                                      callback: function($$v) {
                                                        _vm.$set(
                                                          _vm.form,
                                                          "start",
                                                          $$v
                                                        )
                                                      },
                                                      expression: "form.start"
                                                    }
                                                  }),
                                                  _vm._v(" "),
                                                  _c(
                                                    "div",
                                                    {
                                                      staticClass: "text-danger"
                                                    },
                                                    [_vm._v(_vm._s(errors[0]))]
                                                  )
                                                ]
                                              }
                                            }
                                          ],
                                          null,
                                          true
                                        )
                                      })
                                    ],
                                    1
                                  ),
                                  _vm._v(" "),
                                  _c("div", { staticClass: "col-md-2 pt-3" }, [
                                    _c("label", {
                                      attrs: { for: "exampleInputEmail1" }
                                    }),
                                    _vm._v(" "),
                                    _c("p", { staticClass: "text-center" }, [
                                      _vm._v("To")
                                    ])
                                  ]),
                                  _vm._v(" "),
                                  _c(
                                    "div",
                                    { staticClass: "col-md-5" },
                                    [
                                      _c("ValidationProvider", {
                                        staticClass: "form-group pt-2",
                                        attrs: {
                                          tag: "div",
                                          rules: "required",
                                          name: "Time To"
                                        },
                                        scopedSlots: _vm._u(
                                          [
                                            {
                                              key: "default",
                                              fn: function(ref) {
                                                var errors = ref.errors
                                                return [
                                                  _c("label", {
                                                    attrs: {
                                                      for: "exampleInputEmail1"
                                                    }
                                                  }),
                                                  _vm._v(" "),
                                                  _c("flat-pickr", {
                                                    staticClass: "form-control",
                                                    attrs: {
                                                      config: _vm.timeConfig
                                                    },
                                                    model: {
                                                      value: _vm.form.to,
                                                      callback: function($$v) {
                                                        _vm.$set(
                                                          _vm.form,
                                                          "to",
                                                          $$v
                                                        )
                                                      },
                                                      expression: "form.to"
                                                    }
                                                  }),
                                                  _vm._v(" "),
                                                  _c(
                                                    "div",
                                                    {
                                                      staticClass: "text-danger"
                                                    },
                                                    [_vm._v(_vm._s(errors[0]))]
                                                  )
                                                ]
                                              }
                                            }
                                          ],
                                          null,
                                          true
                                        )
                                      })
                                    ],
                                    1
                                  )
                                ]),
                                _vm._v(" "),
                                _c("ValidationProvider", {
                                  staticClass: "form-group",
                                  attrs: {
                                    tag: "div",
                                    rules: "required",
                                    name: "Due Date"
                                  },
                                  scopedSlots: _vm._u(
                                    [
                                      {
                                        key: "default",
                                        fn: function(ref) {
                                          var errors = ref.errors
                                          return [
                                            _c(
                                              "label",
                                              { attrs: { for: "inputEmail4" } },
                                              [_vm._v("Due Date:")]
                                            ),
                                            _vm._v(" "),
                                            _c("input", {
                                              directives: [
                                                {
                                                  name: "model",
                                                  rawName: "v-model",
                                                  value: _vm.form.due_date,
                                                  expression: "form.due_date"
                                                }
                                              ],
                                              ref: "due_date",
                                              staticClass: "form-control",
                                              attrs: {
                                                type: "date",
                                                id: "inputEmail4",
                                                placeholder: "May 2, 2020"
                                              },
                                              domProps: {
                                                value: _vm.form.due_date
                                              },
                                              on: {
                                                input: function($event) {
                                                  if ($event.target.composing) {
                                                    return
                                                  }
                                                  _vm.$set(
                                                    _vm.form,
                                                    "due_date",
                                                    $event.target.value
                                                  )
                                                }
                                              }
                                            }),
                                            _vm._v(" "),
                                            _c(
                                              "div",
                                              { staticClass: "text-danger" },
                                              [_vm._v(_vm._s(errors[0]))]
                                            )
                                          ]
                                        }
                                      }
                                    ],
                                    null,
                                    true
                                  )
                                }),
                                _vm._v(" "),
                                _c(
                                  "div",
                                  [
                                    _c(
                                      "label",
                                      { staticStyle: { "font-size": "14px" } },
                                      [
                                        _c("span", [
                                          _vm._v("Add Property Images:*")
                                        ])
                                      ]
                                    ),
                                    _vm._v(" "),
                                    _c("ValidationProvider", {
                                      ref: "uploaderProvider",
                                      staticClass:
                                        "btn btn-success fileinput-button",
                                      attrs: {
                                        name: "Images",
                                        rules: "required|mimes:image/*"
                                      },
                                      scopedSlots: _vm._u(
                                        [
                                          {
                                            key: "default",
                                            fn: function(ref) {
                                              var validate = ref.validate
                                              var errors = ref.errors
                                              return [
                                                _c("span", [
                                                  _c("i", {
                                                    staticClass:
                                                      "fas fa-cloud-download-alt"
                                                  }),
                                                  _vm._v(" Upload Maximum 3")
                                                ]),
                                                _vm._v(" "),
                                                _c("input", {
                                                  ref: "files",
                                                  attrs: {
                                                    type: "file",
                                                    name: "files",
                                                    id: "files",
                                                    multiple: "",
                                                    accept:
                                                      "image/jpeg, image/png, image/gif,"
                                                  },
                                                  on: { change: _vm.getFiles }
                                                }),
                                                _c("br"),
                                                _vm._v(" "),
                                                _c("span", [
                                                  _vm._v(_vm._s(errors[0]))
                                                ])
                                              ]
                                            }
                                          }
                                        ],
                                        null,
                                        true
                                      )
                                    }),
                                    _vm._v(" "),
                                    _c(
                                      "div",
                                      { staticClass: "d-flex mt-3 mb-3" },
                                      _vm._l(_vm.form.files, function(file) {
                                        return _c(
                                          "div",
                                          {
                                            staticClass:
                                              "w-75 p-2 m-2 border border-dark"
                                          },
                                          [
                                            _c("img", {
                                              staticClass: "w-100 h-100",
                                              staticStyle: {
                                                "object-fit": "cover"
                                              },
                                              attrs: { src: file.base64 }
                                            })
                                          ]
                                        )
                                      }),
                                      0
                                    )
                                  ],
                                  1
                                ),
                                _vm._v(" "),
                                _vm.property
                                  ? _c("ValidationProvider", {
                                      staticClass: "form-group",
                                      attrs: {
                                        tag: "div",
                                        rules: "required",
                                        name: "Employee Name"
                                      },
                                      scopedSlots: _vm._u(
                                        [
                                          {
                                            key: "default",
                                            fn: function(ref) {
                                              var errors = ref.errors
                                              return [
                                                _c(
                                                  "label",
                                                  {
                                                    attrs: {
                                                      for: "exampleInputEmail1"
                                                    }
                                                  },
                                                  [_vm._v("Employee:")]
                                                ),
                                                _vm._v(" "),
                                                _c("v-select", {
                                                  attrs: {
                                                    options: _vm.employees,
                                                    label: "name",
                                                    multiple: ""
                                                  },
                                                  on: {
                                                    input: _vm.setEmployee
                                                  },
                                                  model: {
                                                    value: _vm.employee,
                                                    callback: function($$v) {
                                                      _vm.employee = $$v
                                                    },
                                                    expression: "employee"
                                                  }
                                                }),
                                                _vm._v(" "),
                                                _c(
                                                  "div",
                                                  {
                                                    staticClass: "text-danger"
                                                  },
                                                  [_vm._v(_vm._s(errors[0]))]
                                                )
                                              ]
                                            }
                                          }
                                        ],
                                        null,
                                        true
                                      )
                                    })
                                  : _vm._e()
                              ],
                              1
                            ),
                            _vm._v(" "),
                            _c("div", { staticClass: "modal-footer" }, [
                              _c(
                                "button",
                                {
                                  staticClass: "create-task-footer-btn",
                                  attrs: { type: "submit" }
                                },
                                [
                                  _vm._v(
                                    "Create\n                                Task"
                                  )
                                ]
                              )
                            ])
                          ]
                        )
                      ]
                    }
                  }
                ])
              })
            ],
            1
          )
        ])
      ]
    )
  ])
}
var staticRenderFns = [
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("div", { staticClass: "modal-header" }, [
      _c(
        "button",
        {
          staticClass: "close",
          attrs: {
            type: "button",
            "data-dismiss": "modal",
            "aria-label": "Close"
          }
        },
        [_c("span", { attrs: { "aria-hidden": "true" } }, [_vm._v("×")])]
      )
    ])
  }
]
render._withStripped = true



/***/ }),

/***/ "./resources/js/src/views/Pages/web/Task/components/Create.vue":
/*!*********************************************************************!*\
  !*** ./resources/js/src/views/Pages/web/Task/components/Create.vue ***!
  \*********************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _Create_vue_vue_type_template_id_1960b33b___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./Create.vue?vue&type=template&id=1960b33b& */ "./resources/js/src/views/Pages/web/Task/components/Create.vue?vue&type=template&id=1960b33b&");
/* harmony import */ var _Create_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./Create.vue?vue&type=script&lang=js& */ "./resources/js/src/views/Pages/web/Task/components/Create.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport *//* harmony import */ var _node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../../../../../../../node_modules/vue-loader/lib/runtime/componentNormalizer.js */ "./node_modules/vue-loader/lib/runtime/componentNormalizer.js");





/* normalize component */

var component = Object(_node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__["default"])(
  _Create_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__["default"],
  _Create_vue_vue_type_template_id_1960b33b___WEBPACK_IMPORTED_MODULE_0__["render"],
  _Create_vue_vue_type_template_id_1960b33b___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"],
  false,
  null,
  null,
  null
  
)

/* hot reload */
if (false) { var api; }
component.options.__file = "resources/js/src/views/Pages/web/Task/components/Create.vue"
/* harmony default export */ __webpack_exports__["default"] = (component.exports);

/***/ }),

/***/ "./resources/js/src/views/Pages/web/Task/components/Create.vue?vue&type=script&lang=js&":
/*!**********************************************************************************************!*\
  !*** ./resources/js/src/views/Pages/web/Task/components/Create.vue?vue&type=script&lang=js& ***!
  \**********************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_Create_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../../../../../node_modules/babel-loader/lib??ref--4-0!../../../../../../../../node_modules/vue-loader/lib??vue-loader-options!./Create.vue?vue&type=script&lang=js& */ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/src/views/Pages/web/Task/components/Create.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport */ /* harmony default export */ __webpack_exports__["default"] = (_node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_Create_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__["default"]); 

/***/ }),

/***/ "./resources/js/src/views/Pages/web/Task/components/Create.vue?vue&type=template&id=1960b33b&":
/*!****************************************************************************************************!*\
  !*** ./resources/js/src/views/Pages/web/Task/components/Create.vue?vue&type=template&id=1960b33b& ***!
  \****************************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_Create_vue_vue_type_template_id_1960b33b___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../../../../../node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!../../../../../../../../node_modules/vue-loader/lib??vue-loader-options!./Create.vue?vue&type=template&id=1960b33b& */ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/src/views/Pages/web/Task/components/Create.vue?vue&type=template&id=1960b33b&");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "render", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_Create_vue_vue_type_template_id_1960b33b___WEBPACK_IMPORTED_MODULE_0__["render"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_Create_vue_vue_type_template_id_1960b33b___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"]; });



/***/ })

}]);