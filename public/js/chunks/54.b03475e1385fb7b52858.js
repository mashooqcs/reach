(window["webpackJsonp"] = window["webpackJsonp"] || []).push([[54],{

/***/ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/src/views/Pages/admin/User/Show.vue?vue&type=script&lang=js&":
/*!*******************************************************************************************************************************************************************************!*\
  !*** ./node_modules/babel-loader/lib??ref--4-0!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/src/views/Pages/admin/User/Show.vue?vue&type=script&lang=js& ***!
  \*******************************************************************************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @babel/runtime/regenerator */ "./node_modules/@babel/runtime/regenerator/index.js");
/* harmony import */ var _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var vuex__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! vuex */ "./node_modules/vuex/dist/vuex.esm.js");


function asyncGeneratorStep(gen, resolve, reject, _next, _throw, key, arg) { try { var info = gen[key](arg); var value = info.value; } catch (error) { reject(error); return; } if (info.done) { resolve(value); } else { Promise.resolve(value).then(_next, _throw); } }

function _asyncToGenerator(fn) { return function () { var self = this, args = arguments; return new Promise(function (resolve, reject) { var gen = fn.apply(self, args); function _next(value) { asyncGeneratorStep(gen, resolve, reject, _next, _throw, "next", value); } function _throw(err) { asyncGeneratorStep(gen, resolve, reject, _next, _throw, "throw", err); } _next(undefined); }); }; }

function ownKeys(object, enumerableOnly) { var keys = Object.keys(object); if (Object.getOwnPropertySymbols) { var symbols = Object.getOwnPropertySymbols(object); if (enumerableOnly) symbols = symbols.filter(function (sym) { return Object.getOwnPropertyDescriptor(object, sym).enumerable; }); keys.push.apply(keys, symbols); } return keys; }

function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; if (i % 2) { ownKeys(Object(source), true).forEach(function (key) { _defineProperty(target, key, source[key]); }); } else if (Object.getOwnPropertyDescriptors) { Object.defineProperties(target, Object.getOwnPropertyDescriptors(source)); } else { ownKeys(Object(source)).forEach(function (key) { Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key)); }); } } return target; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

//
//
//
//
//
//
//
//
//
//
//
//
//


var EmployeeTable = function EmployeeTable() {
  return __webpack_require__.e(/*! import() */ 5).then(__webpack_require__.bind(null, /*! ./components/EmployeeTable.vue */ "./resources/js/src/views/Pages/admin/User/components/EmployeeTable.vue"));
};

var OwnerTable = function OwnerTable() {
  return __webpack_require__.e(/*! import() */ 6).then(__webpack_require__.bind(null, /*! ./components/OwnerTable.vue */ "./resources/js/src/views/Pages/admin/User/components/OwnerTable.vue"));
};

var OwnerDetail = function OwnerDetail() {
  return __webpack_require__.e(/*! import() */ 57).then(__webpack_require__.bind(null, /*! ./components/OwnerDetail.vue */ "./resources/js/src/views/Pages/admin/User/components/OwnerDetail.vue"));
};

var EmployeeDetail = function EmployeeDetail() {
  return __webpack_require__.e(/*! import() */ 56).then(__webpack_require__.bind(null, /*! ./components/EmployeeDetail.vue */ "./resources/js/src/views/Pages/admin/User/components/EmployeeDetail.vue"));
};

var Application = function Application() {
  return __webpack_require__.e(/*! import() */ 55).then(__webpack_require__.bind(null, /*! ./components/Application.vue */ "./resources/js/src/views/Pages/admin/User/components/Application.vue"));
};

/* harmony default export */ __webpack_exports__["default"] = ({
  computed: _objectSpread({}, Object(vuex__WEBPACK_IMPORTED_MODULE_1__["mapState"])('admin', ['user', 'search'])),
  components: {
    EmployeeTable: EmployeeTable,
    OwnerTable: OwnerTable,
    OwnerDetail: OwnerDetail,
    EmployeeDetail: EmployeeDetail,
    Application: Application
  },
  created: function created() {
    var _this = this;

    return _asyncToGenerator( /*#__PURE__*/_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.mark(function _callee() {
      var page;
      return _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.wrap(function _callee$(_context) {
        while (1) {
          switch (_context.prev = _context.next) {
            case 0:
              page = _this.$route.query.page;
              _context.next = 3;
              return _this.fetch(page);

            case 3:
            case "end":
              return _context.stop();
          }
        }
      }, _callee);
    }))();
  },
  watch: {
    search: function search(val, oldVal) {
      this.fetch();
    }
  },
  data: function data() {
    return {
      application: {}
    };
  },
  methods: _objectSpread(_objectSpread({}, Object(vuex__WEBPACK_IMPORTED_MODULE_1__["mapActions"])('admin', ['getAll'])), {}, {
    fetch: function fetch() {
      var _arguments = arguments,
          _this2 = this;

      return _asyncToGenerator( /*#__PURE__*/_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.mark(function _callee2() {
        var page, _this2$$route$params, type, user, params, _yield$_this2$getAll, data, _this2$$route$params2, _type, _user, _params, _yield$_this2$getAll2, _data;

        return _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.wrap(function _callee2$(_context2) {
          while (1) {
            switch (_context2.prev = _context2.next) {
              case 0:
                page = _arguments.length > 0 && _arguments[0] !== undefined ? _arguments[0] : 1;

                if (!(_this2.routeParams.status != 'application')) {
                  _context2.next = 11;
                  break;
                }

                _this2$$route$params = _this2.$route.params, type = _this2$$route$params.type, user = _this2$$route$params.user;
                params = {
                  route: route('admin.user.show', {
                    user: user
                  }),
                  mutation: 'SET_USER',
                  variable: 'user',
                  data: {
                    type: type,
                    search: _this2.search
                  }
                };
                _context2.next = 6;
                return _this2.getAll(params);

              case 6:
                _yield$_this2$getAll = _context2.sent;
                data = _yield$_this2$getAll.data;

                if (page != 1) {
                  _this2.addRouteQuery({
                    page: page
                  });
                } else {
                  _this2.addRouteQuery({});
                }

                _context2.next = 19;
                break;

              case 11:
                _this2$$route$params2 = _this2.$route.params, _type = _this2$$route$params2.type, _user = _this2$$route$params2.user;
                _params = {
                  route: route('admin.user.application', {
                    application: _user
                  }),
                  data: {
                    type: _type,
                    search: _this2.search
                  }
                };
                _context2.next = 15;
                return _this2.getAll(_params);

              case 15:
                _yield$_this2$getAll2 = _context2.sent;
                _data = _yield$_this2$getAll2.data;
                _this2.application = _data.application; // console.log(this.application);

                if (page != 1) {
                  _this2.addRouteQuery({
                    page: page
                  });
                } else {
                  _this2.addRouteQuery({});
                }

              case 19:
              case "end":
                return _context2.stop();
            }
          }
        }, _callee2);
      }))();
    }
  })
});

/***/ }),

/***/ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/src/views/Pages/admin/User/Show.vue?vue&type=template&id=e2f7f808&":
/*!***********************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/src/views/Pages/admin/User/Show.vue?vue&type=template&id=e2f7f808& ***!
  \***********************************************************************************************************************************************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "render", function() { return render; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return staticRenderFns; });
var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c("div", { staticClass: "content-wrapper" }, [
    _c(
      "div",
      { staticClass: "content-body" },
      [
        _vm.$route.params.type == "property-owner"
          ? _c("owner-detail")
          : _vm._e(),
        _vm._v(" "),
        [
          _vm.$route.params.type == "employee" &&
          _vm.routeParams.status != "application"
            ? _c("employee-detail", { on: { "on-update": _vm.fetch } })
            : _vm.$route.params.type == "employee" &&
              _vm.routeParams.status == "application"
            ? _c("application", {
                attrs: { application: _vm.application },
                on: { update: _vm.fetch }
              })
            : _vm._e()
        ]
      ],
      2
    )
  ])
}
var staticRenderFns = []
render._withStripped = true



/***/ }),

/***/ "./resources/js/src/views/Pages/admin/User/Show.vue":
/*!**********************************************************!*\
  !*** ./resources/js/src/views/Pages/admin/User/Show.vue ***!
  \**********************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _Show_vue_vue_type_template_id_e2f7f808___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./Show.vue?vue&type=template&id=e2f7f808& */ "./resources/js/src/views/Pages/admin/User/Show.vue?vue&type=template&id=e2f7f808&");
/* harmony import */ var _Show_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./Show.vue?vue&type=script&lang=js& */ "./resources/js/src/views/Pages/admin/User/Show.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport *//* harmony import */ var _node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../../../../../../node_modules/vue-loader/lib/runtime/componentNormalizer.js */ "./node_modules/vue-loader/lib/runtime/componentNormalizer.js");





/* normalize component */

var component = Object(_node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__["default"])(
  _Show_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__["default"],
  _Show_vue_vue_type_template_id_e2f7f808___WEBPACK_IMPORTED_MODULE_0__["render"],
  _Show_vue_vue_type_template_id_e2f7f808___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"],
  false,
  null,
  null,
  null
  
)

/* hot reload */
if (false) { var api; }
component.options.__file = "resources/js/src/views/Pages/admin/User/Show.vue"
/* harmony default export */ __webpack_exports__["default"] = (component.exports);

/***/ }),

/***/ "./resources/js/src/views/Pages/admin/User/Show.vue?vue&type=script&lang=js&":
/*!***********************************************************************************!*\
  !*** ./resources/js/src/views/Pages/admin/User/Show.vue?vue&type=script&lang=js& ***!
  \***********************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_Show_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../../../../node_modules/babel-loader/lib??ref--4-0!../../../../../../../node_modules/vue-loader/lib??vue-loader-options!./Show.vue?vue&type=script&lang=js& */ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/src/views/Pages/admin/User/Show.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport */ /* harmony default export */ __webpack_exports__["default"] = (_node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_Show_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__["default"]); 

/***/ }),

/***/ "./resources/js/src/views/Pages/admin/User/Show.vue?vue&type=template&id=e2f7f808&":
/*!*****************************************************************************************!*\
  !*** ./resources/js/src/views/Pages/admin/User/Show.vue?vue&type=template&id=e2f7f808& ***!
  \*****************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_Show_vue_vue_type_template_id_e2f7f808___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../../../../node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!../../../../../../../node_modules/vue-loader/lib??vue-loader-options!./Show.vue?vue&type=template&id=e2f7f808& */ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/src/views/Pages/admin/User/Show.vue?vue&type=template&id=e2f7f808&");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "render", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_Show_vue_vue_type_template_id_e2f7f808___WEBPACK_IMPORTED_MODULE_0__["render"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_Show_vue_vue_type_template_id_e2f7f808___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"]; });



/***/ })

}]);