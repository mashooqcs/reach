<?php

use App\Http\Controllers\Admin\AccountController as AdminAccount;
use App\Http\Controllers\Admin\AdminController;
use App\Http\Controllers\Admin\Auth\AuthController;
use App\Http\Controllers\Admin\FeedbackController as AdminFeedback;
use App\Http\Controllers\Admin\HomeController;
use App\Http\Controllers\Admin\NotificationController as AdminNotification;
use App\Http\Controllers\Admin\PackageController as AdminPackage;
use App\Http\Controllers\Admin\PassResetController;
use App\Http\Controllers\Admin\PropertyController as AdminProperty;
use App\Http\Controllers\Admin\SubscriptionController as AdminSubscription;
use App\Http\Controllers\Admin\TaskController as AdminTask;
use App\Http\Controllers\Admin\UserController;
use App\Http\Controllers\AppController;
use App\Http\Controllers\Auth\RegisterController;
use App\Http\Controllers\Auth\ResetPasswordController;
use App\Http\Controllers\web\AccountController;
use App\Http\Controllers\web\ApplicationController;
use App\Http\Controllers\web\ContactController;
use App\Http\Controllers\web\EmployeeController;
use App\Http\Controllers\web\PropertyController;
use App\Http\Controllers\web\ReportController;
use App\Http\Controllers\web\SubscriptionController;
use App\Http\Controllers\web\TaskController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
 */
Route::name('admin.')->prefix('admin')->group(function () {

	Route::name('auth.')->prefix('auth')->group(function () {
		Route::post('/login', [AuthController::class, 'login'])->name('login');
	});

	Route::name('password.')->prefix('password')->group(function () {
		Route::post('/verify', [PassResetController::class, 'create'])->name('verify');
		Route::post('password/validate-token/{token}', [PassResetController::class, 'verify_code'])->name('token');
		Route::post('password/change', [PassResetController::class, 'update_password'])->name('update');

	});

	Route::group(['middleware' => 'auth:admin'], function () {
		Route::get('/home', [HomeController::class, 'dashboard'])->name('home');
		Route::post('user/{user}/block', [UserController::class, 'block'])->name('user.block');
		Route::post('property/{property}/block', [AdminProperty::class, 'block'])->name('property.block');
		Route::get('task/employees/{employee}', [AdminTask::class, 'get_tasks'])->name('employee.tasks');
		Route::post('remove-property/{employee}/{property}', [AdminProperty::class, 'unassign_property'])->name('employee.remove-property');
		Route::post('account/password/update', [AdminAccount::class, 'update_password'])->name('account.password');
		Route::post('account/logout', [AdminAccount::class, 'logout'])->name('account.logout');

		Route::name('notification.')->prefix('notification')->group(function () {
			Route::get('/', [AdminNotification::class, 'index'])->name('index');
			Route::get('/bell', [AdminNotification::class, 'bell'])->name('bell');
			Route::put('/mark-as-read/{notification}', [AdminNotification::class, 'mark_as_read'])->name('markRead');

		});
		Route::name('user.')->prefix('user')->group(function () {
			Route::get('owners', [UserController::class, 'owners'])->name('owners');
			Route::post('associate', [AdminProperty::class, 'associate'])->name('associate');
			Route::get('{application}/application', [UserController::class, 'application'])->name('application');
			Route::post('{application}/application/{status}', [UserController::class, 'application_status'])->name('application_status');

		});
		
		Route::resources([
			'user' => UserController::class,
			'property' => AdminProperty::class,
			'subscription' => AdminSubscription::class,
			'task' => AdminTask::class,
			'feedback' => AdminFeedback::class,
			'package' => AdminPackage::class,
			'admin' => AdminController::class,
			'account' => AdminAccount::class,
		]);

	});
});

Route::post('broadcasting/auth', function () {

	return request()->all();
});

Route::name('app.')->group(function () {
	Route::get('app/subscriptions', [SubscriptionController::class, 'app_subscriptions'])->name('subscriptions');
	Route::post('app/contact-us', [ContactController::class, 'send'])->name('contact-us');
});
Route::post('register.check-email', [RegisterController::class, 'check_email'])->name('register.check-email');
Route::group(['middleware' => 'auth:web,employee'], function () {
	Route::post('account/update', [AccountController::class, 'update'])->name('account.update');
	Route::post('account/password/update', [AccountController::class, 'update_password'])->name('account.password');
	Route::post('account/application/resubmit', [AccountController::class, 'resubmit_application'])->name('account.application.resubmit');
	Route::get('account/application', [AccountController::class, 'application'])->name('account.application');
	
	Route::prefix('employee')->name('employee.')->group(function () {
		Route::post('remove-property/{employee}', [EmployeeController::class, 'remove_property'])->name('remove-property');
		Route::get('get-tasks/{employee}', [EmployeeController::class, 'get_tasks'])->name('get-tasks');
	});
	Route::prefix('property')->name('property.')->group(function () {
		Route::get('get-employees/{property}', [PropertyController::class, 'get_all_employees'])->name('get-employees');
		Route::get('get-all-properties/{employee}', [PropertyController::class, 'get_all_properties'])->name('get-all-properties');
		Route::post('assign-property/{employee}', [PropertyController::class, 'assign_property'])->name('assign-property');
	});

	Route::prefix('task')->name('task.')->group(function () {

		Route::get('board', [TaskController::class, 'board_tasks'])->name('board');
		Route::get('calendar', [TaskController::class, 'calendar_tasks'])->name('calendar');
		Route::get('board/show/{task}', [TaskController::class, 'board_task'])->name('board-show');
		Route::post('/task/{task}/status', [TaskController::class, 'update_task_status'])->name('status');
		Route::post('comment/{task}', [TaskController::class, 'comment'])->name('add-comment');
		Route::post('handover', [TaskController::class, 'handover_tasks'])->name('handover');
		Route::post('discrepancy/{task}', [TaskController::class, 'discrepancy'])->name('discrepancy');
	});

	Route::resources([
		'property' => PropertyController::class,
		'employee' => EmployeeController::class,
		'task' => TaskController::class,
		'report' => ReportController::class,
		'subscription' => SubscriptionController::class,
	]);
});
Route::get('/{path?}', [AppController::class, '__invoke'])->where('path', '.*');

Route::name('password.')->prefix('reset')->group(function () {
	Route::post('verify/code', [ResetPasswordController::class, 'verify_code'])->name('verify-code');
	Route::post('password/validate-token/{token}', [ResetPasswordController::class, 'validate_token'])->name('token');
});

Route::name('employee.')->prefix('employee')->group(function () {
	Route::group(['prefix' => 'auth'], function () {
		Route::post('register', [RegisterController::class, 'employee_registeration'])->name('register');
		Route::post('check-my-request', [RegisterController::class, 'check_employee_existance'])->name('check-my-request');
		Route::post('store-user-info', [ApplicationController::class, 'store_user_info'])->name('store-appliction');
		Route::post('complete-application', [ApplicationController::class, 'complete_application'])->name('complete-application');

	});
});
Route::auth();
