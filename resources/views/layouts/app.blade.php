<!doctype html>
<html lang="en">
@if(request()->is('site-manager') || request()->is('site-manager/*'))
    <head>
        <meta name="csrf_token" content="{{csrf_token()}}">
        <base href="{{asset('/')}}">
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimal-ui">
        <title>
            <?=((isset($title))?$title:'SCNELL - Home');?>
        </title>
        <link rel="icon" type="image/gif" sizes="32x32" href="{{asset('favicon.ico')}}">
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.11.2/css/all.min.css">
        <link href="https://fonts.googleapis.com/css2?family=Poppins:wght@300;400;500;600;700&display=swap"
            rel="stylesheet">
        <!-- <link rel="shortcut icon" href="images/favic=.png" /> -->
        <link href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700&display=swap" rel="stylesheet">
        <link rel="stylesheet" type="text/css" href="{{asset('admin/app-assets/css/vendors.css')}}">
        <link rel="stylesheet" type="text/css" href="{{asset('admin/app-assets/css/app.css')}}">
        <link rel="stylesheet" type="text/css" href="{{asset('admin/app-assets/css/core/menu/menu-types/vertical-menu.css')}}">
        <link rel="stylesheet" type="text/css" href="{{asset('admin/app-assets/vendors/css/charts/morris.css')}}">
        <link rel="stylesheet" type="text/css" href="{{asset('admin/app-assets/vendors/css/tables/datatable/datatables.min.css')}}">
        <link rel="stylesheet" type="text/css" href="{{asset('admin/app-assets/vendors/css/charts/morris.css')}}">
        <link href="https://unpkg.com/gijgo@1.9.13/css/gijgo.min.css" rel="stylesheet" type="text/css" />
        <link rel="stylesheet" href="{{asset('admin/assets/css/intlTelInput.css')}}">
        <link rel="stylesheet" type="text/css" href="{{asset('admin/assets/css/circle.css')}}">
        <link rel="stylesheet" type="text/css" href="{{asset('admin/assets/css/style.css')}}">
        <link rel="stylesheet" type="text/css" href="{{asset('admin/assets/css/responsive.css')}}">
         <link rel="stylesheet" href="{{asset('css/app.css')}}">
        @if (Auth::guard('admin')->check())
            <script>
               window.Laravel = {!!json_encode([
                   'isLoggedin' => true,
                   'user' => Auth::guard('admin')->user(),
                   'as' => 'admin',
               ])!!}
            </script>
        @else
            <script>
                window.Laravel = {!!json_encode([
                    'isLoggedin' => false,
                    'user' => null,
                    'as' => 'user',
                ])!!}
            </script>
        @endif
    </head>
    <body class="vertical-layout vertical-menu 2-columns menu-expanded fixed-navbar" data-open="click"
    data-menu="vertical-menu" data-col="2-columns">
@else
    <head>
        <!-- Required meta tags -->
        <base href="{{asset('/')}}">
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <meta name="csrf_token" content="{{csrf_token()}}">

        <title> SCNELL - <?php echo ((isset($title)) ? $title : 'Home'); ?></title>
        <link rel="stylesheet" href="{{asset('/assets/css/bootstrap.min.css')}}">
        <link rel="stylesheet" href="{{asset('/assets/css/all.css')}}">
        <link rel="stylesheet" href="{{asset('/assets/css/owl.theme.default.min.css')}}">
        <link rel="stylesheet" href="{{asset('/assets/css/owl.carousel.min.css')}}">
        <link rel="icon" type="image/gif" sizes="32x32" href="{{asset('favicon.ico')}}">
        <link rel="stylesheet" href="{{asset('assets/css/style.css')}}">
        <link rel="stylesheet" href="{{asset('css/app.css')}}">
        @if (Auth::check())
            <script>
               window.Laravel = {!!json_encode([
                   'isLoggedin' => true,
                   'user' => Auth::user(),
                   'as' => 'user'
               ])!!}
            </script>
        @elseif (Auth::guard('employee')->check())
            <script>
               window.Laravel = {!!json_encode([
                   'isLoggedin' => true,
                   'user' => Auth::guard('employee')->user(),
                   'as' => 'employee'
               ])!!}
            </script>
        @else
            <script>
                window.Laravel = {!!json_encode([
                    'isLoggedin' => false,
                    'user' => null,
                    'as' => 'user'
                ])!!}
            </script>
        @endif
    </head>
    <body>
@endif

    <!-- <section > -->
        <div id="app"></div>



<!-- Optional JavaScript; choose one of the two! -->
<!-- Option 1: jQuery and Bootstrap Bundle (includes Popper) -->
<script src="https://code.jquery.com/jquery-3.5.1.min.js">
</script>
<script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.1/dist/umd/popper.min.js">
</script>
<script src="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/js/bootstrap.min.js">
</script>
<script src="{{asset('js/app.js')}}"></script>

@routes


@if(request()->is('site-manager') || request()->is('site-manager/*'))   
@else
<script src="{{asset('assets/js/all.js')}}"></script>
<script src="{{asset('assets/js/owl.carousel.min.js')}}"></script>
<script src="{{asset('assets/js/datatables.min.js')}}"></script>
<script src="{{asset('assets/js/datatable-basic.js')}}"></script>
<script src="{{asset('assets/js/function.js')}}"></script>
@endif

</body>

</html>