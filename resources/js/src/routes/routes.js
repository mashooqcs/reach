// importing components

import web from './web'
import admin from './admin'

// building routes & injecting components
const routes = [
    admin,
    web,
];

export default routes;
