
import meta from './meta';
const admin = {
    layout: {
        fullPage: () => import('@views/Layout/admin/Full-page.vue').then(m => m.default),
    },
    auth: {
        login: () => import('@views/Pages/admin/Auth/Login.vue'),
    },
    account: {
        index : ()=> import('@views/Pages/admin/Account/Index.vue'),
        edit : ()=> import('@views/Pages/admin/Account/Edit.vue'),
        password : ()=> import('@views/Pages/admin/Account/Password.vue'),
    },
    error404 : ()=> import('@views/Pages/admin/Errors/404.vue'),
    dashboard : ()=> import('@views/Pages/admin/Dashboard.vue'),
    users : {
        index : ()=> import('@views/Pages/admin/User/Index.vue'),
        show : ()=> import('@views/Pages/admin/User/Show.vue'),
        logs : ()=> import('@views/Pages/admin/User/Logs.vue'),        

    },
    properties : {
        index : ()=> import('@views/Pages/admin/Property/Index.vue'),
        users : ()=> import('@views/Pages/admin/Property/Users.vue'),
        show : ()=> import('@views/Pages/admin/Property/Show.vue'),
    	datatable : ()=> import('@views/Pages/admin/Property/Datatable.vue'),
    },
    subscriptions : {
        index : ()=> import('@views/Pages/admin/Subscription/Index.vue'),
        show : ()=> import('@views/Pages/admin/Subscription/Show.vue'),
    },
    logs : {
        index : ()=> import('@views/Pages/admin/Task/Index.vue'),
        show : ()=> import('@views/Pages/admin/Task/Show.vue'),
    },
    feedback : {
        index : ()=> import('@views/Pages/admin/Feedback/Index.vue'),
        show : ()=> import('@views/Pages/admin/Feedback/Show.vue'),
    },
    packages : {
        index : ()=> import('@views/Pages/admin/Package/Index.vue'),
        edit : ()=> import('@views/Pages/admin/Package/Edit.vue'),
    },
    admins : {
        index : ()=> import('@views/Pages/admin/Admin/Index.vue'),
        create : ()=> import('@views/Pages/admin/Admin/Create.vue'),
        edit : ()=> import('@views/Pages/admin/Admin/Edit.vue'),
        show : ()=> import('@views/Pages/admin/Admin/Show.vue'),
    },
    notifications : {
        index : ()=> import('@views/Pages/admin/Notification/Index.vue'),
    },
    agents : {
        index : ()=> import('@views/Pages/admin/Agent/Index.vue'),       
    },
    
};


const routes = {
        path: '/site-manager/',
        components: {
            default: admin.layout.fullPage,
        },
        children: [
            {
                path: '/',
                component: admin.auth.login,
                name: 'admin.auth.login',
                meta: { ...meta, loggedInCan: false },
            },
            {
                path: 'dashboard',
                component: admin.dashboard,
                name: 'admin.dashboard',
                meta: { ...meta, requiresAuth: true,permission : null},
            },
            {
                path: 'account',
                component: admin.account.index,
                name: 'admin.account.index',
                meta: { ...meta, requiresAuth: true,permission : null },
            },
            {
                path: 'account/edit',
                component: admin.account.edit,
                name: 'admin.account.edit',
                meta: { ...meta, requiresAuth: true,permission : null },
            },
            {
                path: 'account/password/change',
                component: admin.account.password,
                name: 'admin.account.password',
                meta: { ...meta, requiresAuth: true,permission : null },
            },
            {
                path: 'users/:type/:status?',
                component: admin.users.index,
                name: 'admin.users.index',
                meta: { ...meta, requiresAuth: true,permission : null },
            },
            {
                path: 'tasks-logs/user/:user',
                component: admin.users.logs,
                name: 'admin.users.logs',
                meta: { ...meta, requiresAuth: true,permission : null },
            },
            {
                path: 'user/:type/:user/:status?',
                component: admin.users.show,
                name: 'admin.users.show',
                meta: { ...meta, requiresAuth: true,permission :null },
            },
            /*{
                path: 'datatable-testing',
                component: admin.properties.datatable,
                name: 'admin.properties.datatable',
                meta: { ...meta, requiresAuth: true,permission : null },
            },*/
            {
                path: 'property/:property',
                component: admin.properties.show,
                name: 'admin.properties.show',
                meta: { ...meta, requiresAuth: true,permission : null },
            },
            {
                path: 'properties/:status?',
                component: admin.properties.index,
                name: 'admin.properties.index',
                meta: { ...meta, requiresAuth: true,permission : null },
                beforeEnter(to,from,next){
                	if(to.params.status == 'blocked' || !to.params.status){
                		next();
                	}else{
                		next({name : 'admin.404'});
                	}
                }
            },
            {
                path: 'user-properties/:user',
                component: admin.properties.users,
                name: 'admin.properties.users',
                meta: { ...meta, requiresAuth: true,permission : null },
            },
            {
                path: 'subscriptions',
                component: admin.subscriptions.index,
                name: 'admin.subscriptions.index',
                meta: { ...meta, requiresAuth: true,permission : null }
            },
            {
                path: 'subscription/:id',
                component: admin.subscriptions.show,
                name: 'admin.subscriptions.show',
                meta: { ...meta, requiresAuth: true,permission : null }
            },
            {
                path: 'tasks-logs',
                component: admin.logs.index,
                name: 'admin.logs.index',
                meta: { ...meta, requiresAuth: true,permission : null }
            },
            {
                path: 'tasks-logs/:task',
                component: admin.logs.show,
                name: 'admin.logs.show',
                meta: { ...meta, requiresAuth: true,permission : null }
            },
            {
                path: 'feedback-logs',
                component: admin.feedback.index,
                name: 'admin.feedback.index',
                 meta: { ...meta, requiresAuth: true,permission : null }
            },
            {
                path: 'feedback-log/:feedback',
                component: admin.feedback.show,
                name: 'admin.feedback.show',
                meta: { ...meta, requiresAuth: true,permission : null}
            },
            {
                path: 'packages',
                component: admin.packages.index,
                name: 'admin.packages.index',
                meta: { ...meta, requiresAuth: true,permission : null }
            },
            {
                path: 'packages/edit',
                component: admin.packages.edit,
                name: 'admin.packages.edit',
                meta: { ...meta, requiresAuth: true,permission : null }
            },
            {
                path: 'admins',
                component: admin.admins.index,
                name: 'admin.admins.index',
                meta: { ...meta, requiresAuth: true,permission : null }
            },
            {
                path: 'admins/create',
                component: admin.admins.create,
                name: 'admin.admins.create',
                meta: { ...meta, requiresAuth: true,permission : null }
            },
            {
                path: 'admins/edit/:admin',
                component: admin.admins.edit,
                name: 'admin.admins.edit',
                meta: { ...meta, requiresAuth: true,permission : null }
            },
            {
                path: 'admins/:admin',
                component: admin.admins.show,
                name: 'admin.admins.show',
                meta: { ...meta, requiresAuth: true,permission : null }
            },
            {
                path: 'notifications',
                component: admin.notifications.index,
                name: 'admin.notifications.index',
                meta: { ...meta, requiresAuth: true,permission : null }
            },

            {
                path: 'agents',
                component: admin.agents.index,
                name: 'admin.agents.index',
                meta: { ...meta, requiresAuth: true,permission : null }
            },
             {
                path: '404',
                name: 'admin.404',
                component: admin.error404,
                meta: { ...meta, requiresAuth: true,permission : null },
            },
            {
                path: '*',
                redirect: { name: 'admin.404',permission : null }
            },

        ],
    }

    export default routes;