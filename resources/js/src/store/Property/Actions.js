export default{
	async getAll({commit},params){
		try {
			let response = await axios.get(params.route,{params : params.data});
			// statements
			if(params.mutation){
				commit(params.mutation,response.data.properties);
			}
			return response;
		} catch(e) {
			// statements
			return e.response
			// console.log(e);
		}
	},
	async get({commit},params){
		try {
			let response = await axios.get(params.route,{params : params.data});
			// statements
			commit('show',response.data.property);
			return response;
		} catch(e) {
			// statements
			return e.response
			// console.log(e);
		}
	},
	async delete({ commit }, params) {
        try {
            let response = await axios.delete(params.route, { params: params.data });
            commit('remove_property', response.data.id);
            return response;
        } catch (e) {
            // statements
            return e.response
            // console.log(e);
        }
    },
	async store({commit},params){
		let method = typeof params.method == 'undefined' || params.method.toLowerCase() == 'post'?'post' : 'put';

		try {
			let response = await axios.[method](params.route,params.data);
			// statements
			if (params.mutation) {
                commit(params.mutation, response.data);
            }
			return response;
		} catch(e) {
			// statements
			return e.response
			// console.log(e);
		}
	}	
}