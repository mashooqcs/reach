export default{
	index(state,data){
		Vue.set(state,'properties',data);
	},
	show(state,data){
		Vue.set(state,'property',data);
	},
	remove_property(state,id){
		let index = _.findIndex(state.properties.data,(property)=>{return property.id == id});
		state.properties.data.splice(index,1);
	},
	remove_employee(state,data){
		let requestedData = data.data;
		let index = _.findIndex(state.property.employees,(employee)=>{return employee.id == requestedData.employee});
		state.property.employees.splice(index,1);
	},
}