import Vue from 'vue'
import Vuex from 'vuex'
Vue.use(Vuex);

// importing modules
import property from './Property/index.js';
import employee from './Employee/index.js';
import task from './Task/index.js';
import admin from './Admin/index.js';

export default new Vuex.Store({
	modules : {
		property,
		employee,
		task,
		admin
	}
});