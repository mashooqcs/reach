export default{
	index(state,data){
		Vue.set(state,'employees',data);
	},
	show(state,data){
		Vue.set(state,'employee',data);
	},
	remove_property(state,data){
		let requestedData = data.data;
		let index = _.findIndex(state.employee.properties,(property)=>{return property.id == requestedData.property});
		state.employee.properties.splice(index,1);
	},
	remove_employee(state,id){
		let index = _.findIndex(state.employees.data,(employee)=>{return employee.id == id});
		state.employees.data.splice(index,1);
	},
	set_employees(state,data){
		Vue.set(state,'all_employees',data);
	},
	set_properties(state,data){
		// let properties = state.employee.properties;
		state.employee.properties.push(data);
		// Vue.set(state.employee.properties)
	},
}