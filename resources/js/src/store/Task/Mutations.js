export default{
	index(state,data){
		Vue.set(state,'tasks',data);
	},
	set_completed(state,data){
		Vue.set(state,'completed',data);
	},
	set_in_progress(state,data){
		Vue.set(state,'in_progress',data);
	},
	remove_task(state,id){
		let index = _.findIndex(state.tasks.data,(task)=>{return task.id == id});
		state.tasks.data.splice(index,1);

		let completedIndex = _.findIndex(state.completed.data,(task)=>{return task.id == id});
		state.completed.data.splice(completedIndex,1);

		let inProgressIndex = _.findIndex(state.in_progress.data,(task)=>{return task.id == id});
		state.in_progress.data.splice(inProgressIndex,1);
	},
	show(state,data){
		Vue.set(state,'task',data);
	},
	set_task_status(state,data){
		Vue.set(state,'task_status',data);
	},
	set_status(state,data){
		Vue.set(state.task,'status',data);
	},
	show_discrepancy_popup(state){
		state.discrepancy_popup = true;
	}
}