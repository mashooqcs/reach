export default{
	async getAll({commit},params){
		try {
			let response = await axios.get(params.route,{params : params.data});
			// statements
			if(params.mutation){

			commit(params.mutation,response.data.tasks);
			}else{

			commit('index',response.data.tasks);
			}
			return response;
		} catch(e) {
			// statements
			return e.response
			// console.log(e);
		}
	},
	async delete({ commit }, params) {
        try {
            let response = await axios.delete(params.route, { params: params.data });
            commit('remove_task', response.data.id);
            return response;
        } catch (e) {
            // statements
            return e.response
            // console.log(e);
        }
    },
	async get({commit},params){
		try {
			let response = await axios.get(params.route,{params : params.data});
			// statements
			commit('show',response.data.task);
			return response;
		} catch(e) {
			// statements
			return e.response
			// console.log(e);
		}
	},
	async store({commit},params){
		let method = typeof params.method == 'undefined' || params.method.toLowerCase() == 'post'?'post' : 'put';
		try {
			let response = await axios.[method](params.route,params.data);
			console.log(response);
			// statements
			return response;
		} catch(e) {
			// statements
			return e.response
			// console.log(e);
		}
	}	
}