<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class PropertyRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            //
            'name' => 'required',
            'phone' => 'required',
            'address' => 'required',
            'address_1' => 'required',
            'city' => 'required',
            'country' => 'required',
            'state' => 'required',
            'zipcode' => 'required',
            'description' => 'required',
            'floors' => 'required',
            'bedrooms' => 'required',
            'bathrooms' => 'required',
            'balconies' => 'required',
            'kitchens' => 'required',
            'files' => 'nullable|array',
        ];
    }
}
