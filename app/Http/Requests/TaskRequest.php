<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class TaskRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            //
            'employeeIds' => 'required|array',
            'property_id' => 'required',
            'files' => 'required|array',
            'start' => 'required',
            'to' => 'required',
            'name' => 'required',
            'due_date' => 'required',
            'status' => 'required',
        ];
    }
}
