<?php

namespace App\Http\Controllers\web;

use App\Http\Controllers\Controller;
use App\Http\Requests\EmployeeRequest;
use App\Models\Employee;
use App\Models\EmployeeProperty;
use App\Models\EmployeeTask;
use App\Models\Task;
use Illuminate\Http\Request;
use Illuminate\Support\Str;

class EmployeeController extends Controller {
	//

	public function index() {
		$employees = Employee::withCount(['properties', 'tasks', 'tasks as pending_tasks' => function ($q) {
			$q->where('tasks.status', 0);
		}])->where('owner_id', auth()->id());
		if(request()->filled('search')){
			$employees->where('name','like','%'.request('search').'%');
		}
		if ( request()->filled('exclude') && !is_countable( request('exclude') )) {
			$employees->where('id','!=',request('exclude'));
		}elseif( request()->filled('exclude') && is_countable( request('exclude') )){

			$employees->whereNotIn('id',request('exclude'));
		}

		if ( request()->filled('page') ) {
			$employees = $employees->paginate(10);
		} else {

			$employees = $employees->get();
		}
		return response()->json(compact('employees'));
	}
	public function show($id) {
		$employee = Employee::with(['properties' => function ($q) use ($id) {
			$q->addSelect([
				'_total_tasks_pending' => EmployeeTask::selectRaw('COUNT(*)')->whereRaw('employee_tasks.employee_id = (SELECT id from employees WHERE id = ? LIMIT 1)', ['id' => $id])->whereRaw('properties.property_id IN (SELECT property_id FROM tasks WHERE status = 0)'),
			]);
		}])->withCount('properties')->where('employee_id', $id)->first();
		return response()->json(compact('employee'));
	}
	public function store(EmployeeRequest $request, Employee $employee) {
		$employee->fill($request->validated());
		$employee->employee_id = (String) Str::uuid();
		$password = Str::random(8);
		$code = Str::random(8);
		$employee->owner_id = auth()->id();
		$employee->password = $password;
		$employee->generated_password = $password;
		$employee->code = $code;
		$employee->save();
		$employee->properties()->attach($request->property);
		return response()->json(['status' => true, 'message' => 'Employee has been added successfully', 'employee' => $employee]);
	}

	public function remove_property($id, Request $request) {
		$employee = Employee::where('employee_id', $id)->first();
		$result = EmployeeProperty::where('employee_id', $employee->id)->where('property_id', $request->property)->delete();
		return response()->json(['status' => true, 'msg' => 'employee has been removed successfully', 'data' => $request->all()]);
		// properties::where('')->
	}

	public function get_tasks($id) {

		$tasks = Task::with('property', 'task.employee')->whereIn('property_id', function ($builder) {

			$builder->select('id')->from('properties')
				->where('owner_id', auth()->id());

		});
		/*if($request->filled('property_id')){
			            $tasks->where('property_id',$request->property_id);
		*/
		/*if($request->filled('status')){
			            $status = $request->status == 'Completed'? 1 : 0;
			            $tasks->where('status',$status);

		*/
		/*if($request->filled('from') && $request->filled('to')){
			            $tasks->where(function($q){
			                $q->where('created_at','>=',request('from').' 23:59:59')
			            ->where('created_at','<=',request('to').' 23:59:59');
			            });

		*/
		$todaystasks = clone $tasks;
		$recentTasks = clone $tasks;
		$upcomingTasks = clone $tasks;

		$recentTasks = $recentTasks->whereDay('created_at', today())->whereBetween('created_at', [now()->subHours(2), now()])->get();
		$todaystasks = $todaystasks->whereDay('due_date', today())->get();
		$upcomingTasks = $upcomingTasks->whereDay('due_date', '>', today())->get();
		$data['recentTasks'] = $recentTasks;
		$data['todaystasks'] = $todaystasks;
		$data['upcomingTasks'] = $upcomingTasks;

		return response()->json(['task' => $data]);
	}

	public function destroy($id) {
		$employee = Employee::find($id);
		$employee->properties()->detach();
		$employee->delete();
		return response()->json(['status' => true, 'msg' => 'deleted successfully','id'=>$id]);
	}
}
