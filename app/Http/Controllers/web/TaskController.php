<?php

namespace App\Http\Controllers\web;

use App\Http\Controllers\Controller;
use App\Http\Requests\TaskRequest;
use App\Models\Admin;
use App\Models\Comment;
use App\Models\Discrepancy;
use App\Models\Employee;
use App\Models\Property;
use App\Models\Task;
use App\Models\User;
use App\Notifications\NotifyAdmin;
use App\Notifications\NotifyProprietor;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Notification;

class TaskController extends Controller {
	/**
	 * Display a listing of the resource.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function index(Request $request, Task $task) {
		if (auth('web')->check()) {
			$tasks = $task->with('property', 'task.employee')->whereIn('property_id', function ($builder) {

				$builder->select('id')->from('properties')
					->where('owner_id', auth()->id());

			});
		} else {
			$tasks = $task->with('property', 'task.employee')->whereIn('id',function($query){
				$query->select('employee_tasks.task_id')
				->from('employee_tasks')
				->where('employee_id',auth('employee')->id());
			});
		}
		if ($request->filled('property_id')) {
			$tasks->where('property_id', $request->property_id);
		}
		if ($request->filled('status')) {
			if(strtolower($request->status) == 'completed'){
				$tasks->where('status', 1);
			}elseif(strtolower($request->status) == 'incomplete'){

				$tasks->whereIn('status', [-1,0]);
			}elseif(strtolower($request->status) == 'reported'){
				$tasks->whereIn('id',function($query){
				$query->select('task_reports.task_id')
				->from('task_reports')
				->where('reporter',auth('employee')->id());
			});
				// $tasks->whereIn('status', [-1,0]);
			}elseif(strtolower($request->status) == 'leave discrepancy'){

				$tasks->whereIn('id',function($query){
					$query->select('discrepancies.task_id')
					->from('discrepancies')
					->where('employee_id',auth('employee')->id());
				});
			}else{

			}
			// $status = $request->status == 'completed' ? 1 : 0;
			// $tasks->where('status', $status);
		}
		if ($request->filled('from') && $request->filled('to')) {
			$tasks->where(function ($q) {
				$q->where('created_at', '>=', request('from') . ' 23:59:59')
					->where('created_at', '<=', request('to') . ' 23:59:59');
			});

		}
		$tasks = $tasks->paginate(10);
		//
		return response()->json(compact('tasks'));
	}

	public function calendar_tasks(Request $request, Task $task) {
		if (auth('web')->check()) {
			$tasks = $task->with('property', 'task.employee')->whereIn('property_id', function ($builder) {

				$builder->select('id')->from('properties')
					->where('owner_id', auth()->id());

			});
		} else {
			$tasks = $task->with('property', 'task.employee')->whereIn('id',function($query){
				$query->select('employee_tasks.task_id')
				->from('employee_tasks')
				->where('employee_id',auth('employee')->id());
			});
		}
		if ($request->filled('property_id')) {
			$tasks->where('property_id', $request->property_id);
		}
		if (request()->filled('search')) {
			$tasks->where('name', 'like', '%' . request('search') . '%');
		}
		if ($request->filled('from') && $request->filled('to')) {
			$tasks->whereDate('created_at', '>=', request('from'))
				->whereDate('created_at', '<=', request('to'));

		}
		if ($request->filled('page')) {

			$tasks = $tasks->paginate(10);
		} else {
			$tasks = $tasks->get();

		}
		//
		return response()->json(compact('tasks'));
	}

	/**
	 * Display a listing of the resource.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function board_tasks(Request $request, Task $task) {
		if (auth('web')->check()) {
			$tasks = $task->with('property', 'task.employee')->whereIn('property_id', function ($builder) {

				$builder->select('id')->from('properties')
					->where('owner_id', auth()->id());

			});
		} else {
			$tasks = $task->with('property', 'task.employee')->whereIn('id',function($query){
				$query->select('employee_tasks.task_id')
				->from('employee_tasks')
				->where('employee_id',auth('employee')->id());
			});
		}
		if ($request->filled('property_id')) {
			$tasks->where('property_id', $request->property_id);
		}
		if ($request->filled('status')) {
			$status = $request->status == 'completed' ? 1 : 0;
			$tasks->where('status', $status);
		} else {

			$tasks->where('status', -1);
		}
		if (request()->filled('search')) {
			$tasks = $tasks->where('name', 'like', '%' . request('search') . '%');
		}
		if ($request->filled('from') && $request->filled('to')) {
			$tasks->whereDate('created_at', '>=', request('from'))
				->whereDate('created_at', '<=', request('to'));

		}
		if ($request->filled('page')) {

			$tasks = $tasks->paginate(10);
		} else {
			$tasks = $tasks->get();

		}
		//
		return response()->json(compact('tasks'));
	}

	public function board_task($id, Request $request) {
		if (auth('web')->check()) {
			$tasks = Task::with('property', 'task.employee')->whereIn('property_id', function ($builder) {

				$builder->select('id')->from('properties')
					->where('owner_id', auth()->id());

			});
		} else {
			$tasks = Task::with('property', 'task.employee')->whereIn('id',function($query){
				$query->select('employee_tasks.task_id')
				->from('employee_tasks')
				->where('employee_id',auth('employee')->id());
			});
		}
		if ($request->filled('property_id')) {
			$tasks->where('property_id', $request->property_id);
		}
		if ($request->filled('status')) {
			$status = $request->status == 'Completed' ? 1 : 0;
			$tasks->where('status', $status);

		}
		if ($request->filled('from') && $request->filled('to')) {
			$tasks->where(function ($q) {
				$q->where('created_at', '>=', request('from') . ' 23:59:59')
					->where('created_at', '<=', request('to') . ' 23:59:59');
			});

		}

		$task = $tasks->findOrFail($id);
		//
		return response()->json(compact('task'));
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function create() {
		//
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @return \Illuminate\Http\Response
	 */
	public function store(TaskRequest $request) {
		$task = new Task;
		$task->fill($request->validated())->save();
		$task->employees()->attach($request->employeeIds);
		return response()->json(['status' => true, 'msg' => 'Task has been created successfully']);
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function show($id, Request $request) {
		if (auth('web')->check()) {
			$tasks = Task::with('property', 'task.employee','report')->whereIn('property_id', function ($builder) {

				$builder->select('id')->from('properties')
					->where('owner_id', auth()->id());

			});
		} else {
			$tasks = Task::with('property', 'task.employee','report')->whereIn('id',function($query){
				$query->select('employee_tasks.task_id')
				->from('employee_tasks')
				->where('employee_id',auth('employee')->id());
			});
		}
		if ($request->filled('property_id')) {
			$tasks->where('property_id', $request->property_id);
		}
		if ($request->filled('status')) {
			$status = $request->status == 'Completed' ? 1 : 0;
			$tasks->where('status', $status);

		}
		if ($request->filled('from') && $request->filled('to')) {
			$tasks->where(function ($q) {
				$q->where('created_at', '>=', request('from') . ' 23:59:59')
					->where('created_at', '<=', request('to') . ' 23:59:59');
			});

		}

		$task = $tasks->findOrFail($id);
		//
		return response()->json(compact('task'));
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function edit($id) {
		//
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function update(Request $request, $id) {
		//
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function destroy($id) {
		$task = Task::find($id);
		$task->employees()->detach();
		$task->delete();
		return response()->json(['status' => true, 'msg' => 'Task has been deleted successfully', 'id' => $id]);
	}
	public function handover_tasks(Request $request) {
		$request->validate([
			'assign_to' => 'required',
			'employee' => 'required',
		]);
		DB::beginTransaction();
		$employee = Employee::find($request->employee);
		$employee2 = Employee::find($request->assign_to);

		$tasks = $employee->tasks();
		if ($request->filled('property')) {
			$tasks->where('tasks.property_id', $request->property);
		}
		$tasks = $tasks->get()->load('property');
		// assigning properties if employee dont have related tasks property
		if (!$request->filled('property')) {

			$properties = $tasks->pluck('property');
			foreach ($properties as $key => $property) {
				if (!$employee2->properties->contains($property->id)) {
					$employee2->properties()->save($property);
				}
			}
		} else {
			$property = Property::find($request->property);
			if (!$employee2->properties->contains($property->id)) {
				$employee2->properties()->save($property);
			}
		}

		$task_ids = $tasks->pluck('id');
		$employee2->tasks()->saveMany($tasks);
		$employee->tasks()->detach($task_ids);
		DB::commit();
		return response()->json(['status' => true, 'msg' => 'all Tasks has been handoverd successfully']);
	}
	public function update_task_status($id) {
		$task = Task::find($id);
		$task->status = request()->status;
		$task->save();
		return response()->json(['status' => true, 'msg' => 'task status has been updated successfully']);
	}
	public function comment($id, Request $request) {
		$task = Task::findOrFail($id);
		$task->comments()->save(new Comment(['comment_by' => $request->comment_by, 'comment' => $request->comment]));
		return response()->json(['status' => true, 'msg' => 'commented successfully']);
	}

	public function discrepancy($taskId,Request $request){
		$task = Task::find($request->task);
		if($task){
    	$report = Discrepancy::updateOrCreate(['task_id'=>$request->task,'employee_id'=>auth('employee')->id()],$request->only('type','reason','time_in','time_out'));
    	$admins = Admin::where('role', 'super_admin')->Orwhere('permissions', 'like', '%admin.logs%')->get();

    	Notification::send($admins, new NotifyAdmin([
			'message' => 'new discrepancy submission',
			'title' => auth('employee')->user()->name.' has submitted leave discrepancy',
			'id' => $task->id,
			'route' => [
				'name' => 'admin.logs.show',
				'params' => ['task' => encrypt($task->id)],
			],
		])
		);
			$owner = User::find(auth('employee')->user()->owner_id);
			try{

			Notification::send($owner, new NotifyProprietor([
			'message' => 'new discrepancy submission',
			'title' => auth('employee')->user()->name.' has submitted leave discrepancy',
			'id' => $task->id,
			'route' => [
				'name' => 'web.tasks.show',
				'params' => ['task' => $task->id],
			],
		])
			);
			}catch(\Exception $e){
				return $e->getMessage();
			}
    	return response()->json(['status' => true,'msg'=> 'discrepancy has been submitted successfully']);
    	}else{

    	return response()->json(['status' => false,'msg'=> 'invalid task']);
    	}
	}
}
