<?php

namespace App\Http\Controllers\web;

use App\Http\Controllers\Controller;
use App\Models\Application;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;

class AccountController extends Controller
{
    //
    public function update(Request $request){
    		$validatedData = $request->validate(['name' => 'required','phone' => 'required']);
    		auth()->user()->update($validatedData);

    		return response()->json(['status'=>true,'msg'=>'Account info updated successfully','user'=> auth()->user()]);
    }

    public function update_password(Request $request){
    	$validatedData = $request->validate([
    		'current_password' => 'required',
    		'password' => 'required|confirmed',
    	]);

    	if(Hash::check($request->current_password, auth()->user()->password)){
    		$user = auth()->user();
    		$user->password = bcrypt($request->password);
    		$user->save();
    		return response()->json(['status'=>true,'msg'=>'Password has been updated successfully']);
    	}
    		return response()->json(['status'=>false,'errors'=>['current_password' => 'current password in not valid']],422);	

    }

    public function application(){
        return $application = auth('employee')->user()->application;
    }

    public function resubmit_application(Request $request){

        auth('employee')->user()->fill(
            $request->only('name','phone','address','address_1','gender','service_years','city','state','zipcode','country')
        )->save();
        $stepName = $request->keys()[1];
        $stepFields = $request->application;
        $user = auth('employee')->id();
        $steps = [
                'education',
                'institutions',
                'experiences',
                'certifications',
                'cv',
                'skills',
            ];
        $application = Application::firstOrNew(['employee_id'=>$user,'status'=>'pending']);

        foreach ($steps as $key => $step) {
            
            if(array_key_exists($step, $request->application)){

                if(array_key_exists('file',$request->application[$step]) && $step != 'certifications'){
                    $path = $request->application[$step]['file']->store('public/media');
                    $request->application[$step]['file'] = basename($path);
                }elseif($step == 'certifications' && is_array($request->application[$step])){

                    foreach ($request->application[$step] as $key => $field) {
                        if(array_key_exists('file',$field)){
                            $path = $field['file']->store('public/media');
                            $request->application[$step][$key2]['file'] = basename($path);
                        }
                    }
                }
                $application->{$step} = $request->application[$step];
            }else{

                $application->{$step} = null;
            }
        }
        $application->save();
        return response()->json(['status'=>true,'msg'=>'applied successfully','user'=>auth('employee')->user()]);
    }



}
