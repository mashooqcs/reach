<?php

namespace App\Http\Controllers\web;

use App\Http\Controllers\Controller;
use App\Models\Admin;
use App\Models\Feedback;
use App\Notifications\ContactUsEmail;
use App\Notifications\NotifyAdmin;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Notification;
use Illuminate\Support\Str;

class ContactController extends Controller
{
    //
    public function send(Request $request){
    	$validatedData = $request->validate([
    		'email' => 'required|email',
    		'name' => 'required',
    		'subject' => 'required',
    		'message' => 'required',
    ]);
        $feedback = new Feedback($validatedData);
        $feedback->feedback_id = (String) Str::uuid(); 
        // $feedback = $feedback->create();
        $feedback->save();
        
        $admins = Admin::where('role', 'super_admin')->Orwhere('permissions', 'like', '%admin.feedbacks%')->get();
        Notification::send($admins, new NotifyAdmin([
            'message' => 'New Feedback recieved',
            'title' => 'new feedback form has been submitted',
            'id' => $feedback->feedback_id,
            'route' => [ 
                'name' => 'admin.feedback.show',
                'params' => ['feedback' => $feedback->feedback_id ]  
            ],
        ])
        );

    	Notification::route(env('ADMIN_EMAIL'),new ContactUsEmail($validatedData) );
    	return response()->json(['status'=> true,'msg'=>'your query has been sent to admin successfully']);
    }
}
