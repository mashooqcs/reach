<?php

namespace App\Http\Controllers\web;

use App\Http\Controllers\Controller;
use App\Models\Application;
use Illuminate\Http\Request;
use Illuminate\Support\Str;

class ApplicationController extends Controller
{
    public function store_user_info(Request $request){
    	$stepName = $request->keys()[1];
    	$stepFields = $request->{$stepName};
    	$user = $request->user;
    	$application = Application::firstOrNew(['employee_id'=>$user,'status'=>'pending']);
    	
    	$contains = Str::contains($stepName, ['certifications']);
    	if(!$contains){
	    	if(array_key_exists('file',$stepFields)){
	    		$path = $stepFields['file']->store('public/media');
	    		$stepFields['file'] = basename($path);
	    	}
	    	$application->{$stepName} = $stepFields;
    	}else{
    		foreach ($stepFields as $key => $field) {
    			if(array_key_exists('file',$field)){

		    		$path = $field['file']->store('public/media');
		    		$stepFields[$key]['file'] = basename($path);
	    		}
    		}
	    	$application->{$stepName} = $stepFields;
    	}
    	$application->save();
    }

    public function complete_application(Request $request){
    	$user = $request->user;
    	$application = Application::firstOrNew(['employee_id'=>$user,'status'=>'pending']);
    	$application->skills = $request->skills;
    	$application->status = 'applied';
    	$application->save();
    	return response()->json(['status'=>true,'msg'=>'applied successfully']);
    }
}
