<?php

namespace App\Http\Controllers\web;

use App\Http\Controllers\Controller;
use App\Models\Admin;
use App\Models\Task;
use App\Models\TaskReport;
use App\Models\User;
use App\Notifications\NotifyAdmin;
use App\Notifications\NotifyProprietor;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Notification;

class ReportController extends Controller
{
    public function store(Request $request){
    	$task = Task::find($request->task);
    	if($task){
    	$report = TaskReport::updateOrCreate(['task_id'=>$request->task,'reporter'=>auth('employee')->id()],$request->only('title','reason'));
    	$admins = Admin::where('role', 'super_admin')->Orwhere('permissions', 'like', '%admin.logs%')->get();
		Notification::send($admins, new NotifyAdmin([
			'message' => 'new task reported',
			'title' => auth('employee')->user()->name.' has reported new task',
			'id' => $task->id,
			'route' => [
				'name' => 'admin.logs.show',
				'params' => ['task' => encrypt($task->id)],
			],
		])
		);
			$owner = User::find(auth('employee')->user()->owner_id);
			Notification::send($owner, new NotifyProprietor([
			'message' => 'new task reported',
			'title' => auth('employee')->user()->name.' has reported new task',
			'id' => $task->id,
			'route' => [
				'name' => 'web.tasks.show',
				'params' => ['task' => $task->id],
			],
		])
			);
    	return response()->json(['status' => true,'msg'=> 'task has been reported successfully']);
    	}else{

    	return response()->json(['status' => false,'msg'=> 'invalid task']);
    	}
    }
}
