<?php

namespace App\Http\Controllers\web;

use App\Http\Controllers\Controller;
use App\Http\Requests\PropertyRequest;
use App\Models\Employee;
use App\Models\Property;
use App\Notifications\PropertyAssignment;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;

class PropertyController extends Controller {
	//
	public function index(Request $request) {
		if (auth('web')->check()) {
			// for proprieter auth
			$properties = Property::withCount('employees')->where('owner_id', auth()->id());

			if (request()->filled('search')) {
				$properties->where('name', 'like', '%' . request('search') . '%');
			}
			if ($request->filled('page')) {

				$properties = $properties->paginate(10);
			} else {
				$properties = $properties->get();
			}
			return response()->json(compact('properties'));
		}
		// for employee auth
		$properties = Property::whereIn('id',function($query){
			$query->select('employee_properties.property_id')
			->from('employee_properties')
			->where('employee_id',auth('employee')->id());
		});

		if (request()->filled('search')) {
			$properties->where('name', 'like', '%' . request('search') . '%');
		}
		if ($request->filled('page')) {

			$properties = $properties->paginate(10);
		} else {
			$properties = $properties->get();
		}
		return response()->json(compact('properties'));
	}
	public function show($id) {

		$property = Property::with(['images', 'employees' => function ($q) {
			if (request()->filled('search')) {
				$q->where('name', 'like', '%' . request('search') . '%');
			}
			$q->withCount(['tasks as pending_tasks' => function ($q) {
				$q->where('tasks.status', 0);
			}]);
		}])->where('owner_id', auth()->id())->where('property_id', $id)->first();
		return response()->json(compact('property'));
	}
	public function store(PropertyRequest $request) {
		$property = new Property;
		$property->owner_id = auth()->id();
		$property->property_id = (String) Str::uuid();
		$property->fill($request->validated());
		$property->save();
		return response()->json(['status' => true, 'msg' => 'Property created successfully']);
	}
	public function update($id, PropertyRequest $request) {
		$property = Property::wherePropertyId($id)->first();
		$property->fill($request->validated());
		$property->save();
		return response()->json(['status' => true, 'msg' => 'Property updated successfully']);
	}

	public function get_all_properties($employee_id, Request $request) {

		$employee = Employee::where('employee_id', $employee_id)->first();
		$properties = Property::where('owner_id', auth()->id())->whereRaw('id NOT IN (SELECT property_id FROM employee_properties WHERE employee_id = ?)', ['employee_id' => $employee->id])->paginate(10);
		return response()->json(compact('properties'));
	}

	public function assign_property($employee_id, Request $request) {
		try {
			DB::beginTransaction();
			$employee = Employee::where('employee_id', $employee_id)->first();
			$employee->properties()->attach($request->property);

			$employee->notify(new PropertyAssignment());
			DB::commit();

		} catch (Exception $e) {
			DB::rollback();
			// throw new Exception($e->getMessage(), 1);
			return response()->json(['status' => false, 'msg' => $e->getMessage()], 409);
		}
		return response()->json(['status' => true, 'msg' => 'Property assigned successfully']);
	}
	public function get_all_employees($property) {
		return Property::find($property)->employees()->get();
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function destroy($id) {
		$property = Property::find($id);
		$property->employees()->detach();
		$property->tasks()->delete();
		$property->delete();
		return response()->json(['status' => true, 'msg' => 'Property has been deleted successfully', 'id' => $id]);
	}
}
