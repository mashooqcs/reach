<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Admin;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Str;
use Illuminate\Validation\Rule;

class AccountController extends Controller
{
    
    public function update($admin_id,Request $request){
    	$validatedData = $request->validate([
    		'email' => ['nullable',Rule::unique('admins')->ignore($admin_id,'admin_id')],
    		'first_name' => 'required',
    		'last_name' => 'required',
    	]);
    	$admin = Admin::find($admin_id);
    	$admin->admin_id = (String) Str::uuid();
    	$admin->forceFill($validatedData)->save();

    	return response()->json(['status'=> true,'msg'=>'Admin account has been created successfully','user' => $admin]);
    }

    public function update_password(Request $request){
    	$validatedData = $request->validate([
    		'current_password' => 'required',
    		'password' => 'required|confirmed',
    	]);

    	if(Hash::check($request->current_password, auth('admin')->user()->password)){
    		$user = auth('admin')->user();
    		$user->password = $request->password;
    		$user->save();
    		return response()->json(['status'=>true,'msg'=>'Password has been updated successfully']);
    	}
    		return response()->json(['status'=>false,'errors'=>['current_password' => 'current password in not valid']],422);	

    }

    public function logout(Request $request){
        auth('admin')->logout();
        return response()->json(['status' => true,'msg'=>'Have a good day!']);
    }
}
