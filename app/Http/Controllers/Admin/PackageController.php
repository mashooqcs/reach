<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Plan;
use Illuminate\Http\Request;

class PackageController extends Controller
{
    //
    public function index(){
    	$packages = Plan::all();
    	return response()->json(compact('packages'));
    }

    public function update(Request $request){
    	$validatedData = $request->validate([
    		'packages' => 'required|array',
    	]);

    	foreach ($request->packages as $key => $package) {
    		$plan = Plan::find($package['id']);
    		$plan->forceFill($package)->save();

    	}

    	return response()->json(['status' => true,'msg' => 'Packages updated successfully']);
    }
}
