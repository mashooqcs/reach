<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Application;
use App\Models\Employee;
use App\Models\EmployeeTask;
use App\Models\User;
use Illuminate\Http\Request;

class UserController extends Controller
{
    const USER_STATUS_INACTIVE  = 0;
    const USER_STATUS_ACTIVE    = 1;
    const USER_STATUS_DELETED   = 2;
    const USER_STATUS_REJECTED  = 3;

    const USER_TYPE_ADMIN      = 1;
    const USER_TYPE_MANAGER    = 2;
    const USER_TYPE_AGENT      = 3;
    
    const ACTIVITY_STATUS_ACTIVE        = 1;
    const USER_ACTIVITY_STATUS_COMPLETED= 1;
    //
    public function index(){
        $userId             = Auth()->user()->id;
        $userModel          = new User;
        $users   = User::get();
        dd($users);
        return response()->json(compact('users'));
    }

    public function show($id){
            if(request()->filled('type')){
                if(request('type') == 'property-owner'){
                    $user = new User;
                    $user = $user->withCount('employees','properties')->with('media')->findOrFail($id);
                    return response()->json(compact('user'));
                }
                $user = Employee::with(['properties' => function ($q) use ($id) {
                    if(request()->filled('search')){
                        $q->where('name','like',"%".request('search')."%");
                    }
                $q->addSelect([
                    '_total_tasks_pending' => EmployeeTask::selectRaw('COUNT(*)')->where('employee_id',$id)->whereRaw('properties.property_id IN (SELECT property_id FROM tasks WHERE status = 0)'),
                ]);
                }])->withCount('properties','tasks')->where('id', $id);
                $user = $user->first();
            return response()->json(compact('user'));
            }

    }

    public function block($user){
        $user = User::find($user);
        if($user->status == 1){
            $user->status = 0;
            $user->save();
            return response()->json(['status'=>true,'msg'=>'blocked successfully','data' => $user]);
        }else{
            $user->status = 1;
            $user->save();
            return response()->json(['status'=>true,'msg'=>'unblocked successfully','data' => $user]);
        }
    }

    public function application_status($application,$status){
        $application = Application::find($application);
        $application->status = $status;
        $application->reason = request('reason',NULL);
        $application->save();
        return response()->json(['status'=>true,'msg'=>'application '.$status.' successfully']);
    }

    public function application($applicationId){
        $application = Application::with('employee')->findOrFail($applicationId);
        return response()->json(compact('application'));
    }


    public function owners(){
                    $users = User::with('properties');
                    
                    if(request()->filled('exclude') && request('exclude')){
                        $users = $users->where('id','!=',request('exclude'));
                    }
                    if(request()->filled('search')){
                        $users->where('name','like',"%".request('search')."%");
                    }
                    if(request()->filled('page')){
                        $users = $users->latest('id')->paginate(request('entries',4));
                    }else{
                        $users = $users->get();
                    }
                    return response()->json(compact('users'));

            return response()->json(compact('users'));
    }
}
