<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Task;
use Illuminate\Http\Request;

class TaskController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request, Task $task)
    {

        $tasks = $task->with('property', 'task.employee');
        if ($request->filled('search')) {
            $tasks->where('name','like', "%".$request->search."%");
        }
        if ($request->filled('from') && $request->filled('to')) {
            $tasks->where(function ($q) {
                $q->where('created_at', '>=', request('from') . ' 23:59:59')
                    ->where('created_at', '<=', request('to') . ' 23:59:59');
            });

        }
        $tasks = $tasks->paginate(10);
        //
        return response()->json(compact('tasks'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $id = decrypt($id);
        $task = Task::with('property.owner', 'task.employee');

        $task = $task->findOrFail($id);
        //
        return response()->json(compact('task'));
    }


    public function get_tasks($id) {

        $tasks = Task::with('property', 'task.employee')->whereIn('property_id', function ($builder) {

            $builder->select('id')->from('properties')
                ->where('owner_id', auth()->id());

        });
        $todaystasks = clone $tasks;
        $recentTasks = clone $tasks;
        $upcomingTasks = clone $tasks;

        $recentTasks = $recentTasks->whereDay('created_at', today())->whereBetween('created_at', [now()->subHours(2), now()])->get();
        $todaystasks = $todaystasks->whereDay('due_date', today())->get();
        $upcomingTasks = $upcomingTasks->whereDay('due_date', '>', today())->get();
        $data['recentTasks'] = $recentTasks;
        $data['todaystasks'] = $todaystasks;
        $data['upcomingTasks'] = $upcomingTasks;

        return response()->json(['task' => $data]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
