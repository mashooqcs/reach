<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\UserPlan;
use Illuminate\Http\Request;

class SubscriptionController extends Controller
{
    //
    public function index(){
    	$subscriptions = UserPlan::with('plan','user')->latest('id');
        if(request()->filled('search')){
            $subscriptions = $subscriptions->whereHas('user',function($q){
                $q->where('name','like','%'.request()->search.'%');
            })->orWhereHas('user',function($q){
                $q->where('name','like','%'.request()->search.'%');
            });
        }
        $subscriptions = $subscriptions->paginate(10);
        $subscriptions->load('payment');
        return response()->json(compact('subscriptions'));
    }

    public function show($id){
    	$id = decrypt($id);
    	$subscription = UserPlan::with('plan','user')->find($id);
        $subscription = $subscription->load('payment');
        return response()->json(compact('subscription'));
    }
}
