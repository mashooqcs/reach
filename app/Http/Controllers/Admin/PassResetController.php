<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Admin;
use App\Models\PasswordReset;
use App\Notifications\PasswordResetRequest;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Str;

class PassResetController extends Controller
{
    //
    public function create(Request $request)
    {
        $request->validate([
            'email' => 'required|string|email|exists:admins,email',
        ]);
        if(!Admin::where('email',$request->email)->exists()){
        	return response()->json([
	            'status' => false,
	            'msg' => 'invalid email System! does\'t recognize this email!'
	        ]);
        }
        $user = Admin::where('email', $request->email)->first();
        $passwordReset = PasswordReset::updateOrCreate(
            ['email' => $user->email],
            [
                'email' => $user->email,
                'token' => strtolower(Str::random(6))
            ]
        );
        if ($user && $passwordReset)
            $user->notify(
                new PasswordResetRequest($user, $passwordReset->token)
            );
        return response()->json([
        	'status'=>true,
            'msg' => 'We have sent you a verification code!'
        ]);
    }
    public function verify_code(Request $request){
        $code = PasswordReset::where('token',$request->code)->first();
        if($code){

            return response()->json(['status'=>true,'msg'=>'code has been verified successfully']);
        }else{
            return response()->json(['status'=>false,'errors' => ['code'=>'this code is invalid']],422);

        }
    }

    public function update_password(Request $request){
        $code = PasswordReset::where('token',$request->code)->first();
        $user = Admin::whereEmail($code->email)->first();
        $user->password = $request->password;
        $user->save();
        
        $code->delete();
        // $code = PasswordReset::where('token',$request->code)->delete();
        return response()->json(['status'=>true,'msg'=>'Password changed successfully']);
    }
}
