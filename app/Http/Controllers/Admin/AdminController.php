<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Admin;
use Illuminate\Http\Request;
use Illuminate\Support\Str;
use Illuminate\Validation\Rule;

class AdminController extends Controller
{
    //
    public function index(){
    	$admins = Admin::where('role','sub_admin');
    	if(request()->filled('search')){
    		$admins = $admins->where('first_name' ,'like','%'.request('search').'%')
    		->orWhere('last_name' ,'like','%'.request('search').'%')
    		->orWhere('email' ,'like','%'.request('search').'%');
    	}
    	$admins = $admins->paginate(10);
    	return response()->json(compact('admins'));
    }
    public function show($admin_id){
    	$admin = Admin::where('admin_id',$admin_id)->first();
    	return response()->json(compact('admin'));
    }

    public function store(Request $request){
    	$validatedData = $request->validate([
    		'email' => 'required|unique:admins,email',
    		'first_name' => 'required',
    		'last_name' => 'required',
    		'password' => 'required|confirmed',
    		'permissions' => 'required',
    	]);
    	$admin = new Admin;
    	$admin->admin_id = (String) Str::uuid();
    	$admin->forceFill($validatedData);
    	$admin->save();

    	return response()->json(['status'=> true,'msg'=>'Admin account has been created successfully']);
    }

    public function update($admin_id,Request $request){
    	$validatedData = $request->validate([
    		'email' => ['required',Rule::unique('admins')->ignore($admin_id,'admin_id')],
    		'first_name' => 'required',
    		'last_name' => 'required',
    		'password' => 'nullable|confirmed',
    		'permissions' => 'required',
    	]);
    	$admin = Admin::where('admin_id',$admin_id)->first();
    	$admin->admin_id = (String) Str::uuid();
    	$admin->forceFill($validatedData)->save();

    	return response()->json(['status'=> true,'msg'=>'Admin account has been created successfully']);
    }


    public function destroy($admin){
        $entity = Admin::find($admin);
        $entity->delete();
        return response()->json(['status'=>true,'msg'=>'unblocked successfully','data' => $admin]);
    }
}
