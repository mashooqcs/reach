<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Feedback;
use Illuminate\Http\Request;
use DB;

class FeedbackController extends Controller
{
	const USER_STATUS_INACTIVE  = 0;
    const USER_STATUS_ACTIVE    = 1;
    const USER_STATUS_DELETED   = 2;
    const USER_STATUS_REJECTED  = 3;

    const USER_TYPE_ADMIN      = 1;
    const USER_TYPE_MANAGER    = 2;
    const USER_TYPE_AGENT      = 3;
    //
    public function index(Request $request,Feedback $feedback){
    	//$feedbacks = $feedback->latest('id');
		$feedbacks = Feedback::join('users', 'feedback.user_id', '=', 'users.id');
    	if(request()->filled('search')){
    		$feedbacks->where('name','like','%'.request('search').'%')
    		->orWhere('email','like','%'.request('search').'%');
    	}
    	$feedbacks = $feedbacks->where('users.role_id', self::USER_TYPE_AGENT)->select('feedback.*')->addSelect('users.*')->addSelect('feedback.id as feedback_id')->paginate();
    	return response()->json(compact('feedbacks'));
    }

    public function show($id){
    	$feedback = Feedback::join('users', 'feedback.user_id', '=', 'users.id')->where('feedback.id',$id)->first();
    	return response()->json(compact('feedback'));
    }
}
