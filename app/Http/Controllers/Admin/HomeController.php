<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\User;
use App\Models\Activity;
use App\Models\UserActivity;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class HomeController extends Controller
{
    const USER_STATUS_INACTIVE  = 0;
    const USER_STATUS_ACTIVE    = 1;
    const USER_STATUS_DELETED   = 2;
    const USER_STATUS_REJECTED  = 3;

    const USER_TYPE_ADMIN      = 1;
    const USER_TYPE_MANAGER    = 2;
    const USER_TYPE_AGENT      = 3;
    
    const ACTIVITY_STATUS_ACTIVE        = 1;
    const USER_ACTIVITY_STATUS_COMPLETED= 1;



    //
    function dashboard(){

        $userId             = Auth()->user()->id;
        $userModel          = new User;
        $activityModel      = new Activity;
        $userActivityModel  = new UserActivity;
        $currentDateAndTime = date('Y-m-d H:i:s');
        $oneMonthEarlier = date('Y-m-d H:i:s', strtotime('-1 month'));
        $oneYearEarlier = date('Y-m-d H:i:s', strtotime('-12 month'));

        $data['agents']     = $userModel->where(['role_id' => self::USER_TYPE_AGENT, 'status' => self::USER_STATUS_ACTIVE])->count();
        $data['my_agents']  = $userModel->where(['role_id' => self::USER_TYPE_AGENT, 'manager_id' => $userId, 'status' => self::USER_STATUS_ACTIVE])->count();
        $data['managers']   = $userModel->where(['role_id' => self::USER_TYPE_MANAGER, 'status' => self::USER_STATUS_ACTIVE])->count();

        $totalActivities          = $activityModel->where(['status' => self::ACTIVITY_STATUS_ACTIVE])->count();
        $totalMonthlyCompletedActivities = $userActivityModel->where(['status' => self::ACTIVITY_STATUS_ACTIVE])->where('created_at' , '>',  $oneMonthEarlier)->count();
        if($totalActivities < 1){
            $totalMonthlyPercentOfActivity = 0;
            $totalYearlyPercentOfActivity = 0;
        }
        else{
            $totalMonthlyPercentOfActivity = round(100*$totalMonthlyCompletedActivities/$totalActivities);
            $totalYearlyCompletedActivities = $userActivityModel->where(['status' => self::USER_ACTIVITY_STATUS_COMPLETED])->where('created_at' , '>',  $oneYearEarlier)->count();
            $totalYearlyPercentOfActivity = round(100*$totalYearlyCompletedActivities/$totalActivities);
        }        
        $data['monthly_completed_activities']   = $totalMonthlyPercentOfActivity;
        $data['yearly_completed_activities']    = $totalYearlyPercentOfActivity;



        //dd('a');
//     	$data = [];
//     	$data['property_owners'] = User::count();
//     	$data['registered_employees'] = Employee::count();
//     	$data['properties'] = Property::count();
//     	$data['sub_admins'] = Admin::where('role','sub_admin')->count();
//     	$data['avg_monthly_income'] = Payment::whereRaw('MONTH(created_at) = MONTH(NOW())')->avg('payload->amount');
//     	$data['avg_yearly_income'] = Payment::whereRaw('YEAR(created_at) = YEAR(NOW())')->avg('payload->amount');
//         /*return \DB::select('SELECT YEAR(created_at) AS y, MONTH(created_at) AS m, COUNT(DISTINCT id)
// FROM payments
// GROUP BY y, m');*/
//         $graphData = Payment::selectRaw('IFNULL(sum(json_unquote(json_extract(`payload`, "$.amount"))),0) as sum_amount,MONTHNAME(created_at) as month')->whereRaw('MONTH(created_at) = MONTH(NOW())');
//         $graphData = $graphData->whereRaw('YEAR(created_at) = ?',['year'=>request('year',date('Y'))]);
//         $graphData = $graphData->orderByRaw("FIELD(MONTH,'January','February','March','April','May','June','July','August','September','November','December')")->get();
//         $months = [
//         'January' => 0, 
//         'February' => 0, 
//         'March' => 0,
//         'April' => 0,
//         'May' => 0,
//         'June' => 0,
//         'July' => 0, 
//         'August' => 0, 
//         'September' => 0, 
//         'October' => 0, 
//         'November' => 0, 
//         'December' => 0,
//         ];
//         foreach ($graphData as $key => $month) {
//             if($month->month){

//             $months[$month->month] = $month->sum_amount;
//             }
            
//         }
//         $data['months'] = array_values($months);
        // array_merge($months);
    	return response()->json(compact('data'));
    }
}
