<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class NotificationController extends Controller
{
    //
    public function index(){

    	$notifications = auth('admin')->user()->notifications()->paginate(10);
    	return response()->json(compact('notifications'));
    }
    // 43103-1399381-3
    public function bell(){
    	$data['notifications'] = auth('admin')->user()->unreadNotifications()->get();
    	$data['notification_count'] = auth('admin')->user()->unreadNotifications()->count();
    	return response()->json(compact('data'));
    }

    public function mark_as_read($id){
    	auth('admin')->user()->unreadNotifications()->find($id)->markAsRead();
    	$notification = auth('admin')->user()->notifications()->find($id);
    	return response()->json(['status' =>true, 'notification' => $notification,'msg'=> 'Mark as read successully']);
    }
}
