<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Application;
use App\Models\Employee;
use App\Models\EmployeeProperty;
use App\Models\Property;
use Illuminate\Http\Request;
use Illuminate\Support\Str;

class PropertyController extends Controller {
	public function index(Request $request) {
		/*\DB::listen(function($q){
			    		dump($q->sql);
		*/
		$properties = Property::withCount('employees')->with(['owner' => function ($q) {
			$q->select(['id', 'name']);
		}]);
		if ($request->filled('user')) {
			$properties = $properties->where('owner_id', $request->user);
		}
		if ($request->filled('search')) {
			$properties = $properties->where('name', 'like', '%' . $request->search . '%');
		}
		if (request()->filled('status')) {
			$properties = $properties->where('status', 0);
		} else {

			$properties = $properties->where('status', 1);
		}
		$properties = $properties->paginate();
        
		return response()->json(compact('properties'));
	}

	public function show($id) {

		$property = Property::with(['images', 'employees' => function ($q) {
			if (request()->filled('search')) {
				$q->where('name', 'like', '%' . request('search') . '%');
			}
			$q->withCount(['tasks as pending_tasks' => function ($q) {
				$q->where('tasks.status', 0);
			}]);
		}])->where('property_id', $id)->first();
		return response()->json(compact('property'));
	}

	public function block($property) {
		$property = Property::find($property);
		if ($property->status == 1) {
			$property->status = 0;
			$property->save();
			return response()->json(['status' => true, 'msg' => 'blocked successfully', 'data' => $property]);
		} else {
			$property->status = 1;
			$property->save();
			return response()->json(['status' => true, 'msg' => 'unblocked successfully', 'data' => $property]);
		}
	}

	public function unassign_property($employee, $property) {

		$result = EmployeeProperty::where('employee_id', $employee)->where('property_id', $property)->delete();
		return response()->json(['status' => true, 'msg' => 'employee has been removed successfully']);
	}

	public function associate(Request $request){
		$application = Application::find($request->application);
		$application->status = 'associated';
		$application->save();
		$code = Str::random(8);
		$employee = Employee::find($application->employee_id);
		$employee->owner_id = $request->owner;
		$employee->code = $code;
		$employee->save();
		$employee->properties()->sync($request->property);

		return response()->json(['status' => true, 'msg' => 'employee has been associated successfully']);

	}
}
