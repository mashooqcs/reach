<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Models\Employee;
use App\Models\PasswordReset;
use App\Models\User;
use App\Notifications\PasswordResetRequest;
use Illuminate\Foundation\Auth\SendsPasswordResetEmails;
use Illuminate\Http\Request;
use Illuminate\Support\Str;

class ForgotPasswordController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Password Reset Controller
    |--------------------------------------------------------------------------
    |
    | This controller is responsible for handling password reset emails and
    | includes a trait which assists in sending these notifications from
    | your application to your users. Feel free to explore this trait.
    |
    */

    use SendsPasswordResetEmails;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }

    public function sendResetLinkEmail(Request $request)
    {
        $this->validateEmail($request);
        $user = User::whereEmail($this->credentials($request))->first();
        if(!$user){
            $user = Employee::whereEmail($this->credentials($request))->first();
        }
        if($user){
            $token = strtolower(Str::random(5));
            $passwordReset = PasswordReset::updateOrCreate(['email'=>$request->email],['token'=>$token]);
            $user->notify(
                new PasswordResetRequest($user, $passwordReset->token)
            );
            // $user->sendPasswordResetNotification($reset->token);
            return response()->json([
                'status'=>true,
                'msg' => 'We have sent you a verification code!'
            ]);
        }else{

            return response()->json(['message'=> "Email doesn't exists in our records. "]);
        }
        // We will send the password reset link to this user. Once we have attempted
        // to send the link, we will examine the response then see the message we
        // need to show to the user. Finally, we'll send out a proper response.
    }
}
