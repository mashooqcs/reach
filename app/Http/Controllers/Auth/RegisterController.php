<?php

namespace App\Http\Controllers\Auth;

use App\Events\Registeration;
use App\Http\Controllers\Controller;
use App\Models\Admin;
use App\Models\Employee;
use App\Models\Payment;
use App\Models\Plan;
use App\Models\User;
use App\Models\UserPlan;
use App\Notifications\NotifyAdmin;
use App\Notifications\NotifyProprietor;
use Exception;
use Illuminate\Auth\Events\Registered;
use Illuminate\Contracts\Encryption\DecryptException;
use Illuminate\Foundation\Auth\RegistersUsers;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Notification;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Str;
use Stripe\StripeClient;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

class RegisterController extends Controller {
	/*
		    |--------------------------------------------------------------------------
		    | Register Controller
		    |--------------------------------------------------------------------------
		    |
		    | This controller handles the registration of new users as well as their
		    | validation and creation. By default this controller uses a trait to
		    | provide this functionality without requiring any additional code.
		    |
	*/

	use RegistersUsers;

	/**
	 * Where to redirect users after registration.
	 *
	 * @var string
	 */
	protected $redirectTo = '/dashboard';

	/**
	 * Create a new controller instance.
	 *
	 * @return void
	 */
	public function __construct() {
		$this->middleware('guest');
	}

	/**
	 * Get a validator for an incoming registration request.
	 *
	 * @param  array  $data
	 * @return \Illuminate\Contracts\Validation\Validator
	 */
	protected function validator(array $data) {
		return Validator::make($data, [
			'name' => 'required',
			'email' => ['required', 'string', 'email', 'max:255', 'unique:users'],
			'password' => 'required|confirmed',
			'card_holder_name' => 'required',
			'card_number' => 'required|min:19',
			'cvv' => 'required|min:3|max:4',
			'expiry_month' => 'required|min:2',
			'expiry_year' => 'required|min:4',
			'plan_id' => 'required|exists:plans,id',
		]);
	}

	protected function employee_validator(array $data) {
		return Validator::make($data, [
			'name' => 'required',
			'email' => ['required', 'string', 'email', 'max:255', 'unique:employees'],
			'password' => 'required|confirmed',
			'gender' => 'required',
			'phone' => 'required',
			'address' => 'required',
			'address_2' => 'nullable',
			'city' => 'required',
			'state' => 'required',
			'zipcode' => 'required',
			'country' => 'required',
			'service_years' => 'required',
		]);
	}

	/**
	 * Create a new user instance after a valid registration.
	 *
	 * @param  array  $data
	 * @return \App\User
	 */
	public function pay($request, $plan, $user) {
		$amount = $plan->amount;
		try {

			$stripe = new StripeClient("sk_test_4eC39HqLyjWDarjtT1zdp7dc");
		} catch (\Exception $e) {
			throw new Exception($e->getMessage(), 1);

		}
		try {
			// list($expiry_month,$expiry_year) = explode('/',$request->expiry);
			$token = $stripe->tokens->create([
				'card' => [
					'number' => $request['card_number'],
					'exp_month' => $request['expiry_month'],
					'exp_year' => $request['expiry_year'],
					'cvc' => $request['cvv'],
				],
			]);
		} catch (Exception $e) {

			throw new Exception($e->getMessage(), 1);

		}
		try {
			$charge = $stripe->charges->create([
				'amount' => $amount,
				'currency' => 'usd',
				'source' => $token->id,
			]);
		} catch (Exception $e) {

			throw new Exception($e->getMessage(), 1);

		}
		$user->all_subscriptions()->attach($request['plan_id']);
		$newPlan = UserPlan::where('plan_id', $request['plan_id'])
			->where('user_id', $user->id)->latest('id')->first();
		$payment = $newPlan->payments()->save(new payment([
			'user_id' => $user->id,
			'charge_id' => $charge->id,
			'status' => 1,
			'payload' => $charge,
		]));
		/*$payment = Payment::create([
			            'user_id' => auth()->user()->id,
			            'charge_id' => $charge->id,
			            'status' => 0,
			            'type' => $type,
			            'payload' => $charge
		*/
		return ['id' => $payment->id, 'payload' => $charge];
	}

	protected function create(array $data) {
		$user = User::updateOrCreate([
			// 'name' => $data['name'],
			'email' => $data['email'],
		], $data);
		return $user;
	}

	protected function create_employee(array $data) {

		$user = Employee::firstOrNew([
			'email' => $data['email'],
		], $data);
		$user->employee_id = Str::uuid();
		$user->password = $data['password'];
		$user->save();
		return $user;
	}

	public function register(Request $request) {
		$validatedData = $this->validator($request->all())->validate();
		DB::beginTransaction();
		$user = $this->create($validatedData);
		try {
			$plan = Plan::find($validatedData['plan_id']);
			$this->pay($validatedData, $plan, $user);
		} catch (\Exception $e) {
			DB::rollback();
			return response()->json(['status' => false, 'errors' => ['message' => $e->getMessage()]], 422);
		}
		$admins = Admin::where('role', 'super_admin')->Orwhere('permissions', 'like', '%admin.users%')->get();
		Notification::send($admins, new NotifyAdmin([
			'message' => 'New owner registeration',
			'title' => 'new Owner registeration',
			'id' => $user->id,
			'route' => [
				'name' => 'admin.users.show',
				'params' => ['type' => 'property-owner', 'user' => $user->id],
			],
		])
		);
		event(new Registeration($user));
		DB::commit();
		if ($response = $this->registered($request, $user)) {
			return $response;
		}
		return $request->wantsJson()
		? new JsonResponse(['status' => true, 'msg' => 'Registered Successfully'], 201)
		: redirect($this->redirectPath());
	}
	public function verify_token($token) {
		try {
			$id = decrypt($token);
			try {
				$user = User::findOrFail($id);
				return response()->json(['status' => true, 'data' => $user]);

			} catch (NotFoundHttpException $e) {
				return response()->json(['status' => false, 'msg' => $e->getMessage()]);

			}
		} catch (DecryptException $e2) {
			return response()->json(['status' => false, 'msg' => $e2->getMessage()]);
		}
	}

	public function complete_registeration(Request $request) {
		$user = User::find($request->id);
		$user->forceFill($request->except(['id', 'password', 'comfirm_password']));
		$user->email_verified_at = now();
		$user->password = bcrypt($request->password);
		$user->save();
		return new JsonResponse(['status' => true, 'msg' => 'Registeration completed successfully'], 201);
	}
	public function check_email(Request $request) {
		$request->validate(['email' => 'required|unique:users']);
	}
	public function verifyEmail(Request $request) {
		$user = User::where('email', $request->email)->where('email_verified_at', '!=', NULL)->first();
		if ($user) {
			\Auth::login($user);
			return response()->json(['status' => true, 'msg' => 'Login Successfully']);
		} else {
			return response()->json(['status' => false]);
		}
	}

	public function employee_registeration(Request $request) {
		$validatedData = $this->employee_validator($request->all())->validate();
		DB::beginTransaction();
		$user = $this->create_employee($validatedData);
		$admins = Admin::where('role', 'super_admin')->Orwhere('permissions', 'like', '%admin.users%')->get();
		Notification::send($admins, new NotifyAdmin([
			'message' => 'New employee Registeration',
			'title' => 'new employee registeration',
			'id' => $user->id,
			'route' => [
				'name' => 'admin.users.show',
				'params' => ['type' => 'property-owner', 'user' => $user->id],
			],
		])
		);
		if ($user->code) {
			$owner = User::find($user->owner_id);
			Notification::send($owner, new NotifyProprietor([
				'title' => 'New employee registeration',
				'message' => 'new employee ' . $user->name . ' has been registered',
				'id' => $user->id,
				'route' => [
					'name' => 'web.employees.show',
					'params' => ['employee' => $user->id],
				],
			])
			);
		}
		event(new Registeration($user));
		event(new Registeration($user, 'App.Models.User.'));
		DB::commit();
		return $request->wantsJson()
		? new JsonResponse(['status' => true, 'msg' => 'your account has been made successfully', 'id' => $user->id], 201)
		: redirect($this->redirectPath());
	}

	public function check_employee_existance(Request $request) {
		$id = $request->header('id');
		try {
			$employee = Employee::with('application')->findOrFail($id);
			return response()->json(['status' => true,'application'=>$employee->application]);
		} catch (\Exception $e) {
			return response()->json(['msg' => $e->getMessage(), 'status' => false]);
		}

	}
}
