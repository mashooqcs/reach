<?php 
namespace App\Core\Collections;
use Illuminate\Database\Eloquent\Collection;

class DatatableCollection extends Collection{

	function __construct($models,$model)
	{	
		parent::__construct();
		$this->items = $models;
		$this->model = $model;
	}

	private function getTotalRecords(){
		return $this->model->count();
	}
	public function previewTable($total = null){
		if(!$total){
			$total = $this->getTotalRecords();
		}
		return [
			'draw' => request('draw',1),
			'recordsTotal' => $total,
			'data' => $this->items,			
			'recordsFiltered' => $total
		];
	}

}
?>