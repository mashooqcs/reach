<?php
namespace App\Core\Scopes;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Scope;

class DatatableScope implements Scope{
	protected $extensions = ['paginateTable'];
	
	public function apply(Builder $builder ,Model $model){

	}	

	public function extend(Builder $builder)
    {
        foreach ($this->extensions as $extension) {
            $this->{"add{$extension}"}($builder);
        }
        return $builder;
    }
	protected function addPaginateTable(Builder $builder)
    {
    	$result = $builder->macro('paginateTable', function (Builder $builder) {
            $model = $builder->getModel();
            $builder->withoutGlobalScope($this)
            ->skip(request('start'))
            ->take(request('length'));
            return $builder;
        });
    }
}