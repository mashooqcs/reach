<?php 

namespace App\Core\Traits;

use App\Core\Collections\DatatableCollection;
use App\Core\Filters\DatatableFilter;
use App\Core\Scopes\DatatableScope;

trait Datatable{
	public function __construct(){
		$this->columnNames = data_get(request('columns'),'*.data');
		$this->orders = request('order');
        $this->searchValue = data_get(request('search'), 'value');
	}
	public function scopePaginateTable(){
		$result = $this->withoutGlobalScope($this)
		->skip(request('start'))
        ->take(request('length'));
        $result = $this->mapSearch($result);
        $result = $this->mapOrderBy($result);
        return $result;
	}
	private function mapSearch($result){
		foreach ($this->columnNames as $key => $name) {
        	$result->where($name,'like','%'.$this->searchValue.'%');
        }
        return $result;
	}

	private function mapOrderBy($result){
        foreach ($this->orders as $key => $order) {
        	$column = $this->columnNames[$key];
        	$direction = $order['dir'];
        	$result->orderBy($column,$direction);
        }
        return $result;
	} 
	public function newCollection(array $models = [])
    {
    	return new DatatableCollection($models,$this);
    }
} 