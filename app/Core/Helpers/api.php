<?php 
function api_successWithData($data,$message){
	return ['status'=>true,'message'=>$message,'detail'=> $data];
}

function api_success($message){
	return ['status'=>true,'message'=>$message];
}

function api_error($message){
	return ['status'=>false,'message'=>$message];
}

function api_validation_errors($errors,$message){
	return ['status'=>false,'message'=>$message,'detail'=>$errors];
}
?>