<?php

namespace App\Core\Filters;



class DatatableFilter extends Filters {

    protected $filters = ['columns', 'zipcode', 'specialities','modality','treatment','agegroup','ethnicity','language','state'];

    public function columns($columns)
    {   
        dd(data_get(request('columns'),'*.data'));
        if($name)
            $this->builder->where("full_name", 'LIKE', "%{$name}%");
    }


    public function zipcode($zipcode)
    {
        if($zipcode)
            $this->builder->whereHas('offices',function($q)use($zipcode){
                $q->where("zipcode", 'LIKE', "%{$zipcode}%");
            });
    }

    public function state($state)
    {
        if($state)
            $this->builder->whereHas('offices',function($q)use($state){
                $q->where("state", 'LIKE', "%{$state}%");
            });
    }

    public function specialities($specialities){
        if(count($specialities)){
            $this->builder->where("specialities_id",'LIKE','%'.implode(',',$specialities).'%');

        }
    }
    public function modality($modality){
        if(count($modality)){
            $this->builder->where("modalities_id",'LIKE', '%'.implode(',',$modality).'%');

        }
    }
    public function treatment($treatment){
        if(count($treatment)){
            $this->builder->where("treatments_id",'LIKE', '%'.implode(',',$treatment).'%');
        }
    }
    public function agegroup($agegroup){
        if(count($agegroup)){
            $this->builder->where("age_groups_id",'LIKE', '%'.implode(',',$agegroup).'%');

        }
    }
    public function ethnicity($ethnicity){
        if(count($ethnicity)){
            $this->builder->where("ethnicities_id",'LIKE', '%'.implode(',',$ethnicity).'%');

        }
    }
    public function language($language){
        if(count($language)){
            $this->builder->where("languages_id", 'LIKE', '%'.implode(',',$language).'%');

        }
    }


}
