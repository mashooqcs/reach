<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Str;

class View extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'make:view {filename}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Create new View File';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $filename = $this->argument('filename');
        if(!Str::endsWith($filename,'.php')){
            $extension = '.blade.php';
        }
        $path = resource_path('views/'.$filename.$extension);
        Storage::put($path, '');
        return 0;
    }
}
