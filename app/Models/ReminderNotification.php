<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ReminderNotification extends Model
{
    use HasFactory;
    protected $fillable = [
        'user_id', 'title', 'start_date', 'start_time', 'repeat_on', 'status'
    ];
}
