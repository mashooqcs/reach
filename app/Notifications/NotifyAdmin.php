<?php

namespace App\Notifications;

use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Messages\BroadcastMessage;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Notifications\Notification;

class NotifyAdmin extends Notification {
	use Queueable;

	/**
	 * Create a new notification instance.
	 *
	 * @return void
	 */
	private $payload;
	public function __construct($payload) {
		$this->payload = $payload;
	}

	/**
	 * Get the notification's delivery channels.
	 *
	 * @param  mixed  $notifiable
	 * @return array
	 */
	public function via($notifiable) {
		return ['database', 'broadcast'];
	}

	/**
	 * Get the mail representation of the notification.
	 *
	 * @param  mixed  $notifiable
	 * @return \Illuminate\Notifications\Messages\MailMessage
	 */
	public function toDatabase($notifiable) {
		return [
			'message' => $this->payload['message'],
			'title' => $this->payload['title'],
			'id' => $this->payload['id'],
			'route' => $this->payload['route'],
		];
	}

	public function toBroadcast($notifiable) {
		return new BroadcastMessage([
			'id' => $this->payload['id'],
			'data' => [
				'message' => $this->payload['message'],
				'title' => $this->payload['title'],
				'route' => $this->payload['route'],
			],
			'created_at' => now(),
		]);
	}

	/*public function broadcastType()
		    {
		        return 'admin.notify';
	*/
	public function toMail($notifiable) {
		return (new MailMessage)
			->line('The introduction to the notification.')
			->action('Notification Action', url('/'))
			->line('Thank you for using our application!');
	}

	/**
	 * Get the array representation of the notification.
	 *
	 * @param  mixed  $notifiable
	 * @return array
	 */
	public function toArray($notifiable) {
		return [
			//
		];
	}
}
